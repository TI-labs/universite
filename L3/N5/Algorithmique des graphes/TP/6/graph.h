/**
 * \graphe.h
 * \V2  2023
 * \author Frédéric Roupin
**/

#ifndef GRAPH
#define GRAPH

/*------------------------------------------------------------------------------------------*/
/* Structure du graphe									    */
/*------------------------------------------------------------------------------------------*/

struct graph_SP {
	unsigned n;
	unsigned m;
	unsigned delta;
	unsigned **S;
	unsigned **P;	
};

typedef struct graph_SP graph_SP;

/* Initialise le graphe dont l'adresse est donnée par g, grapheà n sommets et sans
 * arc dont les degrés intérieurs et extérieurs sont et seront tous bornés par delta0
*/
void g_init(graph_SP *g, unsigned n, unsigned delta0);

/* Libère toute la mémoire associée au graphe d'adresse g.
 * Ne fait rien si g est le pointeur NULL */
void g_free(graph_SP *g);

/* Retourne l'ordre (nombre de sommets) du graphe d'adresse g */
unsigned g_n(const graph_SP *g);

/* Retourne le nombre d'arêtes du graphe d'adresse g */
unsigned g_m(const graph_SP *g);

/* Retourne la borne delta sur les degrés intérieurs et extérieurs du graphe d'adresse g */
unsigned g_delta(const graph_SP *g);

/* Retourne 1 si v est prédécesseur de w dans le graphe d'adresse g existe et retourne 0 sinon.
 * On suppose que v et w sont entre 1 et n */
unsigned g_pred(const graph_SP *g, unsigned v, unsigned w);

/* Retourne 1 si v est successeur de w dans le graphe d'adresse g existe et retourne 0 sinon.
 * On suppose que v et w sont entre 1 et n */
unsigned g_succ(const graph_SP *g, unsigned v, unsigned w);

/* ajoute un arc {v, w} au graphe d'adresse g.
 * On suppose que v et w sont entre 1 et n */
unsigned g_add_edge(graph_SP *g, unsigned v, unsigned w);

/* supprime un arc {v, w} dans le graphe d'adresse g si il y en a un
 * (sinon, ne modifie pas le graphe).
 * On suppose que v et w sont entre 1 et n */
unsigned g_rm_edge(graph_SP *g, unsigned v, unsigned w);

/* Fonction d'entrée-sortie
 * Affiche des informations sur le graphe d'adresse g dans le terminal */
void g_disp(const graph_SP *g);

/* Fonction d'entrée-sortie
 * Écrit le graphe d'adresse g au format dot dans le fichier de nom filename.
 * retourne une valeur négative en cas d'erreur (fichier non ouvrable en
 * écriture, ...) et 0 sinon. */
int g_write_dot(const graph_SP *g, const char *filename);

/*********************************************************************************************
/* Fonctions dont le corps est à compléter dans graph.c
/*********************************************************************************************

/* Retourne le degré extérieur du sommet v dans le graphe d'adresse g
 * On suppose que v est entre 1 et n  */
unsigned g_degre_plus(const graph_SP *g, unsigned v);

/* Retourne le degré intérieur du sommet v dans le graphe d'adresse g
 * On suppose que v est entre 1 et n  */
unsigned g_degre_moins(const graph_SP *g, unsigned v);

/* Retourne la matrice P des prédécesseurs connaissant S la matrice des sucesseurs
* d'un graphe simple orienté G comportant n sommets dont les degrés intérieurs et
* extérieurs sont tous bornés par delta.
* Les indices des sommets sont compris entre 1 et n  */
unsigned **g_S_vers_P(const unsigned **S, unsigned n, unsigned delta);

/* Retourne la matrice S des sucesseurs connaissant P la matrice des prédécesseurs
* d'un graphe simple orienté G comportant n sommets dont les degrés intérieurs et
* extérieurs sont tous bornés par delta.
* Les indices des sommets sont compris entre 1 et n  */
unsigned **g_P_vers_S(const unsigned **P, unsigned n, unsigned delta);

/* Éffectue un tri topologique des sommets du graphe d'adresse g.
 * retourne une valeur négative en cas de cricuit et 0 sinon. */
int g_tri_topologique(graph_SP *g);

/* Applique l'agorithme de Bellman sur le graphe donné par l'adresse g.
   * Lambda est un tableau de dimension g->n contient les valeurs
   * des chemins les plus courts en nombre d'arcs
   * P est un tableau de dimension g->n qui contiendra les prédécesseurs
   * dans l'arborescence des chemins optimaux.
   * Retourne une valeur négative si le graphe contient un circuit,
   * 0 sinon (utiliser g_tri_topologique)*/
int *g_Bellman(graph_SP *g, unsigned *lambda, unsigned *P);

/* Teste si le graphe donné par l'adresse g est connexe.
   Retourne une valeur négative si le graphe n'est pas connexe, 0 sinon*/
int *g_connexe(graph_SP *g);

/* Applique l'agorithme de Dijkstra sur le graphe donné par l'adresse g.
   * Lambda est un tableau de dimension g->n qui contiendra les valeurs
   * des chemins les plus courts en nombre d'arcs
   * P est un tableau de dimension g->n qui contiendra les prédécesseurs
   * dans l'arborescence des chemins optimaux.*/
void *g_dijkstra(graph_SP *g, unsigned *lambda, unsigned *P);

/* Retourne le nombre de chemins disjoints au sens des arêtes entre les sommets u et v
   * dans le graphe donné par l'adresse g.*/
unsigned *g_chem_dis(graph_SP *g, unsigned u, unsigned v);

#endif /* ifndef GRAPH */
