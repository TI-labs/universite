/**
 * \graphe.c
 * \V2 2023
 * \author Frédéric Roupin
**/

#include "graph.h"
#include <stdlib.h>
#include <stdio.h>

#define verbose 2 //niveau de verbosité pour les printf

/*------------------------------------------------------------------------------------------*/
/* Fonctions fournies   								    */
/*------------------------------------------------------------------------------------------*/

void g_init(graph_SP* g, unsigned n, unsigned delta0)
{
  unsigned i,j;

  g->n = n;
  g->m = 0;
  g->delta = delta0;

  g->S = (unsigned **) malloc((n+1) * sizeof(unsigned *));
  g->P = (unsigned **) malloc((n+1) * sizeof(unsigned *));

  for(i = 1; i <= n; i++){
    g->S[i] = (unsigned *) malloc(delta0 * sizeof(unsigned));
    g->P[i] = (unsigned *) malloc(delta0 * sizeof(unsigned));  
    for(j = 0; j < delta0; j++){
      g->S[i][j] = 0;
      g->P[i][j] = 0;
    }
  }
}

void g_free(graph_SP *g)
{
  unsigned i;
  
  for(i = 0; i <= g->n; i++){
    free(g->S[i]);
    free(g->P[i]);
  }
  free(g->P);free(g->S);

}

unsigned g_n(const graph_SP *g)
{
  return g->n;
}

unsigned g_m(const graph_SP *g)
{
  return g->m;
}

unsigned g_delta(const graph_SP *g)
{
  return g->delta;
}

unsigned g_pred(const graph_SP *g, unsigned v, unsigned w)
{
  unsigned i,pred;
  
  pred = 0;
  i = 0;
  while( (i < g->delta) && !pred){
    pred = (g->P[w][i] == v) || pred;
    i++;
  }
  return pred;
}

unsigned g_succ(const graph_SP *g, unsigned v, unsigned w)
{
  unsigned i,succ;
  
  succ = 0;
  i = 0;
  while( (i < g->delta) && !succ){
    succ = (g->S[w][i] == v) || succ;
    i++;
  }
  return succ;
}

unsigned g_add_edge(graph_SP *g, unsigned v, unsigned w)
{
  unsigned i;
  
  i = 0;
  while (g->S[v][i]) i++;
  if(i >= g->delta) {
     if (verbose>0) printf("Le degré extérieur du sommet %d n'est plus borné par %d ! L'arc n'a pas été ajouté.\n",v,g->delta);
     return 0;
  }   
  else g->S[v][i] = w; 

  i = 0;
  while (g->P[w][i]) i++;
  if(i >= g->delta){
    if (verbose>0) printf("Le degré intérieur du sommet %d n'est plus borné par %d ! L'arc n'a pas été ajouté.\n",w,g->delta);
    return 0;
  }    
  else {
    g->P[w][i] = v; g->m += 1;
    if (verbose>1) printf("L'arc (%d,%d) a été ajouté.\n",v,w);
  }
  return 1;     
}

unsigned g_rm_edge(graph_SP *g, unsigned v, unsigned w)
{
  unsigned i;
  
  i = 0;
  while (g->S[v][i] != w) i++;
  if(i >= g->n + 1){
    if (verbose>0) printf("L'arc (%u,%u) n'existe pas.\n",v,w);
    return 0;
  }
  else g->S[v][i] = 0;

  i = 0;
  while (g->P[w][i] != v) i++;
  if(i >= g->n + 1){
    if (verbose>0) printf("L'arc (%u,%u) n'existe pas.\n",v,w);
    return 0;
  }
  else {
    g->P[w][i] = 0; g->m -= 1;
    if (verbose>1) printf("L'arc (%u,%u) a été supprimé.\n",v,w);
  }
  return 1;
}

void g_disp(const graph_SP *g)
{
	unsigned v, w;
	unsigned i,j;
	
	printf("n = %d, m = %d, delta = %d\n", g_n(g), g_m(g),g_delta(g));
	for (v = 1; v <= g->n; v++){
		if (verbose>1) printf("Le degré extérieur de %u est : %u\n",v,g_degre_plus(g,v));
		if (verbose>1) printf("Le degré intérieur de %u est : %u\n",v,g_degre_moins(g,v));
		for (i= 0; i < g->delta; i++) {
		  if (verbose>1) if(g->S[v][i]) printf("%u est successeur de %u \n", g->S[v][i], v);
		  if (verbose>1) if(g->P[v][i]) printf("%u est prédécesseur de %u \n", g->P[v][i], v);
		}
		printf("\n");
	}
	for (i = 1; i <= g->n; i++){	
		for (j = 0; j < g->delta; j++){
		   if (verbose>1) printf("S(%u,%u)=%u ",i,j,g->S[i][j]);
        	   if (verbose>1) printf("P(%u,%u)=%u ",i,j,g->P[i][j]);
        	}
        	if (verbose>1) printf("\n");
       } 
}

int g_write_dot(const graph_SP *g, const char *filename)
{
	FILE *f;
	unsigned v, w, k;

	f = fopen(filename, "w");
	if (f == NULL) {
		perror("fopen in g_write_dot");
		return -1;
	}

	fprintf(f, "digraph {\n");
	for (v = 1; v <= g_n(g); v++)
		fprintf(f, "\t%d;\n", v);

	fprintf(f, "\n");

	for (v = 1; v <= g_n(g); v++)
	  for (k = 0; k < g_delta(g); k++)
	    if (g->S[v][k]) fprintf(f, "\t%d -> %d;\n", v, g->S[v][k]);
	fprintf(f, "}\n");
	fclose(f);
	if (verbose>1) printf("Fichier %s créé.\n",filename);
	return 0;
}

/*------------------------------------------------------------------------------------------*/
/* Fonctions à compléter								    */
/*------------------------------------------------------------------------------------------*/

unsigned g_degre_plus(const graph_SP *g, unsigned v)
{	
  return 0;
}

unsigned g_degre_moins(const graph_SP *g, unsigned v)
{
  return 0;
}

unsigned ** g_S_vers_P(const unsigned ** S, unsigned n, unsigned delta)
{
  return NULL;
}

unsigned ** g_P_vers_S(const unsigned ** P, unsigned n, unsigned delta)
{
  return NULL;
}

int g_tri_topologique(graph_SP *g)
{
  return 0;
}

int * g_Bellman(graph_SP *g, unsigned *lambda, unsigned *P)
{
  return 0;
}

int * g_connexe(graph_SP *g)
{
  return 0;
}

void * g_dijkstra(graph_SP *g, unsigned *lambda, unsigned *P)
{

}

unsigned * g_chem_dis(graph_SP *g, unsigned u, unsigned v)
{
  return 0;
}
