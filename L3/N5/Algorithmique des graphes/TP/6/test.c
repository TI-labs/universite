/**
 * \test.c
 * \V2 2023
 * \author Frédéric Roupin
**/

#include "graph.h"
#include <stdio.h>

int main()
{
	graph_SP g;
	g_init(&g,4,2);

	g_add_edge(&g, 1, 2);
	g_add_edge(&g, 2, 1);
	g_add_edge(&g, 3, 4);
	g_add_edge(&g, 4, 1);
	g_add_edge(&g, 4, 2);

	g_disp(&g);
	
	if (g_pred(&g, 3, 4)) printf("3 est prédécesseur de 4\n");
	if (!g_pred(&g, 2, 4)) printf("2 n'est pas prédécesseur de 4\n");
	if (g_succ(&g, 1, 4)) printf("1 est successeur de 4\n");
	if (!g_succ(&g, 3, 1)) printf("3 n'est pas successeur de 1\n");
	
	g_write_dot(&g, "test.dot");
	
	g_free(&g);

	return 0;
}
