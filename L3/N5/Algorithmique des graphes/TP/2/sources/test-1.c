#include <stdio.h>

#include "graph_mat-1.h"

int main()
{
	graph_mat *g1 = gm_init(5);

	gm_add_edge(g1, 0, 1);
	gm_add_edge(g1, 0, 4);
	gm_add_edge(g1, 1, 4);
	gm_add_edge(g1, 4, 1);
	gm_add_edge(g1, 2, 2);
	gm_add_edge(g1, 2, 3);

	gm_disp(g1);
	printf("%u\n", gm_degree(g1, 0));
	printf("%u\n", gm_degree(g1, 1));
	printf("%u\n", gm_degree(g1, 2));
	printf("%u\n", gm_degree(g1, 3));
	printf("%u\n", gm_degree(g1, 4));

	graph_mat *g2 = gm_init(5);

	gm_add_edge(g2, 0, 1);
	gm_add_edge(g2, 0, 4);
	gm_add_edge(g2, 1, 4);
	gm_add_edge(g2, 4, 1);
	gm_add_edge(g2, 2, 2);
	gm_add_edge(g2, 2, 3);

	gm_disp(g2);
	printf("%u\n", gm_degree(g2, 0));
	printf("%u\n", gm_degree(g2, 1));
	printf("%u\n", gm_degree(g2, 2));
	printf("%u\n", gm_degree(g2, 3));
	printf("%u\n", gm_degree(g2, 4));

	graph_mat *g = gm_sum(g1, g2);
	gm_disp(g);
	printf("%u\n", gm_degree(g, 0));
	printf("%u\n", gm_degree(g, 1));
	printf("%u\n", gm_degree(g, 2));
	printf("%u\n", gm_degree(g, 3));
	printf("%u\n", gm_degree(g, 4));
	gm_free(g);

	g = gm_prod(g1, g2);
	gm_disp(g);
	printf("%u\n", gm_degree(g, 0));
	printf("%u\n", gm_degree(g, 1));
	printf("%u\n", gm_degree(g, 2));
	printf("%u\n", gm_degree(g, 3));
	printf("%u\n", gm_degree(g, 4));
	gm_free(g);

	// gm_write_dot(g1, "test-1.dot");
	gm_free(g1);
	gm_free(g2);

	return 0;
}
