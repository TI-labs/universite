\subsection{Catégories petites et localement petites}

On peut aussi donner une autre définition des catégories en regroupant chaque sous-collection de 
flèches en fixant la source et le but. On remarque aussi que les flèches sont souvent appelées \emph{morphismes} ou \emph{homomorphismes}.
Soit $f$ une flèche de source $X$ et de but $Y$, on dira que 
$f$ appartient à la collection des morphismes de $X$ dans $Y$, qu'on notera au choix 
\[\operatorname{Flech}_\mathcal{C}(X,Y)=\operatorname{Hom}_\mathcal{C}(X, Y) = \operatorname{Mor}_\mathcal{C}(X, Y) = \mathcal{C}(X, Y).\]

Ceci donne la définition équivalente suivante de catégorie, où il n'y a plus besoin d'applications <<$\mathrm{source}$>> et 
<<$\mathrm{but}$>> !

\begin{defi}[Catégorie (bis)]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
	\item une collection d'\emph{objets}, notée $\mathrm{Obj}(\mathcal{C})$ : ${X}, {Y}, {Z},  \ldots$, 
	\item des collections de \emph{flèches} (ou \emph{morphismes}) pour toute paire d'objets $X$ et $Y$, notées $\mathrm{Hom}_\mathcal{C}(X, Y)$ : $f, g, h, \ldots$,
	\item une \emph{composition} :
        pour toute paire de flèches $f$ de $\operatorname{Hom}_\mathcal{C}(X, Y)$
        et $g$ de $\mathrm{Hom}_\mathcal{C}(Y, Z)$, on a une flèche $g \circ f$
        dans $\mathrm{Hom}_\mathcal{C}(X, Z)$,
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$ \mathrm{id}_X$ dans $\mathrm{Hom}_\mathcal{C}(X, X)$.
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}

Dans la définition de la notion de catégorie, on utilise des collections d'objets et de morphismes, et non
des ensembles. Cela nous donne plus de flexibilité, et nous évite certaines
restrictions vis-à-vis des ensembles. Néanmoins, il peut arriver que certaines catégories soient constituées d'ensembles. 

\begin{defi}[Petite catégorie]
Une catégorie $\mathcal{C}$ est dite \emph{petite} si ses objets $\mathrm{Obj}(\mathcal{C})$ forment un ensemble.
\end{defi}

\begin{exem}
Les catégories des ensembles finis $\mathcal{E}ns_{\mathcal{F}in}$, des
ensembles finis injectifs $\mathcal{E}ns_{\mathcal{F}in}$ et des ensembles
finis surjections $\mathcal{E}ns_{\mathcal{F}in}$ sont définies par :

\begin{itemize}[label=\textbullet]
    \item[\textbullet] $\mathcal{E}ns_{\mathcal{F}in} =$
        \begin{itemize}[label=\textbullet]
            \item $\mathrm{Obj} = \{ \text{ensembles finis} \}$
            \item $\mathrm{Hom} = \{ \text{applications ensemblistes} \}$ 
        \end{itemize}
    \item[\textbullet] $\mathcal{E}ns_{\mathcal{F}in^{inj}} =$
        \begin{itemize}[label=\textbullet]
            \item $\mathrm{Obj} = \{ \text{ensembles finis} \}$
            \item $\mathrm{Hom} = \{ \text{injections} \}$ 
        \end{itemize}
    \item[\textbullet] $\mathcal{E}ns_{\mathcal{F}in_{surj}} =$
        \begin{itemize}[label=\textbullet]
            \item $\mathrm{Obj} = \{ \text{ensembles finis} \}$
            \item $\mathrm{Hom} = \{ \text{surjections} \}$ 
        \end{itemize}
\end{itemize}
Ces catégories sont petites car les ensembles finis forment un ensemble. 

En effet, l'ensemble des ensembles finis est défini. De la même façon, la
catégorie des corps finis, des espaces vectoriels de dimension finie et toute
catégorie dont les objets sont finis sont des petites catégories.
\end{exem}

\begin{exem}
La catégorie composée d'un seul objet est définie comme ceci :
\begin{itemize}[label=\textbullet]
    \item $\mathrm{Obj} = \{ \cdot \}$
    \item $\mathrm{Hom} = \{ id_\cdot : \cdot \rightarrow \cdot \}$
\end{itemize}

\begin{center}
\begin{tikzpicture}
    \node (a) at (1,0) {$\cdot$};
    \draw[->] (1.5,0.075) arc (-50:230:8mm);
\end{tikzpicture}
\end{center}

Cette catégorie est une petite catégorie.
\end{exem}

Néanmoins, limiter les objets aux ensembles n'est pas toujours ce que l'on
recherche, et peut donc se révéler trop restrictif. Par exemple, la catégorie
des ensembles ne valide pas cette propriété. Nous proposons donc plutôt de
limiter nos morphismes aux ensembles.

\begin{defi}[Catégorie localement petite]
Une catégorie est dite \emph{localement petite} si, pour toute paire d'objets $X,Y$ les morphismes $\operatorname{Hom}_\mathcal{C}(X, Y)$ de $X$ vers $Y$  forment un ensemble.
\end{defi}

\begin{exem}
La catégorie des ensembles $\mathcal{E}ns$ n'est pas une petite catégorie. En
effet, l'ensemble des ensembles n'existe pas (cf. Paradoxe de Russel).

La catégorie des espaces vectoriels et des corps ne forment pas non plus de
petites catégories, parce qu'il n'existent pas d'ensembles de tous les espaces
vectoriels, ni d'ensemble de tous les corps, on doit donc se limiter aux
collections.

Néanmoins, les $\operatorname{Hom}_{\mathcal{E}ns}(X, Y)$ forment des ensembles.
C'est donc une catégorie localement petite. De même pour les espaces vectoriels
et les corps.
\end{exem}

\begin{rema}
Catégorie petite $\centernot \implies$ Catégorie localement petite.

En effet, construisons cette catégorie comme contre-exemple :
\begin{itemize}[label=\textbullet]
    \item $\mathrm{Obj} = \{ \cdot \}$
    \item $\mathrm{Hom}_{\mathcal C}(\cdot, \cdot) = \text{la collection des ensembles}$
    \item $\circ =$ l'union disjointe des ensembles : Pour tous $A$, $B$ des
        ensembles, $A \sqcup B$ est un ensemble, donc fait partie de
        $\mathrm{Hom}_{\mathcal C}(\cdot, \cdot)$
    \item $id_\cdot = \emptyset$
\end{itemize}

Cette catégorie vérifie les propriétés :
\begin{itemize}
    \item $(A \sqcup B) \sqcup C = A \sqcup (B \sqcup C)$
    \item $A \sqcup id_\cdot = A \sqcup \emptyset = A$ et
        $id_\cdot \sqcup B = \emptyset \sqcup B = B$
\end{itemize}

Ici, nos objets forment un ensemble, l'ensemble à un seul élément, c'est donc
une petite catégorie. Pourtant, les morphismes ne forment pas un ensemble,
ce n'est donc pas une catégorie localement petite.
\end{rema}

\subsection{Constructions de catégories}

\subsubsection{Catégories associées à des ensembles partiellement ordonnés}

\begin{defi}[Ensemble partiellement ordonné]
Un \emph{ensemble partiellement ordonné} $\pi=(E, \leq)$ est un ensemble $E$ muni 
d'une relation d'ordre $\le$, c'est-à-dire une relation binaire vérifiant :
\begin{itemize}[label=\textbullet]
    \item \textsc{réflexivité} : $\forall x \in E,\ x \leq x$,
    \item \textsc{transitivité} : $\forall x, y, z \in E,\ x \leq y \wedge y \leq z \Rightarrow x \leq z$,
    \item \textsc{antisymétrie} : $\forall x, y \in E,\ x \leq y \wedge y \leq x \Rightarrow x = y$.
\end{itemize}
\end{defi}


\begin{exem}[Ensemble des parties]
Pour tout ensemble $X$, l'ensemble $E \coloneq \mathcal{P}(X)=\{A\subset X\}$ de ses parties est un ensemble partiellement ordonné  où la relation d'ordre est donnée par l'inclusion 
$\leq\, \coloneq\, \subset$.
\end{exem}

On peut représenter un tel ensemble avec un diagramme, le diagramme de Hasse,
qui met en évidence l'ordre établi dans l'ensemble.


\begin{center}
\begin{tikzpicture}
    \node (X) at (1,3) {X};
    \node (12) at (0,2) {$\{1, 2\}$};
    \node (13) at (1,2) {$\{1, 3\}$};
    \node (23) at (2,2) {$\{2, 3\}$};
    \node (1) at (0,1) {$\{1\}$};
    \node (2) at (1,1) {$\{2\}$};
    \node (3) at (2,1) {$\{3\}$};
    \node (e) at (1,0) {$\emptyset$};

    \draw[->] (e) to (1);
    \draw[->] (e) to (2);
    \draw[->] (e) to (3);
    \draw[->] (1) to (12);
    \draw[->] (1) to (13);
    \draw[->] (2) to (12);
    \draw[->] (2) to (23);
    \draw[->] (3) to (23);
    \draw[->] (3) to (13);
    \draw[->] (12) to (X);
    \draw[->] (23) to (X);
    \draw[->] (13) to (X);
\end{tikzpicture}
\end{center}

On peut associer canoniquement une catégorie à tout ensemble partiellement ordonné.

\begin{defprop}[Catégorie associée à un ensemble partiellement ordonné]
    Soit $\pi = (E, \leq)$ un ensemble partiellement ordonné. Les données suivantes 
    forment une catégorie, notée $\mathcal{C}_\pi$ :   
       \begin{itemize}[label=\textbullet]
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item la composée est donnée par la transitivité : En effet,
            $\forall e, e', e'' \in E$, tel que $e \rightarrow e'$
            et $e' \rightarrow e''$, alors on a $e \leq e'$ et $e' \leq e''$,
            et par transitivité, on a $e \leq e''$ et donc, on a,
            $e \rightarrow e''$
        \item les identités sont données par la réflexivité : En effet,
            $\forall e \in E, e \rightarrow e$, car $e \leq e$
    \end{itemize}
\end{defprop}

\begin{proof}
\
    \begin{itemize}
    	\item Associativité :
            $\forall e, e', e'', e''' \in E$, grâce à la transitivité, on a :

            \[
            \begin{split}
                &(e \leq e' \text{ et } e' \leq e'') \text{ et } e'' \leq e'''\\
                \iff &e \leq e'' \text{ et } e'' \leq e'''\\
                \iff &e \leq e'''\\
                \iff &e \leq e' \text{ et } e' \leq e'''\\
                \iff &e \leq e' \text{ et } (e' \leq e'' \text{ et } e'' \leq e''')
            \end{split}
            \]
        \item Unitaire :
            $\forall e, e' \in E$, par réfléxivité, on a :
            $e \leq e$ et $e \leq e' \iff e \leq e'$, ainsi que,
            $e \leq e'$ et $e' \leq e' \iff e \leq e'$
    \end{itemize}
\end{proof}

\subsubsection{Catégorie associée à un graphe}

Les graphes sont des outils centraux en mathématiques et en informatique. 
On peut déjà remarquer une certaine proximité entre les graphes et les catégories : on représente souvent des
catégories au moyen de graphes. On peut aussi créer des graphes à partir d'ensembles partiellement ordonnés via la notion de diagramme de Hasse. 

\begin{defi}[Graphes]\leavevmode
\begin{itemize}[label=\textbullet]
    \item Un \emph{graphe} $G=(S,A)$ est un ensemble $S$ (fini et non vide) de \emph{sommets} 
    et un ensemble $A$ de paires de sommets appelées \emph{arêtes}.
    \item Un graphe $G=(S,A)$ est \emph{dirigé} lorsque toutes ses arêtes sont des paires \emph{ordonnées} de sommets, c'est-à-dire $A\subset S\times S$. 
    \item Un graphe $G$ est dit \emph{complet} lorsqu'il est constitué de toutes les arêtes possibles. 
\end{itemize}
\end{defi}

On ne considérera ici que des graphes dirigés. On peut maintenant se demander quels types de graphes dirigés donnent naissance à des catégories. 

\begin{defi}[Graphe transitifs, graphes réflexifs]\leavevmode
\begin{itemize}[label=\textbullet]
	\item Un graphe dirigé $G=(S,A)$ est \emph{transitif} si  
     $(x, y)\in A$ et $(y, z) \in A$ implique 
    $(x, z) \in A$.
    \item Un graphe dirigé $G=(S,A)$ est \emph{réflexif} si 
    pour tout $x \in S$, on a $(x, x) \in A$.
\end{itemize}
\end{defi}

\begin{defprop}[Catégorie associée à un graphe transitif et réflexif]
Pour tout graphe dirigé transitif et réflexif $G=(S,A)$, les données suivantes forment une catégorie donnée $\mathcal{C}_G$ :
    \begin{itemize}
        \item $\operatorname{Obj} = S$
        \item $
            \operatorname{Hom}(s, s') =
            \begin{cases}
            	s \rightarrow s'\ \text{si } (s, s') \in A\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item la composée est donnée par la transitivité du graphe : En effet,
            $\forall s, s', s'' \in S$, tel que $s \rightarrow s'$ et
            $s' \rightarrow s''$, alors on a $(s, s') \in A$ et
            $(s', s'') \in A$, et par transitivité du graphe, on a
            $(s, s'') \in A$ et donc, on a, $s \rightarrow s''$
        \item les identités sont données par la réflexivité du graphe : En effet,
            $\forall s \in S, s \rightarrow s$, car $(s, s) \in A$
    \end{itemize}
\end{defprop}

\begin{proof}
\
    \begin{itemize}
    	\item Associativité :
            $\forall s, s', s'', s''' \in S$, grâce à la transitivité, on a :

            \[
            \begin{split}
                &((s, s') \in A \text{ et } (s', s'') \in A) \text{ et } (s'', s''') \in A\\
                \iff &(s, s'') \in A \text{ et } (s'', s''') \in A\\
                \iff &(s, s''') \in A\\
                \iff &(s, s') \in A \text{ et } (s', s''') \in A\\
                \iff &(s, s') \in A \text{ et } ((s', s'') \in A \text{ et } (s'', s''') \in A)
            \end{split}
            \]
        \item Unitaire :
            $\forall s, s' \in S$, par réfléxivité, on a :
            $(s, s) \in A$ et $(s, s') \in A \iff (s, s') \in A$, ainsi que,
            $(s, s') \in A$ et $(s', s') \in A \iff (s, s') \in A$
    \end{itemize}
\end{proof}

\begin{prop}
On peut construire à partir de tout ensemble partiellement ordonné un graphe transitif réflexif, et de tout graphe transitif réflexif on peut construire un ensemble partiellement ordonné.

\[
        \text{Ensembles partiellement ordonnés}
            \leftrightsquigarrow
        \text{Graphes transitifs réflexifs}
\]
\end{prop}

\begin{proo}
    \begin{itemize}
        \item $\text{Ensembles partiellement ordonnés} \rightsquigarrow \text{Graphes transitifs réflexifs}$
        
        Montrons que nous pouvons construire un graphe transitif réflexif pour tout ensemble partiellement ordonné :

        Soit $(E, \leq)$ un ensemble partiellement ordonnée. Et soit $G = (S, A)$ un graphe tel que $S = E$ et $A = \{(s, s') \in S^2, s \leq s'\}$.

        Alors, ce graphe est transitif, parce que $\forall (s, s'), (s', s'') \in A, (s, s'') \in A$, car si $s \leq s'$ et $s' \leq s''$, alors $s \leq s''$ par transitivité. Et, ce graphe est réflexif, parce que $\forall s \in S, (s, s) \in A$, car $s \leq s$ par réflexivité.

        Donc, $G$ est un graphe transitif réflexif.

        \item $\text{Ensembles partiellement ordonnés} \leftsquigarrow \text{Graphes transitifs réflexifs}$
        
        Montrons que nous pouvons construire un ensemble partiellement ordonné pour tout graphe transitif réflexif :

        Soit $G = (S, A)$ un graphe transitif réflexif. Et soit $(E, \leq)$ un ensemble muni d'une relation tel que $E = S$ et $\forall e, e' \in E, e \leq e' \iff (e, e') \in A$.

        Alors, $\leq$ est réflexif, car, $\forall e \in E, e \leq e$, car $(e, e) \in A$ par réflexivité du graphe. Et, $\leq$ est transitif, car, $\forall e, e', e'' \in E, e \leq e' \text{ et } e' \leq e'' \implies (e, e'), (e', e'') \in A \implies (e, e'') \in A \implies e \leq e''$ par transitivité du graphe. Enfin, $\leq$ doit être antisymétrique, or, le graphe est simple, donc, s'il existe $e, e' \in E$, tel que $e \leq e'$, il existe donc $(e, e') \in A$, d'où, le seul cas où $(e', e) \in A$ (pour que $e' \leq e$) est celui où $e = e'$, sinon, le graphe ne serait pas simple.

        $\leq$ est donc une relation d'ordre.
    \end{itemize}
\end{proo}

\begin{rema}
    On a sait qu'à partir d'un ensemble partiellement ordonné on
    peut construire un graphe transitif et réflexif via le diagramme de Hasse. 
    Et à partir d'un graphe transitif et réflexif, on peut construire une
    catégorie.

    \[
        \text{Ensembles partiellement ordonnés}
            \leftrightsquigarrow
        \text{Graphes transitifs réflexifs}
            \rightsquigarrow
        \text{Catégories}
    \]

    Nous avons donc réussi à englober les ensembles partiellement ordonnées
    et les graphes transitifs réflexifs dans les catégories. Après tout,
    nous avons schématiquement représenté plusieurs de nos catégories par
    des graphes et des ensembles partiellement ordonnés.
\end{rema}
