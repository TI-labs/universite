\babel@toc {french}{}\relax 
\contentsline {section}{\tocsection {}{1}{Introduction : l'algèbre moderne}}{1}{section.1}%
\contentsline {subsection}{\tocsubsection {}{1.1}{L'Algèbre ou l'analyse des équations}}{1}{subsection.1.1}%
\contentsline {subsection}{\tocsubsection {}{1.2}{L'algèbre ou l'axiomatisation des structures algébriques}}{2}{subsection.1.2}%
\contentsline {subsection}{\tocsubsection {}{1.3}{Intérêts de l'axiomatisation }}{3}{subsection.1.3}%
\contentsline {subsection}{\tocsubsection {}{1.4}{L'Algèbre moderne}}{4}{subsection.1.4}%
\contentsline {subsection}{\tocsubsection {}{1.5}{Bourbaki}}{4}{subsection.1.5}%
\contentsline {section}{\tocsection {}{2}{Catégories}}{4}{section.2}%
\contentsline {subsection}{\tocsubsection {}{2.1}{Définition}}{5}{subsection.2.1}%
\contentsline {subsection}{\tocsubsection {}{2.2}{Exemples}}{5}{subsection.2.2}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.1}{Catégories <<concrètes>>}}{5}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.2}{Catégories <<non-concrètes>>}}{6}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.3}{Contre-exemple}}{6}{subsubsection.2.2.3}%
\contentsline {subsection}{\tocsubsection {}{2.3}{Catégories petites et localement petites}}{7}{subsection.2.3}%
\contentsline {subsection}{\tocsubsection {}{2.4}{Constructions de catégories}}{8}{subsection.2.4}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.4.1}{Catégories associées à des ensembles partiellement ordonnés}}{9}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.4.2}{Catégorie associée à un graphe}}{9}{subsubsection.2.4.2}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.4.3}{Union disjointe de catégories}}{11}{subsubsection.2.4.3}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.4.4}{Joint de catégories}}{12}{subsubsection.2.4.4}%
\contentsline {subsubsection}{\tocsubsubsection {}{2.4.5}{Produit de catégories}}{12}{subsubsection.2.4.5}%
\contentsline {section}{\tocsection {}{3}{Propriétés des morphismes}}{12}{section.3}%
\contentsline {subsection}{\tocsubsection {}{3.1}{Isomorphisme dans une Catégorie}}{12}{subsection.3.1}%
\contentsline {subsection}{\tocsubsection {}{3.2}{Monomorphismes et Applications Injectives}}{13}{subsection.3.2}%
\contentsline {subsection}{\tocsubsection {}{3.3}{Epimorphisme dans une catégorie}}{15}{subsection.3.3}%
\contentsline {section}{\tocsection {}{4}{Quelques exemples de cat\'egories en informatique}}{17}{section.4}%
