\subsection{Catégories petites et localement petites}

On peut aussi donner une autre définition des catégories en regroupant chaque sous-collection de 
flèches en fixant la source et le but. On remarque aussi que les flèches sont souvent appelées \emph{morphismes} ou \emph{homomorphismes}.
Soit $f$ une flèche de source $X$ et de but $Y$, on dira que 
$f$ appartient à la collection des morphismes de $X$ dans $Y$, qu'on notera au choix 
\[\operatorname{Flech}_\mathcal{C}(X,Y)=\operatorname{Hom}_\mathcal{C}(X, Y) = \operatorname{Mor}_\mathcal{C}(X, Y) = \mathcal{C}(X, Y).\]

Ceci donne la définition équivalente suivante de catégorie, où il n'y a plus besoin d'applications <<$\mathrm{source}$>> et 
<<$\mathrm{but}$>> !

\begin{defi}[Catégorie (bis)]
Une \emph{catégorie} $\mathcal{C}$ est composée des éléments suivants :
\begin{itemize}[label=\textbullet]
	\item une collection d'\emph{objets}, notée $\mathrm{Obj}(\mathcal{C})$ : ${X}, {Y}, {Z},  \ldots$, 
	\item des collections de \emph{flèches} (ou \emph{morphismes}) pour toute paire d'objets $X$ et $Y$, notées $\mathrm{Hom}_\mathcal{C}(X, Y)$ : $f, g, h, \ldots$,
	\item une \emph{composition} :
        pour toute paire de flèches $f$ de $\operatorname{Hom}_\mathcal{C}(X, Y)$
        et $g$ de $\mathrm{Hom}_\mathcal{C}(Y, Z)$, on a une flèche $g \circ f$
        dans $\mathrm{Hom}_\mathcal{C}(X, Z)$,
\item des \emph{identités} : pour tout objet $X$ de $\mathcal{C}$,  il existe une flèche 
$ \mathrm{id}_X$ dans $\mathrm{Hom}_\mathcal{C}(X, X)$.
\end{itemize}
La composition est \emph{associative} 
\[h\circ (g\circ f) = (h\circ g)\circ f\]
et \emph{unitaire} 
\[f\circ \mathrm{id}_X=f \quad \text{et} \quad \mathrm{id}_Y \circ f=f.\]
\end{defi}

Dans la définition de la notion de catégorie, on utilise des collections d'objets et de morphismes, et non
des ensembles. Cela nous donne plus de flexibilité, et nous évite certaines
restrictions vis-à-vis des ensembles. Néanmoins, il peut arriver que certaines catégories soient constituées d'ensembles. 

\begin{defi}[Petite catégorie]
Une catégorie $\mathcal{C}$ est dite \emph{petite} si ses objets $\mathrm{Obj}(\mathcal{C})$ forment un ensemble.
\end{defi}

\begin{exem}
La catégorie des ensembles finis $\mathcal{Ens}_{fini}$ est définie par : \Bruno{à reprendre}
\begin{itemize}[label=\textbullet]
	\item $\mathrm{Obj} = \{ \text{ensembles finis} \}$
	\item $\mathrm{Hom} =$ \begin{itemize}
                                     \item[\textbullet] applications ensemblistes
                                     \item[\textbullet] injections
                                     \item[\textbullet] surjections
                                 \end{itemize}
\end{itemize}
Cette catégorie est petite car les ensembles finis forment un ensemble. 
\end{exem}

\begin{exem}
La catégorie composée d'un seul objet est une petite catégorie \Bruno{à reprendre : ne pas mélanger "définition" et "propriété" dans la même phrase} :
\begin{itemize}[label=\textbullet]
    \item $\mathrm{Obj} = \{ \cdot \}$
    \item $\mathrm{Hom} = \{ id_\cdot : \cdot \rightarrow \cdot \}$
\end{itemize}

\textcolor{red}{(insérer ici un graphe d'un point qui revient vers lui-même)}
\end{exem}

En effet, l'ensemble des ensembles finis est défini. De la même façon, la
catégorie des corps finis, des espaces vectoriels de dimension finie et toute
catégorie dont les objets sont finis sont des petites catégories.

\Bruno{ajouter une phrase de commentaire avant ou après disant que la propriété d'être petite est souvent trop restrictive et porte sur les objets.}

\begin{defi}[Catégorie localement petite]
Une catégorie est dite \emph{localement petite} si, pour toute paire d'objets $X,Y$ les morphismes $\operatorname{Hom}_\mathcal{C}(X, Y)$ de $X$ vers $Y$  forment un ensemble.
\end{defi}

La catégorie des ensembles $\mathcal{E}ns$ n'est pas une petite catégorie. En
effet, l'ensemble des ensembles n'existe pas (cf. Paradoxe de Russel).
\textcolor{red}{(pas sûr d'avoir eu une bonne lecture du tableau sur ce qui suit)}
\Bruno{oui, c'est bien}
Néanmoins, les $\operatorname{Hom}_{\mathcal{E}ns}(X, Y)$ forment des ensembles.
C'est donc une catégorie localement petite.

La catégorie des espaces vectoriels et des corps ne forment pas non plus de
petites catégories, parce qu'il n'existent pas d'ensembles de tous les espaces
vectoriels, ni d'ensemble de tous les corps, on doit donc se limiter aux
collections.

\begin{prop}
Si une catégorie est petite, alors, elle est aussi localement petite. 

\textcolor{red}{(à prouver ? mais difficile sans une bonne compréhension de la différence entre
ensemble et collection)}
\end{prop}

\Bruno{cette proposition est fausse ! Mais c'est très bien de t'être posé la question. (Du coup, je suis d'accord la terminologie choisie peut prêter à confusion : <<petit>> n'implique pas <<localement petit>>. On peut le sentir car la première condition porte sur les objets et la seconde sur les morphismes ... En voici un contre-exemple, qu'il faut taper. :) On considère la catégorie à un objet et dont la collection des flèches est les ensembles. On la munit de la composition donnée par l'union (disjointe) et l'identité est l'ensemble vide !)}

\subsection{Constructions de catégories}

\subsubsection{Catégories associées à des ensembles partiellement ordonnés}

\begin{defi}[Ensemble partiellement ordonné]
Un \emph{ensemble partiellement ordonné} $\pi=(E, \leq)$ est un ensemble $E$ muni 
d'une relation d'ordre $\le$, c'est-à-dire une relation binaire vérifiant :
\begin{itemize}[label=\textbullet]
    \item \textsc{réflexivité} : $\forall x \in E,\ x \leq x$,
    \item \textsc{transitivité} : $\forall x, y, z \in E,\ x \leq y \wedge y \leq z \Leftrightarrow x \leq z$,
    \item \textsc{antisymétrie} : $\forall x, y \in E,\ x \leq y \wedge y \leq x \Leftrightarrow x = y$.
\end{itemize}
\end{defi}


\begin{exem}[Ensemble des parties]
Pour tout ensemble $X$, l'ensemble $E \coloneq \mathcal{P}(X)=\{A\subset X\}$ de ses parties est un ensemble partiellement ordonné  où la relation d'ordre est donnée par l'inclusion 
$\leq\, \coloneq\, \subset$.
\end{exem}

On peut représenter un tel ensemble avec un diagramme, le diagramme de Hasse,
qui met en évidence l'ordre établi dans l'ensemble.

\textcolor{red}{(insérer ici un diagramme de Hasse) dans le cas $X = \{1, 2, 3\}$.}

On peut associer canoniquement une catégorie à tout ensemble partiellement ordonné.

\begin{defprop}[Catégorie associée à un ensemble partiellement ordonné]
    Soit $\pi = (E, \leq)$ un ensemble partiellement ordonné. Les données suivantes 
    forment une catégorie, notée $\mathcal{C}_\pi$ :   
       \begin{itemize}[label=\textbullet]
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item \Bruno{La composée est donnée par ...}
        \item \Bruno{les identités sont ...}
    \end{itemize}
\end{defprop}

\begin{proof}
\Bruno{La composition est associative car (transitivité), elle est unitaire car (...)}
\end{proof}

\subsubsection{Catégorie associée à un graphe}

Les graphes sont des outils centraux en mathématiques et en informatique. 
On peut déjà remarquer une certaine proximité entre les graphes et les catégories : on représente souvent des
catégories au moyen de graphes. On peut aussi créer des graphes à partir d'ensembles partiellement ordonnés via la notion de diagramme de Hasse. 

\begin{defi}[Graphes]\leavevmode
\begin{itemize}[label=\textbullet]
    \item Un \emph{graphe} $G=(S,A)$ est un ensemble $S$ (fini et non vide) de \emph{sommets} 
    et un ensemble $A$ de paires de sommets appelées \emph{arêtes}.
    \item Un graphe $G$ est dit simple s'il n'y a au plus qu'une seule arête
        reliant deux sommets. \Bruno{à virer : on va faire plus simple : par défaut nos graphes sont "simples" (sinon on les appelerait des "multigraphes").}
    \item Un graphe $G=(S,A)$ est \emph{dirigé} lorsque toutes ses arêtes sont des paires \emph{ordonnées} de sommets, c'est-à-dire $A\subset S\times S$. 
    \item Un graphe $G$ est dit \emph{complet} lorsqu'il est constitué de toutes les arêtes possibles. 
\end{itemize}
\end{defi}

On ne considérera ici que des graphes dirigés. On peut maintenant se demander quels types de graphes dirigés donnent naissance à des catégories. 

\begin{defi}[Graphe transitifs, graphes réflexifs]\leavevmode
\begin{itemize}[label=\textbullet]
	\item Un graphe dirigé $G=(S,A)$ est \emph{transitif} si  
     $(x, y)\in A$ et $(y, z) \in A$ implique 
    $(x, z) \in A$. \Bruno{une définition n'est pas un équivalence, elle n'est pas symétrique. on ne dit donc pas "si et seulement si", mais seulement "si".}
\item     Un graphe dirigé $G=(S,A)$ est \emph{réflexif} si 
    pour tout $x \in S$, on a $(x, x) \in A$.
\end{itemize}
\end{defi}

\begin{defprop}[Catégorie associée à un graphe transitif et réflexif]
Pour tout graphe dirigé transitif et réflexif $G=(S,A)$, les données suivantes forment une catégorie donnée $\mathcal{C}_G$ :
    \begin{itemize}
        \item $\operatorname{Obj} = E$
        \item $
            \operatorname{Hom}(e, e') =
            \begin{cases}
            	e \rightarrow e'\ \text{si } e \leq e'\\
                \emptyset\ \text{sinon}
            \end{cases}
        $
        \item \Bruno{La composée est donnée par ...}
        \item \Bruno{les identités sont ...}
    \end{itemize}
\end{defprop}

\begin{proof}
\Bruno{La composition est associative car (transitivité), elle est unitaire car (...)}
\end{proof}

\begin{rema}
    On a sait qu'à partir d'un ensemble partiellement ordonné on
    peut construire un graphe transitif et réflexif via le diagramme de Hasse. 
    Et à partir d'un
    graphe transitif et réflexif,
    on peut construire une catégorie.

    \[
        \text{Ensembles partiellement ordonnés}
            \rightsquigarrow
        \text{Graphes transitifs réflexifs}
            \rightsquigarrow
        \text{Catégories}
    \]
\end{rema}
\Bruno{et alors ? Commenter ? la seconde construction est-elle plus générale ou équivalente à la première ? \`A finir.}
