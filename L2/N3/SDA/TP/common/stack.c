#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

stack_t* stack_push(element_t value, stack_t* stack) {
	stack_t* s = (stack_t*) malloc(sizeof(stack_t));

	if (s == NULL) {
		perror("malloc error");
		return NULL;
	}

	s->stack = stack;
	s->value = value;
	return s;
}

stack_t* stack_pop(stack_t* stack) {
	assert(stack != NULL);

	stack_t* temp = stack->stack;
	free(stack);
	return temp;
}

element_t stack_peek(stack_t* stack) {
	assert(stack != NULL);

	return stack->value;
}

int stack_height(stack_t* stack) {
	if (stack == NULL)
		return 0;
	return 1 + stack_height(stack->stack);
}

void __stack_delete_node(stack_t* stack) {
	if (stack == NULL)
		return;
	__stack_delete_node(stack->stack);
	free(stack);
}

void stack_delete(stack_t** stack) {
	__stack_delete_node(*stack);
	*stack = NULL;
}
