#include "binary_tree_char.h"
#include "bst.h"

#define N 5

int main() {
	int tab[N] = { 'a', 'B', '3', ';', '\'' };

	link abr = empty_tree();

	// O(n²)
	for (int i = 0; i < N; i++) {
		abr = insert_BST_it(abr, tab[i]); // O(n)
	}

	traverse_inorder_binary_tree(abr, &print_label); // O(n)

	delete_binary_tree(&abr);

	return 0;
}
