#include <stdlib.h>
#include "binary_tree_char.h"
#include "bst.h"

#define less(A,B) (A < B)
#define eq(A,B) (A == B)

/** Searches a BST for a label and returns a pointer to a node with this label */
/* Returns NULL if there is no node with this label */
link search_BST(link h, element_t v) {
  if (h == NULL) return NULL;
  element_t t = get_binary_tree_root(h);
  if eq(v, t) return h;
  if less(v, t) return search_BST(h->left, v);
    else return search_BST(h->right, v);
  }

/** Inserts a node with a given label in a BST */
/* and returns a link to the updated BST */
link insert_BST(link h, element_t v) {
  if (h == NULL) return cons_binary_tree(v, NULL, NULL);
  if less(v, get_binary_tree_root(h)) {
    h->left = insert_BST(h->left, v);
  }
  else {
    h->right = insert_BST(h->right, v);
  }
  return h;
}

/** Selects the kth label of a BST */
/* and returns a pointer to the corresponding node */
/* Returns NULL if the BST does not contain k labels */
link select_BST(link h, int k) {
  int t = size_binary_tree(h->left);
  if (h == NULL) return NULL;
  if (t > k) return select_BST(h->left, k);
  if (t < k) return select_BST(h->right, k-t-1);
  return h;
}

/********************************************************/
/* The following functions will not be studied during   */
/* Friday's lecture but will be written by the students */
/* in class and lab                                     */
/********************************************************/

/** Inserts iteratively a node with a given label in a BST */
/* and returns the updated BST */
link insert_BST_it(link h, element_t v) {
	if (h == NULL)
		return cons_binary_tree(v, NULL, NULL);

	/*
	link c = h;

	while (1) {
		if less(v, get_binary_tree_root(c)) {
			 if (c->left == NULL) {
				 c->left = cons_binary_tree(v, NULL, NULL);
				 break;
			 }
			 c = c->left;
		} else {
			 if (c->right == NULL) {
				 c->right = cons_binary_tree(v, NULL, NULL);
				 break;
			 }
			 c = c->right;
		}
	}

	return h;
	*/

	link p, x = h;
	if (h == NULL) return cons_binary_tree(v, NULL, NULL);

	while (x != NULL) {
		p = x;
		x = less(v, get_binary_tree_root(x)) ? x->left : x->right;
	}

	x = cons_binary_tree(v, NULL, NULL);

	if less(v, p->label) {
		p->left = x;
	} else {
		p->right = x;
	}

	return h;
}

/*************     ROTATIONS        *************/

/** Applies a right rotation the root of a BST */
link rotate_right(link h) {
	link x;
	if (NULL == h && NULL == h->left) return NULL;
	x = h->left;
	h->left = x->right;
	x->right = h;
	return x;
}

/** Applies a left rotation to the root of a BST */
link rotate_left(link h) {
	link x;
	if (NULL == h && NULL == h->right) return NULL;
	x = h->right;
	h->right = x->left;
	x->left = h;
	return x;
}

/** Inserts a node with a given label at the root of BST */
/* and returns the updated BST */
link insert_BST_root(link h, element_t v) {
	if (NULL == h) return cons_binary_tree(v, NULL, NULL);
	if less(v, get_binary_tree_root(h)) {
		h->left = insert_BST_root(h, v);
		h = rotate_right(h);
	} else {
		h->right = insert_BST_root(h, v);
		h = rotate_left(h);
	}
	return h;
}

link partition_BST (link h , int k) {
	int t = size_binary_tree(h->left);
	if (is_empty_binary_tree(h)) return h;
	if (t > k) {
		h->left = partition_BST(h->left, k);
		h = rotate_right(h);
	}
	if (t < k) {
		h->right = partition_BST(h->right, k-t-1);
		h = rotate_left(h);
	}
	return h;
}

link balance_BST(link h) { // Cost : n²
	int t = size_binary_tree(h);      // n
	if (t < 2) return h;
	h = partition_BST(h, t/2);        // n
	h->left = balance_BST(h->left);   // ↓
	h->right = balance_BST(h->right); // n²
	return h;
}

link delete_BST(link h, element_t v) {
	if (NULL == h) return h;
	if (get_binary_tree_root(h) < v) {
		h->left = delete_BST(h->left, v);
	} else if (get_binary_tree_root(h) > v) {
		h->right = delete_BST(h->right, v);
	} else {
		if (h->left == NULL) return h->right;
		else if (h->right == NULL) return h->left;
		else {
			link x = h;
			h->right = partition_BST(h->right, 0);
			h->right->left = h->left;
			h = h->right;
			free(x);
		}
	}
	return h;
}
