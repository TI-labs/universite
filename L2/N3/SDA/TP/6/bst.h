#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include "binary_tree_char.h"

link insert_BST(link, element_t);
link search_BST(link, element_t);
link select_BST(link, int);
link insert_BST_it(link, element_t);
link rotate_right(link);
link rotate_left(link);
link insert_BST_root(link, element_t);

#endif
