#include "binary_tree_char.h"
#include <stdlib.h>
#include <stdio.h>

/** "Creates" an empty tree and returns its address */
binary_tree_t * empty_tree() {
  return NULL;
}

/* Constructs a binary tree from a label and two binary trees */
/* (allocates memory and returns the allocated block's address) */
binary_tree_t * cons_binary_tree(element_t x, binary_tree_t *pt_l, binary_tree_t *pt_r) {
  binary_tree_t * pt = malloc(sizeof(binary_tree_t));
  pt->label = x;
  pt->left = pt_l;
  pt->right = pt_r;
  return pt;
}

/* Frees all memory allocated to a tree ans its subtrees... */
void delete_BT(binary_tree_t * pt) {
  if (NULL != pt) {
    delete_BT(left(pt));
    delete_BT(right(pt));
    free(pt);
  }
}
/* ... and sets pointeur to deleted binary tree to NULL */
void delete_binary_tree(binary_tree_t ** pt) {
  delete_BT(*pt);
  *pt = NULL;
}

/** Returns the left subtree of a tree */
/* (requires that the tree be non-empty) */
binary_tree_t * left(const binary_tree_t * pt) {
  return pt->left;
}

/** Returns the right subtree of a tree */
/* (requires that the tree be non-empty) */
binary_tree_t * right(const binary_tree_t * pt) {
  return pt->right;
}

/** Returns the label of the root of a tree */
/* (requires that the tree be non-empty) */
element_t get_binary_tree_root(const binary_tree_t * pt) {
  return pt->label;
}

/** Tests a tree for emptyness */
int is_empty_binary_tree(const binary_tree_t * pt) {
  return (NULL == pt);
}

/** Computes recursively and returns the size of a tree */
int size_binary_tree(const binary_tree_t * tree) {
  if (tree == NULL) return 0;
  return size_binary_tree(tree->left) + size_binary_tree(tree->right) + 1;
}

/** Prints the label of a node with the appropriate shift */
void print_node_shift(element_t c, int shift) {
  int i;
  for (i = 0; i < shift; i++) printf("  ");
  printf("%c\n", c);
}
/** Prints a tree (rotated by -\pi/2) */
void show_binary_tree(const binary_tree_t * tree, int shift) {
  if (NULL == tree) return;
  show_binary_tree(tree->right, shift + 1);
  print_node_shift(get_binary_tree_root(tree), shift);
  show_binary_tree(tree->left, shift + 1);
}
