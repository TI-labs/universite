#ifndef QUEUE_H
#define QUEUE_H

#include "tree.h"

typedef binary_tree_t* queue_element_t;

typedef struct queue_s {
	element_t value;
	struct queue_s* queue;
} queue_t;

queue_t* queue_enqueue(queue_t* queue, queue_element_t value);
queue_t* queue_dequeue(queue_t* queue);
queue_element_t queue_peek(queue_t* queue);
int queue_length(queue_t* queue);
void queue_delete(queue_t** queue);

#endif
