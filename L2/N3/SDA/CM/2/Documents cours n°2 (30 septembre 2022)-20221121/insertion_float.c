void insertion(float *a, int l, int r) {
  int i, j;
  for (i = l+1; i <= r; ++i) {
    for (j = i; j > l && a[j] < a[j-1]; --j) {
      swap(a+j-1, a+j);
    }
  }
}
