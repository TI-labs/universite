void swap (float *x, float *y) {
  float tmp = *x;
  *x = *y;
  *y = tmp;
}

void sink(float *a, int l, int r) {
  int i, j;
  for (i = l; i < r; ++i) {
    for (j = r; j > i; --j) {
      if (a[j] < a[j-1]) {
        swap(a+j-1, a+j);
      }
    }
  }
}
