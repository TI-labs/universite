void shell(float *a, int l, int r) {
  int i, j, h;
  for (h = 1; 3*h <= r-l-1; h = 3*h + 1);
  for ( ; h > 0; h /= 3) {
    for (i = l+h; i <= r; ++i) {
      for (j = i; j >= l+h && a[j] < a[j-h]; j -= h) {
        swap(a+j-h, a+j);
      }
    }
  }
}
