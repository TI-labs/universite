// Lyes SAADI 12107246
// Mohamed Lamine MERAH 12213408

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define max(x,y) ((x)<(y)?(y):(x))
#define min(x,y) ((x)<(y)?(x):(y))

typedef int element_t;

void merge_sort(element_t[], int);

int main() {
	element_t t [] = { 1, 3, 2, 6, 7, 5, 4};
	int i;

	for(i=0; i < 7; ++i)
		printf("%d, ", t[i]);
	putchar('\n');

	merge_sort(t, 7);

	for(i=0; i < 7; ++i)
		printf("%d, ", t[i]);
	putchar('\n');

	return 0;
}


void merge_two_blocks(element_t t[], int n1, int n2) {
	element_t aux[n1+n2];
	int i, j, k;

	if (n2 < 1)
		return;
	
	/* copy elements from 1st array */
	for(i = 0; i < n1; ++i)
		aux[i] = t[i];
	
	/* copy elements from 2nd array */
	for(i = n1, j = n1 + n2 - 1; j > n1 - 1; --j)
		aux[i++] = t[j];
	
	/* merge */
	i = 0;
	j = n1 + n2 - 1;
	for(k = 0; k < n1 + n2; ++k) {
		if (aux[i] < aux[j]) {
			t[k] = aux[i++];
		} else {
			t[k] = aux[j--];
		}
	}
}

void merge_sort(element_t t[], int n) {
	int block_size = 1;
	
	while (block_size < n) {
		int i;
		int n1 = block_size; /* size of 1st block */
		int n2 = block_size; /* size of 2nd block */
		
		/* merge adjacent blocks (same same size = block_size) */
		for(i = 0; i + (2 * block_size) < n; i += (2 * block_size))
			merge_two_blocks(t + i, n1, n2);
		/* merge last two blocks : their size may be smaller then block_size */
		n1 = min(n % (2*block_size), block_size);
		n2 = max(n % (2*block_size) - block_size, 0);
		merge_two_blocks(t + i, n1, n2);

		/* update block size */
		block_size = 2 * block_size;
	}
}
