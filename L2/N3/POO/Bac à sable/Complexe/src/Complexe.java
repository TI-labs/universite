public class Complexe {
	private double re;
	private double im;
	
	public static void main(String[] args) {
		Complexe c = new Complexe(2.5, 8.9999);
		
		c.print();

		Complexe.mult(c, new Complexe(2, 0)).print();

		c.print();
	}

	public Complexe(double reel, double imaginaire) {
		re = reel;
		im = imaginaire;
	}

	public Complexe() {
		re = 0;
		im = 0;
	}

	public Complexe(Complexe c) {
		re = c.re;
		im = c.im;
	}

	public double getReal() {
		return re;
	}
	
	public double getImaginary() {
		return im;
	}
	
	public Complexe getSquare() {
		return new Complexe(re * re - im * im, 2 * re * im);
	}

	public Complexe getConjugate() {
		return new Complexe(re, -im);
	}
	
	public double getModule() {
		return Math.sqrt(re * re + im * im);
	}

	public Complexe add(Complexe c) {
		return new Complexe(re + c.re, im + c.im);
	}

	public Complexe mult(Complexe c) {
		return new Complexe(re * c.re - im * c.im, re * c.im + c.re * im);
	}

	public void print() {
		System.out.printf("%.2f + i%.2f\n", re, im);
	}

	public static Complexe add(Complexe r, Complexe l) {
		return r.add(l);
	}

	public static Complexe mult(Complexe r, Complexe l) {
		return r.mult(l);
	}
}