public class Equipe {
	private String nom;
	private final static int TAILLE_MONSTREDEX = 6;
	private MonstrePoche[] monstredex;
	private MonstrePoche actif;
	
	public Equipe(String nom, boolean robot) {
		this.nom = nom;
		this.monstredex = new MonstrePoche[TAILLE_MONSTREDEX];
		this.actif = null;
	}

	public String getNom() {
		return nom;
	}

	public boolean ajoutMonstrePoche(MonstrePoche mp) {
		if (tailleMonstredex() == TAILLE_MONSTREDEX) {
			return false;
		}

		monstredex[tailleMonstredex()] = mp;

		return true;
	}

	public boolean enleverMonstrePoche(MonstrePoche mp) {
		int i = chercherMonstrePoche(mp);
		
		if (i == -1)
			return false;
		
		if (actif == monstredex[i])
			actif = null;
		
		monstredex[i] = null;
		
		for (; i < tailleMonstredex() - 1; i++) {
			monstredex[i] = monstredex[i + 1];
			monstredex[i + 1] = null;
		}

		return true;
	}

	public int chercherMonstrePoche(MonstrePoche mp) {
		for (int i = 0; i < TAILLE_MONSTREDEX; i++) {
			if (monstredex[i] == mp)
			return i;
		}
		
		return -1;
	}

	public MonstrePoche[] getMonstredex() {
		return monstredex;
	}

	public int tailleMonstredex() {
		for (int i = 0; i < TAILLE_MONSTREDEX; i++) {
			if (monstredex[i] == null)
				return i;
		}
		return TAILLE_MONSTREDEX;
	}

	public MonstrePoche getMonstre(int p) {
		return monstredex[p];
	}

	public void choisirMonstrePoche(MonstrePoche mp) {
		actif = mp;
	}

	public void choisirMonstrePoche() {
		int random = (int)(Math.random() * (TAILLE_MONSTREDEX - 1));

		actif = monstredex[random];
	}
}