public class EssaiObjetZork {
	public static void main(String args[]) {
		ObjetZork banane = new ObjetZork("BANANA", true, 42);
		System.out.println(banane.hashCode());
		ObjetZork banane2 = new ObjetZork("BANANA2", true, 69420);
		System.out.println(banane2.hashCode());
		ObjetZork banane3 = new ObjetZork("BANANA", true, 42);
		System.out.println(banane3.hashCode());
		ObjetZork LAbanane = new ObjetZork("BANANA", false, 42);
		System.out.println(LAbanane.hashCode());

		System.out.println(banane.equals(banane2));
		System.out.println(banane.equals(banane3));
		System.out.println(banane.equals(LAbanane));
	}
}