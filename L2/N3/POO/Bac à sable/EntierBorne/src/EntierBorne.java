public class EntierBorne {
	private int i;

	public EntierBorne() {
		this.i = 0;
	}

	public EntierBorne(int i) throws HorsBornesException {
		if (i < -10000 || i > 10000) throw new HorsBornesException();
		this.i = i;
	}

	public int get() {
		return i;
	}

	public static EntierBorne addition(EntierBorne l, EntierBorne r) throws HorsBornesException {
		return new EntierBorne(l.get() + r.get());
	}

	public static EntierBorne soustraction(EntierBorne l, EntierBorne r) throws HorsBornesException {
		return new EntierBorne(l.get() - r.get());
	}

	public static EntierBorne multiplication(EntierBorne l, EntierBorne r) throws HorsBornesException {
		return new EntierBorne(l.get() * r.get());
	}

	public static EntierBorne division(EntierBorne l, EntierBorne r) throws HorsBornesException, DivisionParZeroException {
		if (r.get() == 0)
			throw new DivisionParZeroException();
		return new EntierBorne(l.get() / r.get());
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EntierBorne)) {
			return false;
		}
		EntierBorne i = (EntierBorne) o;
		return (get() == i.get());
	}

	@Override
	public int hashCode() {
		return i;
	}

	@Override
	public String toString() {
		return Integer.toString(i);
	}
}