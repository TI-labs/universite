public class Magicien extends Perso {
	private int pm;
	private int pmMax;
	private int spi;

	public Magicien(String nom, int pv, int lvl, int pmMax, int spi) {
		super(nom, pv, lvl);
		this.pmMax = pmMax;
		this.pm = pmMax;
		this.spi = spi;
	}

	public Magicien(String nom, int pv, int pmMax, int spi) {
		super(nom, pv);
		this.pmMax = pmMax;
		this.pm = pmMax;
	}

	public int getEsprit() {
		return spi;
	}
	
	public int getPointsMagie() {
		return pm;
	}
	
	public int getMaxPointsMagie() {
		return pmMax;
	}
	
	public boolean electrocuter(Perso p) {
		if (pm < 10)
			return false;
		p.prendreDegats(getNiveau() * 2);
		pm -= 10;
		return true;
	}

	public boolean payerMagie(int cout) {
		if (pm < cout) {
			return false;
		} else {
			pm -= cout;
			return true;
		}
	}

	@Override
	public void seReposer() {
		super.seReposer();
		if (estVivant() && pm <= pmMax - spi)
			pm += spi;
	}

	@Override
	public void levelUp() {
		super.levelUp();
		pmMax++;
		spi++;
	}

	@Override
	public String toString() {
		return "Magicien " + getNom()
		+ " de niveau " + getNiveau()
		+ ", de vie " + getVie() + "/" + PV_MAX
		+ "hp, de magie " + pm + "/" + pmMax
		+ "pm et d'esprit " + spi;
	}
}