public class Carte {
	private final Rang rang;
	private final Famille famille;
	private final boolean joker;

	public Carte(Rang rang, Famille famille) {
		this.rang = rang;
		this.famille = famille;
		joker = false;
	}

	private Carte() {
		joker = true;
		rang = null;
		famille = null;
	}

	public static Carte Joker() {
		Carte j = new Carte();
		return j;
	}

	public Rang getRang() {
		return rang;
	}

	public Famille getFamille() {
		return famille;
	}

	public boolean isJoker() {
		return joker;
	}
}