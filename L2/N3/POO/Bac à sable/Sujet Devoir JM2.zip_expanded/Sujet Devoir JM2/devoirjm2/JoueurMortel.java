/*
 * Lyes Saadi 12107246
 * Ceci est mon travail personnel.
 */

package devoirjm2;

import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * <p>
 * Un joueur pour le jeu Zork, ayant la particularité de devoir s'alimenter pour
 * pouvoir survivre et de pouvoir ramasser et transporter des aliments dans un
 * garde-manger. Un {@code JoueurMortel} est entièrement caractérisé par:
 * <ul>
 * <li>son nom;</li>
 * <li>son capital de points de vie;</li>
 * <li>la pièce dans laquelle il se trouve;</li>
 * <li>les aliments transportés par ce joueur dans son garde-manger. Un
 * {@code JoueurMortel} peut transporter un nombre illimité d'aliments.</li>
 * </ul>
 * </p>
 *
 * <p>
 * A sa création un {@code JoueurMortel} possède un capital de points de vie
 * à sa valeur maximale (i.e. {@code Joueur.MAX_CAPITAL_VIE}), qui va diminuer ou
 * augmenter à chaque fois qu'il effectue une action, ce nombre pouvant varier
 * en fonction de l'action réalisée. Un {@code JoueurMortel} ne peut effectuer
 * une action que s'il est vivant. Dans cette version de {@code JoueurMortel}
 * les seules actions implémentées sont:
 * <dl>
 * <dt>se déplacer d'une pièce à l'autre:</dt>
 * <dd>chaque déplacement fait diminuer de 10 points le capital vie du joueur et
 * le nombre de points de vie du joueur diminue d'une unité supplémentaire par
 * aliment transporté;</dd>
 * <dt>ramasser un aliment présent dans la pièce courante:</dt>
 * <dd>l'aliment ramassé est retiré de la pièce courante et est ajouté au
 * garde-manger du joueur. Cette action coûte 2 points de vie au joueur;</dd>
 * <dt>manger:</dt>
 * <dd>si le {@code JoueurMortel} possède au moins un aliment, l'action manger
 * retire un aliment des aliments qu'il transporte dans son garde-manger et
 * augmente son capital vie de la valeur nutritive de cet aliment dans la limite
 * du capital vie maximal {@code JoueurMortel.MAX_CAPITAL_VIE}.</dd>
 * </dl>
 * Lorsque le capital vie du joueur atteint (ou passe en dessous de) la valeur
 * de 0 points de vie, le {@code JoueurMortel} meurt définitivement et ne peut
 * donc plus faire aucune action, ni même s'alimenter.
 * </p>
 *
 *
 * @invariant getCapitalVie() <= JoueurMortel.MAX_CAPITAL_VIE;
 * @invariant estVivant() <==> (getCapitalVie() > 0);
 * @invariant getNom() != null && getNom().length() > 0;
 * @invariant getPieceCourante() != null;
 *
 * @author Marc Champesme
 * @since 6 mars 2013
 * @version 4 décembre 2022
 */
public class JoueurMortel extends LinkedListCollection<Aliment> {
	/**
	 * Valeur maximale du capital vie d'un {@code JoueurMortel}.
	 */
	public static final int MAX_CAPITAL_VIE = 40;

	private String nom;
	private int capitalVie;
	private Piece pieceCourante;

	/**
	 * Initialise un nouveau {@code JoueurMortel} vivant situé dans la pièce
	 * spécifiée et portant le nom spécifié. Le garde-manger de ce nouveau
	 * {@code JoueurMortel} est vide.
	 *
	 * @requires nom != null && nom.length() > 0;
	 * @requires pieceCourante != null;
	 * @ensures estVivant();
	 * @ensures getNom().equals(nom);
	 * @ensures getPieceCourante().equals(pieceCourante);
	 * @ensures getCapitalVie() == JoueurMortel.MAX_CAPITAL_VIE;
	 * @ensures isEmpty()();
	 *
	 * @param nom
	 *            le nom de ce {@code JoueurMortel}.
	 * @param pieceCourante
	 *            la pièce dans laquelle se trouve ce {@code JoueurMortel}.
	 *
	 * @throws NullPointerException si le nom ou la pièce spécifié est null
	 * @throws IllegalArgumentException si la longueur du nom est 0
	 */
	public JoueurMortel(String nom, Piece pieceCourante) {
		super();
		if (nom == null || pieceCourante == null) throw new NullPointerException();
		if (nom.length() == 0) throw new IllegalArgumentException();
		this.nom = nom;
		this.pieceCourante = pieceCourante;
		this.capitalVie = MAX_CAPITAL_VIE;
	}

	/**
	 * Initialise un nouveau {@code JoueurMortel} vivant portant le nom
	 * spécifié, situé dans la pièce spécifiée et dont le garde-manger contient
	 * les {@code Aliment} spécifiés. Le garde-manger de ce nouveau
	 * {@code JoueurMortel} contient les {@code nbAliments} premiers éléments du
	 * tableau spécifié.
	 *
	 * @requires nom != null && nom.length() > 0;
	 * @requires pieceCourante != null;
	 * @requires tabAliments != null;
	 * @requires (nbAliments >= 0) && (nbAliments <= tabAliments.length);
	 * @requires (\forall int i; i >= 0 && i < nbAliments; tabAliments[i] !=
	 *           null);
	 * @ensures estVivant();
	 * @ensures getNom().equals(nom);
	 * @ensures getPieceCourante().equals(pieceCourante);
	 * @ensures getCapitalVie() == JoueurMortel.MAX_CAPITAL_VIE;
	 * @ensures size() == nbAliments;
	 * @ensures (\forall int i; i >= 0 && i < nbAliments;
	 *          contains(tabAliments[i]));
	 *
	 * @param nom
	 *            le nom de ce {@code JoueurMortel}.
	 * @param pieceCourante
	 *            la pièce dans laquelle se trouve ce {@code JoueurMortel}.
	 * @param tabAliments
	 *            le tableau dont les {@code nbAliments} premiers éléments
	 *            doivent être ajoutés au garde-manger de ce
	 *            {@code JoueurMortel}.
	 * @param nbAliments
	 *            nombre d'{@code Aliment} du tableau à ajouter au garde-manger
	 *            de ce {@code JoueurMortel}.
	 *
	 * @throws NullPointerException si le nom, la pièce ou le tableau spécifié est null
	 								ou bien si un des nbAliments premiers éléments du
									tableau spécifié est null
	 * @throws IllegalArgumentException si la longueur du nom est 0 ou bien que l'entier
	 *								spécifié est < 0 ou strictement supérieur à la
	 *								longueur du tableau spécifié
	 *
	 */
	public JoueurMortel(String nom, Piece pieceCourante, Aliment[] tabAliments, int nbAliments) {
		this(nom, pieceCourante);
		
		if (tabAliments == null) throw new NullPointerException();
		if (nbAliments < 0 || nbAliments > tabAliments.length) throw new IllegalArgumentException();
		
		for (int i = 0; i < nbAliments; i++) {
			if (tabAliments[i] == null) throw new NullPointerException();
			add(tabAliments[i]);
		}
	}

	/**
	 * Initialise un nouveau {@code JoueurMortel} vivant portant le nom
	 * spécifié, situé dans la pièce spécifiée et dont le garde-manger contient
	 * les {@code Aliment} spécifiés. Le garde-manger de ce nouveau
	 * {@code JoueurMortel} contient les éléments de la Collection spécifiée.
	 *
	 * @requires nom != null && nom.length() > 0;
	 * @requires pieceCourante != null;
	 * @requires c != null;
	 * @requires !c.contains(null);
	 * @ensures estVivant();
	 * @ensures getNom().equals(nom);
	 * @ensures getPieceCourante().equals(pieceCourante);
	 * @ensures getCapitalVie() == JoueurMortel.MAX_CAPITAL_VIE;
	 * @ensures size() == c.size();
	 * @ensures contentEquals(c);
	 *
	 * @param nom
	 *            le nom de ce {@code JoueurMortel}.
	 * @param pieceCourante
	 *            la pièce dans laquelle se trouve ce {@code JoueurMortel}.
	 * @param c
	 *            la Collection dont les éléments doivent être ajoutés au
	 *				garde-manger de ce {@code JoueurMortel}.
	 *
	 * @throws NullPointerException si le nom, la pièce ou la Collection spécifiée est null
	 								ou bien si la Collection spécifiée contient null
	 * @throws IllegalArgumentException si la longueur du nom est 0
	 *
	 */
	public JoueurMortel(String nom, Piece pieceCourante, Collection<? extends Aliment> c) {
		this(nom, pieceCourante);
		if (c == null || c.contains(null)) throw new NullPointerException();
		addAll(c);
	}

	// Accesseurs:
	/**
	 * Renvoie le nom de ce {@code JoueurMortel}.
	 *
	 * @return le nom de ce {@code JoueurMortel}.
	 *
	 * @pure
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie le nombre de points de vie de ce {@code JoueurMortel}.
	 *
	 * @return le nombre de points de vie de ce {@code JoueurMortel}.
	 *
	 * @pure
	 */
	public int getCapitalVie() {
		return capitalVie;
	}

	/**
	 * Renvoie {@code true} si ce {@code JoueurMortel} a un capital vie
	 * strictement supérieur à 0.
	 *
	 * @return {@code true} si ce {@code JoueurMortel} a un capital vie
	 *         strictement supérieur à 0; {@code false} sinon.
	 *
	 * @pure
	 */
	public boolean estVivant() {
		return capitalVie > 0;
	}

	/**
	 * Renvoie la {@code Piece} dans la quelle se trouve ce
	 * {@code JoueurMortel.}
	 *
	 *
	 * @return la {@code Piece} dans la quelle se trouve ce
	 *         {@code JoueurMortel.}
	 *
	 * @pure
	 */
	public Piece getPieceCourante() {
		return pieceCourante;
	}

	/**
	 * Renvoie l'{@code Aliment} du garde manger de ce {@code JoueurMortel}
	 * dont la valeur nutritive est la plus élevée. Si plusieurs {@code Aliment}
	 * possède cette valeur maximale, un quelconque de ces {@code Aliment} est
	 * renvoyé.
	 *
	 * @requires !isEmpty();
	 * @ensures \result != null;
	 * @ensures contains(\result);
	 * @ensures \result.getValeurNutritive() == (\max Aliment al; contains(al); al.getValeurNutritive());
	 *
	 * @throws NoSuchElementException si le garde-manger de ce {@code JoueurMortel} est vide
	 *
	 * @pure
	 */
	public Aliment bestAliment() {
		if (isEmpty()) throw new NoSuchElementException();
		Aliment best = getAliment();
		for (Aliment a : this) {
			if (a.getValeurNutritive() > best.getValeurNutritive())
				best = a;
		}
		return best;
	}

	/**
	 * Renvoie un {@code Aliment} dont la valeur nutritive est comprise entre
	 * les deux entier spécifiés et qui est présent dans le garde-manger de ce
	 * {@code JoueurMortel}. Si un tel {@code Aliment} n'est pas présent dans
	 * le garde-manger de ce  {@code JoueurMortel} un {@code Aliment} quelconque
	 * est renvoyé.
	 *
	 * @requires !isEmpty();
	 * @requires min >= 0;
	 * @requires min <= max;
	 * @ensures \result != null;
	 * @ensures contains(\result);
	 * @ensures (\exists Aliment al; contains(al); al.getValeurNutritive() >= min && al.getValeurNutritive() <= max)
	 * 				==> (\result.getValeurNutritive() >= min && \result.getValeurNutritive() <= max);
	 *
	 * @return un {@code Aliment} du garde-manger
	 *
	 * @throws NoSuchElementException si le garde-manger de ce {@code JoueurMortel} est vide
	 * @throws IllegalArgumentException si min < 0 ou min > max
	 *
	 * @pure
	 */
	public Aliment getAliment(int min, int max) {
		if (isEmpty()) throw new NoSuchElementException();
		if (min < 0 || min > max) throw new IllegalArgumentException();
		for (Aliment a : this) {
			if (a.getValeurNutritive() >= min && a.getValeurNutritive() <= max)
				return a;
		}
		return getAliment();
	}

	/**
	 * Renvoie un {@code Aliment} présent dans le garde-manger de ce
	 * {@code JoueurMortel}. Si le garde-manger de ce {@code JoueurMortel} n'est
	 * pas vide, renvoie l'{@code Aliment} transporté qui sera consommé par la
	 * prochaine action {@code manger} si aucun {@code Aliment} n'est ramassé
	 * par ce {@code JoueurMortel} dans l'interval.
	 *
	 * @requires !isEmpty();
	 * @ensures \result != null;
	 * @ensures contains(\result);
	 *
	 * @return l'{@code Aliment} du garde-manger de ce {@code JoueurMortel} qui
	 *         sera consommé par la prochaine action {@code manger}.
	 *
	 * @throws NoSuchElementException si le garde-manger de ce {@code JoueurMortel} est vide
	 *
	 * @pure
	 */
	public Aliment getAliment() {
		if (isEmpty()) throw new NoSuchElementException();
		return iterator().next();
	}

	// Actions:
	/**
	 * Déplace ce {@code JoueurMortel} dans la direction spécifié. La pièce
	 * courante doit posséder une sortie dans la direction spécifiée. Remplace
	 * la pièce courante de ce {@code JoueurMortel} par la sortie de cette pièce
	 * située dans la direction spécifiée. Le déplacement fait diminuer le
	 * capital vie de ce {@code JoueurMortel} d'un nombre de points de vie égal
	 * à 10 + le nombre d'{@code Aliment} transportés par ce
	 * {@code JoueurMortel} dans son garde-manger.
	 *
	 * @requires estVivant();
	 * @requires direction != null;
	 * @requires getPieceCourante().pieceSuivante(direction) != null;
	 * @ensures getPieceCourante() ==
	 *          \old(getPieceCourante().pieceSuivante(direction));
	 * @ensures getCapitalVie() == \old(getCapitalVie()) - (10 + size());
	 *
	 * @param direction
	 *            la direction dans laquelle ce {@code JoueurMortel} doit se
	 *            déplacer.
	 *
	 * @throws IllegalStateException si ce JoueurMortel est mort
	 * @throws NullPointerException si la direction spécifiée est null
	 * @throws IllegalArgumentException s'il n'y a pas de sortie dans la direction spécifiée
	 */
	public void deplacer(Direction direction) {
		if (!estVivant()) throw new IllegalStateException();
		if (direction == null) throw new NullPointerException();
		Piece p = pieceCourante.pieceSuivante(direction);
		if (p == null) throw new IllegalArgumentException();
		capitalVie -= 10 + size();
		pieceCourante = p;
	}

	/**
	 * Retire l'aliment spécifié de la pièce courante de ce {@code JoueurMortel}
	 * et l'ajoute au garde-manger de ce {@code JoueurMortel}. Diminue le
	 * capital vie de ce {@code JoueurMortel} de 2 points de vie.
	 *
	 * @requires estVivant();
	 * @requires getPieceCourante().contains(unAliment);
	 * @ensures getPieceCourante().size() == (\old(getPieceCourante().size()) - 1);
	 * @ensures size() == (\old(size()) + 1);
	 * @ensures contains(unAliment);
	 * @ensures getCapitalVie() == (\old(getCapitalVie()) - 2);
	 * @ensures getPieceCourante().frequency(unAliment) == \old(getPieceCourante().frequency(unAliment) - 1);
	 * @ensures frequency(unAliment) == \old(frequency(unAliment) + 1);
	 *
	 * @param unAliment
	 *            l'{@code Aliment} à ramasser par ce {@code JoueurMortel}.
	 *
	 * @throws IllegalStateException si ce JoueurMortel est mort
	 * @throws NullPointerException si l'Aliment spécifié est null
	 * @throws IllegalArgumentException si l'Aliment spécifié n'est pas présent dans la pièce courante
	 */
	public void ramasser(Aliment unAliment) {
		if (!estVivant()) throw new IllegalStateException();
		if (unAliment == null) throw new NullPointerException();
		if (!pieceCourante.remove(unAliment)) throw new IllegalArgumentException();
		add(unAliment);
		capitalVie -= 2;
	}

	/**
	 * Ajoute la valeur nutritive d'un {@code Aliment} du garde-manger au
	 * capital vie de ce {@code JoueurMortel}. Retire du garde-manger de ce
	 * {@code JoueurMortel} un exemplaire de l'{@code Aliment getAliment()} et
	 * ajoute la valeur nutritive de cet {@code Aliment} au capital vie de ce
	 * {@code JoueurMortel} dans la limite du {@code JoueurMortel.MAX_CAPITAL_VIE}.
	 *
	 * @requires estVivant();
	 * @requires !gardeMangerEstVide();
	 * @ensures getCapitalVie() == Math.min(\old(getCapitalVie() +
	 *          getAliment().getValeurNutritive())),
	 *          JoueurMortel.MAX_CAPITAL_VIE);
	 * @ensures frequency(\old(getAliment())) == \old(frequency(getAliment()) - 1);
	 * @ensures size() == \old(size()) - 1;
	 *
	 * @throws IllegalStateException si ce JoueurMortel est mort
	 * @throws NoSuchElementException si le garde-manger de ce {@code JoueurMortel} est vide
	 */
	public void manger() {
		if (!estVivant()) throw new IllegalStateException();
		if (isEmpty()) throw new NoSuchElementException();
		Aliment a = getAliment();
		remove(a);
		capitalVie = Math.min(capitalVie + a.getValeurNutritive(), MAX_CAPITAL_VIE);
	}
}
