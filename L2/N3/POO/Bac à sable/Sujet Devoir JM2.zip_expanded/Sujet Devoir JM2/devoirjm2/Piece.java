/*
 * Lyes Saadi 12107246
 * Ceci est mon travail personnel.
 */

package devoirjm2;

import java.util.Collection;

/**
 * Une piece dans un jeu d'aventure.
 * <p>
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * </p>
 * <p>
 * Une "Piece" represente un des lieux dans lesquels se déroule l'action du jeu.
 * Elle est reliée a au plus quatre autres "Piece" par des sorties. Les sorties
 * sont étiquettées "nord", "est", "sud", "ouest". Pour chaque direction, la
 * "Piece" possède une référence sur la piece voisine ou null s'il n'y a pas de
 * sortie dans cette direction.
 * </p>
 * <p>
 *
 * Une piece peut aussi contenir des objets représentés par des instances de la
 * classe Aliment. Durant le déroulement du jeu des objets peuvent etre ajoutés
 * ou supprimés de la piece.
 * </p>
 *
 * @invariant descriptionCourte() != null;
 * @invariant descriptionSorties() != null
 * @invariant descriptionLongue() != null;
 * @invariant descriptionLongue().contains(descriptionCourte());
 * @invariant descriptionLongue().contains(descriptionSorties());
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @version 1.1
 * @since August 2000
 */
public class Piece  extends LinkedListCollection<Aliment> {
	private String description;
	private Piece nord;
	private Piece est;
	private Piece sud;
	private Piece ouest;

	/**
	 * Initialise une piece décrite par la chaine de caractères spécifiée.
	 * Initialement, cette piece ne possède aucune sortie. La description
	 * fournie est une courte phrase comme "la bibliothèque" ou
	 * "la salle de TP". La piece initialisée ne contient aucun objets.
	 *
	 * @requires description != null;
	 * @ensures descriptionCourte().equals(description);
	 * @ensures pieceSuivante(Direction.NORD) == null;
	 * @ensures pieceSuivante(Direction.SUD) == null;
	 * @ensures pieceSuivante(Direction.EST) == null;
	 * @ensures pieceSuivante(Direction.OUEST) == null;
	 * @ensures isEmpty();
	 *
	 * @param description
	 *            Description de la piece.
	 *
	 * @throws NullPointerException si la description spécifiée est null
	 */
	public Piece(String description) {
		super();
		if (description == null) throw new NullPointerException();
		this.description = description;
		this.nord = null;
		this.est = null;
		this.sud = null;
		this.ouest = null;
	}

	/**
	 * Initialise une piece décrite par la chaine de caractères spécifiée et
	 * contenant les objets de la Collection spécifiée.
	 *
	 * @requires description != null;
	 * @requires c != null;
	 * @requires !contains(null);
	 * @ensures descriptionCourte().equals(description);
	 * @ensures pieceSuivante(Direction.NORD) == null;
	 * @ensures pieceSuivante(Direction.SUD) == null;
	 * @ensures pieceSuivante(Direction.EST) == null;
	 * @ensures pieceSuivante(Direction.OUEST) == null;
	 * @ensures size() == c.size();
	 * @ensures contentEquals(c);
	 *
	 * @param description
	 *            Description de la piece
	 * @param c
	 *            Objets à placer dans la piece.
	 *
	 * @throws NullPointerException si la description ou la Collection spécifiée est null,
	 * 			ou bien si la Collection spécifiée contient null
	 */
	public Piece(String description, Collection<? extends Aliment> c) {
		this(description);
		if (c == null) throw new NullPointerException();
		addAll(c);
	}

	/**
	 * Initialise une piece décrite par la chaine de caractères spécifiée et
	 * contenant les nbObjets premiers objets du tableau spécifié.
	 *
	 * @requires description != null;
	 * @requires tabAliments != null;
	 * @requires nbAliments >= 0;
	 * @requires nbAliments <= tabAliments.length;
	 * @requires (\forall int i; i >= 0 && i < nbAliments; tabAliments[i] != null);
	 * @ensures descriptionCourte().equals(description);
	 * @ensures pieceSuivante(Direction.NORD) == null;
	 * @ensures pieceSuivante(Direction.SUD) == null;
	 * @ensures pieceSuivante(Direction.EST) == null;
	 * @ensures pieceSuivante(Direction.OUEST) == null;
	 * @ensures size() == nbAliments;
	 * @ensures (\forall int i; i >= 0 && i < nbAliments;
	 * 				frequency(tabAliments[i])
	 *				== (\num_of int j; j >= 0 && j < nbAliments;
	 *						tabAliments[j].equals(tabAliments[i])));
	 *
	 * @param description
	 *            Description de la piece
	 * @param tabAliments
	 *            tableau contenant les Aliment à placer dans la piece.
	 * @param nbAliments
	 *            nombre d'Aliment du tableau à intégrer à cette pièce
	 *
	 * @throws NullPointerException si la description ou le tableau spécifié est null, ou bien si un des nbAliments premiers éléments du
	 *								tableau spécifié est null
	 * @throws IllegalArgumentException si l'entier spécifié est strictement négatif ou strictement supérieur à la taille du tableau
	 */
	public Piece(String description, Aliment[] tabAliments, int nbAliments) {
		this(description);
		if (tabAliments == null) throw new NullPointerException();
		if (nbAliments < 0 || nbAliments > tabAliments.length) throw new IllegalArgumentException();
		for (int i = 0; i < nbAliments; i++) {
			if (tabAliments[i] == null)
				throw new NullPointerException();
			add(tabAliments[i]);
		}
	}

	/**
	 * Définie les sorties de cette piece. A chaque direction correspond ou bien
	 * une piece ou bien la valeur null signifiant qu'il n'y a pas de sortie
	 * dans cette direction.
	 *
	 * @ensures pieceSuivante(Direction.NORD) == nord;
	 * @ensures pieceSuivante(Direction.SUD) == sud;
	 * @ensures pieceSuivante(Direction.EST) == est;
	 * @ensures pieceSuivante(Direction.OUEST) == ouest;
	 *
	 * @param nord
	 *            La sortie nord
	 * @param est
	 *            La sortie est
	 * @param sud
	 *            La sortie sud
	 * @param ouest
	 *            La sortie ouest
	 */
	public void setSorties(Piece nord, Piece est, Piece sud, Piece ouest) {
		this.nord = nord;
		this.est = est;
		this.sud = sud;
		this.ouest = ouest;
	}

	/**
	 * Renvoie la description de cette piece (i.e. la description spécifiée lors
	 * de la création de cette instance).
	 *
	 * @ensures \result != null;
	 *
	 * @return Description de cette piece
	 *
	 * @pure
	 */
	public String descriptionCourte() {
		return description;
	}

	/**
	 * Renvoie une description de cette piece mentionant ses sorties et
	 * directement formatée pour affichage, de la forme:
	 *
	 * <pre>
	 *  Vous etes dans la bibliothèque.
	 *  Sorties: nord ouest
	 * </pre>
	 *
	 * Cette description contient les chaines de caractères renvoyées par les méthodes
	 * descriptionCourte et descriptionSorties pour décrire les sorties de cette piece.
	 *
	 * @ensures \result != null;
	 * @ensures \result.indexOf(descriptionCourte()) >= 0;
	 * @ensures \result.indexOf(descriptionSorties()) >= 0;
	 *
	 * @return Description affichable de cette piece
	 *
	 * @pure
	 */
	public String descriptionLongue() {
		return descriptionCourte() + "\n" + descriptionSorties();
	}

	/**
	 * Renvoie une description des sorties de cette piece, de la forme:
	 *
	 * <pre>
	 *  Sorties: nord ouest
	 * </pre>
	 *
	 * Cette description est utilisée dans la description longue d'une piece.
	 *
	 * @ensures \result != null;
	 * @ensures (pieceSuivante(Direction.NORD) != null) ==> \result.contains("NORD");
	 * @ensures (pieceSuivante(Direction.SUD) != null) ==> \result.contains("SUD");
	 * @ensures (pieceSuivante(Direction..EST) != null) ==> \result.contains("EST");
	 * @ensures (pieceSuivante(Direction.OUEST) != null) ==> \result.contains("OUEST");
	 *
	 * @return Une description des sorties de cette pièce.
	 *
	 * @pure
	 */
	public String descriptionSorties() {
		return "Sorties :"
				+ (nord != null ? " NORD" : "")
				+ (est != null ? " EST" : "")
				+ (sud != null ? " SUD" : "")
				+ (ouest != null ? " OUEST" : "");
	}

	/**
	 * Renvoie la piece atteinte lorsque l'on se déplace à partir de cette piece
	 * dans la direction spécifiée. Si cette piece ne possède aucune sortie dans
	 * cette direction, renvoie {@code null}.
	 *
	 * @requires direction != null;
	 *
	 * @param direction
	 *            La direction dans laquelle on souhaite se déplacer.
	 * @return La piece atteinte lorsque l'on se déplace dans la direction
	 *         spécifiée ou {@code null}.
	 *
	 * @throws NullPointerException si la Direction spécifiée est null
	 *
	 * @pure
	 */
	public Piece pieceSuivante(Direction d) {
		if (d == null) throw new NullPointerException();
		switch (d) {
			case NORD:
				return nord;
			case EST:
				return est;
			case SUD:
				return sud;
			case OUEST:
				return ouest;
			default:
				return null;
		}
	}
}
