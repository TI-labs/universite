/*
 * Lyes Saadi 12107246
 * Ceci est mon travail personnel.
 */

package devoirjm2;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Implémentation de l'interface Collection utilisant une LinkedList pour
 * mémoriser les éléments de la collection. Cette implémentation permet de :
 * représenter des collections modifiables ne pouvant pas contenir la valeur
 * null. Ajout de quelques méthodes permettant de gérer les occurences multiples
 * des éléments appartenant à une Collection.
 *
 * @param <E> type des éléments de cette Collection
 *
 * @invariant !contains(null);
 * @invariant (\forall E obj; contains(obj); frequency(obj) <= size());
 *
 * @author Marc Champesme
 * @since 24/11/2022
 * @version 24/11/2022
 */
public class LinkedListCollection<E> extends AbstractCollection<E> {
	private LinkedListCollection<E> next;
	private E value;

    /**
     * Initialise une Collection vide.
     *
     * @ensures isEmpty();
     */
    public LinkedListCollection() {
    	super();
    	next = null;
    	value = null;
    }

    /**
     * Initialise une Collection contenant les éléments de la Collection spécifiée.
     *
     * @param c la Collection dont les éléments  doivent être placés dans la liste
     *
     * @requires c != null;
     * @requires !c.contains(null);
     * @ensures containsAll(c) && c.containsAll(this);
     * @ensures size() == c.size();
     *
     * @throws NullPointerException si la Collection spécifiée est null ou contient null
     */
    public LinkedListCollection(Collection<? extends E> c) {
    	this();
    	if (c == null || c.contains(null)) throw new NullPointerException();
    	addAll(c);
    }


    @Override
    public boolean add(E e) {
    	if (e == null) throw new NullPointerException();
    	if (next != null) return next.add(e);
    	
    	next = new LinkedListCollection<E>();
    	this.value = e;
    	return true;
    }

    @Override
    public Iterator<E> iterator() {
    	return new LinkedListIterator<E>(this);
    }

    @Override
    public boolean remove(Object o) {
    	if (next == null && value == null) return false;
    	if (value.equals(o)) {
    		value = next.value;
    		next = next.next;
    		return true;
    	}
    	return next.remove(o);
    }

    @Override
    public int size() {
    	if (next == null) return 0;
    	return next.size() + 1;
    }

    /**
	 * Renvoie le nombre d'occurences de l'objet spécifié dans cette Collection.
	 *
     * @ensures \result >= 0 && \result <= size();
	 * @ensures \result > 0 <==> contains(o);
	 *
	 * @param o L'objet cherché.
     *
	 * @return Le nombre d'occurences de l'objet spécifié dans cette piece
	 *
	 * @pure
	 */
    public int frequency(Object o) {
    	int i = 0;
    	for (E e : this) {
    		if (e.equals(o)) {
    			i++;
    		}
    	}
    	return i;
    }

    /**
     * Renvoie un ensemble contenant tous les éléments de cette Collection qui
     * sont présents en plusieurs exemplaires.
     *
     * @return un ensemble contenant tous les éléments dupliqués de cette Collection
     *
     * @ensures \result != null;
     * @ensures \result.size() <= size() / 2;
     * @ensures (\forall E elt; \result.contains(elt); frequency(elt) > 1);
     * @ensures (\forall E elt; frequency(elt) > 1; \result.contains(elt));
     *
     * @pure
     */
    public Set<E> selectDuplicates() {
    	Set<E> s = new HashSet<E>();
    	for (E e : this) {
    		if (frequency(e) > 1) {
    			s.add(e);
    		}
    	}
    	return s;
    }

    /**
     * Renvoie un ensemble contenant tous les éléments de cette Collection qui
     * sont présents en un seul exemplaire.
     *
     * @return un ensemble contenant tous les éléments uniques dans cette Collection
     *
     * @ensures \result != null;
     * @ensures \result.size() <= size();
     * @ensures (\forall E elt; \result.contains(elt); frequency(elt) == 1);
     * @ensures (\forall E elt; frequency(elt) == 1; \result.contains(elt));
     *
     * @pure
     */
    public Set<E> selectUniques() {
    	Set<E> s = new HashSet<E>();
    	for (E e : this) {
    		if (frequency(e) == 1) {
    			s.add(e);
    		}
    	}
    	return s;
    }

    /**
     * Retire de cette Collection toutes les occurences de l'objet spécifié.
     *
     * @return le nombre d'éléments retirés de cette Collection
     *
     * @ensures \result == \old(frequency(e));
     * @ensures !contains(e);
     * @ensures size() == \old(size() - frequency(e));
     */
    public int removeAll(E e) {
    	int i = 0;
    	while (remove(e)) i++;
    	return i;
    }

    /**
	 * Ajoute à cette collection le nombre d'occurences spécifié de l'objet spécifié.
	 *
	 * @requires e != null;
	 * @requires n >= 0;
	 * @ensures n > 0 ==> contains(e);
	 * @ensures size() == \old(size()) + n;
	 * @ensures frequency(e) == \old(frequency(e)) + n;
     *
	 *
	 * @param e  L'objet à ajouter
	 * @param n Le nombre d'occurences de l'objet à ajouter
     *
     * @throws IllegalArgumentException si l'entier spécifié est strictement négatif
     * @throws NullPointerException si l'objet spécifié est null
	 */
    public void addNCopiesOf(int n, E e) {
    	if (n < 0) throw new IllegalArgumentException();
    	if (e == null) throw new NullPointerException();
    	for (int i = 0; i < n; i++) {
    		add(e);
    	}
    }


    /**
     * Renvoie true si la Collection spécifiée contient les même éléments que cette Collection.
     *
     * @param c Collection à comparer avec cette Collection
     *
     * @return true si la Collection spécifiée contient les mêmes éléments que cette Collection; false sinon
     *
     * @throws NullPointerException si la Collection spécifiée est null
     *
     * @requires c != null;
     * @ensures \result ==> (size() == c.size() && containsAll(c) && c.containsAll(this));
     * @ensures isEmpty() && c.isEmpty() ==> \result;
     *
     * @pure
     */
    public boolean contentEquals(Collection<?> c) {
    	if (c == null) throw new NullPointerException();

    	LinkedListCollection<E> copy = new LinkedListCollection<E>(this);

    	for (Object o : c) {
    		if (!copy.contains(o))
    			return false;
    		copy.remove(o);
    	}
    	
    	if (copy.isEmpty())
			return true;
    	else
    		return false;
    }

    // Iterator
    public class LinkedListIterator<F> implements Iterator<F> {
    	private LinkedListCollection<F> cursor;
    	private LinkedListCollection<F> parent;

    	public LinkedListIterator(LinkedListCollection<F> l) {
    		cursor = l;
    		parent = null;
    	}

		@Override
		public boolean hasNext() {
			if (cursor.value == null || cursor == null) return false;
			return true;
		}

		@Override
		public F next() {
			if (!hasNext()) throw new NoSuchElementException();
			parent = cursor;
			F f = cursor.value;
			cursor = cursor.next;
			return f;
		}

		@Override
		public void remove() {
			if (parent == null) throw new IllegalStateException();
			parent.value = cursor.value;
			parent.next = cursor.next;
			cursor = parent;
			parent = null;
		}
    }
}
