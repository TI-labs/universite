package devoirjm2.test;

import java.util.stream.Stream;
import java.util.List;
import java.util.Arrays;
import java.util.Random;
import java.util.Collections;
import java.util.HashSet;
import java.util.Collection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.params.provider.Arguments;

import devoirjm2.Aliment;
import devoirjm2.Direction;
import devoirjm2.JoueurMortel;
import devoirjm2.LinkedListCollection;
import devoirjm2.Piece;

/**
 * Regroupement de méthodes static permettant de générer des données aléatoires
 * pour des tests JUnit.
 *
 * @author Marc Champesme
 * @version 4 décembre 2022
 *
 */
public class DataProvider {
    public static int LG_STREAM = 200;  // Nombre d'éléments par Stream
                                        // correspond au nombre de test par
                                        // méthode de test.
    public static int PIECE_NB = 30;	// Nombre de pièces créées, doit être > 6
    public static int MAX_COLL_SIZE = 20;
    public static List<String> alimentNames = Arrays.asList("carotte", "pomme de terre",
                                            "fromage", "jus de fruit", "haricots",
                                            "oranges", "pommes", "poires", "burger",
                                            "couscous", "adobo", "pho", "cassoulet",
                                            "confit de canard", "chou", "Pomme de terre",
                                            "frites", "poireau", "salade", "tomate",
                                            "burger", "sandwich", "entrecôte", "steak",
                                            "poisson", "pain", "brioche", "calamar",
                                            "biscotte", "tarte", "quiche", "abricot");
    public static List<String> pieceNames = Arrays.asList("cuisine", "salle TD", "Amphi A",
                                            "Amphi B", "Amphi Becquerel", "Amphi Ampère",
                                            "Amphi Copernic", "Amphi Darwin", "Amphi Euler",
                                            "F 201", "F 202", "Salle TP", "F 203", "F 204",
                                            "F 205", "F 206", "F 207", "F 208", "Amphi C", "Amphi D",
                                            "D 312", "B 303", "B 305", "B 307", "B 308", "B 309",
                                            "C 301", "C 302", "C 303", "C 304", "C 305", "C 306",
                                            "C 307", "C 308", "C 309", "C 310", "C 311", "C 312",
                                            "G 201", "G 202", "G 203", "G 204", "G 205", "G 206",
                                            "Taverne", "BDE", "Cafetéria", "Bibliothèque");

    public static List<String> joueurNames = Arrays.asList("Marcel", "Adam", "Sonia",
                                            "Idir", "Mohamed", "Marc", "Ali", "Ziad",
                                            "Lyes", "Ayman", "Mounir", "Pierre", "Chanez",
                                            "Lamia", "Yanis", "Faycal", "Boris", "Imam",
                                            "Naila", "Zahra", "Rosa", "Lisa", "Sheraz",
                                            "Nima", "Aliou", "Issa", "Mamadou", "Ismael");

    public static List<Piece> pieceInstances = new ArrayList<Piece>(PIECE_NB);

    // Providers des classes TestCollectionContract et TestLinkedListCollection

    public static Stream<Collection<Aliment>> collectionProvider() {
    	Collection<Aliment> collWithNull = new LinkedList<Aliment>(newCollAliment());
    	collWithNull.add(null);
    	collWithNull.addAll(newCollAliment());
    	Stream<Collection<Aliment>> casEx = Stream.of(Collections.emptyList(), collWithNull);
        return Stream.concat(casEx, Stream.generate(() -> newCollAliment())).limit(LG_STREAM);
    }

    public static Stream<LinkedListCollection<Aliment>> LinkedListCollProvider() {
        return Stream.generate(() -> newLinkedListCollection()).limit(LG_STREAM);
    }

	public static Stream<Arguments> instanceAndEltProvider() {
		LinkedListCollection<Aliment> emptyColl = new LinkedListCollection<Aliment>();
		Stream<Arguments> casEx = Stream.of(Arguments.of(emptyColl, newAliment()),
				Arguments.of(newLinkedListCollection(), newAliment()),
				Arguments.of(newLinkedListCollection(), null));
		return Stream.concat(casEx, LinkedListCollProvider().map(c -> Arguments.of(c, c.isEmpty() ? newAliment() : getRandomElt(c))));
	}

	public static Stream<Arguments> instanceAndIntAndEltProvider() {
		return LinkedListCollProvider().map(c -> Arguments.of(c, randInt(-2, 5), c.isEmpty() ? null : getRandomElt(c)));
	}

	public static Stream<Arguments> instanceAndCollProvider() {
		List<Aliment> aList = new ArrayList<Aliment>(newCollAliment());
		List<Aliment> shuffleList1 = new ArrayList<Aliment>(aList);
		Collections.shuffle(shuffleList1);
		List<Aliment> shuffleList2 = new ArrayList<Aliment>(aList);
		Collections.shuffle(shuffleList2);
    	Collection<Aliment> collWithNull = new LinkedList<Aliment>(newCollAliment());
    	collWithNull.add(null);
    	collWithNull.addAll(newCollAliment());
    	LinkedListCollection<Aliment> uneColl = newLinkedListCollection();
		Stream<Arguments> casEx = Stream.of(Arguments.of(new LinkedListCollection<Aliment>(aList), new LinkedListCollection<Aliment>(shuffleList1)),
				Arguments.of(newLinkedListCollection(), null),
				Arguments.of(newLinkedListCollection(), collWithNull),
				Arguments.of(uneColl, uneColl),
				Arguments.of(new LinkedListCollection<Aliment>(aList), new LinkedListCollection<Aliment>(shuffleList2)));
		return Stream.concat(casEx, LinkedListCollProvider().map(c -> Arguments.of(c, getRandomColl(c))));
	}

	public static Stream<Arguments> instanceAndTabProvider() {
		return LinkedListCollProvider().map(l -> Arguments.of(l, new Aliment[randInt(5)]));
	}

	// Providers de la classe JoueurMortel

    public static Stream<JoueurMortel> joueurProvider() {
    	JoueurMortel deadPlayer = killPlayer(newJoueurMortel());
    	JoueurMortel emptyPlayer = newJoueurMortel();
    	emptyPlayer.clear();
    	Stream<JoueurMortel> casEx = Stream.of(deadPlayer, emptyPlayer);
        return Stream.concat(casEx, Stream.generate(() -> newJoueurMortel())).limit(LG_STREAM);
    }

    /**
     * Arguments pour le 1er constructeur de JoueurMortel.
     */
	public static Stream<Arguments> nameAndPieceProvider() {
		Stream<Arguments> casEx = Stream.of(Arguments.of(null, newPiece()), Arguments.of("", newPiece()),
				Arguments.of(null, null), Arguments.of(getRandomElt(joueurNames), null));
		return Stream.concat(casEx, Stream.generate(() -> newPiece()).map(p -> Arguments.of(getRandomElt(joueurNames), p))).limit(LG_STREAM);
	}

    /**
     * Arguments pour le 2ème constructeur de JoueurMortel (tableau).
     */
    public static Stream<Arguments> nameAndPieceAndTabProvider() {
    	Aliment[] unTab = newTabAliment();
    	Aliment[] tabWithNull = new Aliment[5];
    	tabWithNull[0] = newAliment();
    	tabWithNull[2] = newAliment();
    	Aliment[] emptyTab = new Aliment[0];
		Stream<Arguments> casEx = Stream.of(Arguments.of(null, newPiece(), newTabAliment(), -2), 
				Arguments.of(getRandomElt(joueurNames), newPiece(), null, 0),
				Arguments.of(getRandomElt(joueurNames), newPiece(), unTab, unTab.length + 5),
				Arguments.of(getRandomElt(joueurNames), newPiece(), tabWithNull, 3),
				Arguments.of(getRandomElt(joueurNames), newPiece(), emptyTab, 0),
				Arguments.of(getRandomElt(joueurNames), newPiece(), newTabAliment(), 0),
				Arguments.of(null, null, null, 12), 
				Arguments.of(getRandomElt(joueurNames), null, unTab, 0));
        return Stream.concat(casEx, Stream.generate(() -> newTabAliment())
            .map(tab -> Arguments.of(getRandomElt(joueurNames), newPiece(), tab, tab.length)))
            .limit(LG_STREAM);
    }

   /**
    * Arguments pour le 3ème constructeur de JoueurMortel (Collection).
    */
   public static Stream<Arguments> nameAndPieceAndCollProvider() {
	   LinkedList<Aliment> collWithNull = new LinkedList<Aliment>(newCollAliment());
	   collWithNull.add(null);
	   collWithNull.addAll(newCollAliment());
		Stream<Arguments> casEx = Stream.of(Arguments.of(null, newPiece(), newCollAliment()), 
				Arguments.of(getRandomElt(joueurNames), null, newCollAliment()),
				Arguments.of(getRandomElt(joueurNames), newPiece(), collWithNull),
				Arguments.of(getRandomElt(joueurNames), newPiece(), null));
		return Stream.concat(casEx, Stream.generate(() -> newCollAliment())
           .map(c -> Arguments.of(getRandomElt(joueurNames), newPiece(), c)))
           .limit(LG_STREAM);
   }

   /**
    * Arguments pour la méthode getAliment(int min, int max)
    */
   public static Stream<Arguments> joueurAndIntIntProvider() {
       return Stream.generate(() -> newJoueurMortel())
               .map(j -> Arguments.of(j, randInt(10) - 2, randInt(JoueurMortel.MAX_CAPITAL_VIE) + 5))
               .limit(LG_STREAM);

   }

    /**
     * Data provider for method contains.
     */
    public static Stream<Arguments> joueurAndAlimentProvider() {
        return Stream.generate(() -> newJoueurMortel())
            .map(j -> Arguments.of(j,
                    randBool() ? getRandomElt(j)
                                : newAliment()))
            .limit(LG_STREAM);
    }

    /**
     * Data provider for method ramasser.
     */
    public static Stream<Arguments> joueurAndPAlimentProvider() {
        for (Piece p : pieceInstances) {
            p.addNCopiesOf(10, newAliment());
        }
        JoueurMortel deadPlayer = killPlayer(newJoueurMortel());
        JoueurMortel playerWithEmptyPiece = newJoueurMortel();
        playerWithEmptyPiece.getPieceCourante().clear();

    	Stream<Arguments> casEx = Stream.of(Arguments.of(newJoueurMortel(), null),
    			Arguments.of(deadPlayer, getRandomElt(deadPlayer.getPieceCourante())),
    			Arguments.of(playerWithEmptyPiece, newAliment()));
        return Stream.concat(casEx, Stream.generate(() -> newJoueurMortel())
            .filter(j -> j.getPieceCourante().size() > 0)
            .map(j -> Arguments.of(j,
                    getRandomElt(j.getPieceCourante()))))
            .limit(LG_STREAM);
    }

    /**
     * Data provider for method deplacer.
     */
    public static Stream<Arguments> joueurAndDirectionProvider() {
        JoueurMortel deadPlayer = killPlayer(newJoueurMortel());
    	Stream<Arguments> casEx = Stream.of(Arguments.of(newJoueurMortel(), null),
    			Arguments.of(deadPlayer, choixSortie(deadPlayer.getPieceCourante())),
    			Arguments.of(deadPlayer, null));

        return Stream.concat(casEx, Stream.generate(() -> Arguments.of(newJoueurMortel(), getRandomElt(Direction.values()))))
                .limit(LG_STREAM);

    }

    // Providers pour la classe Piece

    public static Stream<Piece> pieceProvider() {
        return Stream.generate(() -> newPiece()).limit(LG_STREAM);
    }

    /**
     * Arguments pour le 1er constructeur de Piece.
     */
    public static Stream<String> namePieceProvider() {
    	Stream<String> casEx = Stream.of(null, "", "c");
        return Stream.concat(casEx, Stream.generate(() -> getRandomElt(pieceNames))).limit(LG_STREAM);
    }

    /**
     * Arguments pour le 2ème constructeur de JoueurMortel (tableau).
     */
    public static Stream<Arguments> nameAndTabProvider() {
    	Aliment[] unTab = newTabAliment();
    	Aliment[] tabWithNull = new Aliment[5];
    	tabWithNull[0] = newAliment();
    	tabWithNull[2] = newAliment();
    	Aliment[] emptyTab = new Aliment[0];
    	Stream<Arguments> casEx = Stream.of(Arguments.of(null, newTabAliment(), randInt(5)),
    			Arguments.of("", newTabAliment(), randInt(5)),
    			Arguments.of(getRandomElt(pieceNames), newTabAliment(), -5),
    			Arguments.of(getRandomElt(pieceNames), unTab, unTab.length + 5),
    			Arguments.of(getRandomElt(pieceNames), tabWithNull, unTab.length),
    			Arguments.of(getRandomElt(pieceNames), emptyTab, 0),
    			Arguments.of(getRandomElt(pieceNames), null, randInt(5)));
        return Stream.concat(casEx, Stream.generate(() -> newTabAliment())
            .map(tab -> Arguments.of(getRandomElt(pieceNames), tab, randInt(-2, tab.length + 2))))
            .limit(LG_STREAM);
    }

   /**
    * Arguments pour le 3ème constructeur de JoueurMortel (Collection).
    */
   public static Stream<Arguments> nameAndCollProvider() {
	   LinkedList<Aliment> collWithNull = new LinkedList<Aliment>(newCollAliment());
	   collWithNull.add(null);
	   collWithNull.addAll(newCollAliment());
	   Stream<Arguments> casEx = Stream.of(Arguments.of(null, newCollAliment()),
			   Arguments.of(getRandomElt(pieceNames), null),
			   Arguments.of(getRandomElt(pieceNames), collWithNull));
       return Stream.concat(casEx, Stream.generate(() -> Arguments.of(getRandomElt(pieceNames), newCollAliment())))
           .limit(LG_STREAM);
   }

    /**
     * Data provider for method pieceSuivante.
     */
    public static Stream<Arguments> pieceAndDirectionProvider() {
 	   Stream<Arguments> casEx = Stream.of(Arguments.of(newPiece(), null));
        return Stream.concat(casEx, Stream.generate(() -> Arguments.of(newPiece(), getRandomElt(Direction.values()))))
                .limit(LG_STREAM);

    }

    /**
     * Data provider for method setSorties.
     */
    public static Stream<Arguments> fivePieceProvider() {
        return Stream.generate(() -> Arguments.of(newPiece(), newPiece(), newPiece(), newPiece(), newPiece()))
                .limit(LG_STREAM);

    }



    // FIN SECTION 2

    // ######################################
    // NE PAS MODIFIER LES LIGNES SUIVANTES !
    // ######################################
    private static Random randGen = new Random();


    public static Aliment newAliment() {
        return new Aliment(getRandomElt(alimentNames), randInt(1, 50));
    }

    public static Aliment[] newTabAliment() {
        int length = randInt(5, 10);
        Aliment[] tab = new Aliment[length];
        for (int i = 0; i < length; i++) {
            tab[i] = newAliment();
        }
        return tab;
    }

    public static Collection<Aliment> newCollAliment() {
    	int size = randInt(MAX_COLL_SIZE);
    	Collection<Aliment> resCol = null;
    	switch (randInt(10)) {
    		case 0: // LinkedList
    			resCol = new LinkedList<Aliment>();
    			break;
    		case 1: // ArrayList
    			resCol = new ArrayList<Aliment>(size);
    			break;
    		case 2: // HashSet
    			resCol = new HashSet<Aliment>();
    			break;
    		case 3: // JoueurMortel
    			resCol = new JoueurMortel("Testeur", newPiece());
    			break;
    		case 4: // Piece
    			resCol = new Piece("Piece de test");
    			break;
    		default: //LinkedListCollection
    			resCol = new LinkedListCollection<Aliment>();
    	}
        for (int i = 0; i < size; i++) {
            resCol.add(newAliment());
        }
    	return resCol;
    }

    public static Piece newIsolatedPiece() {
        Aliment[] tabAl = newTabAliment();
        return new Piece(getRandomElt(pieceNames), tabAl, tabAl.length);
    }

    public static void initPieceList() {
        if (!pieceInstances.isEmpty()) {
            return;
        }
        int nbCol = 5;
        int nbLignes = (PIECE_NB / nbCol) + 1;
        Piece[][] cartePieces = new Piece[nbLignes][nbCol];
        for (int i = 0, l = 0, c = 0; i < PIECE_NB; i++, c++) {
            if (c == nbCol) {
            	l++;
            	c = 0;
            }
        	Piece p = newIsolatedPiece();
            pieceInstances.add(p);
            cartePieces[l][c] = p;
        }
        for (int l = 0; l  < nbLignes; l++) {
        	for (int c = 0; c < nbCol; c++) {
        		Piece p = cartePieces[l][c];
        		if (p == null) {
        			break;
        		}
                Piece piece1 = null, piece2 = null, piece3 = null, piece4 = null;
                if ((l - 1) >= 0) {
                	piece1 = cartePieces[l - 1][c];
                }
                if ((c + 1) < nbCol) {
                	piece2 = cartePieces[l][c + 1];
                }
                if ((l + 1) < nbLignes) {
                	piece3 =  cartePieces[l + 1][c];
                }
                if ((c - 1) >= 0) {
                	piece4 = cartePieces[l][c - 1];
                }
                p.setSorties(piece1, piece2, piece3, piece4);
        	}
        }
    }

    public static Piece newPiece() {
        initPieceList();
        return getRandomElt(pieceInstances);
    }

    public static JoueurMortel newJoueurMortel() {
        Aliment[] tabAl = newTabAliment();
        JoueurMortel j = new JoueurMortel(getRandomElt(joueurNames), newPiece(), tabAl, tabAl.length);
        j.deplacer(choixSortie(j.getPieceCourante()));
        return j;
    }
    
    public static JoueurMortel killPlayer(JoueurMortel player) {
        while (player.estVivant() && (choixSortie(player.getPieceCourante()) != null)) {
        	player.deplacer(choixSortie(player.getPieceCourante()));
        }
        while (player.estVivant()) {
        	Aliment al = newAliment();
        	player.getPieceCourante().add(al);
        	player.ramasser(al);
        }
        return player;
    }

    public static LinkedListCollection<Aliment> newLinkedListCollection() {
    	Collection<Aliment> newColl = newCollAliment();
    	if (newColl instanceof LinkedListCollection<?>) {
    		return (LinkedListCollection<Aliment>) newColl;
    	}
    	return new LinkedListCollection<Aliment>(newColl);
    }

    // General purpose utilities:

	/**
	 * Choisit une direction dans laquelle la pièce spécifiée possède une sortie si
	 * une telle sortie existe.
	 * Renvoie null si (et seulement si) la pièce spécifiée ne possède aucune sortie.
	 *
	 * @param p la pièce dont on veut choisir une sortie
	 * @return une direction dans laquelle la pièce spécifiée possède une sortie
	 *
	 * @requires p != null;
	 * @ensures \result == null <==> (\forall Direction d; true; p.pieceSuivante(d)
	 *          == null);
	 * @ensures \result != null ==> p.pieceSuivante(\result) != null;
	 *
	 * @throws NullPointerException si l'argument spécifié est null
	 *
	 * @pure
	 */
	public static Direction choixSortie(Piece p) {
		for (Direction d : Direction.values()) {
			if (p.pieceSuivante(d) != null && randBool()) {
				return d;
			}
		}
		for (Direction d : Direction.values()) {
			if (p.pieceSuivante(d) != null) {
				return d;
			}
		}
		return null;
	}

	public static Collection<Aliment> getRandomColl(Collection<Aliment> c) {
		if (c == null) {
			return newCollAliment();
		}
		Collection<Aliment> res = null;
		switch (randInt(15)) {
		case 0:
			return c;
		case 1:
			return new HashSet<Aliment>(c);
		case 2:
			return new LinkedList<Aliment>(c);
		case 3:
			// Instance of the tested class:
			Optional<LinkedListCollection<Aliment>> resOpt = LinkedListCollProvider().filter(col -> col != null).findAny();
			if (resOpt.isEmpty()) {
				return null;
			} else {
				return resOpt.get();
			}
		case 4:
			res = new LinkedList<Aliment>(c);
			res.add(newAliment());
			return res;
		case 5:
			if (!c.isEmpty()) {
				res = new LinkedList<Aliment>(c);
				res.remove(getRandomElt(c));
				return res;
			}
		case 6:
			return null;
		case 7:
			JoueurMortel j = newJoueurMortel();
			j.addAll(c);
			return j;
		case 8:
			Piece p = newPiece();
			p.addAll(c);
			return p;
		default:
			return newCollAliment();
		}
	}


    /**
	 * Renvoie une sous-liste de la liste spécifiée de longueur aléatoire. La
     * liste retournée est une vue de la liste spécifiée, toute modification de
     * cette liste provoque donc une modification de la liste spécifiée.
	 *
	 * @param <T> le type des éléments de la liste
	 * @param l   la liste dont la sous-liste est extraite.
	 * @return une sous-liste de la liste spécifiée
	 *
	 * @requires l != null;
	 * @ensures \result != null;
	 * @ensures (\exists int lower, upper; lower >=0 && lower <= upper && upper <
	 *          l.size(); \result.equals(l.subList(lower, upper)));
	 * @ensures l.containsAll(\result);
	 * @ensures \result.size() <= l.size();
	 * @ensures l.isEmpty() => \result.isEmpty();
	 *
	 */
	public static <T> List<T> randomSubList(List<T> l) {
		if (l.isEmpty()) {
			return Collections.emptyList();
		}
		int upper = randGen.nextInt(l.size());
		int lower = randGen.nextInt(upper + 1);
		return l.subList(lower, upper);
	}
    public static <T> List<T> random4SubList(List<T> l) {
		int lower = randGen.nextInt(l.size() - 6);
		return l.subList(lower, l.size());
	}

    public static <T> Collection<T> removeNullFrom(Collection<T> c) {
        if (!c.contains(null)) {
            return c;
        }
        c = new LinkedList<T>(c);
        List<Object> nullList = new LinkedList<Object>();
        nullList.add(null);
        c.removeAll(nullList);
        return c;
    }
    /**
     * Renvoie un élément tiré aléatoirement parmi les éléments de la collection
     * spécifiée.
     *
     * @requires c != null;
     * @requires !c.isEmpty();
     * @ensures c.contains(\result);
     *
     * @param <T> Type des éléments de la collection spécifiée
     * @param c   collection dans laquelle est choisi l'élément retourné
     *
     * @return un élément tiré aléatoirement parmi les éléments de la collection
     *         spécifiée
     */
    public static <T> T getRandomElt(Collection<T> c) {
        int index = randInt(c.size());
        if (c instanceof List<?>) {
            return ((List<T>) c).get(index);
        }
        int i = 0;
        for (T elt : c) {
            if (i == index) {
                return elt;
            }
            i++;
        }
        throw new NoSuchElementException();
    }

    public static <T> T getRandomElt(T[] tab) {
        return getRandomElt(Arrays.asList(tab));
    }

    /**
	 * Renvoie un int obtenue par un générateur pseudo-aléatoire.
	 *
	 * @param max la valeur maximale du nombre aléatoire attendu
	 *
	 * @return un entier >= 0 et < max
	 *
	 * @throws IllegalArgumentException si max <= 0
	 *
	 * @requires max > 0;
	 * @ensures \result >= 0;
	 * @ensures \result < max;
	 */
	public static int randInt(int max) {
		return randGen.nextInt(max);
	}

	/**
	 * Renvoie un int obtenue par un générateur pseudo-aléatoire.
	 *
	 * @param min la valeur minimale du nombre aléatoire attendu
	 * @param max la valeur maximale du nombre aléatoire attendu
	 *
	 * @return un entier >= min et < max
	 *
	 * @throws IllegalArgumentException si max <= min
	 *
	 * @requires max > min;
	 * @ensures \result >= min;
	 * @ensures \result < max;
	 */
	public static int randInt(int min, int max) {
		return randInt(max - min) + min;
	}

    /**
     * Renvoie une valeur booléenne obtenue par un générateur pseudo-aléatoire.
     *
     * @return une valeur booléenne aléatoire
     */
    public static boolean randBool() {
        return randGen.nextBoolean();
    }

    /**
     * Renvoie une valeur booléenne obtenue par un générateur pseudo-aléatoire.
     * La valeur renvoyée a une probabilité d'être true similaire à la probabilité
     * que randInt(max) renvoie la valeur 0.
     *
     * @return une valeur booléenne aléatoire
     */
    public static boolean randBool(int max) {
        return randGen.nextInt(max) == 0;
    }


}
