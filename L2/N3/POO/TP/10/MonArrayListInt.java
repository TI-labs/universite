public class MonArrayListInt implements Cloneable {
    private int sz;
    private int[] vals;
    private final static int CAP_DEFAUT = 20;
    private final static int CAP_MULTIPLIER = 2;
    public MonArrayListInt() {
        sz = 0;
        vals = new int[CAP_DEFAUT];
    }
    public MonArrayListInt(int capacity) {
        sz = 0;
        vals = new int[capacity];
    }
    public int size() { return sz; }
    public boolean isEmpty() { return size() == 0; }
    public void ensureCapacity(int minCapacity) {
        int oldCap = vals.length;
        if (oldCap < minCapacity) {
            int[] old = vals;
            vals = new int[minCapacity];
            for (int i = 0; i < sz; ++i)
                vals[i] = old[i];
        }
    }
    public void add(int e) {
        if (size() == vals.length)
            ensureCapacity(CAP_MULTIPLIER * vals.length);
        vals[sz++] = e;
    }
    public void add(int index, int e) {
        if (sz == vals.length)
            ensureCapacity(CAP_MULTIPLIER * vals.length);
        for (int i = sz; i > index; --i)
            vals[i] = vals[i - 1];
        vals[index] = e;
        ++sz;
    }
    public int get(int index) { return vals[index]; }
    public int set(int index, int e) {
        int res = get(index);
        vals[index] = e;
        return res;
    }
    public int remove(int index) {
        int res = vals[index];
        for (int i = index; i < sz - 1; ++i)
            vals[i] = vals[i + 1];
        --sz;
        return res;
    }
    public String toString() {
        if (isEmpty())
            return "[]";
        String res = "[";
        for (int i = 0; i < size() - 1; ++i)
            res += get(i) + ", ";
        res += get(size() - 1) + "]";
        return res;
    }
    public boolean equals(Object o) {
        if (!(o instanceof MonArrayListInt))
            return false;
        MonArrayListInt l = (MonArrayListInt) o;
        if (l.size() != size())
            return false;
        for (int i = 0; i < size(); ++i)
            if (!(get(i) == l.get(i)))
                return false;
        return true;
    }
    public int hashCode() {
        int res = 1;
        for (int i = 0; i < size(); ++i)
            res = 31 * res + get(i);
        return res;
    }
    public Object clone() {
        try {
            MonArrayListInt res = (MonArrayListInt) super.clone();
            res.vals = vals.clone();
            return res;
        } catch (CloneNotSupportedException c) {
            /* ne devrait jamais arriver, car implements Cloneable */
            throw new InternalError();
        }
    }
}
