import java.util.Scanner;
public class EssaiTableauxJava {
    public static void main(String[] args) {
        int[] tab = new int[3];
        tab[0] = 10;
        tab[1] = 20;
        tab[2] = 30;
        /* Parcourir un tableau avec les indices. */
        int i;
        for (i = 0; i < 3; ++i)
            System.out.printf("tab[%d] = %d\n", i, tab[i]);
        /* On peut aussi déclarer l'indice de boucle dans la boucle */
        for (int j = 0; j < 3; ++j)
            System.out.printf("tab[%d] = %d\n", j, tab[j]);
        /* En java, les tableaux connaissent leurs tailles ! */
        System.out.printf("Taille du tableau : %d\n", tab.length);
        /* Les tableaux sont itérables. */
        for (int e : tab)
            System.out.println(e);
        /* Les tableaux sont (évidemment) mutables */
        System.out.println(tab);
        tab[2] = 40;
        for (int e : tab)
            System.out.println(e);
        /* Références multiples : attention ! */
        System.out.println(tab); /* affiche "l'adresse" du tableau */
        int[] tab2 = tab;
        tab2[2] = 120;
        for (int e : tab)
            System.out.println(e);
        System.out.println(tab2);
        /* On peut faire une copie du tableau. */
        tab2 = tab.clone();
        tab2[2] = -1000;
        for (int e : tab)
            System.out.println(e);
        System.out.println(tab2);
        /* On peut aussi faire la copie à la main */
        int[] copie = new int[tab.length];
        for (int j = 0; j < tab.length; ++j)
            copie[j] = tab[j];
        /* La taille d'un tableau peut n'être connue qu'à l'exécution. */
        System.out.printf("Donner la taille du tableau : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int[] tab3 = new int[Integer.parseInt(s)];
        tab3[tab3.length - 1] = 42;
        System.out.printf("tab3[%d] = %d\n", tab3.length - 1, tab3[tab3.length - 1]);
        sc.close();

        System.out.printf("tab3[%d] = %d", tab3.length, tab3[tab3.length]);
        /* Mais par contre elle est fixée une bonne fois pour toute,
         * on ne peut pas écrire par exemple ++tab3.length. */

        /* À l'exécution (pas à la compilation), contrairement à ce qui se passe
         * en C, il y a vérification que les indices sont bien compris entre
         * 0 et (taille du tableau - 1), levée d'exception. */
    }
}
