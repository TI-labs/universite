#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

//Taille du buffer au début du programme
#define sizeof_Buffer 256

int main()
{
	char buffer[sizeof_Buffer];
	int fd1, fd3, i, charLu, taille;
	//f1
	if((fd1=open("recs.txt", O_RDONLY))==-1)
	{
		printf("Erreur à l'ouverture de recs.txt \n");
		exit(1);
	}
	//f3
	if((fd3=open("recs-inv.txt", O_WRONLY | O_CREAT | O_TRUNC, 777))==-1)
	{
		printf("Erreur à l'ouverture de recs-inv.txt\n");
		exit(1);
	}
	while((charLu=read(fd1, buffer,sizeof_Buffer))!=0);
	close(fd1);
	//On trouve la taille de ce qu'on a réellement lu
	for(taille=0; buffer[taille]!='\0'; taille++);

	for(i=0; i < taille; i++)
	{
		write(fd3, buffer+taille-i, 1);
	}
	close(fd3);
	return 0;

}
