#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main()
{
	int fd1;

	//fd1
	if((fd1=open("recs-inv.txt", O_WRONLY | O_CREAT | O_TRUNC, 0777))==-1)
	{
		printf("Erreur à l'ouverture de recs-inv.txt\n");
		exit(1);
	}
	if(fork() == 0)
	{
		dup2(fd1, 1);
		execlp("ls", "ls", "-l", (char*) NULL);
		printf("Erreur execlp\n");
		exit (1);
	}
	wait(NULL);

	close(fd1);
	return 0;

}
