#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main()
{
  int filsPere[2], pereFils[2], tube1, tube2, nbEntiers, i, val;
  pid_t fils;
  if((tube1 = pipe(pereFils)) == -1)
  {
    printf("Erreur création tube 1");
    return 1;
  }
  if((tube2 = pipe(filsPere)) == -1)
  {
    printf("Erreur création tube 2");
    return 1;
  }
  if((fils = fork()) == -1)
  {
    printf("Erreur fork");
    return 1;
  }
  if(fils == 0)
  {
    close(filsPere[0]);
    close(pereFils[1]);
    //fils
    do{
      printf("Combien d'entiers ?\n");
      scanf("%d", &nbEntiers);
    } while(nbEntiers <= 0);
    const int tailleTabF = nbEntiers + 1;
    int tableauF[tailleTabF];
    for(i = 0; i < tailleTabF; i++)
    {
      printf("Quel est l'entier positif numéro : %d", i);
      scanf("%d", &val);
      if(val >= 0)
        tableauF[i] = val;
      else
        i--;
    }
    tableauF[tailleTabF] = 0;
    write(filsPere[1], &tableauF, sizeof(int) * tailleTabF);
    close(filsPere[1]);

  }
  close(pereFils[0]);
  close(filsPere[1]);
  int tailleEffective = 0, tailleMax = 1;
  int* tableauP = (int*) malloc(sizeof(int) * 2);
  do {
    read(filsPere[0], &val, sizeof(int));
    if(tailleEffective == tailleMax)
    {
      tailleMax *= 2;
      tableauP =realloc(tableauP, sizeof(int) * tailleMax);
    }
    tableauP[tailleEffective] = val;
    tailleEffective++;
  } while(val!= 0);

  int somme, nbrValeursLues = 0, valMax = 0;
  float moyenne;
  for(i = 0; i < tailleEffective ; i++)
  {
    printf("tableauP[i] = %d \n", tableauP[i]);
  }

  return 0;
}
