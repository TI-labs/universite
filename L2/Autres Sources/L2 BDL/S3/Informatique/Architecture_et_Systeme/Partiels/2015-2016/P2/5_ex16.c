#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#define sizeOfBuffer 512

int main()
{
  int fd, i, byteLu;
  char buffer[sizeOfBuffer];
  if((fd = open("test",  O_RDONLY | O_CREAT, 0600)) == -1)
  {
    printf("Erreur ouverture fichier\n");
    exit (1);
  }
  for(i = 0; i < sizeOfBuffer; i++)
  {
    buffer[i] = 0;
  }

  do {
    byteLu = read(fd, buffer, sizeOfBuffer);
    if(byteLu >= sizeOfBuffer)
      write(1, buffer, sizeOfBuffer);
    else
      write(1, buffer, byteLu);
  } while(byteLu >= sizeOfBuffer);

  return 0;
}
 
