;Programme qui va compter la taille d'une chaine
; Pour trouver la longueur de la chaine, on fera une soustraction entre son adresse et celle du dernier caractère de la chaîne (qui n'est pas \0
;@Param une chaine de caractère
;@Return la longueur de la chaine est retournée dans R1
; @author: Nicolas Floquet

.ORIG x3000
	LEA R0, STR	; L'adresse de la chaîne STR est passée dans le registre RO
	LEA R1, STR	; R1 contient aussi l'adresse de STR

	JSR findEnd	; Sous procédure de parcours de chaîne


	; Sous Procédure qui va parcourir une chaîne stocker l'adresse du dernier caractère dans R0
	; @Param R0 = Adresse d'une chaîne de caractères
	; @Return l'adresse du dernier caractère dans R0
	findEnd :LDR R2, R0, 0 	; La valeur à l'adresse R0 est entrée dans R2
		BRz fini 	; Si le caractère est nul, c'est le caractère de fin de chaîne et on a fini
		ADD R0, R0, 1	; On incrémente la valeur de R0, donc on passe à l'adresse suivante
		RET		; On revient au début de notre boucle



fini : 	NOT R1, R1	; R1= non(R1) pour complément à deux
	ADD R1, R1, 1	; R1= R1+1, donc R1 vaut -(adresse de STR)
	ADD R1, R1, R0	; R1= R0+R1 <==> -(Adresse_chaine + taille_chaine) - Adresse_chaine = taille_chaine
	HALT

STR :	.STRINGZ "123"


.END
