; Floquet Nicolas 13/10/97 11706348
; Chandara Alexis 04/03/99 11702812


; Exercice 1 :

; Le programme met à zero le registre R5, copier la valeur du registre R0 dans le registre R2, copier la valeur contenue dans le location de mémoire x vers la location de mémoire y, copier dans la location z l’adresse de la location x.

.ORIG x3000
	
	AND R5, R5, 0		; Mise à 0 du registre R5
	
	ADD R0, R0, 1		; Affectation de 1 dans le registre R0 
	ADD R2, R0, 0 		; Copie de la valeur de R0 dans R2
	
	LD R1, x			; Chargement de la valeur de x
	ST R1, y			; Affectation de R1 qui contient la valeur de x dans y
	
	LEA R5, x			; R5 contient de l'adresse de x
	ST R5, z			; Affectation de R5 dans z
	
	HALT				
	
x: .FILL 3				; On met n'importe quelle valeur dans le label x.
y: .FILL 1				; On met n'importe quelle valeur dans le label y.
z: .FILL 2				; On met n'importe quelle valeur dans le label z.

.END					; Fin de programme


