#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void sighand(int sig);

int main()
{
  for(int sig=0; sig<NSIG; sig++)
  {
    if(signal(sig, sighand)==SIG_ERR)
      printf("Le signal %d est non déroutable\n", sig);
    else
      printf("Le signal %d est déroutable\n", sig);
  }
  printf("pid : %d\n", getpid());
  while(1)
  {
    sleep(5);
  }
  return EXIT_SUCCESS;
}

void sighand(int sig)
{
  printf("Reçu : %d\n", sig);
}
