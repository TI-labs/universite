#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include<sys/wait.h>
//man open
#define BUFFER_SIZE 256

int main ()
{
  char fileName[100], buffer[129];
  pid_t process;
  int fd, attente, keepReading, i, fileSize;
  printf("Entrez le nom d'un fichier dans lequel vous voulez écrire\n> ");
  scanf("%s", fileName);
  if((process=fork())==-1)
  {
    perror("fork");   //Renvoie un message d'erreur en plus du message "fork"
    exit(1);
  }
  if(process == 0)
  {
  printf("test");
    fd = open(fileName, O_WRONLY | O_CREAT , S_IRUSR  | S_IWUSR);
  	if(fd == -1)
  	{
  		printf("Erreur à la première ouverture du fichier\n");
  		exit (1);
  	}
    dup2(fd, 1);
    printf("test");
    execl("/bin/ls","ls","-l","./", (char *) NULL);
    close (fd);
  }
  attente = wait(NULL);
  fd = open(fileName, O_RDONLY , S_IRUSR  | S_IWUSR);
  if(fd == -1)
  {
  		printf("Erreur à la deuixème ouverture du fichier\n");
  		exit (1);
  }
  //Entrée indépendante de la taille de ls, si ls est < 128 alors il sera
  // répété dans buffer, si ls est plus grand que 128 alors
  // il ne sera pas écrit entièrement
  do
  {
  	keepReading=read(fd, buffer+fileSize, 1);
  	fileSize++;
  }
  while(keepReading!=0 && fileSize <BUFFER_SIZE);
  buffer[fileSize]='\0';

  printf("test");
  printf("Le buffer a %s \n", buffer);
  for(i=0; buffer[i]!='\0' && i<BUFFER_SIZE; i++)
    write(1, buffer, BUFFER_SIZE);

  return EXIT_SUCCESS;
}
