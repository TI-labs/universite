#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
	

	// Exercice 1
int main(int argc, char const* argv[])
{
	int i = 0;
	char fileName[100] buffer[100];
	
	if (argc < 2)
	{
		printf("Entrez un nom pour votre fichier : ");
		scanf("%s", fileName);
	}
	else
	{
		strcpy(fileName,argv[1]);
	}
	printf("Ouvrons le fichier de nom %s\n", fileName);
	
	//fd = file descriptor, décrit le fichier ouvert
	
	int fd = open(fileName, O_RDONLY | O_CREAT , S_IRUSR  | S_IWUSR);
	if(fd == -1)
	{
		printf("Erreur à l'ouverture du fichier\n");
		return EXIT_FAILURE;
	}
	
	read(fd, buffer, 10);
	//Solution de facilité
	write(1, buffer, 10);
	printf("\n");

	return EXIT_SUCCESS;
}

