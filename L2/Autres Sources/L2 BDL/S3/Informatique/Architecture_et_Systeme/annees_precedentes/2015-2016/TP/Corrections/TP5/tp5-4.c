#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

extern int errno;

DIR *open_dir(char * path) {
  DIR *dirp;

  if ((dirp = opendir(path)) == NULL) {
    fprintf(stderr, "Attention : erreur d'ouverture du répertoire %s\n", path);
    fprintf(stderr, "Vérifier le nom du répertoire ou les droits d'accès\n");
  }
  
  return dirp;
}

int print_dir() {
  DIR *dirp;
  struct dirent *entp;

  if ((dirp = open_dir(".")) == NULL)
    return 0;

  while ((entp = readdir((DIR *) dirp)) != NULL)
    if (entp->d_name[0] != '.' ||
	(entp->d_name[1] != '\0' &&
	 (entp->d_name[1] != '.' || entp->d_name[2] != '\0')))
    puts(entp->d_name);

  return 1;
}

#define valid_choice(c)     (((c) >= '0' && (c) <= '5') || ((c) != '9'))

char get_choice() {
  char c;

  printf("Entrer un choix : ");
  for (c = getchar(); ! valid_choice(c); c = getchar())
    printf("Valeur non valide. Entrer un nouveau choix : ");
    
  return c;
}

int main(int argc, char *argv[]) {
  //  DIR *dirp;
  //  char *path;
  unsigned n;
  char path[256];

  if (argc > 1)
    chdir(argv[1]);
  
  do {
    print_dir();
    switch (n = get_choice()) {
    case '0':
      return 0;
    case '1':
      printf("Entrer le nom du fichier à effacer : ");
      scanf("%255s", path);
      if (unlink(path) == -1)
	printf("Erreur : le fichier \"%s\" n'a pas été effacé [errno : %d]\n", path, errno);
      break;
    case '2':
      printf("Entrer le nom du répertoire à effacer : ");
      scanf("%255s", path);
      if (rmdir(path) == -1)
	printf("Erreur : le répertoire \"%s\" n'a pas été effacé [errno : %d]\n", path, errno);
      break;
    case '3':
      printf("Entrer le nom du répertoire à créer : ");
      scanf("%255s", path);
      if (mkdir(path, 0755) == -1)
	printf("Erreur : le répertoire \"%s\" n'a pas été crée [errno : %d]\n", path, errno);
      break;
    case '4':
      chdir("..");
      break;
    case '5':
      printf("Entrer le nom du nouveau répertoire curant : ");
      scanf("%255s", path);
      chdir(path);
      break;
    case '9':
      break;
    }
  } while (n != 0);
  
  exit(0);
}
