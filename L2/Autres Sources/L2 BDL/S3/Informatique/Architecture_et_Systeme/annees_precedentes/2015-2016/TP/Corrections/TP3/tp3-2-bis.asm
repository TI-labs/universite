;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;;  
;;;  TP 3 - exercice 2
;;;  06/10/2015
;;;
;;;  version avec un compteur

	.ORIG x3000
	
	;; un morceau de code qui appelle la sous-routine strlen
	LEA R0, str		; on passe l'adresse de la chaîne dans R0
	JSR strlen		; on appelle la sous-routine
	HALT			; on continue après la sous-routine

	;; une chaîne d'exemple pour le test
str:	.STRINGZ "toto"

	;; strlen : sous-routine qui calcule la longueur d'une chaîne
	;; @param R0 : adresse de la chaîne
	;; @result R1 : la longueur de la chaîne

	;; boucle de recherche du caractère de fin de chaîne
	;; on utilise R1 comme compteur et R0 pour parcourir la chaîne
strlen:	AND R1, R0, 0		; R1 := 0
loop:	LDR R2, R0, 0		; R2 := *R0 le caractère courant
	BRZ fin			; saute si fin de chaîne (caractère courant = 0)
	ADD R1, R1, 1		; R1++ un caractère en plus
	ADD R0, R0, 1		; R0++ on avance le pointeur
	BR loop			; on itère jusqu'à on trouve le fin de chaîne

	;; R1 contient la longueur de la chaine
fin:	RET			; retour

	.END
