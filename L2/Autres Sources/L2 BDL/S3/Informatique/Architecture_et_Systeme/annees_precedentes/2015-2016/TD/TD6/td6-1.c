#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>

int main(void) {
  int x = 0;
  pid_t pidfils = fork();
  if (pidfils == 0) {
    x=1;
  } else {
    printf("x=\%d\n", x);
  }
  printf("x=\%d\n", x+1);
}
