#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int fd;
  unsigned char buffer[10];
  int nbyte;

  /* printf("%d\n", argc); */
  /* for (int i = 0; i < argc; i++) */
  /*   printf("%s\n", argv[i]); */

  if (argc < 2) {
    fprintf(stderr, "Attention : donner un nom de fichier\n");
    exit(-1);
  }
  
  if ((fd = open(argv[1], O_RDONLY)) == -1) {
    fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[1]);
    exit(-1);
  }

  if ((nbyte = read(fd, buffer, 10)) < 10) {
    fprintf(stderr, "Attention : j'ai lu seulement %d octets\n", nbyte);
  }
 
  write(1, buffer, nbyte);

  close(fd);

  putchar('\n');
}
