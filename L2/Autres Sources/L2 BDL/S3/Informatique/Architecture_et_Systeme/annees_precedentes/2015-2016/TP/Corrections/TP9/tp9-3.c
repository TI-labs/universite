#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
  int fdp[2];
  pid_t f1, f2;

  if (pipe(fdp) == -1) {
    perror("pipe:");
    return 1;
  }

  if ((f1 = fork()) == -1) {
    perror("fork 1");
    return 1;
  } else if (f1 == 0) {
    /* fils 1 */
    close(fdp[0]);
    dup2(fdp[1], 1);
    execlp("cat", "cat", NULL);
    perror("exec 1");
  }

  /* père */
  if ((f2 = fork()) == -1) {
    perror("fork 2");
    return 1;
  } else if (f2 == 0) {
    /* fils 2 */
    close(fdp[1]);
    dup2(fdp[0], 0);
    execlp("cat", "cat", NULL);
    perror("exec 2");
  }

  close(fdp[0]);
  close(fdp[1]);
  wait(NULL);
  wait(NULL);
  printf("%d : %d -> %d\n", getpid(), f1, f2);

  return 0;
}
