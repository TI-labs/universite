#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int main(int argc, char *argv[]) {
  DIR *dirp;
  struct dirent *entp;
  char *path;

  if (argc < 2) {
    path = ".";
  } else {
    path = argv[1];
  }
    
  if ((dirp = opendir(path)) == NULL) {
    fprintf(stderr, "Attention : erreur d'ouverture du répertoire %s\n", path);
    fprintf(stderr, "Vérifier le nom du répertoire ou les droits d'accès\n");
    exit(-1);    
  }
  while ((entp = readdir((DIR *) dirp)) != NULL)
    printf("%s %llu\n", entp->d_name, entp->d_ino);

  exit(0);
}
