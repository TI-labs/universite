;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;;  
;;;  TP 2 - exercice 4
;;;  22/09/2015

	.ORIG x3000
	;; on initialise R0 à N
	AND	R1, R0, 0	; R1 := 0
	LD	R0, N		; init R0 au nombre d'itérations
	;; boucle while
	;; les bits PZN correspondent déjà à la valeur de R0
START:	BRZ	END		; fin de la boucle si R0 = 0
	ADD	R1, R1, 1	; R1 := R1 + 1
	ADD	R0, R0, -1	; R0 := R0 - 1
	BR	START		; on itère la boucle
	;; on saute directement au test car
	;; le bit PZN correspondent à la valeur de R0
END:	HALT
N:	.FILL	16
.END
	
