/*
 * Utilisation de pipe et date
 * Fils :
 *   exécute la commande date
 *   en écrivant dans le tube
 * Père :
 *   lit dans le tube
 *   écrit la date sur la sortie standard
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#include<wait.h>

int main (void) {
  int n, p[2];
  char c;

  if(pipe(p) != 0){
    perror("Erreur sur pipe");
    exit(1);
  }

  if((n = fork()) == -1){
    perror("Echec du fork");
    exit(2);
  } else if (n == 0) { /* processus fils */
    dup2(p[1], 1); /* sortie standard redirigée vers l'entrée du tube */
    close(p[0]); /* le fils ne lit pas dans le tube */
    close(p[1]); /* p[1] est devenu inutile */
    execlp("date", "date", "+%d/%m/%Y à  %Hh%M", NULL);
    perror("execlp date");
    exit(2);
  } else { /* père */
    char ChD [50];
    int i;
    close(p[1]); /* le père n'écrit pas dans le tube */
    i = 0;
    while(read(p[0], &c, 1) != 0) {
      ChD[i] = c;
      i++;
    }
    ChD[i-1] = '\0'; /* détruit le "\n" final */
    printf("Date du jour : %s\n",ChD);
    close(p[0]);
  }
}
