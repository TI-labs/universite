#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <time.h>

/*
 * Exercice 1 TD 8
 *  Compiler avec
 *    gcc -DUSELOCK tp8-1.c -o tp8-1
 * pour compiler avec le verrou
 * La fonction mssleep(n) permet d'introduire une pause
 * de n millisec.
 * Les delay introduits avec mssleep permettent d'apprecier
 * des differences entre la version sans verrou et la version avec
 */

void mssleep(int ms) {
  struct timespec tv = { .tv_sec = 0, .tv_nsec = ms * 1000 };
  nanosleep(&tv, NULL);
}

int main(int argc, char *argv[]) {
  pid_t pid = getpid();
  int fd;
  char buf[32];
  int i, nch;

  if (argc < 1) {
    fprintf(stderr, "Non du fichier non spécifié sur la ligne de commande.\n");
    return -1;
  }

  if ((fd = open(argv[1], O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR)) == -1) {
    fprintf(stderr, "On ne peut pas ouvrir le fichier %s\n", argv[1]);
    return -1;
  }

  i = 0;
  while (i < 100) {
#ifdef USELOCK
    flock(fd, LOCK_EX);
#endif
    do {
      mssleep(1);
      nch = sprintf(buf, "%d %d\n", pid, ++i);
      nch = write(fd, buf, nch);
      printf("%d %d\n", pid, i);
    } while (i % 5 != 0);
#ifdef USELOCK
    flock(fd, LOCK_UN);
#endif
    mssleep(10);
  }
  
  close(fd);
  return 0;
}
