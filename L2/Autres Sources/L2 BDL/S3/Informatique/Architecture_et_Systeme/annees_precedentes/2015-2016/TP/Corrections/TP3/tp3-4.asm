;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;;  
;;;  TP 3 - exercice 4
;;;  06/10/2015
	
	.ORIG x3000
	
	;; initialise le SP (le registre R6 en LC-3)
	LEA R6, pile

	;; met dans les registres R1 et R2
	;; les adresses des deux chaînes d'exemple
	LEA R1, s1
	LEA R2, s2

	;; on passe dans la pile les paramètres
	ADD R6, R6, -2
	STR R1, R6, 0
	STR R2, R6, 1
	;; on saute à la sous-routine
	JSR strcmp
	;; on élimine les paramètres de la pile
	ADD R6, R6, 2
	HALT

	;; routine récursive pour la comparaison de deux chaines
	;; @param : les adresses des deux chaines en tête à la pile
	;; @result R0 : 
	;; la routine modifie les registres R1, R2, R3, R4
strcmp:	LDR R1, R6, 0	; on met dans R1 l'adresse de la première chaîne
	LDR R2, R6, 1	; on met dans R2 l'adresse de la deuxième chaîne
	LDR R3, R1, 0	; R3 = *R1 le caractère en tète à la première chaîne
	LDR R4, R2, 0	; R4 = *R2 le caractère en tète à la deuxième chaîne
	;; on compare les deux caractères
	NOT R4, R4
	ADD R4, R4, 1		; R4 = -R4
	ADD R0, R3, R4		; R0 = R3 - R4
	BRZ ch_egaux		; si zéro, les deux caractères sont égaux
	;; sinon on a trouvé le premier caractère qui diffère 
	;; R0 contient une valeur >0 si le caractère dans la première
	;; chaîne a une valeur plus grande, ou une valeur <0 s'il a une
	;; valeur plus petite
	RET 			; return
	
	;; on vérifie si le caractère (le même dans les deux chaînes)
	;; est le fin de chaîne (caractère 0)
ch_egaux: ADD R3, R3, 0
	BRNP next		; si non zéro, les caractères diffèrent
	;; on a trouvé le fin de chaîne
	;; R0 contient la valeur 0
	RET    			; return
	;; il faut comparer les caractères suivants des chaînes
	;; on avances les pointers à la tête de la chaîne
next:	ADD R1, R1, 1
	ADD R2, R2, 1
	;; appel récursif sur les restes des deux chaînes
	;; on sauvegarde dans la pile l'adresse de retour (le registre R7)
	ADD R6, R6, -1
	STR R7, R6, 0
	;; on passe dans la pile les paramètres
	ADD R6, R6, -2
	STR R1, R6, 0
	STR R2, R6, 1
	;; on saute à la sous-routine
	JSR strcmp
	;; on élimine les paramètres
	ADD R6, R6, 2
	;; on rétablit l'adresse de retour de la sous-routine
	;; sauvegardé dans la pile et on l'élimine de la pile
	LDR R7, R6, 0
	ADD R6, R6, 1
	;; fin de la sous-routine
	RET			; return
	
	;; deux chaînes pour tester la routine
s1:	.STRINGZ "toto"
s2:	.STRINGZ "toso"

	;; la pile
	.BLKW 128
pile:	
	.END
