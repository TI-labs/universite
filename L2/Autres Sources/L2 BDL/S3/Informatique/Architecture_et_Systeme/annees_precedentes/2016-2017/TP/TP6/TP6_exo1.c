#include <stdio.h>
#include <stdlib.h>


int main (int argc, char const*argv[])
{
	int i = 0;
	if (argc < 2)
	{
		printf("Vous n'avez pas entré assez d'arguments\n");
		return EXIT_FAILURE;
	}
	printf("Les arguments entrés sont les suivants\n");
	for (i = 1; i < argc; i++)
	{
		printf("Arg %d : %s\n", i, argv[i]);
	}
}
