#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

//Taille du buffer au début du programme
#define sizeof_Buffer 256

int main(int argc, char* argv[])
{
	int flag2Args=0;
	if(argc < 3)
	{
		printf("Vous n'avez pas entré assez d'arguments, donnez moi le nom de 2 ou 3 fichiers!\n");
		return 1;
	}
	if(argc == 3)
	{
		flag2Args=1;
	}

	int fd1, fd2, fd3, i, charLu, taille;
	char nomFichier[100], buffer[sizeof_Buffer];
	for(i=0; i<sizeof_Buffer; i++)
	{
		buffer[i]='\0';
	}

	if(flag2Args)
	{
		//f1
		strcpy(nomFichier, argv[1]);
		if((fd1=open(nomFichier, O_RDONLY))==-1)
		{
			printf("Erreur à l'ouverture du premier fichier : %s\n", nomFichier);
			exit(1);
		}

		//f2
		strcpy(nomFichier, argv[2]);
		if((fd2=open(nomFichier, O_RDONLY))==-1)
		{
			printf("Erreur à l'ouverture du deuxième fichier : %s\n", nomFichier);
			exit(1);
		}


		while((charLu=read(fd1, buffer,sizeof_Buffer))!=0);
		close(fd1);
		for(taille=0; buffer[taille]!='\0'; taille++);

		//On peut ré-écrire le fichier1 entièrement dans la sortie standard
		write(1, buffer, taille-1);

		for(i=0; i<sizeof_Buffer; i++)
		{
			buffer[i]='\0';
		}

		while((charLu=read(fd2, buffer,sizeof_Buffer))!=0);
		close(fd2);
		//On peut ré-écrire le fichier2 entièrement dans la sortie standard

		for(taille=0; buffer[taille]!='\0'; taille++);
		write(1, buffer, taille);
		return 0;
	}
	else
	{
		//f1
		strcpy(nomFichier, argv[1]);
		if((fd1=open(nomFichier, O_RDONLY))==-1)
		{
			printf("Erreur à l'ouverture du premier fichier : %s\n", nomFichier);
			exit(1);
		}

		//f2
		strcpy(nomFichier, argv[2]);
		if((fd2=open(nomFichier, O_RDONLY))==-1)
		{
			printf("Erreur à l'ouverture du deuxième fichier : %s\n", nomFichier);
			exit(1);
		}

		//f3
		strcpy(nomFichier, argv[3]);
		if((fd3=open(nomFichier, O_WRONLY))==-1)
		{
			printf("Erreur à l'ouverture du troisième fichier : %s\n", nomFichier);
			exit(1);
		}
		while((charLu=read(fd1, buffer,sizeof_Buffer))!=0);
		close(fd1);
		//On peut ré-écrire le fichier1 entièrement dans le fichier3
		for(taille=0; buffer[taille]!='\0'; taille++);
		write(fd3, buffer, taille-1);


		for(i=0; i<sizeof_Buffer; i++)
		{
			buffer[i]='\0';
		}
		while((charLu=read(fd2, buffer,sizeof_Buffer))!=0);
		close(fd2);
		//On peut ré-écrire le fichier2 entièrement dans le fichier3
		for(taille=0; buffer[taille]!='\0'; taille++);
		write(fd3, buffer, taille);

		close(fd3);

		return 0;
	}
}
