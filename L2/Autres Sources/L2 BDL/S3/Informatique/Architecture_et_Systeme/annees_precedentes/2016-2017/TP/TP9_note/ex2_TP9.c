#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
	pid_t fils1, fils2;
	//Crééons un tube pour communiquer avec les fils
	int fd[2], plusGrand, tube;

	if((tube=pipe(fd))==-1)
	{
		printf("Erreur tube\n");
		exit(1);
	}
	if((fils1=fork())==-1)
	{
		printf("Erreur fork1\n");
		exit(1);
	}
	if(fils1==0)
	{	//Fils1
		//On n'écrit pas dans le tube
		close(fd[1]);
		pid_t n;
		read(fd[0], &n, sizeof(pid_t));
		if(n==getpid())
			printf("Processus %d fils 1 de %d\n", getpid(), getppid());
		else
			printf("Processus %d fils 2 de %d\n", getpid(), getppid());
		close(fd[0]);
		exit(0);
	}
	if((fils2=fork())==-1)
	{
		printf("Erreur fork2\n");
		exit(1);
	}
	if(fils2==0)
	{	//Fils2
		//On n'écrit pas dans le tube
		close(fd[1]);
		pid_t n;
		read(fd[0], &n, sizeof(pid_t));
		if(n==getpid())
			printf("Processus %d fils 1 de %d\n", getpid(), getppid());
		else
			printf("Processus %d fils 2 de %d\n", getpid(), getppid());
		close(fd[0]);
		exit(0);
	}
	//Père
	//On ne lit rien de la part des fils
	close(fd[0]);

	//La variable plusGrand contient en fait le résultat de la soustraction du pid de fils2 au pid de fils1
	//Si le résultat est < 0 alors le fils1 est le premier fils du père, sinon c'est le deuxième
	plusGrand=fils1-fils2;
	if(plusGrand<0)
	{
		//fils1 est le premier, on envoie son pid dans le tube
		//Si un fils reçoit son pid dans le tube, il l'interpretera comme une indication qu'il est le premier
		write(fd[1], &fils1, sizeof(pid_t));
		write(fd[1], &fils1, sizeof(pid_t));
	}
	else if(plusGrand>0)
	{
			//fils2 est le premier, on envoie son pid dans le tube
			write(fd[1], &fils2, sizeof(pid_t));
			write(fd[1], &fils2, sizeof(pid_t));
	}
	close(fd[1]);
	wait(NULL);
	printf("Processus %d père de %d et %d\n", getpid(), fils1, fils2);
	return 0;
}
