#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>


	
int main(int argc, char const* argv[])
{
	int i = 0;
	if (argc < 2)
	{
		printf("Vous n'avez pas entré assez d'arguments\n");
		return EXIT_FAILURE;
	}
	printf("Les arguments entrés sont les suivants\n");
	for (i = 1; i < argc; i++)
	{
		printf("Arg %d : %s\n", i, argv[i]);
	}
	printf("Ouvrons un fichier dont le nom est spécifié dans la ligne de commande, l'argv[1]\n");
	
	//fd = file descriptor, décrit le fichier ouvert
	
	int ouvert = open(argv[1], O_RDWR | O_CREAT , S_IRUSR  | S_IWUSR);
	if(ouvert == -1)
	{
		printf("Erreur à l'ouverture du fichier\n");
		return EXIT_FAILURE;
	}


	char testString[100] = " ";
    
	printf("%s \n",testString);
	return EXIT_SUCCESS;
	

}
