#include<stdlib.h>
#include<stdio.h>

typedef struct node_st {
  int v;
  struct node_st *g, *d;
} Node;

typedef Node * Tree;

/* Renvoie l'arbre construit à partir de la valeur val, des sous-arbres droit et gauche */
Tree build (int val, Tree gauche, Tree droit) {
  Tree nw = (Tree) malloc(sizeof(Node));
  nw->v = val;
  nw->g = gauche;
  nw->d = droit;
  return nw;
}

/* Affichage infixe des valeurs d'un arbre binaire */
void print_inorder (Tree A) {
  if (A != NULL) {
    print_inorder(A->g);
    printf("%d ",A->v);
    print_inorder(A->d);
  }
}

/* Renvoie un arbre binaire pour exemple */
Tree arbre_test() {
  Tree a = build(5,
		 build(6,
		       build(3,
			     build(10,
				   NULL,
				   NULL
				   ),
			     build(42,
				   NULL,
				   build(0,
					 NULL,
					 NULL
					 )
				   )
			     ),
		       NULL
		       ),
		 build(12,
		       NULL,
		       build(2,
			     NULL,
			     NULL
			     )
		       )
		 );
  return a;
}

int main() {
  Tree a = arbre_test();
	a->d=arbre_test();
//	a->d->d->d->d->d->d=arbre_test();
  print_inorder(a);
  printf("\n");
  return 0;
}
