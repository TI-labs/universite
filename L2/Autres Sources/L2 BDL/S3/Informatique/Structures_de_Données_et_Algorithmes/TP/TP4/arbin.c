#include<stdlib.h>
#include<stdio.h>
#include"arbin.h"

//Test des fonctions crées
int main()
{
	int i,j;
	node* root=creer(0,NULL,NULL);
	inserer(2,root);
	inserer(1,root);
	inserer(-500,root);
	inserer(0,root);
	inserer(651,root);
	inserer(9,root);

	node* aNegatif500 = creer(-500,NULL, NULL);
	node* a101 = creer(100,NULL, NULL);
	node* a100 = creer(100,a101, a101);
	
	node* arbreNonABR=creer(0,aNegatif500,a100);
	printf("La valeur de vérité de root est un ABR est %d, \n", estABR(root));
	printf("La valeur de vérité de arbreNonABR est un ABR est %d, \n", estABR(arbreNonABR));
	printf("La valeur de vérité de a100 est un ABR est %d, \n", estABR(a100));
	printf("La valeur de vérité de null est un ABR est %d, \n", estABR(NULL));

	//Hauteur : 5050
	for(i=0; i<=10; i++)
	{
		for(j=0; j<i; j++)
		{
			inserer(i,root);
		}
		for(j=0; j>i; j--)
		{
			inserer(-i,root);
		}
	}
	//Logigue, penser à faire un arbre qui se trie à chaque insertion
	

	node* The_Roots_feat_Common=creer(100,root,NULL);
	node* I_AM_Groot=creer(42069,The_Roots_feat_Common,NULL);

	read_it_all(I_AM_Groot);

	free_arbre(I_AM_Groot);

	node* lognt_root=creer(0,NULL,NULL);
	for(i=1; i<10; i++)	
	{
		inserer (i,lognt_root);
	}
	read_it_all(lognt_root);
	for(i=9; i>0; i--)
	{
		i--;
		supprimer(i, &lognt_root);
	}


	read_it_all(lognt_root);
	
	return EXIT_SUCCESS;
}




//Définition des fonctions manipulant des Arbres Binaires (type node)

//__________________________________________

/*
Fonction qui va créer un arbre binaire à partir de la valeur v de la clef de la racine et des ses sous arbres droit (fils droit; fd) et gauche (fg)
Attention, pour avoir un arbre binaire de recherche, il est nécessaire d'avoir :
	fg->val < v && fd >= v
	De plus fg et fd doivent être des ABR, sinon l'arbre crée n'aura pas les fils erronés
	*/



node* creer (int v, node* fg, node* fd)
{
	node* nouvel_arbre=(node*) malloc(sizeof(node));
	nouvel_arbre->val=v;
/*
Rendu optionnel pour tester estABR
	if(estABR(fg) && fg->val < v)
	{
		nouvel_arbre->gauche=fg;
	}
	else
	{
		nouvel_arbre->gauche=NULL;
	}
	if(estABR(fd) && fd->val >= v)
	{
		nouvel_arbre->droite=fd;
	}
	else
	{
		nouvel_arbre->droite=NULL;
	}*/
		nouvel_arbre->gauche=fg;
		nouvel_arbre->droite=fd;
	return nouvel_arbre;
}

//__________________________________________
/*Fonction qui vérifie si un arbre est un Arbre Binaire de Recherche
Renvoie 1 si c'est le cas
Renvoie 0 sinon
Fonction faite après (15/11/18)
*/
int estABR(node* A)
{
	if(A==NULL)
	{
		return 0;
	}
	else
	{
		if(A->gauche==NULL && A->droite==NULL)
		{
			return 1;
		}
		if(A->gauche!=NULL && A->droite!=NULL)
		{
			if(A->val > A->gauche->val && A->val <= A->droite->val)
			{
				//Car 1*1 = 1 et 1*0 = 0*0 = 0*1 = 0
				return estABR(A->gauche) * estABR(A->droite);
			}
			return 0;
		}
		else if(A->gauche!=NULL) //&& A->droite==NULL implicite
		{
			if(A->val > A->gauche->val)
			{
				return estABR(A->gauche);
			}
			return 0;
		}
		else		//(A->droite!=NULL) && A->gauche==NULL implicites
		{
			if(A->val <= A->droite->val)
			{
				return estABR(A->droite);
			}
			return 0;
		}
	}
}

//__________________________________________

//Impression infixe d'un arbre binaire

	
void afficher_en_ordre(node* A)
{
	if(A!=NULL)
	{
		afficher_en_ordre(A->gauche);
		printf("%d\n", A->val);
		afficher_en_ordre(A->droite);
	}
}

//__________________________________________

//Cherche si un élément est dans l'arbre binaire ou non

int chercher(int v, node* A)
{
	if(A==NULL)
	{
		return 0;
	}
	if(v==A->val)
	{
		return 1;
	}
	if(v<A->val)
	{
		return chercher(v, A->gauche);
	}
	return chercher(v, A->droite);
}


//__________________________________________

int minimum(node* A)
{
	if(A->gauche==NULL)
	{
		return A->val;
	}
	return minimum(A->gauche);
}

//__________________________________________

int maximum(node* A)
{
	if(A->droite==NULL)
	{
		return A->val;
	}
	return maximum(A->droite);
}

//__________________________________________

int nombre_de_noeuds(node* A)
{
	if(A==NULL)
		return 0;
	return 1+nombre_de_noeuds(A->gauche)+nombre_de_noeuds(A->droite);

}



//__________________________________________
/*

	Fonction qui trouve la hauteur d'un arbre.
	Un noeud NULL retourne -1
	Sinon la hauteur restante d'un noeud est le max entre la hauteur de sa branche de droite et celle de gauche

*/
int hauteur(node* A)
{
/*
_________________
	ANCIENNE
_________________
	int Hg=0;	//La hauteur à gauche
	int Hd=0;	 //La hauteur à droite
	if(A==NULL)
	{
		return 0;
	}
	if((A->gauche==NULL)&&(A->droite==NULL))
	{
		return 0;
	}
	else
	{
		Hg=hauteur(A->gauche);
		Hd=hauteur(A->droite);
		if(Hg>Hd)
		{
			return Hg+1;
		}
		else
		{
			return Hd+1;
		}
	}
*/
	if(A==NULL)
	{
		return -1;
	}
	int Hg = hauteur(A->gauche);	//La hauteur à gauche
	int Hd = hauteur(A->droite);	//La hauteur à droite
	if(Hg > Hd)
	{
		return 1 + Hg;
	}
	else
	{
		return 1 + Hd;
	}
	
}

//__________________________________________
/*
	Fonction qui insère des valeurs dans l'arbre binaire de manière ordonnée
	Si la valeur est strictement inférieur à celle d'un noeud, alors on va essayer de l'insérer à la gauche de celui ci
	Sinon on va insérer la valeur à droite
	On parcourt l'arbre tant que c'est nécessaire "planter une feuille"

*/
void inserer(int v, node* A)
{
	if(v<A->val)
	{
		if(A->gauche==NULL)
		{
			A->gauche=creer(v,NULL,NULL);
		}
		else
		{
			inserer(v,A->gauche);
		}
	}
	else
	{
		if(A->droite==NULL)
		{
			A->droite=creer(v,NULL,NULL);
		}
		else
		{
			inserer(v,A->droite);
		}
	}
}

//__________________________________________
//Execute les différentesn fonctions qui affichent des infos sur les arbres

void read_it_all(node* A)
{
	
	afficher_en_ordre(A);
	printf("Le noeud à la plus grand valeur a pour valeur : %d\n", maximum(A));
	printf("Le noeud à la plus petite valeur a pour valeur : %d\n", minimum(A));	
	printf("La hauteur de l'arbre est de : %d\n", hauteur(A));
}

//__________________________________________

//Suppression d'un élément de l'arbre

int supprimer(int v, node** A)
{
	if((*A)==NULL)
	{
		return 0;
	}
	if(v==(*A)->val)
	{
		if((*A)->gauche==NULL)
		{
			node* tmp=(*A)->droite;
			free(*A);
			(*A)=tmp;
			return 1;
		}
		if((*A)->droite==NULL)
		{
			node* tmp=(*A)->gauche;
			free(*A);
			(*A)=tmp;
			return 1;
		}
		int successeur=minimum((*A)->droite);		//You will never get the SUCC
		supprimer(successeur,&(*A)->droite);
		(*A)->val=successeur;
		return 1;
	}
	else
	{
		if(v<(*A)->val)
		{
			return supprimer(v, &((*A)->gauche));
		}
		else
		{
			return supprimer(v, &((*A)->droite));
		}
	}
}

//__________________________________________

//Fonction va libérer la mémoire qu'on a allouée pour un arbre

void free_arbre(node* A)
{
	if(A!=NULL)
	{
		free_arbre(A->gauche);
		free_arbre(A->droite);
	}
	free(A);
}


//__________________________________________

