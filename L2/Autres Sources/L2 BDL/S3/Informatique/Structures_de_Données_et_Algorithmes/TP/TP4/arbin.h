#ifndef _ARBIN_H_
#define _ARBIN_H_

#include<stdio.h>
#include<stdlib.h>


typedef struct st_node{
	int val;		//La valeur/clef
	struct st_node *gauche;
	struct st_node *droite;
}node;



//__________________________________________

node* creer (int v, node* fg, node* fd);

//__________________________________________

int estABR(node* A);

//__________________________________________
	
void afficher_en_ordre(node* A);

//__________________________________________

int chercher(int v, node* A);

//__________________________________________

int minimum(node* A);

//__________________________________________

int maximum(node* A);

//__________________________________________

int nombre_de_noeuds(node* A);

//__________________________________________

int hauteur(node* A);

//__________________________________________

void inserer(int v, node* A);

//__________________________________________

void read_it_all(node* A);

//__________________________________________

int supprimer(int v, node** A);

//__________________________________________

void free_arbre(node* A);

//__________________________________________


#endif
