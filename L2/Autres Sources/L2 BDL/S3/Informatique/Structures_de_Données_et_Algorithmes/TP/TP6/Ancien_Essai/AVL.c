#include<stdio.h>
#include<stdlib.h>
#include"AVL.h"

//Test des fonctions crées
int main()
{
	int i,j;
	arbre root=creer(NULL, NULL, 0);
	inserer(2,&root);
	/*inserer(1,&root);
	inserer(-500,&root);
	inserer(0,&root);
	inserer(651,&root);
	inserer(9,&root);



	//Hauteur : 5050 pour i = 100
	for(i=0; i<=10; i++)
	{
		for(j=0; j<i; j++)
		{
			inserer(i,&root);
		}
		for(j=0; j>i; j--)
		{
			inserer(-i,&root);
		}
	}
	//Logigue, penser à faire un arbre qui se trie à chaque insertion
	
	read_it_all(root);
	
	printf("Maintenant lançons la fonction miroir qui va donner un nouvel arbre symétrique à l'ancien");
	miroir(root);
*/	
	//read_it_all(root);

	return EXIT_SUCCESS;
}




//Définition des fonctions manipulant des Arbres Binaires (type arbre)

//__________________________________________

//Fonction qui va créer un arbre binaire à partir de la valeur v de la clef de la racine et des ses sous arbres droit (fils droit; fd) et gauche (fg)



arbre creer (arbre fg, arbre fd, int v)
{
	arbre nouvel_arbre=(arbre) malloc(sizeof(node));
	nouvel_arbre->val=v;
	if(estABR(fg) && fg->val < v)
	{
		nouvel_arbre->gauche=fg;
	}
	else
	{
		nouvel_arbre->gauche=NULL;
	}
	if(estABR(fd) && fd->val >= v)
	{
		nouvel_arbre->droite=fd;
	}
	else
	{
		nouvel_arbre->droite=NULL;
	}
	nouvel_arbre->hauteur=0;
	nouvel_arbre->coeffEqui=0;
	return nouvel_arbre;
}


//__________________________________________
/*
	Fonction qui insère des valeurs dans l'arbre binaire de manière ordonnée
	Si la valeur est strictement inférieur à celle d'un noeud, alors on va essayer de l'insérer à la gauche de celui ci
	Sinon on va insérer la valeur à droite
	On parcourt l'arbre tant que c'est nécessaire "planter une feuille"

*/
void inserer(int v, arbre* A)
{
	if((*A)==NULL)
	{
		A = creer(NULL,NULL,v);
	}
	if((*A)!=NULL)
	{
		if(v<(*A)->val)
		{
			if((*A)->gauche==NULL)
			{
				(*A)->gauche=creer(NULL,NULL,v);
				if(AVLouPas(A)==-5)
				{
					//Quelle Rotation
				}
			}
			else
			{
				inserer(v,(*A)->gauche);
				if(AVLouPas(A)==-5)
				{
					//Quelle Rotation
				}
			}
		}
		else
		{
			if((*A)->droite==NULL)
			{
				(*A)->droite = creer(NULL,NULL,v);
				if(AVLouPas(A)==-5)
				{
					//Quelle Rotation
				}
			}
			else
			{
				inserer(v,(*A)->droite);
				if(AVLouPas(A)==-5)
				{
					//Quelle Rotation
				}
			}
		}
	}
}

//__________________________________________
/*Fonction qui vérifie si un arbre est un Arbre Binaire de Recherche
Renvoie 1 si c'est le cas
Renvoie 0 sinon
Fonction faite après (15/11/18)
*/
int estABR(node* A)
{
	if(A==NULL)
	{
		return 0;
	}
	else
	{
		if(A->gauche==NULL && A->droite==NULL)
		{
			return 1;
		}
		if(A->gauche!=NULL && A->droite!=NULL)
		{
			if(A->val > A->gauche->val && A->val <= A->droite->val)
			{
				//Car 1*1 = 1 et 1*0 = 0*0 = 0*1 = 0
				return estABR(A->gauche) * estABR(A->droite);
			}
			return 0;
		}
		else if(A->gauche!=NULL) //&& A->droite==NULL implicite
		{
			if(A->val > A->gauche->val)
			{
				return estABR(A->gauche);
			}
			return 0;
		}
		else		//(A->droite!=NULL) && A->gauche==NULL implicites
		{
			if(A->val <= A->droite->val)
			{
				return estABR(A->droite);
			}
			return 0;
		}
	}
}

//__________________________________________
// Test si un ABR est un AVL ou non MARCHE PAS


int AVLouPas(arbre* A)
{
	if(A==NULL)
	{
		return -1;
	}
	int hg, hd;
	hd=AVLouPas((*A)->droite);
	hg=AVLouPas((*A)->gauche);
	if( (hg=-5) || (hd=-5) || (val_absolue(hg-hd)>=2))
	{
		return -5;
	}
	return 1+max(hg, hd);
}

//__________________________________________
// Fonction qui equilibre l'arbre par rotations

void equilibrer(arbre* A)
{
	printf("In construction\n");
}

//__________________________________________

//PAS SUR QUE ÇA FONCTIONNE, DE PLUS OÙ SONT LES DOUBLES ROTATIONS?

void rot_G (arbre* A)
{
	node *p = *A;
	node *q = p->gauche;
	p->gauche = q->droite;
	q->droite = p;
	// La racine du sous-arbre etait p, elle devient q
	*A = q; 
	
	p->hauteur = max(hauteur(p->gauche) + 1, hauteur(p->droite));
	p->hauteur = max(hauteur(p), hauteur(q->droite) + 1);
}

//__________________________________________

//PAS SUR QUE ÇA FONCTIONNE, DE PLUS OÙ SONT LES DOUBLES ROTATIONS?

void rot_D (arbre* A)
{
	node *p = *A;
	node *q = p->droite;
	p->droite = q->gauche;
	q->gauche = p;
	// La racine du sous-arbre etait p, elle devient q
	*A = q; 
	
	p->hauteur = max(hauteur(p->gauche), hauteur(p->droite) + 1);
	
	p->hauteur = max(hauteur(p), hauteur(q->droite) + 1);
}

//__________________________________________

//Impression infixe d'un arbre binaire

	
void afficher_en_ordre(arbre A)
{
	if(A!=NULL)
	{
		afficher_en_ordre(A->gauche);
		printf("%d\n", A->val);
		afficher_en_ordre(A->droite);
	}
}
//__________________________________________

//Fonction qui va imprimer un arbre de manière infixe et lisible

void imprimer_un_arbre(arbre A)
{
	if(A==NULL)
		return;
	printf("\n");
	imprimer_un_arbre(A->gauche);
	printf("\n\t(%d)", A->val);
	imprimer_un_arbre(A->droite);
	printf("\n");

}
//__________________________________________

//Execute les différentes fonctions qui affichent des infos sur les arbres

void read_it_all(arbre A)
{
	
	//afficher_en_ordre(A);
	imprimer_un_arbre(A);
	printf("Le noeud à la plus grand valeur a pour valeur : %d\n", valeur_maximum(A));
	printf("Le noeud à la plus petite valeur a pour valeur : %d\n", valeur_minimum(A));
	printf("Le nombre de noeuds est %d\n", nombre_de_noeuds(A));
	printf("La hauteur de l'arbre est de : %d\n", hauteur(A));
}

//__________________________________________

//Cherche si un élément est dans l'arbre binaire ou non

int chercher(int v, arbre A)
{
	if(A==NULL)
	{
		return 0;
	}
	if(v==A->val)
	{
		return 1;
	}
	if(v<A->val)
	{
		return chercher(v, A->gauche);
	}
	return chercher(v, A->droite);
}


//__________________________________________

//Fonction qui parcourt un arbre et renvoie la plus petite valeur

int valeur_minimum(arbre A)
{
	if(A->gauche==NULL)
	{
		return A->val;
	}
	return valeur_minimum(A->gauche);
}

//__________________________________________

//Fonction qui parcourt un arbre et renvoie le noeud contenant la plus petite valeur

arbre noeud_minimum(arbre A)
{
	if(A->gauche==NULL)
	{
		return A;
	}
	return noeud_minimum(A->gauche);
}

//__________________________________________

//Fonction qui parcourt un arbre et renvoie la plus petite valeur

int valeur_maximum(arbre A)
{
	if(A->droite==NULL)
	{
		return A->val;
	}
	return valeur_maximum(A->droite);
}
//__________________________________________

//Fonction qui parcourt un arbre et renvoie le noeud contenant la plus grande valeur

arbre noeud_maximum(arbre A)
{
	if(A->droite==NULL)
	{
		return A;
	}
	return noeud_maximum(A->droite);
}


//__________________________________________

//Fonction qui va trouver le nombre de noeuds dans un arbre 

int nombre_de_noeuds(arbre A)
{
	if(A==NULL)
		return 0;
	return 1+nombre_de_noeuds(A->gauche)+nombre_de_noeuds(A->droite);
}



//__________________________________________
/*

	Fonction qui renvoie la hauteur d'un arbre.
	Un noeud null renvoie -1

*/
int hauteur(node* A)
{
	return (A == NULL) ? -1 : A->hauteur;	
}

//__________________________________________

node* pere(arbre A, node* x)
{
	if(A==NULL)
	{
		return NULL;
	}
	if(A->gauche == x || A->droite == x)
	{
		return A;
	}
	else
	{
		if(A->val > x->val)
		{
			return pere(A->droite, x);
		}
		else
		{
			return pere(A->gauche, x);
		}
	}
}
//__________________________________________

node* succ(arbre A, node* x)
{
/*O(n)*/
	if(x==NULL)
	{
		return NULL;
	}
	if(x->droite != NULL)
	{
		return noeud_minimum(x->droite);
	}
	if(valeur_maximum(A->gauche) >= x->val)
	{
		if(noeud_maximum(A->gauche) == x)
		{
			return A;
		}
		return succ(A->gauche, x);
	}
	if(A->droite != NULL)
	{
			return succ(A->droite, x);
	}
	return NULL;
}

//__________________________________________

int nombre_de_feuilles(arbre A)
{
	if(A==NULL)
	{
		return 0;
	}
	if(A->gauche == NULL && A->droite == NULL)
	{
		return 1;
	}
	
	return nombre_de_feuilles(A->gauche) + nombre_de_feuilles(A->droite);
}

//__________________________________________
/* Compte et renvoie le nombre de noeuds internes dans un arbre*/

int nombre_noeuds_internes(arbre A)
{
	if(A!=NULL)
	{
		if(A->gauche==NULL && A->droite==NULL)
		{
			return 0;
		}
	
		else
		{
			int res=1;
			if(A->gauche!=NULL)
			{
				res+=nombre_noeuds_internes(A->gauche);
			}
			if(A->droite!=NULL)
			{
				res+=nombre_noeuds_internes(A->droite);
			}
			return res;
	
		}
	}
	return 0;
}

//__________________________________________

void miroir (arbre A)
{
	if(A!=NULL)
	{
		arbre tmp=A->droite;
		A->droite=A->gauche;
		A->gauche=tmp;
		if(A->gauche!=NULL)
		{
			miroir(A->gauche);
		}
		if(A->droite!=NULL)
		{
			miroir(A->droite);
		}
	}
}

//__________________________________________

int compare (arbre A1, arbre A2)
{
	return EXIT_FAILURE;
}

//__________________________________________

int k_eme (arbre A, int k)
{
	return EXIT_FAILURE;
}

//__________________________________________

int est_somme (arbre A)
{
	return EXIT_FAILURE;
}

//__________________________________________

//Suppression d'un élément de l'arbre

int supprimer(int v, arbre* A)
{
	if((*A)==NULL)
	{
		return 0;
	}
	if(v==(*A)->val)
	{
		if((*A)->gauche==NULL)
		{
			arbre tmp=(*A)->droite;
			free(*A);
			(*A)=tmp;
			return 1;
		}
		if((*A)->droite==NULL)
		{
			arbre tmp=(*A)->gauche;
			free(*A);
			(*A)=tmp;
			return 1;
		}
		int successeur=valeur_minimum((*A)->droite);		//You will never get the SUCC
		supprimer(successeur,&(*A)->droite);
		(*A)->val=successeur;
		return 1;
	}
	else
	{
		if(v<(*A)->val)
		{
			return supprimer(v, &((*A)->gauche));
		}
		else
		{
			return supprimer(v, &((*A)->droite));
		}
	}
}

//__________________________________________

int max(int x, int y)
{
	if(x < y)
	{
		return y;
	}
	return x;
}

//__________________________________________

int val_absolue(int x)
{
	if(x < 0)
	{
		return -x;
	}
	return x;
}

//__________________________________________

// Libération de la mémoire qu'on a allouée pour un arbre


void free_arbre(arbre A)
{
	if(A!=NULL)
	{
		free_arbre(A->gauche);
		free_arbre(A->droite);
	}
	free(A);
}

