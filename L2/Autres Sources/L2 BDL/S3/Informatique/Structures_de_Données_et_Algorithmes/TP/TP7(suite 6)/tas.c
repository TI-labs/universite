#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include"tas.h"


int main(int argc, char* const argv[])
{
	int testMax, nbrElem, puissanceDe2[31], i;
	srand(time(NULL));
	tas* leTas = init_tas(1);
	if(argc == 1)
	{
		printf("Combien d'éléments voulez-vous initialement mettre dans votre tas?\n");
		scanf("%d", &nbrElem);
	}
	else
	{
		nbrElem=atoi(argv[1]);
	}
/*	Exo 1.d
	tas_insertion(55, leTas);
	tas_insertion(22, leTas);
	tas_insertion(18, leTas);
	tas_insertion(14, leTas);
	tas_insertion(11, leTas);
	tas_insertion(16, leTas);
	tas_insertion(9, leTas);
	tas_insertion(3, leTas);
	tas_insertion(12, leTas);
	tas_insertion(6, leTas);
	affiche_tas_Tableau(leTas);
	tas_insertion(43, leTas);
	affiche_tas_Tableau(leTas);
	printf("Retrait du plus grand élément, il valait %d\n", extraction(leTas));
	affiche_tas_Tableau(leTas);
	*/

//	testMax=-1;
	for(i=0; i<nbrElem; i++)
	{
		tas_insertion(rand()%100, leTas);
//		affiche_tas_Tableau(leTas);
//		affiche_Est_Tas_Max(leTas);
/*		if(testMax>leTas->tab[0])
		{
			printf("erreur");
			return EXIT_FAILURE;
		}
		testMax = leTas->tab[0];
*/
	}
	affiche_tas_Tableau(leTas);
	affiche_tas_Arbre(leTas);



	//Test de est_tas_max
/*		leTas->tab[4]=-1;
		affiche_tas_Tableau(leTas);
		affiche_Est_Tas_Max(leTas);
*/
//	printf("Retrait de tous les éléments sauf le dernier\n");

	testMax = leTas->tab[0];
	for(i=0; i<nbrElem-1; i++)
	{
		extraction(leTas);
//		affiche_tas_Arbre(leTas);
//		affiche_tas_Tableau(leTas);
//		affiche_Est_Tas_Max(leTas);


		if(testMax<leTas->tab[0])
		{
			printf("erreur");
			return EXIT_FAILURE;
		}
		testMax = leTas->tab[0];

	}

	printf("Extractions\n");

	affiche_tas_Arbre(leTas);

	printf("Retrait du plus grand élément, il valait %d\n", extraction(leTas));
	affiche_tas_Tableau(leTas);
	sommet(leTas);
	detruire_tas(leTas);

	//Test des fonctions adjacentes non liées à la structure

	printf("%d\n", max(5, 6));
	int x = 5, y = 19;
	swap (&x, &y);
	printf("%d %d\n", x, y);
	sommet(leTas);
	printf("2³ = %d\n", puissance(2,3));
	for(i=0; i<31; i++)
	{
		puissanceDe2[i]=puissance(2, i);
	}
	/*
	for(i=0; i<10; i++)
	{
		if(appartient(puissanceDe2, 31, i))
			printf("%d est une puissance de 2\n", i);
		else
			printf("%d n'est pas une puissance de 2\n", i);
	}*/

	return EXIT_SUCCESS;
}

//____________________________________
/*
Création d'un tas vide de taille m (et allocation de la mémoire)
*/

tas* init_tas(int m)
{
	tas* nouveau_Tas = (tas*) malloc(sizeof(tas));
	if(m>0)
	{
		nouveau_Tas->max = m;
	}
	else
	{
		nouveau_Tas->max = 1;
	}
	nouveau_Tas->n = 0;
	nouveau_Tas->tab = (int*) malloc(m * sizeof(int));
	return nouveau_Tas;
}

//____________________________________
/*
Impression (en largeur) du contenu d'un tas
*/

void affiche_tas_Tableau(tas* T)
{
	if(T->n>0)
	{
		printf("Voici le tas:\n");
		for(int i = 0; i<T->n;i++)
		{
			printf("Indice %3d : |%3d|\n", i, T->tab[i]);
		}
		printf("\n");
	}
	else
	{
		printf("Le tas est vide\n");
	}
}

//____________________________________

void affiche_tas_Arbre(tas* T)
{
	if(T->n)
	{
		int i,j,k,largeur=T->n*3;
		int puissancesDe2[31], divisions[31];
		//Int codés sur 4 octets, ça n'aurait pas de sens de dépasser 2³¹
		for(i=0; i<31; i++)
			puissancesDe2[i] = puissance(2,i);
		for(i=0; i<31; i++)
			divisions[i] = puissancesDe2[i]+1;
		for(i=0; i<T->n;i++)
		{
			for(j=0;j<puissance(2,i) && puissance(2,i)<T->n; j++)
			{
				if(j+puissance(2,i)-1 < T->n-1)
				{
					for(k=0; k<largeur/divisions[min(i, 30)];k++)
						printf(" ");
					printf("%3d", T->tab[j+puissance(2,i)-1]);
					if(j+puissance(2,i)==puissance(2,i+1)-1)
						printf("\n");
				}
			}
		}
		if(appartient(puissancesDe2, 31, T->n))
			printf("\n%3d", T->tab[T->n-1]);
		else
		{
			for(k=0; k<=largeur/divisions[min(i, 30)];k++)
				printf(" ");
			printf("%3d", T->tab[T->n-1]);
		}
	}
	else
	{
		printf("Le tas est vide\n");
	}
	printf("\n");
}

//____________________________________
/*
Fonction qui test si un tas est vraiment un tas max
Nécessite un 0 en deuxième argument quand elle est appelée pour la première fois car c'est l'indice du début
Renvoie 1 si c'en est un et 0 sinon
*/
int est_Tas_Max(tas* T, int i)
{
	// Le plus petit fils
	int f = i*2+1;
	if(f + 1 >= T->n)
	{
		if(f >= T->n)
		{
			return 1;
		}
		else
		{
			if(T->tab[i]>=T->tab[f])
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	if(T->tab[i]>=T->tab[f] && T->tab[i]>=T->tab[f+1] && est_Tas_Max(T, f) && est_Tas_Max(T, f+1))
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

//____________________________________
/*
Fonction qui affiche si un tas est un tas max en appelant la fonction est_Tas_Max
*/

void affiche_Est_Tas_Max(tas* T)
{
	if(est_Tas_Max(T, 0))
	{
		printf("Le tas est un tas max\n");
	}
	else
	{
		printf("Le tas n'est pas un tas max\n");
	}
}

//____________________________________
/*
Fonction qui renvoie le premoer élément d'un tas
*/
int tas_maximum(tas* T)
{
	return T->tab[0];
}

//____________________________________
/*
Fonction qui affiche si le tas est vide et sinon renvoie son sommet (son plus grand élément)
*/
void sommet(tas* T)
{
	if(T->n == 0)
	{
		printf("Le tas est vide\n");
	}
	else
	{
		printf("Le sommet du tas est : %d\n", tas_maximum(T));
	}
}
//____________________________________

/*
Fonction qui insère un entier k à une place appropriée dans le tas
*/

void tas_insertion(int k, tas* T)
{
//	affiche_tas_Arbre(T);
	int i=T->n;
	if(T->n == T->max)
	{
		T->max = T->max * 2;
		T->tab = realloc(T->tab, T->max*sizeof(int));
	}
	T->tab[T->n]=k;
//	affiche_tas_Arbre(T);
	int pere = (i-1)/2;

	while((pere >=0) && T->tab[pere] < T->tab[i])
	{
		swap(&T->tab[i], &T->tab[pere]);
		i = pere;
		pere = (i-1)/2;
//	affiche_tas_Arbre(T);
	}
	T->n++;
}

//____________________________________
/*
Fonction qui retire le premier élément d'un tas et met le nouveau plus grand élément à sa place en respectant les propriétés du tas
Renvoie la valeur du premier élément
*/
int extraction(tas* T)
{
	int i=0, res=T->tab[0], fils = 2*i+1;
	T->tab[0]=T->tab[T->n-1];
	int valFils=max(T->tab[fils], T->tab[fils+1]);
//	affiche_tas_Arbre(T);
	while(i < (T->n-1)/2 && T->tab[i] < valFils)
	{
		valFils=max(T->tab[fils], T->tab[fils+1]);
		if(T->tab[i] < valFils)
		{
			if(valFils == T->tab[fils])
			{
				swap(&T->tab[i], &T->tab[fils]);
//	affiche_tas_Arbre(T);
				i = fils;
				fils = 2*i+1;
			}
			else
			{
				swap (&T->tab[i], &T->tab[fils+1]);
//	affiche_tas_Arbre(T);
				i = fils + 1;
				fils = 2*i+1;
			}
		}
	}
	T->n--;
//	affiche_tas_Arbre(T);
	return res;
}

//____________________________________

void detruire_tas(tas* T)
{
	free (T->tab);
	free (T);
}

//____________________________________

int max(int x, int y)
{
	return (x<y) ? y : x;
}

//____________________________________

int min(int x, int y)
{
	return (x>y) ? y : x;
}

//____________________________________

int puissance(int x, int y)
{
	int res=1;
	for(int i = 0; i<y; i++)
	{
		res *= x;
	}
	return res;
}

//____________________________________

void swap(int* x, int* y)
{
	int tmp=*x;
	*x=*y;
	*y=tmp;
}

//____________________________________

int appartient(int* tab, int tabSize, int x)
{
	for(int i=0; i<tabSize; i++)
	{
		if(tab[i]==x)
			return 1;
	}
	return 0;
}
//____________________________________
