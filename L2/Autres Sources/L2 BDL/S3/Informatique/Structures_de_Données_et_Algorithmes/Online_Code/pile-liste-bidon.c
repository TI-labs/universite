#include <stdlib.h>
#include <stdio.h>

/* Declaration de la structure de la pile */

typedef struct cellule_st{
   int val;
   struct cellule_st * suivant;
}cell;

/*******************************************/

void push (cell *, int);     /* empiler */
int  pop  (cell *);          /* dépiler */

void exo1 (cell *, cell *, cell *);
void afficher_pile (cell *);


/************* ppal **********************/

int main ()
{

  /* On empile les éléments 5,3,2,1,4 dans la pile Pile1 et l'exo1 vide la Pile1 et laisse */
  /* les entiers pairs dans la pile Pile2 et les impairs dans la pile Pile3                            */

  cell * Pile1 = NULL;
  cell * Pile2 = NULL;
  cell * Pile3 = NULL;

  push(Pile1,5); /* on n'a pas alloue de la memoire à Pile1, donc ca ne marche pas */
  push(Pile1,3);
  push(Pile1,2);
  push(Pile1,1);
  push(Pile1,4);

  exo1(Pile1, Pile2, Pile3);
  printf("La pile 1 est : ");
  afficher_pile (Pile1); printf("\n");
  printf("La pile 2 est : ");
  afficher_pile (Pile2); printf("\n");
  printf("La pile 3 est : ");
  afficher_pile (Pile3); printf("\n");
  
  return 0;
}

/************ fonctionnes ****************/

/*****************************************/
void afficher_pile( cell * P)
{
  cell * Q = P;

  while (Q != NULL){
   printf("%d ",Q->val);
   Q = Q->suivant;
  }
}
/*****************************************/
void push(cell * P, int a)
{
   cell * new;

   new = (cell *) malloc (sizeof(cell));
   new->val = a;
   new->suivant = P;
   P = new;
}
/*****************************************/
int pop (cell * P)
{
  int x;
  cell * temp;

  if (P == NULL){
     printf("erreur pile vide\n");
     return -1;
  }
  else{
  x = P->val;
  temp = P;
  P = P->suivant;
  free(temp);
  return x;
  }
}

/*****************************************/
/* Exo 1.1 du TD 2                       */
/*****************************************/
void exo1 (cell * P1, cell * P2, cell * P3)
{
  while (P1 != NULL){
    if (P1->val % 2)
      push(P3, pop(P1));      /* ici les appels de pop et push se font directement */
    else push(P2, pop(P1));   /* sur P1, P2 et P3 qui sont déjà de type "cell **"   */
  }
}

