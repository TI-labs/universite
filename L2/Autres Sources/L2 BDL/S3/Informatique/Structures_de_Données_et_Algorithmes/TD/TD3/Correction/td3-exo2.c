#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef int E;

typedef struct cell_st{
   E val;
   struct cell_st * suivant;
}cell;

typedef cell* LList;

void inserer_tete(LList *, E);
void inserer_ordonne_it (LList *, E); /* exo 2.4 iteratif */
void inserer_ordonne_rec (LList *,E); /* exo 2.4 recursif */
LList concat_listes(LList, LList);    /* exo 2.3 */
void ordonner_liste(LList *);         /* exo 2.10 */
void invertir_liste (LList *, int *, LList *);        /* exo 2.13 */

/* fonctions auxilieres */

int taille (LList);
void afficher_liste (LList);


/*******************************************/

/* programme ppal pour tester les fonctions */

int main ()
{
  LList L1 = NULL;
  LList L2 = NULL;
  LList L3 = NULL;
  LList L4 = NULL;
  LList L5 = NULL;
  
  inserer_tete(&L1,5);
  inserer_tete(&L1,3);
  afficher_liste(L1); 
  
  inserer_tete(&L2,1);
  inserer_tete(&L2,2);
  afficher_liste(L2); 

  L3 = concat_listes(L1,L2);
  printf("\n liste L3 concatene\n");
  afficher_liste(L3); 

  ordonner_liste(&L3);
  printf("\n liste L3 ordonnee\n");
  afficher_liste(L3); 

  inserer_ordonne_it(&L4,5);
  inserer_ordonne_it(&L4,3);
  inserer_ordonne_it(&L4,6);
  inserer_ordonne_it(&L4,4);
  printf("\n Liste L4 ordonnée \n");
  afficher_liste(L4);

  inserer_ordonne_rec(&L5,5);
  inserer_ordonne_rec(&L5,3);
  inserer_ordonne_rec(&L5,6);
  inserer_ordonne_rec(&L5,4);
  printf("\n Liste L5 ordonnée \n");
  afficher_liste(L5);

  int x = 0;
  LList C = L5;
  invertir_liste(&L5,&x,&C);
  printf("\n Liste L5 inversée \n");
  afficher_liste(L5);

  return EXIT_SUCCESS;
}

/*******************************************/
void invertir_liste (LList * L, int * x, LList *C)
{
  LList A,B;

  if (*L != NULL)
  { if  ((*L)->suivant == NULL) *C = *L;
    else{
      A = *L;
      B = A->suivant;
      (*x)++;
      invertir_liste (&B, x, C);
      (*x)--;
      B->suivant = A;
      A->suivant = NULL;
      if (*x == 0) *L = *C;
    }
  }
}

/*******************************************/
void ordonner_liste (LList * L)
{
  LList A,B;
  E x;

  A = *L;
  while (A != NULL)
  {
    B = A;
    while (B->suivant != NULL)
    {
      if (B->suivant->val < A->val)
      {
        x = A->val;
        A->val = B->suivant->val;
        B->suivant->val = x;
      }
      B = B->suivant;
    }
    A = A->suivant;
  }
}
/*******************************************/
void inserer_ordonne_rec (LList * L, E x)
{
  if (*L == NULL || (*L)->val >= x) inserer_tete(L,x);
  else{
    LList A = (*L)->suivant;
    inserer_ordonne_rec(&A,x);
    (*L)->suivant = A;
  }
}
/*******************************************/
void inserer_ordonne_it (LList *L, E x)
{
  LList A,B;

  LList T;
  inserer_tete(&T,x);
  
  if (*L == NULL) *L = T;
  else
  {
    if ((*L)->val >= x) {
     T->suivant = *L; *L = T;
    }
    else {
     A = *L;
     while (A != NULL && A->val < x) {B=A; A = A->suivant;}
     B->suivant = T;T->suivant = A;
    }
  }
}
/*******************************************/
LList concat_listes(LList L1, LList L2)
{
  LList R = NULL;
  LList T = L1;
  LList Q;

  while (T != NULL) {
   inserer_tete(&R,T->val);
   T = T->suivant;
  }
  T = L2;
  while (T != NULL) {
   inserer_tete(&R,T->val);
   T = T->suivant;
  }
  T = NULL;
  while (R != NULL){
   inserer_tete(&T,R->val);
   Q = R;
   R = R->suivant;
   free(Q);
  }
  return T;
}
/*******************************************/
void inserer_tete (LList * L, E x)
{
  LList R = (LList) malloc(sizeof(LList));
  
  R->val = x;
  R->suivant = *L;
  *L = R;
}
/*******************************************/
void afficher_liste(LList L)
{
  while (L != NULL)
  {
    printf("%d ",L->val);
    L = L->suivant;
  }
  printf("\n");
}
/*******************************************/
int taille (LList L)
{
  if (L == NULL) return 0;
  return taille(L->suivant) + 1; 
}
