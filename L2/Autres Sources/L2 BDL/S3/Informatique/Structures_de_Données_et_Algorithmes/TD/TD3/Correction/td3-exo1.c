#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef char E;

typedef struct {
   E* val;
   int capacite;
   int taille;
}tableau;


/* on passe le tableau comme parametre dans toutes les fonctions. */
/* Ca evite de declarer une variable "globale" de type pointeur à tableau */

void afficher_tableau (tableau *);

/* exo 1.1 */
tableau * creer_tab_alea (int);  

/* exo 1.2 */

E get (int, tableau *);
E set (int, E, tableau *);
void add (int, E, tableau *);
E removes (int, tableau *);

/*******************************************/

/* programme ppal pour tester les fonctions */

int main ()
{
  tableau * T;

  srand(time(NULL)); /* initialisation du generateur aleatoire de c */

  /* test exo 1.1 */
  T = creer_tab_alea(5);
  afficher_tableau(T);

  /* test exo 1.2.1 et 1.2.2 */
  printf ("\n element %d dans T : %c \n", 3,get(3,T));
  printf ("\n element %d dans T = %c, change à %c :\n", 3, set(3,'t',T), 't');
  afficher_tableau(T);

  /* test exo 1.2.3 */

  add(3,'m',T);
  add(4,'a',T);
  add(7,'r',T);

  printf("taille %d capacite %d \n", T->taille, T->capacite);
  afficher_tableau(T);

 /* test exo 1.2.4 */

  printf("caracter %c en position %d elimine\n",removes(4,T),4);
  printf("taille %d capacite %d \n", T->taille, T->capacite);

  printf("caracter %c en position %d elimine\n",removes(4,T),4);
  printf("taille %d capacite %d \n", T->taille, T->capacite);

  printf("caracter %c en position %d elimine\n",removes(4,T),4);
  printf("taille %d capacite %d \n", T->taille, T->capacite);
  
  printf("caracter %c en position %d elimine\n",removes(2,T),2);
  printf("taille %d capacite %d \n", T->taille, T->capacite);
  afficher_tableau(T);

  return EXIT_SUCCESS;
}

/*******************************************/

tableau * creer_tab_alea (int m)
{
  int i;
  E c;
  tableau * R = (tableau *) malloc (sizeof(tableau));

  R->capacite = m;
  R->taille = 0;
  R->val = (E *)malloc(sizeof(E)*m);
  for (i=0; i < R->capacite; i++)
  {

    /* code ascii : majuscules 65 à 90 et minuscules 97 à 122 */

    c = rand() % 123;
    while (c < 65 || (c > 90 && c < 97)) c = rand() % 123;
    R->val[R->taille] = c;
    R->taille++;
  } 
  return R;
}

/*******************************************/
E get (int i, tableau * T)
{

  if ((0 > i) || (i >= T->taille)){
    printf("\n position inexistante \n");
    return 33; /* character d'exclamation */
  }
  return T->val[i];
}
/*******************************************/
E set (int i, E x, tableau * T)
{
  E c;
  if ((0 > i) || (i >= T->taille)){
    printf("\n position inexistante \n");
    return 33; /* character d'exclamation */
  }
  c = T->val[i];
  T->val[i] = x;
  return c; 
}
/*******************************************/
void add (int i, E x, tableau * T)
{
  if ((0 > i) || (i > T->taille))
    printf("\n position incorrecte \n");
  else {
    if (T->taille == T->capacite)
    {
      T->capacite *= 2;
      T->val = realloc(T->val, T->capacite);
    }
    if (i < T->taille)
    {
    
      for (int j = i; j < T->taille; j++)
        x = set(j,x,T);
      T->val[T->taille] = x;
    }
    else T->val[i] = x;
    T->taille++;
  }
}

/*******************************************/
E removes (int i, tableau * T)
{
  E x;
 
  if ((0 > i) || (i >= T->taille)){
    printf("\n position incorrecte \n");
    return 33;
  }
  x = get(i,T);
  if (i < T->taille - 1)
    for (int j=i; j < T->taille - 1; j++)
      set(j,T->val[j+1],T);
  T->taille --;
  
  /* optimisation de la memoire */

  if (2*T->taille < T->capacite) {
    T->capacite = 2*T->taille;
    T->val = realloc(T->val,T->capacite);
  } 
  return x;
}
/*******************************************/
void afficher_tableau (tableau * R)
{
  for (int i=0; i< R->taille; i++)
   printf("%c ",R->val[i]);
  printf("\n");
}
