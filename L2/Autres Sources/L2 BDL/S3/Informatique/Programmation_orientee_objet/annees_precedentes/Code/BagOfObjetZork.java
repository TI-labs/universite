package examjanvier2018;

import java.util.Collection;
import java.util.Iterator;

import examjanvier2016bis.ObjetZork;

/**
 * LinkedListCollection d'ObjetZork possédant quelques méthodes supplémentaires
 * permettant de faciliter la gestion des ObjetZork compte tenu de leurs
 * caractéritisques telles que le poids ou le fait d'être ou non transportables.
 * 
 * @author Marc Champesme
 * @since 30 decembre 2017
 * @version 30 decembre 2017
 *
 */
public class BagOfObjetZork extends LinkedListCollection<ObjetZork> {
	/**
	 * Initialise un nouveau BagOfObjetZork vide.
	 * 
	 * @ensures this.isEmpty();
	 */
	public BagOfObjetZork() {

	}

	/**
	 * Initialise un nouveau BagOfObjetZork contenant les objets contenus dans la
	 * Collection spécifiée.
	 * 
	 * @param c
	 *            la Collection dont les éléments doivent être placés dans ce
	 *            BagOfObjetZork.
	 * 
	 * @requires c != null;
	 * @requires !c.contains(null);
	 * @ensures this.hasSameContentAs(c);
	 * 
	 * @param c
	 *            the collection whose elements are to be placed into this
	 *            BagOfObjetZork
	 * 
	 * @throws NullPointerException
	 *             if the specified collection is null or contains the null element.
	 */
	public BagOfObjetZork(Collection<? extends ObjetZork> c) {
		super(c);
	}

	/**
	 * Renvoie les ObjetZork transportables présents dans ce BagOfObjetZork.
	 * 
	 * @ensures \result != null;
	 * @ensures (\forall ObjetZork oz ; \result.contains(oz) ; oz.estTransportable()
	 *          && this.contaisn(oz));
	 * @ensures (\forall ObjetZork oz ; this.contains(oz) && oz.estTransportable() ;
	 *          \result.contains(oz));
	 * 
	 * @return une Collection d'ObjetZork contenant tous les ObjetZork
	 *         transportables de ce BagOfObjetZork
	 * 
	 * @pure
	 */
	public Collection<ObjetZork> getTransportableObjects() {
		Collection<ObjetZork> selection = new BagOfObjetZork();
		for (ObjetZork oz : this) {
			if (oz.estTransportable()) {
				selection.add(oz);
			}
		}
		return selection;
	}

	/**
	 * Transfert les ObjetZork transportables de ce BagOfObjetZork vers la
	 * Collection spécifiée dans la limite du poids maximal spécifié. Les objets
	 * transférés sont retirés de ce BagOfObjetZork et ajouté dans la Collection
	 * spécifiée.
	 * 
	 * Les ObjetZork présents dans ce BagOfObjetZork sont parcourus dans un ordre
	 * non spécifié (cet ordre peut, par exemple, être celui dans lequel les
	 * éléments sont retournés par l'itérateur) ; pour chaque ObjetZork
	 * transportable rencontré, l'ObjetZork est transféré si son ajout ne conduit
	 * pas à ce que le poids total des ObjetZork transférés dépasse le poids maximal
	 * spécifié. Lorsque qu'un ObjetZork transportable ne peut être transféré en
	 * raison de son poids, le parcours se poursuit à l'élément suivant.
	 * 
	 * @requires c != null;
	 * @requires maxWeight >= 0;
	 * @ensures \result >= 0 && \result <= c.size();
	 * @ensures \result == (c.size() - \old(c.size()));
	 * @ensures \result == (\old(this.size()) - this.size());
	 * @ensures (\old(getWeightOfTransportableObjects()) <= maxWeight) ==>
	 *          c.containsAll(\old(getTransportableObjects()));
	 * @ensures (\old(getWeightOfTransportableObjects()) <= maxWeight) ==>
	 *          getTransportableObjects().isEmpty();
	 * 
	 * @param c
	 *            la Collection dans laquelle les ObjetZork transférés sont ajoutés
	 * @param maxWeight
	 *            le poids total maximal des ObjetZork transférés.
	 * @return le nombre d'ObjetZork effectivement transférés
	 * 
	 * @throws NullPointerException
	 *             si la Collection spécifiée est null.
	 * @throws IllegalArgumentException
	 *             si le poids maximal spécifié est strictement négatif.
	 * 
	 */
	public int transferTransportableObjectsTo(Collection<? super ObjetZork> c, int maxWeight) {
		if (c == null) {
			throw new NullPointerException("La collection spécifiée doit être non null");
		}
		if (maxWeight < 0) {
			throw new IllegalArgumentException("Le poids maximal spécifié doit être positif");
		}
		int nbElt = 0;
		int currentWeight = 0;
		Iterator<ObjetZork> iter = this.iterator();
		while (iter.hasNext()) {
			ObjetZork oz = iter.next();
			if (oz.estTransportable() && (currentWeight + oz.getPoids()) <= maxWeight) {
				c.add(oz);
				iter.remove();
				nbElt++;
				currentWeight += oz.getPoids();
			}
		}
		return nbElt;
	}

	/**
	 * Reenvoie le poids total des ObjetZork transportables présents dans ce
	 * BagOfObjetZork.
	 * 
	 * @return le poids total des ObjetZork transportables présents dans ce
	 *         BagOfObjetZork
	 * 
	 * @ensures \result == (\sum ObjetZork oz ; this.contains(oz) &&
	 *          oz.estTransportable() ; oz.getPoids());
	 * 
	 * @pure
	 */
	public int getWeightOfTransportableObjects() {
		int poids = 0;
		for (ObjetZork oz : this) {
			if (oz.estTransportable()) {
				poids += oz.getPoids();
			}
		}
		return poids;
	}

}
