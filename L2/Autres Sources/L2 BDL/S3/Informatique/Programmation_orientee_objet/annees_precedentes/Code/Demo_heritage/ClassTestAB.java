
/**
 * Write a description of class ClassTestAB here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ClassTestAB {

    public static void main(String[] args) {
        ClassA a = new ClassA("Instance de ClassA", 5);
        ClassB b = new ClassB("Instance de ClassB", 5);
        ClassA ab = b;
        
        System.out.println("Appel de a.methodRedefinie(7):");
        System.out.println("Resultat de a.methodRedefinie(7)->" + a.methodRedefinie(7));
        System.out.println("-------------------");
        System.out.println("Appel de b.methodRedefinie(7):");
        System.out.println("Resultat de b.methodRedefinie(7)->" + b.methodRedefinie(7));
        System.out.println("-------------------");
        System.out.println("Appel de ab.methodRedefinie(7):");
        System.out.println("Resultat de ab.methodRedefinie(7)->" + ab.methodRedefinie(7));
        System.out.println("-------------------");
        System.out.println("a=" + a);
        System.out.println("b=" + b);
        System.out.println("ab=" + ab);
        System.out.println("-------------------");
        System.out.println("b.x=" + b.x);
        System.out.println("ab.x=" + ab.x);   
    }
}
