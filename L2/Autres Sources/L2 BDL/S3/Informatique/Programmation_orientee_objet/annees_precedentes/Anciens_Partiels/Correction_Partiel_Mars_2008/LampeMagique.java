/**
 * Dans le contexte du jeu Zork, une lampe magique est un objet qui permet
 * d'éclairer une pièce pendant une durée limitée. Un joueur qui possède un tel
 * objet peut éclairer une pièce (par exemple pour pouvoir lire un message
 * laissé dans la pièce) en l'activant. Une fois la lampe activée, elle dégage
 * de la lumière pendant un nombre de tours de jeu fixé à l'avance. Une fois ce
 * nombre de tours de jeu écoulé la lampe s'éteint définitivement et ne pourra
 * plus être réactivée.
 * <p>
 * Une lampe magique est entièrement caractérisée par sa couleur (une chaîne de
 * caractères non {@code null}), son poids et sa durée de vie (deux entiers
 * strictement positifs) et le fait qu'elle soit activée ou non.
 * </p>
 * 
 * @invariant getCouleur() != null;
 * @invariant getPoids() > 0;
 * @invariant getDureeRestante() >= 0;
 * @invariant estActive() ==> (getDureeRestante() > 0);
 * @invariant (getDureeRestante() == 0) ==> !estActive();
 * 
 * @author Marc Champesme
 * @since 4 mars 2008
 * @version 22 mars 2008
 * 
 */
public class LampeMagique implements Cloneable {
	private String couleur;
	private int poids;
	private boolean estActive;
	private int dureeVie;

	/**
	 * Initialise une nouvelle {@code LampeMagique} caractérisée par la couleur, le
	 * poids et la durée de vie spécifiées.
	 * 
	 * @requires couleur != null;
	 * @requires (poids > 0) && (dureeVie > 0);
	 * @ensures getCouleur().equals(couleur);
	 * @ensures getPoids() == poids;
	 * @ensures getDureeRestante() == dureeVie;
	 * @ensures !estActive();
	 * 
	 * @param couleur
	 *            Une chaîne de caractères représentant une couleur.
	 * @param poids
	 *            Un entier strictement positif représentant le poids de cette
	 *            {@code LampeMagique}.
	 * @param dureeVie
	 *            Un entier strictement positif représentant la durée de vie de
	 *            cette {@code LampeMagique}.
	 */
	public LampeMagique(String couleur, int poids, int dureeVie) {
		this.couleur = couleur;
		this.poids = poids;
		this.dureeVie = dureeVie;
	}

	/**
	 * Renvoie la couleur de cette <code>LampeMagique</code>.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return La couleur de cette <code>LampeMagique</code>.
	 */
	public String getCouleur() {
		return this.couleur;
	}

	/**
	 * Renvoie le poids de cette <code>LampeMagique</code>.
	 * 
	 * @return Le poids de cette <code>LampeMagique</code>.
	 */
	public int getPoids() {
		return this.poids;
	}

	/**
	 * Renvoie la durée de vie restante de cette <code>LampeMagique</code>.
	 * 
	 * @return La durée de vie restante de cette <code>LampeMagique</code>.
	 */
	public int getDureeRestante() {
		return this.dureeVie;
	}

	/**
	 * Renvoie {@code true} si et seulement si cette {@code LampeMagique} est activée.
	 * 
	 * @return {@code true} si et seulement si cette {@code LampeMagique} est activée ;
	 *         {@code false} sinon.
	 */
	public boolean estActive() {
		return this.estActive;
	}

	/**
	 * Permet de faire passer la lampe de l'état inactif à l'état actif (aucune
	 * méthode ne permet de faire passer la lampe à l'état inactif: la lampe
	 * s'inactive d'elle même dès que sa durée de vie devient nulle). Cette
	 * méthode ne doit être appelée qu'une fois pour chaque lampe: elle est sans
	 * effet sur une lampe qui a déjà été activée une fois.
	 * 
	 * @ensures estActive() <==> (getDureeRestante() > 0);
	 */
	public void activer() {
		if (dureeVie > 0) {
			this.estActive = true;
		}
	}

	/**
	 * Doit être appelée à chaque tour de jeu quand cette lampe est activée.
	 * Chaque appel à cette méthode diminue la durée de vie de cette lampe d'une
	 * unité et renvoie la durée de vie restante de cette lampe. Lorsque l'appel
	 * de cette méthode fait passer la durée de vie de cette lampe à 0, cet
	 * appel de méthode provoque le passage de cette lampe à l'état inactif. Si
	 * cette lampe n'est pas activée ou que sa durée de vie est nulle les appels
	 * à cette méthode sont sans effet et la valeur retournée est 0.
	 * 
	 * @ensures (!\old(estActive()) || (getDureeRestante() == 0)) ==> ((\result ==
	 *          0) && !estActive());
	 * @ensures estActive() ==> (\result == getDureeRestante());
	 * @ensures \old(estActive()) ==> (getDureeRestante() ==
	 *          \old(getDureeRestante() - 1));
	 * @ensures (\old(estActive()) && (\result == 0)) ==> !estActive();
	 * @ensures (\old(estActive()) && (\result > 0)) ==> estActive();
	 * 
	 * @return Renvoie la durée de vie restante de cette {@code Lampe}.
	 */
	public int eclairer() {
		if (!estActive) {
			return 0;
		}
		dureeVie--;
		if (dureeVie == 0) {
			estActive = false;
		}
		return this.dureeVie;
	}

	/**
	 * Deux instances de la classe {@code LampeMagique} seront considérées
	 * comme {@code equals} si elles possèdent les mêmes caractéristiques: même
	 * couleur, même poids, même durée de vie et même état (i.e. actif ou
	 * inactif).
	 * 
	 * @ensures !(o instanceof LampeMagique) ==> !\result;
	 * @ensures ((o instanceof LampeMagique)
	 * 		&& (getCouleur().equals(((LampeMagique) o).getCouleur()))
	 * 		&& (getPoids() == ((LampeMagique) o).getPoids())
	 * 		&& (getDureeRestante() == ((LampeMagique) o).getDureeRestante()) 
	 * 		&& (estActive() == ((LampeMagique) o).estActive()))
	 * 		<==> \result;
	 * 
	 * @param o l'objet à comparer avec cette {@code LampeMagique}.
	 * @return {@code true} si le paramètre est une {@code LampeMagique} de mêmes
	 * caractéristiques ; {@code false} sinon.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof LampeMagique)) {
			return false;
		}
		LampeMagique lampe = (LampeMagique) o;
		if (!getCouleur().equals(lampe.getCouleur())) {
			return false;
		}
		if ((getPoids() != lampe.getPoids())
				|| getDureeRestante() != lampe.getDureeRestante()) {
			return false;
		}
		if (estActive() != lampe.estActive()) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public LampeMagique clone() {
		Object leClone = null;
		try {
			leClone = super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError();
		}
		return (LampeMagique) leClone;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int code = getCouleur().hashCode();
		code = 31 * code + getPoids();
		code = 31 * code + getDureeRestante();
		code = 31 * code + (estActive() ? 1 : 0);
		return code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String str = "LampeMagique:{";
		str += getCouleur() + ":";
		str += getPoids() + ":";
		str += getDureeRestante() + ":";
		str += (estActive() ? "active" : "inactive");
		return str + "}";
	}
}
