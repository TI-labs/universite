/**
 *  Polynomes non modifiables &agrave; coefficients entiers de la forme c0 +
 *  c1*x +c2*x^2 + ...
 *
 *
 * @author     Marc Champesme
 * @version    6 mars 2011
 * @since      31 janvier 2006
 */
public class PolynomeDense extends Polynome {
        private int[] coefficients;

        /**
         *  Initialise un polynome nul.
         *
         * @ensures estZero();
         */
        /*@
          @ ensures estZero();
          @*/
        public PolynomeDense() {
        	super(0);
                coefficients = new int[1];
        }

        /**
         * Initialise un <code>PolynomeCreux</code>
         * equals au <code>Polynome</code> spécifié. Le <code>Polynome</code> spécifié
         * doit être différent de <code>null</code>.
         *
         * @param  p  <code>Polynome</code> correspondant au nouveau <code>PolynomeCreux</code>.
         *
         * @requires p != null;
         * @ensures getDegre() == p.getDegre();
         * @ensures (\forall int exp; exp >= 0 && exp <= p.getDegre();
         *		getCoefficient(exp) == p.getCoefficient(exp));
         */
        /*@
          @ requires p != null;
          @ ensures getDegre() == p.getDegre();
          @ ensures (\forall int exp; exp >= 0 && exp <= p.getDegre();
          @		getCoefficient(exp) == p.getCoefficient(exp));
        @*/
        public PolynomeDense(Polynome p) {
                super(p.getDegre());
                coefficients = new int[p.getDegre() + 1];
                for (int exp = 0; exp <= p.getDegre(); exp++) {
                        coefficients[exp] = p.getCoefficient(exp);
                }
        }

        /**
         *  Initialise un polynome composé d'un unique terme d'exposant exp et de
         *  coefficient coeff non nul.
         *
         * @param  coeff  Coefficient du terme d'exposant exp du nouveau polynome
         * @param  exp    exposant de l'unique terme de ce polynome
         *
         * @requires coeff != 0;
         * @requires exp >= 0;
         * @ensures getDegre() == exp;
         * @ensures getCoefficient(exp) == coeff;
         * @ensures !estZero();
         */
        /*@
	  @ requires coeff != 0;
	  @ requires exp >= 0 && exp < (Integer.MAX_VALUE / 2);
	  @ ensures getDegre() == exp;
	  @ ensures getCoefficient(exp) == coeff;
	  @ ensures !estZero();
        @*/
        public PolynomeDense(int coeff, int exp) {
                super(exp);
                coefficients = new int[exp + 1];
                coefficients[exp] = coeff;
        }
        
        /**
         *
         * @param coeffs Coefficents de ce Polynome.
         * @param degre Le degré de ce Polynome.
         *
         * @requires coeffs != null;
         * @requires exp >= 0;
         * @requires coeffs.length > exp;
         * @requires (exp > 0) ==> (coeffs[exp] != 0);
         * @ensures getDegre() == exp;
         * @ensures (\forall int i; i >= 0 && i <= exp;
         *		getCoefficient(i) == coeffs[i]);
         */
        /*@
          @ requires coeffs != null;
          @ requires degre >= 0;
          @ requires coeffs.length > degre;
          @ requires (degre > 0) ==> (coeffs[degre] != 0);
          @ ensures getDegre() == degre;
          @ ensures (\forall int i; i >= 0 && i <= degre;
          @		getCoefficient(i) == coeffs[i]);
          @*/
        public PolynomeDense(int[] coeffs, int degre) {
                super(degre);
                coefficients = new int[degre + 1];
                for (int i = 0; i <= degre; i++) {
                	coefficients[i] = coeffs[i];
                }
        }

        public Polynome additionner(Polynome p) {
                if (estZero()) {
                        return p;
                }
                if (p.estZero()) {
                        return this;
                }
                int degreMax;
                degreMax = Math.max(getDegre(), p.getDegre());
                int[] addcoeff = new int[degreMax + 1];
                int addDegre = 0;
                for (int exp = 0; exp <= degreMax; exp++) {
                        int sommeCoeff = getCoefficient(exp) + p.getCoefficient(exp);
                        if (sommeCoeff != 0) {
                                addcoeff[exp] = sommeCoeff;
                                addDegre = exp;
                        }
                }
                return new PolynomeDense(addcoeff, addDegre);
        }

        public int getCoefficient(int exposant) {
                if (exposant > getDegre()) {
                        return 0;
                }
                return coefficients[exposant];
        }
}

