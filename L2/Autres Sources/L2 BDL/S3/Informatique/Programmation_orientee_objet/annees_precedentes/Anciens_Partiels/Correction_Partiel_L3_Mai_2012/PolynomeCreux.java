import java.util.List;
import java.util.ArrayList;

/**
 *  Polynomes non modifiables &agrave; coefficients entiers de la forme c0 +
 *  c1*x +c2*x^2 + ...
 *
 * @author     Marc Champesme
 * @version    17 mars 2012
 * @since      mars 2012
 */
public class PolynomeCreux extends Polynome {
        private ArrayList<Monome> termes;

        /**
         *  Initialise un <code>PolynomeCreux</code> nul.
         *
         * @ensures estZero();
         */
        /*@
          @ ensures estZero();
        @*/
        public PolynomeCreux() {
        	super(0);
                termes = new ArrayList<Monome>(0);
        }

        /**
         *
         * @param lm Liste des termes de ce Polynome.
         * @param degre Le degré de ce Polynome.
         * 
         * @requires lm != null;
         * @requires !lm.contains(null);
         * @requires degre >= 0;
         * @requires !lm.isEmpty() ==> (degre == lm.get(lm.size() - 1).getExposant());
         * @requires lm.isEmpty() ==> (degre == 0);
         * @requires (\forall int i; i >= 0 && i < lm.size();
         *		lm.get(i).getCoefficient() != 0);
         * @requires (\forall int i; i > 0 && i < lm.size();
         *		lm.get(i).getExposant() > lm.get(i-1).getExposant());
         * @ensures lm.isEmpty() ==> estZero();
         * @ensures !lm.isEmpty() ==> (getDegre() == degre);
         * @ensures (\forall int i; i >= 0 && i < lm.size();
         *		getCoefficient(lm.get(i).getExposant()) == lm.get(i).getCoefficient());
         */
        /*@
          @ requires lm != null;
          @ requires !lm.contains(null);
          @ requires degre >= 0;
          @ requires !lm.isEmpty() ==> (degre == lm.get(lm.size() - 1).getExposant());
          @ requires lm.isEmpty() ==> (degre == 0);
          @ requires (\forall int i; i >= 0 && i < lm.size();
          @		lm.get(i).getCoefficient() != 0);
          @ requires (\forall int i; i > 0 && i < lm.size();
          @		lm.get(i).getExposant() > lm.get(i-1).getExposant());
          @ ensures lm.isEmpty() ==> estZero();
          @ ensures getDegre() == degre;
          @ ensures (\forall int i; i >= 0 && i < lm.size();
          @		getCoefficient(lm.get(i).getExposant()) == lm.get(i).getCoefficient());
          @*/
        public PolynomeCreux(List<Monome> lm, int degre) {
        	super(lm.isEmpty() ? 0 : lm.get(lm.size() - 1).getExposant());
        	termes = new ArrayList<Monome>(lm);
        }
        
        /**
         * Initialise un <code>PolynomeCreux</code>
         * equals au <code>Polynome</code> spécifié. Le <code>Polynome</code> spécifié
         * doit être différent de <code>null</code>.
         *
         * @param  p  <code>Polynome</code> correspondant au nouveau <code>PolynomeCreux</code>.
         *
         * @requires p != null;
         * @ensures getDegre() == p.getDegre();
         * @ensures (\forall int exp; exp >= 0 && exp <= p.getDegre();
         *		getCoefficient(exp) == p.getCoefficient(exp));
         */
        /*@
          @ requires p != null;
          @ ensures getDegre() == p.getDegre();
          @ ensures (\forall int exp; exp >= 0 && exp <= p.getDegre();
          @		getCoefficient(exp) == p.getCoefficient(exp));
        @*/
        public PolynomeCreux(Polynome p) {
                super(p.getDegre());
                termes = new ArrayList<Monome>();
                for (int exp = 0; exp <= p.getDegre(); exp++) {
                        int coeff = p.getCoefficient(exp);
                        if (coeff != 0) {
                                termes.add(new Monome(coeff, exp));
                        }
                }
        }
        
        public Polynome additionner(final Polynome p) {
                if (estZero()) {
                        return p;
                }
                if (p.estZero()) {
                        return this;
                }
                ArrayList<Monome> addPolyTermes = new ArrayList<Monome>();
                int expOther = 0;
                int expThis = 0;
                for (Monome thisTerme : this.termes) {
                        int sommeCoeff;
                        expThis = thisTerme.getExposant();
                        while ((expOther < expThis) && (expOther <= p.getDegre())) {
                                sommeCoeff = p.getCoefficient(expOther);
                                if (sommeCoeff != 0) {
                                        addPolyTermes.add(new Monome(sommeCoeff, expOther));
                                }
                                expOther++;
                        }
                        //@ assume (expOther == thisTerme.getExposant()) || (expOther == (p.getDegre() + 1));
                        sommeCoeff = thisTerme.getCoefficient();
                        if (expOther <= p.getDegre()) {
                                sommeCoeff += p.getCoefficient(expOther);
                                expOther++;
                        }
                        if (sommeCoeff != 0) {
                                addPolyTermes.add(new Monome(sommeCoeff, expThis));
                        }
                }
                //@ assume expThis == this.getDegre();
                //@ assume (expOther == (this.getDegre() + 1)) || (expOther == (p.getDegre() + 1));
                //@ assume (\forall int exp; exp >= 0 && exp <= this.getDegre() && ((this.getCoefficient(exp) + p.getCoefficient(exp)) != 0); addPolyTermes.contains(new Monome(this.getCoefficient(exp) + p.getCoefficient(exp), exp)));
                while (expOther <= p.getDegre()) {
                        int coeff = p.getCoefficient(expOther);
                        if (coeff != 0) {
                                addPolyTermes.add(new Monome(coeff, expOther));
                        }
                        expOther++;
                }
                int addDegre = 0;
                if (!addPolyTermes.isEmpty()) {
                	addDegre = addPolyTermes.get(addPolyTermes.size() - 1).getExposant();
                }
                return new PolynomeCreux(addPolyTermes, addDegre);
        }

        public double evaluer(double x) {
                double somme = 0;
                for (Monome m : this.termes) {
                        somme += m.getCoefficient() * Math.pow(x, m.getExposant());
                }
                return somme;
        }

        public int getCoefficient(int exposant) {
                if (exposant > getDegre()) {
                        return 0;
                }
                for (Monome m : termes) {
                        if (m.getExposant() == exposant) {
                                return m.getCoefficient();
                        }
                        if (m.getExposant() > exposant) {
                                return 0;
                        }
                }
                return 0;
        }
}

