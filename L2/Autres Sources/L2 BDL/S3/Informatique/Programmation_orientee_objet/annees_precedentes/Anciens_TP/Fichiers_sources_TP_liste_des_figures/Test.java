import java.awt.Point;
/**
 *Modélise un Carre
 *
 * @author Valentina Dragos
 * @version 19 Mars 2007
 */
 /**
  * Un carré est un rectangle dont la hauteur et la largeur sont 
  * identiques et sont égales au côté du carré.
  */
  
public class Test {

		public static void main (String [] args)
		{
		Rectangle rect1 = new Rectangle();
		Rectangle rect2 = new Rectangle();
		 
		ListeTableau<Figure> listTabFig = new ListeTableau<Figure>();    // ligne 1
		Liste<Figure> listFig = listTabFig;                              // ligne 2
		ListeTableau<Figure> listTCFig = new ListeTableau<Figure>();     // ligne 3
		Liste<Rectangle> listRect = new ListeTableau<Rectangle>();     // ligne 4
		
		listFig.ajouter(new Rectangle());                         // ligne 5
		listFig.ajouter(new Cercle());                            // ligne 6
		
		listRect.ajouter(new Carre());                            // ligne 7
		listRect.ajouter(new Rectangle());                        // ligne 8
		listRect.ajouter(new Polygone());                         // ligne 9
		
		listFig = listRect;                                          // ligne 10
		listFig.ajouterTout(listRect);                               // ligne 11
		listRect.ajouter((Rectangle) listFig.get(0));                // ligne 12
		
		Liste<Object> listObj = new ListeTableau<Object>();          // ligne 13
		
		listObj.ajouter(new Rectangle()); 								// ligne 14
	   listObj.ajouter(new Cercle());    								// ligne 15
		
		if (listFig.contientTout(listObj)) {            				// ligne 16
		}
		
		Liste<?> listInc = listRect;                    // ligne 17
		listInc.ajouter(new Rectangle());            // ligne 18
		Rectangle rect = listInc.get(0);                // ligne 19
		
		Liste<? extends Rectangle> listIncR = listRect; // ligne 20
		listIncR.ajouter(new Rectangle());           // ligne 21
		rect = (Rectangle) listIncR.get(0);             // ligne 22
		
		Liste<? super Rectangle> listIncSR = listRect;  // ligne 23
		listIncSR.ajouter(new Rectangle());          // ligne 24
		rect = (Rectangle) listIncSR.get(0);            // ligne 25

		}
		
}