public class ObjetBonus extends ObjetZork{
  private int points;

  public ObjetBonus()
  {
    super("nom", 0);
  }

  public ObjetBonus(String nom, int points)
  {
    super(nom, 0);
    if(points < 0)
      throw new IllegalArgumentException();
    this.points= points;
  }

  public int getPoints()
  {
    return points;
  }

  public boolean equals(Object o)
  {
    if(! (o instanceof ObjetBonus))
      return false;
    ObjetBonus objB = (ObjetBonus) o;
    return (objB.getPoints() == getPoints()) && (objB.getNom().equals(getNom()));
  }

  public int hashCode()
  {
    return getPoids() + getNom().hashCode();
  }
  public String toString()
  {
    return "ObjetBonus : Nom : " + getNom() + " Points : " + getPoints() + "\n";
  }
}
