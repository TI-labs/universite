public abstract class ObjetZork{
  private String nom;
  private int poids;

  public ObjetZork()
  {
    this.nom="nom";
    this.poids=1;
  }

  public ObjetZork(String nom, int poids)
  {
    if(nom == null)
      throw new NullPointerException();
    if(poids < 0)
      throw new IllegalArgumentException();
    this.nom=nom;
    this.poids=poids;
  }

  public int getPoids()
  {
    return poids;
  }

  public String getNom()
  {
    return nom;
  }

}
