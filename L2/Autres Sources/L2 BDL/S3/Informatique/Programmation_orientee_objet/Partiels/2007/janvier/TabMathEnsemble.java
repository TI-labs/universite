import java.util.Iterator;

/**
 * <p>
 * 
 * Implémentation de l'interface MathEnsemble utilisant un tableau pour
 * mémoriser les éléments de l'ensemble .
 * </p>
 * 
 * @author Marc Champesme
 * @since 1 septembre 2005
 * @version 2 septembre 2005
 */

public class TabMathEnsemble<E> implements MathEnsemble<E> {
	private Object[] elements;

	private int nbElements;

	/**
	 * Initialise un nouvel ensemble vide.
	 * 
	 * @ensures estVide();
	 */
	public TabMathEnsemble() {
		elements = new Object[2];
	}

	/**
	 * Initialise un nouvel ensemble dont les éléments sont les éléments de
	 * l'ensemble spécifié.
	 * 
	 * @requires ens != null;
	 * @ensures equals(ens);
	 * 
	 * @param ens
	 *            L'ensemble des éléments dont ce nouvel ensemble doit etre
	 *            constitué.
	 */
	public TabMathEnsemble(MathEnsemble<? extends E> ens) {
		elements = new Object[ens.cardinal()];

		Iterator<? extends E> iter = ens.iterator();
		while (iter.hasNext()) {
			ajouter(iter.next());
		}
	}

	/**
	 * Constructor for the TabMathEnsemble object
	 * 
	 * @requires tab != null;
	 * @requires (\forall int i; i >= 0 && i < tab.length; tab[i] != null);
	 * @requires (\forall int i, j; i >= 0 && i < j && j < tab.length;
	 *           !tab[i].equals(tab[j]));
	 * @ensures cardinal() == tab.length;
	 * @ensures (\forall int i; i >= 0 && i < tab.length; contient(tab[i]));
	 * 
	 * @param tab
	 *            Description of the Parameter
	 */
	public TabMathEnsemble(E[] tab) {
		elements = (Object[]) tab.clone();
		nbElements = elements.length;
	}

	/**
	 * Renvoie <code>true</code> si cet ensemble contient l'élément spécifié.
	 * Plus précisément, renvoie <code>true</code> si et seulement si cette
	 * liste contient un élément <code>e</code> tel que
	 * <code>elt.equals(e)</code>.
	 * 
	 * @param elt
	 *            L'élément dont on cherche &agrave; savoir s'il appartient
	 *            &agrave; cet ensemble.
	 * @return <code>true</code> si l'élement spécifié est présent ;
	 *         <code>false</code> sinon.
	 */
	public boolean contient(Object elt) {
		return indexDe(elt) != -1;
	}

	private int indexDe(Object elt) {
		int result = -1;
		for (int i = 0; i < nbElements && result == -1; i++) {
			if (elements[i].equals(elt)) {
				result = i;
			}
		}
		return result;
	}

	private void assurerCapaciteMinimum(int minCapacite) {
		if (minCapacite > elements.length) {
			Object[] newTab = new Object[minCapacite];
			for (int i = 0; i < nbElements; i++) {
				newTab[i] = elements[i];
			}
			elements = newTab;
		}
	}

	public boolean ajouter(E elt) {
		if (elt == null) {
			throw new NullPointerException("L'élement à ajouter doit être non null");
		}
		int index = indexDe(elt);
		if (index == -1) {
			assurerCapaciteMinimum(nbElements + 5);
			elements[nbElements] = elt;
			nbElements++;
		}
		return (index == -1);
	}

	public boolean retirer(Object elt) {
		int index = indexDe(elt);
		if (index != -1) {
			elements[index] = elements[nbElements - 1];
			nbElements--;
		}
		return (index != -1);
	}

	/**
	 * Renvoie <code>true</code> si cet ensemble est inclus dans l'ensemble
	 * spécifié. Plus précisément, renvoie <code>true</code> si et seulement
	 * tout élément <code>e</code> de cet ensemble est tel que
	 * <code>ens.contient(e)</code> .
	 * 
	 * @param ens
	 *            L'ensemble dont on souhaite savoir s'il contient cet ensemble.
	 * @return <code>true</code> si cet ensemble est inclus dans l'ensemble
	 *         spécifié ; <code>false</code> sinon.
	 */
	public boolean estInclusDans(MathEnsemble<?> ens) {
		if (ens == null) {
			throw new NullPointerException();
		}
		if (cardinal() > ens.cardinal()) {
			return false;
		}

		boolean estInclus = true;
		for (int i = 0; i < cardinal() && estInclus; i++) {
			estInclus = ens.contient(elements[i]);
		}

		return estInclus;
	}

	/**
	 * Modifie l'ensemble courant de sorte qu'il représente l'union ensembliste
	 * de cet ensemble avec l'ensemble spécifié.
	 * 
	 * @param ens
	 *            L'ensemble avec lequel l'union doit etre effectuée.
	 */
	public void union(MathEnsemble<? extends E> ens) {
		if (ens == null) {
			throw new NullPointerException();
		}
		assurerCapaciteMinimum(nbElements + ens.cardinal());
		Iterator<? extends E> iter = ens.iterator();
		while (iter.hasNext()) {
			ajouter(iter.next());
		}
	}

	/**
	 * Modifie l'ensemble courant de sorte qu'il représente l'intersection
	 * ensembliste de cet ensemble avec l'ensemble spécifié.
	 * 
	 * @param ens
	 *            L'ensemble avec lequel l'intersection doit etre effectuée.
	 */
	public void intersection(MathEnsemble<?> ens) {
		if (ens == null) {
			throw new NullPointerException();
		}

		Iterator<?> iter = iterator();
		while (iter.hasNext()) {
			if (!ens.contient(iter.next())) {
				iter.remove();
			}
		}
	}

	/**
	 * Modifie l'ensemble courant de sorte qu'il représente la différence
	 * ensembliste de cet ensemble avec l'ensemble spécifié.
	 * 
	 * @param ens
	 *            L'ensemble &agrave; soustraire de cet ensemble.
	 */
	public void difference(MathEnsemble<?> ens) {
		if (ens == null) {
			throw new NullPointerException();
		}
		Iterator<?> iter = iterator();
		while (iter.hasNext()) {
			if (ens.contient(iter.next())) {
				iter.remove();
			}
		}
	}

	/**
	 * Teste si cet ensemble ne contient aucun élément.
	 * 
	 * @return <code>true</code> si cet ensemble ne contient aucun élément ;
	 *         <code>false</code> sinon.
	 */
	public boolean estVide() {
		return nbElements == 0;
	}

	/**
	 * Renvoie le nombre d'éléments dans cet ensemble.
	 * 
	 * @return le nombre d'éléments dans cet ensemble.
	 */
	public int cardinal() {
		return nbElements;
	}

	
	public Iterator<E> iterator() {
		return new TableauIterator<E>(elements, nbElements);
	}

	/**
	 * Teste l'égalité entre l'objet spécifié et cet ensemble. Renvoie
	 * <code>true</code> si et seulement si l'objet spécifié est aussi un
	 * <code>Ensemble</code>, que les deux ensembles ont le m&ecirc;me nombre
	 * d'éléments et que les deux ensembles contiennent les m&ecirc;mes
	 * elements.
	 * 
	 * @param obj
	 *            l'objet &agrave; comparer avec cette liste.
	 * @return <code>true</code> si l'objet spécifié est <i>equal </i>
	 *         &agrave; cette liste ; <code>false</code> sinon
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MathEnsemble<?>)) {
			return false;
		}

		MathEnsemble<?> autreEnsemble = (MathEnsemble<?>) obj;
		if (cardinal() != autreEnsemble.cardinal()) {
			return false;
		}

		return this.estInclusDans(autreEnsemble);
	}

	/**
	 * Renvoie la valeur du code de hashage de cet ensemble. Le code de hashage
	 * d'un ensemble est définit comme étant la somme des codes de hashage de
	 * ses éléments. Ce calcul assure que <code>ens1.equals(ens2)</code>
	 * implique que <code>ens1.hashCode()==ens2.hashCode()</code> pour toute
	 * paire d'ensembles, <code>ens1</code> et <code>ens2</code>, comme le
	 * spécifie le contrat de <code>Object.hashCode</code>.
	 * </p>
	 * 
	 * @return la valeur du code de hashage pour cet ensemble.
	 */
	public int hashCode() {
		int code = 0;
		for (int i = 0; i < cardinal(); i++) {
			code += elements[i].hashCode();
		}
		return code;
	}

	/**
	 * Renvoie une copie de cet ensemble, c'est-&agrave;-dire un nouvel ensemble
	 * contenant les meme éléments que cet ensemble.
	 * 
	 * @return Un clone de cette instance.
	 */
	public TabMathEnsemble<E> clone() {
		TabMathEnsemble<?> leClone;

		try {
			leClone = (TabMathEnsemble<?>) super.clone();
		} catch (CloneNotSupportedException e) {
			// Impossible car cette classe implemente
			// clone() et Object aussi
			throw new InternalError(e.toString());
		}
		// @assume leClone.nbElements == nbElements;
		leClone.elements = (Object[]) elements.clone();
		return (TabMathEnsemble<E>) leClone; // unchecked cast
	}

	/**
	 * Renvoie une représentation succinte de cet ensemble
	 * sous forme de chaîne de caractères.
	 * 
	 * @return Une représentation succinte de cet ensemble.
	 */
	public String toString() {
		String res = "{";
		if (!estVide()) {
			res += elements[0].toString();
			for (int i = 1; i < cardinal(); i++) {
				res += ", " + elements[i].toString();
			}
		}
		return res + "}";
	}

}
