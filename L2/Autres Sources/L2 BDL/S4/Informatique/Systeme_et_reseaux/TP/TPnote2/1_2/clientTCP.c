/* client_TCP.c (client TCP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUF_SIZE 256

typedef struct Annonce {
	char *adresse;
	char *texte;
	struct Annonce *suivant;
} Annonce_t;

char* id = 0;
short sport = 0;
int sock = 0; /* socket de communication */


int main(int argc, char** argv)
{
    struct sockaddr_in moi;      /* SAP du client */
    struct sockaddr_in serveur;  /* SAP du serveur */
    int len, charLu, tailleALire, tailleAEcrire;
    char buffer[BUF_SIZE];
    char buffer2[BUF_SIZE];
    char buffer3[BUF_SIZE];

    if (argc != 2)
    {
			fprintf(stderr,"usage: %s id_client\n",argv[0]);
			exit(1);
    }
    id = argv[1];
    sport = 20000;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
       fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
       exit(1);
		}
    serveur.sin_family = AF_INET;
    serveur.sin_port = htons(sport);
    inet_aton("0.0.0.0", (struct in_addr*) &serveur.sin_addr);
    if (connect(sock, (struct sockaddr *) &serveur, sizeof(serveur)) < 0)
    {
        fprintf(stderr,"%s: connect %s\n", argv[0],strerror(errno));
        perror("bind");
        exit(1);
    }
    len = sizeof(moi);
    getsockname(sock, (struct sockaddr *)&moi, (socklen_t *)&len);
    while(1)
    {
			bzero(buffer, BUF_SIZE);
	    if((charLu = read(sock, &tailleALire, sizeof(int))) < 0)
	    {
	    	printf("erreur Read Taille 1\n");
	    	exit(1);
	    }
		  if((charLu = read(sock, buffer, tailleALire)) < 0)
		  {
		  	printf("erreur Read1\n");
		  	exit(1);
		  }
		  buffer[charLu] = '\0';
		  printf("%s\n", buffer);
		  scanf("%s", buffer);
			if((strncmp(buffer, "AJOUT", strlen("AJOUT")) == 0) ||(strncmp(buffer, "SUPPRIMER", strlen("SUPPRIMER")) == 0))
			{
				scanf("%s %[^\n]s", buffer2, buffer3);
		  	sprintf(buffer, "%s %s %s", buffer, buffer2, buffer3);
			}

			tailleAEcrire = strlen(buffer);
	    if((write(sock, &tailleAEcrire, sizeof(int))) < 0)
	    {
	    	printf("erreur Write Taille 2\n");
	    	exit(1);
	    }
		  if((write(sock, buffer, tailleAEcrire)) < 0)
		  {
		  	printf("erreur Write\n");
		  	exit(1);
		  }
			bzero(buffer, BUF_SIZE);
			if((charLu = read(sock, &tailleALire, sizeof(int))) < 0)
			{
				printf("erreur Read Taille 2\n");
				exit(1);
			}
		  if((charLu = read(sock, buffer, tailleALire)) < 0)
		  {
		  	printf("erreur Read2\n");
		  	exit(1);
		  }
		  buffer[charLu] = 0;
		  printf("\n%s\n\n\n", buffer);
			fflush(stdout) ;
    }
    close(sock);
    return 0;
    }
