// gcc -o signal_sigint signal_sigint.c

#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<signal.h>

int main()
{
pid_t pid;
int i=0;
int status;

if ((pid=fork())==0) {
	printf("Fils du PID %d\n",getpid());
	while(1){i=0;};
}

printf("Je suis le pere de PID %d\n",getpid());

if (kill(pid,0)==-1) {
	printf("(de %d) Fils %d inexistant\n",getpid(),pid);
} else {
	sleep(2);
	printf("(de %d) Envoi du signal SIGINT au processus %d\n",getpid(),pid);
	kill(pid,SIGINT);
}

pid=waitpid(pid,&status,0);
printf("(de %d) Status du fils %d : %d\n",getpid(),pid,status);

}

