#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>

#define A sleep(6)

void *fonction(char *argv[]);

int main(int argc, char *argv[])
{
	int i;
	int n=atoi(argv[1]);
	int p=atoi(argv[2]);
	pthread_t fils;
	if (p == 0) {
//		A;
		printf("sortie1\n");
		pthread_exit(0);
	}
	char ***temp;
	temp = (char ***) malloc (n * sizeof(char **));
	for (i=0;i<n;i++) {
		printf("tid:%u et pid:%d et ppid:%d et p=%d\n", (unsigned long int) pthread_self(),getpid(),getppid(),p);
		temp[i] = (char **) malloc (3 * sizeof (char *)); 
		temp[i][0] = (char *) malloc (sizeof (char)); 
		temp[i][1] = (char *) malloc (sizeof(int) + sizeof (char)); 
		temp[i][2] = (char *) malloc (sizeof(int) + sizeof (char)); 
		sprintf(temp[i][0],"");
		sprintf(temp[i][1],"%d",n);
		sprintf(temp[i][2],"%d",p-1);
		if (pthread_create(&fils,NULL,fonction,temp[i])) {
			perror("pthread_create");
		}
	}
//	while (wait(0) != -1);
	printf("Sortie3\n");
	pthread_exit(0);
}

void *fonction(char *argv[]) {
	main(3,argv);
	printf("Sortie2\n");
	pthread_exit(0);
}
