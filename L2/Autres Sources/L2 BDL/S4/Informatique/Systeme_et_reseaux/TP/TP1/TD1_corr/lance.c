/* gcc lance.c -o lance */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> /* pour fork() */
#include <unistd.h> /* idem et pour execv */


int main(int argc, char *argv[]) 
{
	int i, n, pid;
	char *commande;
	
	n = atoi(argv[1]);
	commande = argv[2];
printf("%d\n",n);
	for (i=0;i<n;i++) 
	{
		pid = fork();
		if (pid == -1) 
		{
			perror("Erreur sur fork");
			exit(-1);
		}
		if (pid == 0)
		{
printf("%s\n",argv[2]);
			if (execv(argv[2], argv+2) <0)
			{
				perror("Erreur sur execv");
				exit(-1);
			}
		}
	}

	exit(EXIT_SUCCESS);
}

