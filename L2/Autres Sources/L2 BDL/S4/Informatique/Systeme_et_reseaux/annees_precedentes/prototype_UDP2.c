#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#define MAXMSGSIZE 1024

void erreur(char *message_erreur) 
{
	perror(message_erreur) ;
	exit(1) ;
}

// Client

int main(int argc, char *argv[])
{
	int sock ;
	struct sockaddr_in serveur_adr ;
	char message[MAXMSGSIZE] ;
	int len ;
	
	if (argc  != 2) 
	{
		fprintf(stderr,"usage : %s <@IP_Serveur>\n", argv[0]) ;
		exit(0) ;
	}
	
	sock = socket(AF_INET, SOCK_DGRAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = inet_addr(argv[1]) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(struct sockaddr_in) ;
	
	while (1) 
	{
		printf("Tapez votre message (q ou Q pour quitter) :") ;
		gets(message) ;
	
		if ((strcmp(message, "q") == 0) || strcmp(message, "Q") == 0) exit(0) ;
		else sendto(sock, message, strlen(message), 0, (struct sockaddr *)&serveur_adr, len) ;
	}
}











