#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define MAXMSGSIZE 1024

// Client

void erreur(char *message_erreur)
{
	perror(message_erreur) ;
	exit(1) ;
}

int main(int argc, char **argv)
{
	int sock ;
	unsigned short port ;
	struct sockaddr_in serveuradr ;
	struct hostent *serveur ;
	char *nom_serveur ;
	char message[MAXMSGSIZE] ;
	int nb_octets ;
	int len ;
	
	if (argc  != 3) 
	{
		fprintf(stderr,"usage : %s <nom_Serveur> <port>\n", argv[0]) ;
		exit(0) ;
	}
	
	nom_serveur = argv[1] ;
	port = (unsigned short) atoi(argv[2]) ;
	len = sizeof(struct sockaddr_in) ;
	sock = socket(AF_INET, SOCK_STREAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur = gethostbyname(nom_serveur) ;
	
	if (serveur == NULL) erreur("Le nom de serveur n’est pas connu") ;
	
	bzero((char *) &serveuradr, len) ;
	serveuradr.sin_family = AF_INET ;
	bcopy((char *)serveur->h_addr, (char *)&serveuradr.sin_addr.s_addr, len) ;
	serveuradr.sin_port = htons(port) ;
	
	if (connect(sock, (struct sockaddr *)&serveuradr, len) < 0) erreur("Erreur de connexion") ;
	
	printf("Tapez le message a envoyer : ") ;
	bzero(message, MAXMSGSIZE) ;
	fgets(message, MAXMSGSIZE, stdin) ;
	nb_octets = write(sock, message, strlen(message)) ;
	
	if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket") ;
	
	bzero(message, MAXMSGSIZE) ;
	nb_octets = read(sock, message, MAXMSGSIZE) ;
	
	if (nb_octets < 0) erreur("Erreur de lecture depuis la socket") ;
	
	printf("Reponse du serveur : %s", message) ;
	close(sock) ;
}



























