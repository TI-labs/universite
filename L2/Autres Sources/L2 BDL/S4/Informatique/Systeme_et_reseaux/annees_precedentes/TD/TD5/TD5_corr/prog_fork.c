#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

void (* tab_taches[7]) (void);

void T1(void),T2(void),T3(void),T4(void),T5(void),T6(void),T7(void);

int a,b,c,d,e,f,x1,x2,x3,x4,x5,x6,x7,x8;
int tube14[2],tube13[2],tube24[2],tube23[2],tube48[2],tube36[2],tube56[2],tube67[2],tube78[2];

int main(int argc,char *argv[]) {
	int i;
	pid_t pid;

	if(argc!=7) {
		printf("Veuillez entrer 6 entiers en arguments\n");
		exit(-1);
	}
	a=atoi(argv[1]);b=atoi(argv[2]);c=atoi(argv[3]);
	d=atoi(argv[4]);e=atoi(argv[5]);f=atoi(argv[6]);

	if(pipe(tube14)==-1) {perror("tube14");exit(-1);}
	if(pipe(tube13)==-1) {perror("tube13");exit(-1);}
	if(pipe(tube24)==-1) {perror("tube24");exit(-1);}
	if(pipe(tube23)==-1) {perror("tube23");exit(-1);}
	if(pipe(tube48)==-1) {perror("tube48");exit(-1);}
	if(pipe(tube36)==-1) {perror("tube36");exit(-1);}
	if(pipe(tube56)==-1) {perror("tube56");exit(-1);}
	if(pipe(tube67)==-1) {perror("tube67");exit(-1);}
	if(pipe(tube78)==-1) {perror("tube78");exit(-1);}

	tab_taches[0] = T1;tab_taches[1] = T2;tab_taches[2] = T3;
	tab_taches[3] = T4;tab_taches[4] = T5;tab_taches[5] = T6;
	tab_taches[6] = T7;

	for (i=0;i<7;i++) {
		pid = fork();
		if (pid==-1) {perror("fork");exit(-1);}
		if (pid==0) tab_taches[i]();
	}

	if (read(tube78[0],&x7,sizeof (int) )==-1) {perror("read tube78");exit(-1);}
	close (tube78[0]);
	if (read(tube48[0],&x4,sizeof (int) )==-1) {perror("read tube48");exit(-1);}
	close (tube48[0]);
	x8=x7+x4;
	printf("Resultat : %d\n",x8);
}



void T1(void) {	/*T1: 1-->4 et 1-->3*/  

	x1=a+b;

	close(tube14[0]);
	if(write(tube14[1],&x1,sizeof(int))==-1) {perror("write tube14");exit(-1);}
	close(tube14[1]);

	close(tube13[0]);
	if(write(tube13[1],&x1,sizeof(int))==-1) {perror("write tube13");exit(-1);}
	close(tube13[1]);
	exit(0);
}

void T2(void) {	/*T1: 2-->4 et 2-->3*/

	x2=c-d;

	close(tube24[0]);
	if(write(tube24[1],&x2,sizeof(int))==-1) {perror("write tube24");exit(-1);}
	close(tube24[1]);

	close(tube23[0]);
	if(write(tube23[1],&x2,sizeof(int))==-1) {perror("write tube23");exit(-1);}
	close(tube23[1]);
	exit(0);
}

void T3(void) {	/*T3: 1-->3, 2-->3, 3-->6*/
	if (read(tube13[0],&x1,sizeof (int) )==-1) {perror("read tube13");exit(-1);}
	close (tube13[0]);
	if (read(tube23[0],&x2,sizeof (int) )==-1) {perror("read tube23");exit(-1);}
	close (tube23[0]);

	x3=x1/x2;

	close(tube36[0]);
	if(write(tube36[1],&x3,sizeof(int))==-1) {perror("write tube36");exit(-1);}
	close(tube36[1]);

	exit(0);
}

void T4(void) {	/*T4: 1-->4, 2-->4, 4-->8*/
	if (read(tube14[0],&x1,sizeof (int) )==-1) {perror("read tube14");exit(-1);}
	close (tube14[0]);
	if (read(tube24[0],&x2,sizeof (int) )==-1) {perror("read tube24");exit(-1);}
	close (tube24[0]);

	x4=x1*x2;

	close(tube48[0]);
	if(write(tube48[1],&x4,sizeof(int))==-1) {perror("write tube48");exit(-1);}
	close(tube48[1]);

	exit(0);
}

void T5(void) {	/*T5: 5-->6*/

	x5=e*f;

	close(tube56[0]);
	if(write(tube56[1],&x5,sizeof(int))==-1) {perror("write tube56");exit(-1);}
	close(tube56[1]);

	exit(0);
}

void T6(void) {	/*T6: 3-->6, 5-->6, 6-->7*/
	if (read(tube36[0],&x3,sizeof (int) )==-1) {perror("read tube36");exit(-1);}
	close (tube36[0]);
	if (read(tube56[0],&x5,sizeof (int) )==-1) {perror("read tube56");exit(-1);}
	close (tube56[0]);

	x6=x3+x5;

	close(tube67[0]);
	if(write(tube67[1],&x6,sizeof(int))==-1) {perror("write tube67");exit(-1);}
	close(tube67[1]);

	exit(0);
}

void T7(void) {	/*T7: 6-->7, 7-->8*/
	if (read(tube67[0],&x6,sizeof (int) )==-1) {perror("read tube67");exit(-1);}
	close (tube67[0]);

	x7=2*x6;

	close(tube78[0]);
	if(write(tube78[1],&x7,sizeof(int))==-1) {perror("write tube78");exit(-1);}
	close(tube78[1]);

	exit(0);
}

