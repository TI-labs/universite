/* gcc -o prog_thread prog_thread.c -lpthread */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>

void *(* tab_taches[7]) (void*);

void *T1(void*),*T2(void*),*T3(void*),*T4(void*),*T5(void*),*T6(void*),*T7(void*);

int a,b,c,d,e,f,x1,x2,x3,x4,x5,x6,x7,x8;
pthread_t thread[7];


int main(int argc,char *argv[]) {
	int i;

	if(argc!=7) {
		printf("Veuillez entrer 6 entiers en arguments\n");
		exit(-1);
	}
	a=atoi(argv[1]);b=atoi(argv[2]);c=atoi(argv[3]);
	d=atoi(argv[4]);e=atoi(argv[5]);f=atoi(argv[6]);

	tab_taches[0] = T1;tab_taches[1] = T2;tab_taches[2] = T3;
	tab_taches[3] = T4;tab_taches[4] = T5;tab_taches[5] = T6;
	tab_taches[6] = T7;


/* creation des threads */
	for (i=0;i<7;i++) {
		if (pthread_create(&thread[i], NULL, tab_taches[i], NULL) != 0)
			{perror("pthread_create : erreur");exit(-1);}
	}

	pthread_join(thread[3],NULL);pthread_join (thread[6],NULL);
	x8=x7+x4;
	printf("Resultat : %d\n",x8);
}



void *T1(void *arg) {	/*T1: 1-->4 et 1-->3*/  
	x1=a+b;
	pthread_exit(0);
}

void *T2(void *arg) {	/*T1: 2-->4 et 2-->3*/
	x2=c-d;
	pthread_exit(0);
}

void *T3(void *arg) {	/*T3: 1-->3, 2-->3, 3-->6*/
	pthread_join (thread[0],NULL);pthread_join (thread[1],NULL);
	x3=x1/x2;
	pthread_exit(0);
}

void *T4(void *arg) {	/*T4: 1-->4, 2-->4, 4-->8*/
	pthread_join (thread[0],NULL);pthread_join (thread[1],NULL);
	x4=x1*x2;
	pthread_exit(0);
}

void *T5(void *arg) {	/*T5: 5-->6*/
	x5=e*f;
	pthread_exit(0);
}

void *T6(void *arg) {	/*T6: 3-->6, 5-->6, 6-->7*/
	pthread_join (thread[2],NULL);pthread_join (thread[4],NULL);
	x6=x3+x5;
	pthread_exit(0);
}

void *T7(void *arg) {	/*T7: 6-->7, 7-->8*/
	pthread_join (thread[5],NULL);
	x7=2*x6;
	pthread_exit(0);
}

