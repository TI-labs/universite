// gcc -o tube_signal tube_signal.c

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<signal.h>

#define STDOUT 1
#define STDIN 0

int tube1[2], tube2[2];
	// tube1 sert pour les envois du pere (et donc a la reception pour le fils)
	// tube2 sert pour les envois du fils (et donc a la reception pour le pere)

void processus_fils() {
	char reception[100];
	char envoi[] = "Bien recu";

	close(tube1[STDOUT]); // fils n'ecrit pas dans le tube1
	close(tube2[STDIN]); // fils ne lit pas dans tube2

	read(tube1[STDIN],reception,100);
	printf("Mon pere a ecrit : %s\n",reception);

	write(tube2[STDOUT],envoi,strlen((char *)envoi)+1);
}

void processus_pere(pid_t pid) {
	char reception[100];
	char envoi[] = "Je suis le pere";

	close(tube2[STDOUT]); // pere n'ecrit pas dans le tube2
	close(tube1[STDIN]); // pere ne lit pas dans tube1

	write(tube1[STDOUT],envoi,strlen((char *)envoi)+1);

	read(tube2[STDIN],reception,100);
	if (strcmp(reception,"Bien recu") == 0) {
		kill(pid,SIGTERM);
	} else {
		perror("reception de chaine");
		exit(2);
	}

}

int main(int argc,char **argv) {
	pid_t pid_fils;

	if (pipe(tube1)<0) {
		perror("creation de tube");
		exit(2);
	}
	if (pipe(tube2)<0) {
		perror("creation de tube");
		exit(2);
	}

	switch(pid_fils=fork()) {
		case (pid_t)-1: // cas d'erreur
			perror("creation de processus");
			exit(2);
		case (pid_t)0:	// proc. fils
			processus_fils();
			break;
		default: // proc. pere
			processus_pere(pid_fils);
	}
	return(0);
}
