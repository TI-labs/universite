#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define A sleep(3);

int main(int argc, char *argv[])
{
  if(argc < 3)
  {
    printf("Erreur pas assez d'arguments\n");
    exit(1);
  }
  int i, n, p;
  n = atoi(argv[1]);
  p = atoi(argv[2]);
  if(p == 0)
  {
    A;
    printf("Sortie 1\n");
    exit(0);
  }
  for(i = 0; i < n; i++)
  {
    if(fork() == 0)
    {
      printf("pid : %d et ppid : %d et %d\n", getpid(), getppid(), p);
      sprintf(argv[2], "%d", p-1);
      main(argc, argv);
      printf("Sortie 2\n");
      exit(0);
    }
  }
  while (wait(0) != -1);
  printf("Sortie 3\n");
  exit (0);
}
