#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/wait.h>


int main()
{
  pid_t fils;

  if((fils = fork()) == -1) {perror("fork") ; exit(1); }

  if(fils == 0)
  {
    //Fils
    //printf("Fils de PID : %d\n", getpid());
    while(1);
  }
  //Père
  if((kill(fils, 0)) == -1)
  {  //signal 0 ignoré
    printf("(de %d) Fils %d inexistant\n", getpid(), fils);
  }
  else
  {
    sleep(2);
    //printf("(de %d) Envoi du signal SIGINT au processus %d\n", getpid(), fils);
    kill(fils, 2);
  }
  return EXIT_SUCCESS;
}
