#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/wait.h>


int main()
{
  pid_t fils;
  int tube, fd[2];
  if((tube = pipe(fd)) == -1)
  {
    printf("Erreur pipe\n");
    exit(1);
  }


  if((fils = fork()))
  {
    //Père
    char chaineP[17] = "Je suis le père\0";
    char chaineFdansPere[10];
    int tailleP = strlen(chaineP) + 1;
    write(fd[1], &tailleP, sizeof(int));
    write(fd[1], &chaineP, sizeof(char) * 17);
    close(fd[1]);

    wait(NULL);
    read(fd[0], &chaineFdansPere, sizeof(char) * 10);
    close(fd[0]);
    kill(getpid(), SIGTERM);
  }
  else
  {
    //Fils
    int tailleFilsTmp;
    char chaineF[10] = "Bien reçu";
    read(fd[0], &tailleFilsTmp, sizeof(int));
    const int tailleF = tailleFilsTmp;
    char chainePdansFils[tailleF];
    read(fd[0], &chainePdansFils, sizeof(char) * tailleF);
    close(fd[0]);
    printf("Mon père a écrit %s\n", chainePdansFils);
    write(fd[1], &chaineF, sizeof(char) * 10);
    close(fd[1]);
    exit(0);
  }

  return EXIT_SUCCESS;
}
