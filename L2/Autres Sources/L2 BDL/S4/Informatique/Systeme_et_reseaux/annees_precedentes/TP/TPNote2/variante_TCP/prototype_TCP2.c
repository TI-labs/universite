#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "message.h"

#define MAXMSGSIZE 1024

// Client

void erreur(char *message_erreur)
{
	perror(message_erreur) ;
	exit(1) ;
}

int main(int argc, char **argv)
{
	int sock, identifiant, taille ;
	unsigned short port ;
	struct sockaddr_in serveuradr ;
	struct hostent *serveur ;
	char *nom_serveur ;
	char input[10];
	char buffer [201];
	//char buffer2 [201];
	//char messages[MAXMSGSIZE] ;
	int nb_octets ;
	int len = sizeof(struct sockaddr_in) ;
	message messageTmp;
	
	if (argc  != 3) 
	{
		fprintf(stderr,"usage : %s <nom_Serveur> <port>\n", argv[0]) ;
		exit(0) ;
	}
	
	/*bzero(buffer, 201);
	bzero(buffer2, 201);
	bzero(input, 10);*/
	
	nom_serveur = argv[1] ;
	port = (unsigned short) atoi(argv[2]) ;
	sock = socket(AF_INET, SOCK_STREAM, 0) ;
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur = gethostbyname(nom_serveur) ;
	if (serveur == NULL) erreur("Le nom de serveur n’est pas connu") ;
	
	
	
	bzero((char *) &serveuradr, len) ;
	serveuradr.sin_family = AF_INET ;
	bcopy((char *)serveur->h_addr, (char *)&serveuradr.sin_addr.s_addr, len) ;
	serveuradr.sin_port = htons(port) ;
	
	if (connect(sock, (struct sockaddr *)&serveuradr, len) < 0) erreur("Erreur de connexion") ;
	
	
	do{
		char buf_read[201], buf_write[201];
		nb_octets = read(sock, buffer, 200) ;
		if (nb_octets < 0) erreur("Erreur read 1") ;
		
		buffer[nb_octets] = '\0';
		printf("%s", buffer);
		scanf("%s", input);
		
		nb_octets = write(sock, input, 10) ;
		if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket1") ;
	
		if(strncmp(input, "SUPPRIMER", strlen("SUPPRIMER")) == 0)
		{
			nb_octets = read(sock, buffer, 200) ;
			if(nb_octets  <= 0)
				erreur("Erreur recvfrom SUPPRIMER");
			
			buffer[nb_octets] = '\0';
			printf("%s", buffer);
			scanf("%d", &identifiant);
			
			nb_octets = write(sock, &identifiant, sizeof(int)) ;
			if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket1") ;
	
		}
		
		
		
		if(strncmp(input, "AJOUT", strlen("AJOUT")) == 0)
		{
			bzero(buf_read, strlen(buf_read));
			nb_octets = read(sock, buf_read, 200) ;
			if (nb_octets < 0) erreur("Erreur read 2") ;
			
			buf_read[nb_octets] = '\0';
			printf("%s", buf_read);
			bzero(buf_write, strlen(buf_write));
			scanf("%s", buf_write);	
			nb_octets = write(sock, buf_write, strlen(buf_write) + 1) ;
			if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket1") ;
		}
		
		
		
		
		if(strncmp(input, "LISTER", strlen("LISTER")) == 0)
		{
			nb_octets = read(sock, &taille, sizeof(int)) ;
			if (nb_octets < 0) erreur("Erreur read 3") ;
				
			for(int i = 0; i < taille; i++)
			{
				nb_octets = read(sock, &messageTmp, sizeof(message)) ;
				if (nb_octets < 0) erreur("Erreur read 4") ;

				printf("|\tMessage %d \t| \n\t%s\t\n\n", messageTmp.id, messageTmp.texte);
			}
		}
	
	}while ((strcmp(input, "q") != 0) && strcmp(input, "Q") != 0);
	
	
	
	
	
	
	/*printf("Tapez le message a envoyer : ") ;
	bzero(message, MAXMSGSIZE) ;
	fgets(message, MAXMSGSIZE, stdin) ;
	nb_octets = write(sock, message, strlen(message)) ;
	
	if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket") ;
	
	bzero(message, MAXMSGSIZE) ;
	nb_octets = read(sock, message, MAXMSGSIZE) ;
	
	if (nb_octets < 0) erreur("Erreur de lecture depuis la socket") ;
	
	printf("Reponse du serveur : %s", message) ;*/
	close(sock) ;
	exit(0);
}



























