#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "message.h"


void erreur(char *message_erreur) 
{
	perror(message_erreur) ;
	exit(1) ;
}

// Serveur

int dernierID = 0;

int num_Users = 0;

int main(int argc, char **argv)
{
	char * msg = "\nVeuillez entrer :\t\n\n'AJOUT' pour ajouter un nouveau message\t\n\n'LISTER' pour envoyer la liste des messages\t\n\n'SUPPRIMER' pour supprimer un identifiant de la liste des messages (q ou Q pour quitter)\n\n";
	char * msg2 = "\nVeuillez entrer l'identifiant à supprimer\n";
	char * msg3 = "\nEntrez le message que vous voulez ajouter (200 caractères max)\n> ";
	int sock, i, len, nb_octets, identifiant, ret;
	char input [10], buffer[201];
	struct sockaddr_in serveur_adr ;
	struct sockaddr_in client_adr ;
	int client_len = sizeof(client_adr);
	
		
	message * listeMessage = (message*) malloc(sizeof(message) * 10);
	int tailleCurListe = 0;
	int tailleMaxListe = 10;
	
	sock = socket(AF_INET, SOCK_DGRAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(struct sockaddr_in) ;
	
	if (bind(sock,(struct sockaddr *) &serveur_adr, len) == -1) erreur("Erreur attachement socket") ;
	
	printf("\nAttente client sur port 5000\n") ;
	printf("IP serveur = %s\n", inet_ntoa(serveur_adr.sin_addr));
    ret = recvfrom(sock, &input, 10, 0, (struct sockaddr *)&client_adr, (socklen_t *)&client_len);

    if (ret <= 0) erreur("Erreur recvfrom") ;
	fflush(stdout) ;

	while (1)
	{
		do{			
			if((sendto(sock, msg, 200, 0, (struct sockaddr *)&client_adr,sizeof(client_adr))) == -1)
				erreur("Erreur sendto 1");
			nb_octets = recvfrom(sock, input, 10, 0, (struct sockaddr *) &client_adr,(socklen_t *) &len) ;
			input[nb_octets] = '\0';
			if(strcmp(input, "SUPPRIMER") == 0)
			{
				if((sendto(sock, msg2, 200, 0, (struct sockaddr *)&client_adr,sizeof(client_adr))) == -1)
					erreur("Erreur sendto 2");
				nb_octets = recvfrom(sock, &identifiant, sizeof(int), 0, (struct sockaddr *) &client_adr,(socklen_t *) &len) ;
			}
		} while((strcmp(input, "AJOUT") != 0) && (strcmp(input, "LISTER") != 0) && (strcmp(input, "SUPPRIMER") != 0)) ;
		
		while(num_Users > 1)
		{
			sleep(1);
		}
		if(strcmp(input, "AJOUT") == 0)
		{
			num_Users++;
			if((sendto(sock, msg3, 200, 0, (struct sockaddr *)&client_adr,sizeof(client_adr))) == -1)
				erreur("Erreur sendto 3");
			nb_octets = recvfrom(sock, buffer, 200, 0, (struct sockaddr *) &client_adr,(socklen_t *) &len) ;
			buffer[nb_octets] = '\0';
			message nouveauMessage;
			strcpy(nouveauMessage.texte, buffer);
			nouveauMessage.id = ++dernierID;
		
			if(tailleCurListe == tailleMaxListe)
			{
				tailleMaxListe*=2;
				listeMessage = (message*) realloc(listeMessage, sizeof(tailleMaxListe));
			}
			listeMessage[tailleCurListe] = nouveauMessage;
			tailleCurListe ++;	
			num_Users--;
		}
			
		if(strcmp(input, "LISTER") == 0)
		{
			num_Users++;
			if((sendto(sock, &tailleCurListe, sizeof(int), 0, (struct sockaddr *)&client_adr, client_len)) == -1)
				erreur("Erreur sendto 4");
			for(i = 0; i < tailleCurListe; i++)
			{
				if((sendto(sock, &listeMessage[i], sizeof(message), 0, (struct sockaddr *)&client_adr, client_len) == -1))
					erreur("Erreur sendto 5");
			}
			num_Users--;
		}
			
		if(strcmp(input, "SUPPRIMER") == 0)
		{
			num_Users++;
			for(i = 0; i < tailleCurListe && listeMessage[i].id != identifiant; i++);
			if(i < tailleCurListe)
			{
				while(i < tailleCurListe)
				{
					listeMessage[i] = listeMessage[i+1];
					i++;
				}
				tailleCurListe--;
			}
			num_Users--;
		}
		fflush(stdout) ;
	}
	exit(0);
}











