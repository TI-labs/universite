#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/***************************************************************/
/*      Déclarations des fonctions                             */
/***************************************************************/

double * karatsuba (double *, double *, int);

double * prod_polynome (double *, double *, int);


double * creer_polynome (int);
void     remplir_polynome_alea (double *, int);


void     afficher_polynome (double *, int);

double * somme_polynome (double * , int , double * , int );

double * reste_polynome (double * , int , double * , int );

double * redimension (double *, int , int);

/***************************************************************/
/*                    Programme principal                      */
/***************************************************************/
int main ()
{
  double *F;
  double *G;
  double *H, *H1;

  int n = 10000;             /* le degré des polynômes sera n-1 */
  int dn = 2*n - 1;      /* le degré du polynôme F*G sera 2n - 2  */

  clock_t debut, fin;
  double t;

  srand(time(NULL));

  F = creer_polynome (n);
  G = creer_polynome (n);

/* fonction qui remplie aleatoirement les coefficientes des polynomes */

  remplir_polynome_alea (F,n);       
  remplir_polynome_alea (G,n);      

 /*F[0] = 1; F[1] = 7; F[2] = 5; F[3] = 2; F[4] = 1;
 G[0] = 2; G[1] = 3; G[2] = 1; G[3] = 8; G[4] =2 ;*/

  debut = clock();
  H = karatsuba (F, G, n);
  fin = clock();
  t = ((double) fin - debut)/CLOCKS_PER_SEC;

  /*printf("\nF = ");
  afficher_polynome(F,n);
  printf("G = ");
  afficher_polynome(G,n);
  printf("F*G = ");
  afficher_polynome (H,dn);*/


  printf("\nLe temps d'exécution de l'algorithme de Karatsuba est : %.10f seconds\n", t);

  debut = clock();
  H1 = prod_polynome (F, G, n);
  fin = clock();
  t = ((double) fin - debut)/CLOCKS_PER_SEC;

  

  /*printf("\nF*G = ");
  afficher_polynome (H1,dn);*/

  printf("\nLe temps d'exécution de l'algorithme naïf est : %.10f seconds\n", t);

  free(F); free(G); free(H); free(H1);

  return EXIT_SUCCESS;
}

/***************************************************************/
/*                      Construction des fonctions             */
/***************************************************************/

double * creer_polynome (int n)
{
   double * R = (double *) malloc (n*sizeof(double));
   
   return R;
}
/***************************************************************/

void afficher_polynome (double * H, int n)
{
   int i;

  printf("[");
  for (i=0; i<n-1; i++)
    printf("%.0f,", H[i]);
  printf("%.0f]\n", H[n-1]);
}
/***************************************************************/

/***************************************************************/
/*               Algorithme naïf                               */
/***************************************************************/
 
double * prod_polynome (double * F, double * G, int n)
{
   int i,j;
   double * R = creer_polynome(2*n-1);

   for(i=0; i < n; i++)
   {
     R[i] = 0;
     for(j=0; j <= i; j++)
       R[i] = R[i] + (F[j]*G[i-j]);
   }
   for(i=0; i < n-1; i++)
   {
     R[n+i] = 0;
     for(j=i+1; j < n; j++)
       R[n+i] = R[n+i] + (F[j]*G[n+i-j]);
   }
   return R;
}

/***************************************************************/

double * somme_polynome (double * X, int n1, double * Y, int n2)
{
   int i;
  double * R = creer_polynome(n2);
 
 /* on suppose toujours que n2 >= n1 */

  for (i=0; i<n1; i++)
    R[i] = Y[i] +X[i];
  for (i=n1; i< n2; i++)
    R[i] = Y[i];
  return R;
     
}

/***************************************************************/
double * reste_polynome (double * X, int n1, double * Y, int n2)
{
   int i;
  double * R = creer_polynome(n2);
 
 /* on suppose toujours que n2 >= n1 */

  for (i=0; i<n1; i++)
    R[i] = Y[i] - X[i];
  for (i=n1; i< n2; i++)
    R[i] = Y[i];
  return R;      
}
/***************************************************************/
double * redimension (double * V, int nv, int na)
{
  int i;
  double * Q = creer_polynome(nv + na);

  for (i=0; i< na; i++)
    Q[i] = 0;
  for (i=na; i < na + nv; i++)
    Q[i] = V[i-na];
  return Q;
}

/***************************************************************/
/*                  Algorithme de Karatsuba                    */
/***************************************************************/

double * karatsuba (double * F, double *G, int n)
{
  double * R;
 
  if (n==1){
     R = creer_polynome(1);
    R[0] = F[0]*G[0];
  }
  else
    if (n == 2)
    {
     double a1; double a2; double a3; double a4; double a5; double a6; double a7; 
     double f0; double f1; double g0; double g1;

     R = creer_polynome (3);

     f0 = F[0];  f1 = F[1];
     g0 = G[0];  g1 = G[1];

     a1 = f0*g0;
     a2 = f1*g1;
     a3 = f0 + f1;
     a4 = g0 + g1;
     a5 = a3*a4;
     a6 = a5 - a1;
     a7 = a6 - a2;
     R[0] = a1; R[1] = a7; R[2] = a2;
    }
    else {
     int i; 
     int n2 = n/2; 
     int nr = n-n2;
     double * A1; double * A2; double * A3; double * A4; 
     double * A5; double * A6; double * A7; 
     double * F0; double * F1; double * G0; double * G1;
     double * T1; double * T2; double * T3;  
   

    /* création des sous-polynômes F0, F1, G0, G1 */

     F0 =  creer_polynome(n2);
     F1 =  creer_polynome(nr);
     G0 =  creer_polynome(n2);
     G1 =  creer_polynome(nr);

    /* remplissage des tableaux de coefficients des sous-polynômes F0,G0,F1,G1 */

     for (i=0; i<n2; i++)
     {
      F0[i] = F[i]; G0[i] = G[i];
     }
     for (i=0; i<nr; i++)
     {
      F1[i] = F[n2 + i]; G1[i] = G[n2 + i];
     }

     /* L'algorithme en concrète */
 
     A1 = karatsuba (F0,G0,n2);
     A2 = karatsuba (F1,G1,nr);
     A3 = somme_polynome(F0,n2,F1,nr); 
     A4 = somme_polynome(G0,n2,G1,nr);
     A5 = karatsuba (A3, A4, nr);
     A6 = reste_polynome(A1,(2*n2 - 1),A5, (2*nr - 1));
     A7 = reste_polynome(A2, (2*nr-1), A6, (2*nr - 1)); 

     T1 = redimension (A7, (2*nr-1),n2);
     T2 = redimension (A2, (2*nr-1),2*n2);

     T3 = somme_polynome (A1, (2*n2-1), T1, (2*nr + n2-1));
     R  = somme_polynome (T3, (2*nr + n2-1), T2, 2*n - 1);

     free(F0); free (F1); free(G0); free(G1);
     free(A1); free(A2); free(A3); free(A4); free(A5); free(A6); free(A7); 
     free(T1); free(T2); free(T3); 
    }
  return R;
}

/**********************************************************************/
/* remplissage pseudo-aléatoire d'un tableau de coefficients entiers  */
/**********************************************************************/
void     remplir_polynome_alea (double * A, int n)
{
  int i;
  
  for (i=0; i<n; i++)
    A[i] = rand();
}
/***************************************************************/
