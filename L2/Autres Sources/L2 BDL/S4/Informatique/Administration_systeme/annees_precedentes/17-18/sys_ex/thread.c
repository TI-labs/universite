#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *fonction ( void *arg)
{
	int i;
	for(i=0 ; i<5 ;i++){
		printf("Thread %s (%u) :%d\n",(char*)arg,(unsigned  int) pthread_self(),i);
		sleep (1);
	}
	pthread_exit (0);
}

int main(){
	pthread_t th1, th2;
	void *retour;
	if (pthread_create (&th1,NULL , fonction, "1") < 0){
	fprintf (stderr,"pthread_create error for thread 1\n");
	exit(1);
	}
	
	if(pthread_create(&th2, NULL, fonction, "2")<0){
		fprintf(stderr,"pthread_create error for thread 2\n");
		exit(1);
	}
	
	(void)pthread_join(th1, &retour);
	(void)pthread_join(th2,&retour);
	
}
