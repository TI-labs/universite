/*TP 10*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main(){
	pid_t fils1,fils2;
	fils1=fork();
	
	if (fils1 == -1)
	{
		perror("Erreur lors de la création de fils1");
		exit(-1);
	}
	
	if (fils1 == 0)
	{
		printf("Je suis le fils1 %d, mon pere est  %d !\n", getpid(), getppid());
	} 
	else 
	{
		fils2=fork();
		if (fils2 == -1)
		{
			perror("Erreur lors de la création de fils 2");
			exit(-1);
		}
		
		if(fils2 == 0)
		{
			printf("Je suis le fils2 %d, mon pere est %d !\n", getpid(), getppid());
		} 
		else 
		{
			wait(NULL);
			wait(NULL);
			printf("Je suis le père %d, fils: %d et %d.\n",getpid(), fils1, fils2);
		}
	}
	return 1;
}
