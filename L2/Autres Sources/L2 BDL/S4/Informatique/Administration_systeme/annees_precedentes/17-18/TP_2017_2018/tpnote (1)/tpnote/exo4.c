/*HAMES OUEZNA 11610524*/
/*HENOUNE ASMA  11606588*/
/*TP 10*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

int c = 0;//compteur

void sig_handler(int sig)
{
	printf("signal reçu: %d\n",sig);//affichage du signal
	if ((sig == SIGINT) && (c <3))
	{
		signal(SIGINT, SIG_IGN);
		c++;
		sleep(1);
		printf("Mon pid est : %d\n", getpid());
	} 
	 else if ((sig == SIGINT) && (c == 3))
	 {
		printf("Signal recu 3 fois, términé!\n");
		
	}
}

int main(int argc, char* argv[])
{	
	printf("Mon pid est : %d\n", getpid());
	while(c<3)
	{
		signal(SIGINT, sig_handler);
		
	}
	sleep(1);
	printf("C'est fin du programme\n");
	return 1;
}

