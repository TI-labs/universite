(*
#use "P1_2017.ml";;
*)

let listeIntPair = [2;4;6;8;10;12;14;16;18;20];;
let liste = [9;8;7;6;5;4;3;2;1;99;77;55];;
let liste2 = [6;5;4;3;2;-99;77;55];;


(* 4 *)

let rec pair_impair l =
	match l with
	| [] -> [], []
	| h::t ->
		if((h mod 2) == 0)
		then
			(h::(fst (pair_impair t))), (snd (pair_impair t))
		else
			(fst (pair_impair t)), (h::(snd (pair_impair t)));;

pair_impair listeIntPair;;
pair_impair liste;;
pair_impair liste2;;

(*
#use "P1_2017.ml";;
*)
