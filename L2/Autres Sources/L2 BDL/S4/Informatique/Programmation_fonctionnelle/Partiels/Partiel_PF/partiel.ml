(*
#use "partiel.ml" ;; 
*)

let listeInt = [1;2;3;4;5;6;7;8;9;10] ;;
let listePair = [2;4;6;8;10] ;;
let listeInt2 = [1;3;4;5;6;8;9] ;;

(*	Exo5	*)


(*1*)

let pair n = (n mod 2) == 0;;
pair 1;;
pair 2;;
pair 3;;
pair 4;;
pair 5;;
pair 6;;
pair 7;;
pair 8;;
pair 89;;


(*2*)

let rec alt l =
	match l with
	| h1::h2::t -> if(pair h1 == pair h2)
								then false
								else	
									alt (h2::t)        
	|_->true;;

alt listeInt;;
alt listePair;;
alt listeInt2;;


(*	Exo6	*)

let premier n =
	let rec trouvPremier nb div =
		if(nb <= div)
		then
			true
		else
			if((nb mod div) == 0)
			then
				false
			else
				trouvPremier nb (div+1) in
	if(n >= 2)
	then
		trouvPremier n 2
	else false;;

1;;
premier 1;;
2;;
premier 2;;
3;;
premier 3;;
4;;
premier 4;;
5;;
premier 5;;
6;;
premier 6;;
97;;
premier 97;;
101;;
premier 101;;


	
(*
#use "partiel.ml" ;; 
*)
