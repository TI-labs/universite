(*
#use "TP3.ml" ;;
*)

type arbre = Vide | Noeud of int * arbre * arbre ;;

let abrVide = Vide;;
Noeud(8, Vide, Vide);;
Noeud(1, Vide, Vide);;
let abr1 = Noeud(5, Noeud(1, Vide, Vide), Noeud(8, Vide, Vide));;
Noeud(8, Vide, Noeud(1, Vide, Vide));;
let abr2 = Noeud(5, Noeud(1, Vide, Noeud(2, Vide, Vide)), Noeud(8, Vide, Noeud(10, Vide, Vide)));;
let liste = [9;8;7;6;5;4;3;2;1;99;77;55];;
let listeSimple = [4;3;2;1];;

(* 1 *)

let rec appartient e abr =
	match abr with
	| Vide -> false
	| Noeud(x, g, d) ->
			if(x == e)
			then
				true
			else
				if(e < x)
				then
					appartient e g
				else
					appartient e d
		;;

appartient 5 abr1 ;;
appartient 1 abr1 ;;
appartient 8 abr1 ;;
appartient 5 abrVide;;

(* 2 *)

let rec arbre_2_liste abr =
	match abr with
	| Vide -> []
	| Noeud(x, g, d) -> arbre_2_liste g @ [x] @ arbre_2_liste d ;;

arbre_2_liste abrVide ;;
arbre_2_liste abr1 ;;
arbre_2_liste abr2 ;;

(* 3 *)

let rec inserer_bas e abr =
	match abr with
	| Vide -> Noeud(e, Vide, Vide)
	| Noeud(x, g, d) ->
			if(e < x)
			then
				Noeud(x, inserer_bas e g, d)
			else
				Noeud(x, g, inserer_bas e d) ;;


inserer_bas 50 abrVide ;;
inserer_bas 50 abr1 ;;
inserer_bas 50 abr2 ;;
let abr2 = inserer_bas 25 abr2 ;;
let abr2 = inserer_bas 32 abr2 ;;
let abr2 = inserer_bas 50 abr2 ;;
arbre_2_liste abr2 ;;

(* 4 *)

let rec couper n abr =
	match abr with
	| Vide -> Vide, Vide
	| Noeud(x, g, d) ->
		if(n <= x)
		then
			let gauche = (fst (couper n g)) in
			let droite = Noeud(x, (snd(couper n g)), d) in
			gauche, droite
		else
			let gauche = Noeud(x, g, (fst(couper n d))) in
			let droite = (snd(couper n d)) in
			gauche, droite ;;


arbre_2_liste abr1;;
arbre_2_liste (fst(couper 5 abr1));;
arbre_2_liste (snd(couper 5 abr1));;
arbre_2_liste abr2;;
arbre_2_liste (fst(couper 5 abr2));;
arbre_2_liste (snd(couper 5 abr2));;
arbre_2_liste (fst(couper 10 abr2));;
arbre_2_liste (snd(couper 10 abr2));;
arbre_2_liste (fst(couper 25 abr2));;
arbre_2_liste (snd(couper 25 abr2));;
arbre_2_liste (fst(couper 100 abr2));;
arbre_2_liste (snd(couper 100 abr2));;

let inserer_haut n abr = Noeud(n, (fst (couper n abr)), (snd (couper n abr))) ;;


inserer_haut 1 abr1;;
inserer_haut 2 abr1;;
inserer_haut 5 abr1;;
inserer_haut 10 abr1;;
inserer_haut 20 abr1;;
arbre_2_liste abr1;;
arbre_2_liste (inserer_haut 1 abr1);;
arbre_2_liste (inserer_haut 2 abr1);;
arbre_2_liste (inserer_haut 5 abr1);;
arbre_2_liste (inserer_haut 10 abr1);;
arbre_2_liste (inserer_haut 20 abr1);;
(inserer_haut 10 (inserer_haut 20 abr1));;

(* 5 *)

let rec inserer_liste l =
	match l with
	| [] -> Vide
	| h::t-> (inserer_bas h (inserer_liste t)) ;;

let triListe l =
	match l with
	| [] -> []
	| h::t ->
		let arbreRes = inserer_liste l in
		arbre_2_liste arbreRes ;;

triListe liste ;;
triListe listeSimple ;;

let bigTree = inserer_liste liste ;;



(*
#use "TP3.ml" ;;
*)
