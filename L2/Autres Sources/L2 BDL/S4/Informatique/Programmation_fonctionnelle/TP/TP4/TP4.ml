(*
#use "TP4.ml" ;;
*)

type fp = X of int | Vrai | Faux | Et of fp * fp | Ou of fp * fp | Imp of fp * fp | Equiv of fp * fp | Non of fp ;;


X(3) ;;
Non(Vrai) ;;
let a = Ou(Vrai, Faux) ;;
let b = Et(Ou(Non(Vrai), X(2)), Faux) ;;
let impliVraie = Imp(Faux, Vrai) ;;
let impliFausse = Imp(Vrai, Faux) ;;




(* 1 *)

let rec elim_equiv formula =
  match formula with
  | X(i) -> X(i)
  | Vrai -> Vrai
  | Faux -> Faux
  | Et (a, b) -> Et ((elim_equiv a), (elim_equiv b))
  | Ou (a, b) -> Ou ((elim_equiv a), (elim_equiv b))
  | Imp (a, b) -> Imp ((elim_equiv a), (elim_equiv b))
  | Equiv (a, b) -> Et (Imp (elim_equiv a, elim_equiv b), Imp (elim_equiv b, elim_equiv a))
  | Non a -> Non (elim_equiv a) ;;

elim_equiv a ;;
elim_equiv b ;;
elim_equiv (Equiv(a, a)) ;;
elim_equiv (Equiv(a, b)) ;;
elim_equiv (Equiv(b, a)) ;;
elim_equiv (Equiv(Vrai, Vrai)) ;;
elim_equiv (Equiv(Vrai, a)) ;;
elim_equiv (Equiv(Vrai, Faux)) ;;
elim_equiv (Equiv(Faux, Faux)) ;;
elim_equiv impliVraie ;;
elim_equiv impliFausse ;;

(* 2 *)


let rec elim_imp formula =
  match formula with
  | X(i) -> X(i)
  | Vrai -> Vrai
  | Faux -> Faux
  | Et (a, b) -> Et ((elim_imp a), (elim_imp b))
  | Ou (a, b) -> Ou ((elim_imp a), (elim_imp b))
  | Imp (a, b) -> Ou(Non(elim_imp a), elim_imp b)
  | Equiv (a, b) -> failwith "Deja elimine equiv elim_imp"
  | Non a -> Non (elim_imp a) ;;

elim_imp (elim_equiv a) ;;
elim_imp (elim_equiv b) ;;
elim_imp (elim_equiv (Imp(a, a))) ;;
elim_imp (elim_equiv (Imp(a, b))) ;;
elim_imp (elim_equiv (Imp(b, a))) ;;
elim_imp (elim_equiv (Imp(Vrai, Vrai))) ;;
elim_imp (elim_equiv (Imp(Vrai, a))) ;;
elim_imp (elim_equiv (Imp(Vrai, Faux))) ;;
elim_imp (elim_equiv (Imp(Faux, Faux))) ;;
elim_imp (elim_equiv impliVraie) ;;
elim_imp (elim_equiv impliFausse) ;;



(* 3 *)


let rec repousse_neg formula =
  match formula with
  | X(i) -> X(i)
  | Vrai -> Vrai
  | Faux -> Faux
  | Et (a, b) -> Et (repousse_neg a, repousse_neg b)
  | Ou (a, b) -> Ou (repousse_neg a, repousse_neg b)
  | Imp (a, b) -> failwith "Deja elimine imp repousse_neg"
  | Equiv (a, b) -> failwith "Deja elimine equiv repousse_neg"
  | Non a ->
      (
      match a with
      | X(j) -> Non (X(j))
      | Vrai -> Non a
      | Faux -> Non a
      | Et (a', b') -> Ou (repousse_neg (Non a'), repousse_neg (Non b'))
      | Ou (a', b') -> Et (repousse_neg (Non a'), repousse_neg (Non b'))
      | Non X(i) -> a
      | Non Vrai -> a
      | Non Faux -> a
      | Non a' -> repousse_neg (a')
      | _ -> failwith "Deja elimine 2cas repousse_neg"
      )
      ;;


repousse_neg (elim_imp (elim_equiv a)) ;;
repousse_neg (elim_imp (elim_equiv b)) ;;
repousse_neg (elim_imp (elim_equiv (Imp(a, a)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(a, b)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(b, a)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Vrai)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(Vrai, a)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Faux)))) ;;
repousse_neg (elim_imp (elim_equiv (Imp(Faux, Faux)))) ;;
repousse_neg (elim_imp (elim_equiv impliVraie)) ;;
repousse_neg (elim_imp (elim_equiv impliFausse)) ;;
repousse_neg (elim_imp (elim_equiv (Non impliFausse))) ;;
repousse_neg (Non(Non Vrai)) ;;
repousse_neg (Non(Non(Non Vrai))) ;;
repousse_neg (elim_imp (elim_equiv (Equiv(Et(a, (Non(Non(Non Vrai)))), (Non impliFausse))))) ;;

(* 4 *)


let rec inverse_ou_et formula =
  match formula with
  | Et (a, b) -> Et ((inverse_ou_et a), (inverse_ou_et b))
  | Ou (Et (a, b), c) -> inverse_ou_et (Et((Ou(a,c)),(Ou(b,c))))
  | Ou (a, Et (b, c)) -> inverse_ou_et (Et((Ou(a,c)),(Ou(b,c))))
  | Ou (a, b) -> let (c,d) = (inverse_ou_et a, inverse_ou_et b) in
            if(c == a && d==b)
            then
              formula
            else
              inverse_ou_et (Ou(c, d))
  | Imp (a, b) -> failwith "Deja elimine imp inverse_ou_et"
  | Equiv (a, b) -> failwith "Deja elimine equiv inverse_ou_et"
  | _-> formula
  ;;

a;;
b;;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv a))) ;;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv b))) ;;
(repousse_neg (elim_imp (elim_equiv (Imp(a, a)))));;

inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(a, a)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(a, b)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(a, b)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(b, a)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(b, a)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Vrai)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Vrai)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(Vrai, a)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(Vrai, a)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Faux)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(Vrai, Faux)))));;
(repousse_neg (elim_imp (elim_equiv (Imp(Faux, Faux)))));;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv (Imp(Faux, Faux)))));;
(repousse_neg (elim_imp (elim_equiv impliVraie))) ;;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv impliVraie))) ;;
(repousse_neg (elim_imp (elim_equiv impliFausse))) ;;
inverse_ou_et (repousse_neg (elim_imp (elim_equiv impliFausse))) ;;



(*
  #use "TP4.ml" ;;
*)
