(*
	ocamlc td03_Nico.ml -o td03_Nico.cmx
	ocamlrun td03_Nico.cmx
	
	#use "td03_Nico.ml";;
*)

(*Exercice 1*)


let rec nth x l n = 
if(n<=1)
then
	if(l = [])
	then 
		x
	else
		List.hd (l)
else
	nth x (List.tl (l)) (n-1);;


let listeInt1 = 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: [];;

let listeInt2 = [10; 20; 30; 40; 50; 60; 70; 80; 90];;

print_string "
listeInt1 = 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: []\n

nth -5 listeInt1, 1\n";;
print_int (nth (-5) listeInt1 1);;
print_string "\n";;

print_string "nth -5 listeInt1, 10\n";;
print_int (nth (-5) listeInt1	 10);;
print_string "\n";;
flush stdout;;

(*Exercice 2*)

let rec append l1 l2 = 
	match (l1, l2) with 
	|([], l2) -> l2
	|(l1, []) -> l1
	|(t1 :: q1, t2 :: q2) -> t1 :: append q1 l2;;
	
	append listeInt2 listeInt1;;
	append [4;3;2;1] [5;6;7;8];;

(*Exercice 3*)

let rec rev_append l1 l2 = 
	match (l1, l2) with 
	|([], l2) -> l2
	|(l1, []) -> l1
	|(t1 :: q1, t2 :: q2) -> rev_append q1 (t1 :: l2);;
	
	rev_append listeInt2 listeInt1;;
	rev_append [4;3;2;1] [5;6;7;8];;

(*Exercice 4*)

let rec rev l = 
	match l with 
	|[] -> []
	|t1 :: q1 -> rev_append q1 (t1 :: []);;
	
	rev listeInt1;;
	rev [4;3;2;1];;
	

(*Exercice 5*)

let rec flatten l = 
	match l with 
	|[] -> []
	|t1 :: q1 -> append t1 (flatten q1);;
	
	let listList = [listeInt1; listeInt2];;
	flatten listList;;
	flatten [[4;3];[2];[]; [1;3;5]];;

(*Exercice 6*)

let rec find f l = 
	match l with
	|[] -> 0
	|t1 :: q1 ->
	if(f t1)
	then
		t1
	else find f q1;;

let inf0 x = x < 0 ;;
let equ0 x = x = 0 ;;
let sup0 x = x > 0 ;;

find inf0 listeInt1;;
find inf0 [10;0;-6;-2];;
find equ0 listeInt1;;
find equ0 [10;0;-6;-2];;
find sup0 listeInt1;;
find sup0 [10;0;-6;-2];;
	
	
