(*******************************************************)
(*                                                     *)
(*                                                     *)
(*******************************************************)

(*******************      Types       ******************)

type var = string

type value =
    Vide                     (* nil *)
  | Comb of value * value    (* c . d *)

type expr =
    Var of var              (* X *)
  | Data of value           (* d *)
  | Cons of expr * expr     (* cons E F *)
  | Head of expr            (* hd E *)
  | Tail of expr            (* tl E *)
  | Test of expr * expr     (* =? E F *)

type command =
    Assign of var * expr      (* X := E *)
  | Seq of command * command  (* C ; D *)
  | While of expr * command   (* while E do {C} *)

type program =
    { input : var ;
      body : command ;
      output : var }
      (* read X; C; write Y *)

type environnement = (var * value) list 

(*******************    Exceptions    ******************)

exception Empty_tree
exception Undef_var of var
exception Undef_var_in_expr of var*expr
