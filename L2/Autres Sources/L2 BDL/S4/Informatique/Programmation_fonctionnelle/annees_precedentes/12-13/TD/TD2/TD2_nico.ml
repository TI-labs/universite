(*
#use "TD2_nico.ml" ;;
*)

(* Exo 1 : *)

let f a b c d = (a + b, c + d);;
f 6 8 33 9 ;;

(* Exo 2 : *)

let couple1 = (5, 10) ;;
let couple2 = (30, 60) ;;
let couple3 = (12, 24) ;;

let f a b = (fst a + fst b, snd a + snd b);;
f couple1 couple1 ;;
f couple1 couple2 ;;
f couple1 couple3 ;;
f couple2 couple3 ;;

(* Exo 3 : *)

(* 1. *)

let unit x = x mod 10 ;;
unit 50 ;;
unit 4 ;;
unit 67 ;;
unit 123;;

(* 2. *)

let dizaine x = (x mod 100) / 10;;
dizaine 50 ;;
dizaine 4 ;;
dizaine 67 ;;
dizaine 123;;

(* 3. *)

let rec n_eme m n = if (n <= 0) then unit m else n_eme (m/10) (n-1);;

n_eme 3 0;;
n_eme 123 0;;
n_eme 123 1;;
n_eme 123 2;;
n_eme 123 3;;
n_eme 987654321 8;;
n_eme 987654321 7;;
n_eme 987654321 6;;
n_eme 987654321 5;;
n_eme 987654321 4;;
n_eme 987654321 3;;
n_eme 987654321 2;;
n_eme 987654321 1;;
n_eme 987654321 0;;

(* 4. *)

let rec n_eme_bin m n = if (n <= 0) then m mod 2 else n_eme_bin (m/2) (n-1);;

n_eme_bin 1 0;;
n_eme 101 0;;
n_eme 101 1;;
n_eme 101 2;;
n_eme 101 3;;
n_eme 101011 5;;

(* 5. *)

let rec n_eme_base_B m n b = if (n <= 0) then m mod b else n_eme_base_B (m/b) (n-1) b;;

n_eme_base_B 1 0 7;;
n_eme_base_B 12 0 16;;
n_eme_base_B 12 0 8;;
n_eme_base_B 66 0 7;;
n_eme_base_B 66 1 7;;
n_eme_base_B 65 0 66;;
n_eme_base_B 65 1 66;;
n_eme_base_B 66 0 66;;
n_eme_base_B 66 1 66;;


(* Exo 4: *)

let rec pgcd n m = if(m=0) then n else pgcd m (n mod m);;

pgcd 6 10;;
pgcd 10 6;;
pgcd 6 6;;
pgcd 10 10;;
pgcd 1 10;;
pgcd 2 10;;
pgcd 3 10;;
pgcd 5 10;;
pgcd 21 637;;

(* Exo 5 : *)

let rec exo5 f n m =
	if(n<m)
	then
		if (f n = true)
		then
			n
		else
			exo5 f (n+1) m
	else
		m;;

let multOf30 n = (n mod 30) = 0;;

exo5 multOf30 5 50;;
exo5 multOf30 55 50;;
exo5 multOf30 55 500;;
exo5 multOf30 1 2;;
exo5 multOf30 3340 3350;;
exo5 multOf30 3340 3360;;

(*
#use "TD2_nico.ml" ;;
*)
