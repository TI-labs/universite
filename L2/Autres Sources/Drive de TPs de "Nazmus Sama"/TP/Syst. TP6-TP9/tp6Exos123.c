#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#define NB 16
int main(int argc, char *argv[]) {
	int fd;
	unsigned char buffer[NB];
	int nbyte;
	int i;
	size_t taille, pos;
	if (argc < 2) {
		fprintf(stderr, "Donner un nom de fichier\n");
		return EXIT_FAILURE;
	}
	if ((fd = open(argv[1], O_RDONLY)) == -1) {
		fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[1]);
		return EXIT_FAILURE;
	}
	nbyte = read(fd, buffer, NB);
	for (i = 0; i < nbyte; i++) {
		printf("%02x ", buffer[i]);
	}
	printf("\n");
	taille = lseek(fd, 0, SEEK_END);
	pos = (taille < NB) ? 0 : taille-NB;
	lseek(fd, pos, SEEK_SET);
	nbyte = read(fd, buffer, NB);
	for (i = 0; i < nbyte; i++) {
		printf("%02x ", buffer[i]);
	}
	printf("\n");
	close(fd);
	return EXIT_SUCCESS;
}

//exo 2

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#define N 10
#define BUFSIZE 1024
int decoupe(int fin, char *fname) {
	int fout;
	unsigned char buffer[BUFSIZE];
	int nbyte;
	int l = strlen(fname);
	char *fnamei = malloc(l+10);
	int i, j;
	for (i = 1; (nbyte = read(fin, buffer, BUFSIZE)) > 0; i++) {
		sprintf(fnamei, "%s.%d", fname, i);
		if ((fout = open(fnamei, O_CREAT|O_TRUNC|O_WRONLY, S_IREAD|S_IWRITE)) == -1) {
			fprintf(stderr, "Erreur ouverture fichier \"%s\"\n", fnamei);
			return EXIT_FAILURE;
		}
		if (write(fout, buffer, nbyte) < nbyte) {
			fprintf(stderr, "Erreur ecriture fichier %s\n !\n", fnamei);
			return EXIT_FAILURE;
		}
		//reading things in batches of 1024 to ensure there's enough memory
		for (j = 1; j < N && (nbyte = read(fin, buffer, BUFSIZE)) > 0; j++) {
			if (write(fout, buffer, nbyte) < nbyte) {
				fprintf(stderr, "Erreur ecriture fichier %s\n !\n", fnamei);
				return EXIT_FAILURE;
			}
		}
		close(fout);
	}
	return EXIT_SUCCESS;
}
int main(int argc, char *argv[]) {
	int fin;
	if (argc == 1) {
		fprintf(stderr, "Donner un nom de fichier a decouper !\n");
		return EXIT_FAILURE;
	}
	for (int i = 1; i < argc; i++) {
		if ((fin = open(argv[i], O_RDONLY)) == -1) {
			fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[i]);
			return EXIT_FAILURE;
		}
		int retval = decoupe(fin, argv[i]);
		if (retval != EXIT_SUCCESS)
			return retval;
		close(fin);
	}
	return EXIT_SUCCESS;
}

//exo 3

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#define BUFSIZE 1024
void cat(int fin, int fout) {
unsigned char buffer[BUFSIZE]; /* buffer pour la lecture des données */
int nbyte; /* pour le nombre d’octets effectivement lus à chaque itération */
	while ((nbyte = read(fin, buffer, 10)) > 0) {
		/* nbyte > 0 ont étés lus de l’entrée, affiche le s sur la sortie */
		write(fout, buffer, nbyte);
	}
/* on termine quand 0 byte ont été lus de l’entrée, cela arrive
quand en entrée on est arrivé au EOF */
}
int main(int argc, char *argv[]) {
	int fin;
	int fout = 1; /* sortir standard */
	if (argc == 1) {
		fin = 0; /* entree standard */
		cat(fin, fout);
		return EXIT_SUCCESS;
	}
	for (int i = 1; i < argc; i++) {
		if ((fin = open(argv[i], O_RDONLY)) == -1) {
			fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[i]);
			return EXIT_FAILURE;
		}
		cat(fin, fout);
		close(fin);
	}
	return EXIT_SUCCESS;
}