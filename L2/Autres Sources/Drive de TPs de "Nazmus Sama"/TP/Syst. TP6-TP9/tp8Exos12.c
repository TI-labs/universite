#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
int main(int argc, char *argv[]) {
	pid_t pid = fork();
	if (pid == 0) {
		printf(">> C’est le fils qui parle.\n");
		printf(">>\tMon pid est %ld\n", (long int) getpid());
		printf(">>\tLe pid de mon père est %ld\n", (long int) getppid());
	} else {
		pid_t pidfils;
		wait(&pidfils);
		printf("> C’est le père qui parle.\n");
		printf(">\tLe pid de mon fils est %ld\n", (long int) pid);
		printf(">\tMon pid est %ld\n", (long int) getpid());
		printf(">\tLe pid du grand-père de mon fils, donc mon père est %ld\n",
			(long int) getppid());
	}
}

//exo 2


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/*
* Compiler avec l’option
* -DNOWAIT
* pour une version qui n’attend pas la terminaison du fils
*/
int main(int argc, char *argv[]) {
	char path[256];
	pid_t pid;
	while (1) {
		printf("Chemin : ");
		scanf("%255s", path);
		if ((pid = fork()) == -1) {
			perror("fork échoué");
			return -1;
		}
		if (pid == 0) {
/* le fils */
			execl("/bin/ls", "ls", "-l", path, NULL);
			perror("exec échoué");
		}
#ifndef NOWAIT
/* attend la terminaison du fils */
		pid_t pidw = wait(NULL);
		if (pidw != pid) {
			printf("Le fork pid %d et le wait pid %d different !\n", pidw, pid);
		} else {
			printf("Fils %d a terminé son exécution\n", pid);
		}
#endif
/* le père continue la bloucle */
	}
}