(Voir le cours, ce fichier est imcomplet )
-Il existe 3 types de fichiers : fichier normal, dossier (contiennent des infos sur les fichiers) et les fichiers spéciaux ( ceux qui representent les entrées sorties ...)

-Le principe de partitionnement est important :
*Le disque dur est séparé en plusieurs partitions: la première partition va servir au démarrage de la machine et va  contenir le MBR (master boot record )
le MBR contient des l'adresse de début et de fin de  chaque partition

-Il existe 3 manières d'organiser les fichiers dans un disque (vérifier si c'est disque dur): 
* Allocation contigue : On  les ranges les un après les autres (Comme un tableau), il suffit donc d'avoir l'adresse du premier fichier pour pouvoir accéder a tous les autres
* Allocation par liste chainé :On fait comme dans une liste chainé, le début de chaque fichier contient l'adresse vers le prochain fichier
* Allocation par liste chaînée avec table en mémoire: voir le cours

VOIR LE COURS POUR LE RESTE

