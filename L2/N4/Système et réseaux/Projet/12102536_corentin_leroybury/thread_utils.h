/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */
#ifndef THREAD_UTILS_H
#define THREAD_UTILS_H

/** Les arguments des threads contiennent :
 * - L'adresse de tous les sémaphores et mutex nécessaires au multi-threading
 * - L'adresse de la file d'attente des clients
 * - L'adresse du catalogue de comptines
 * - 
 * - L'adresse du stockage temporaire de la socket et l'ip du client. 
 * - La chaîne de caractère représentant le chemin du dossier contenant les comptines */
typedef struct arguments {
	sem_t *sem_client, *clt_retrieved, *clt_available, *sem_catalogue;
	pthread_mutex_t *mutex_queue, *mutex_log, *mutex_catalogue;
	int nb_using_catalogue;
	struct catalogue *c;
	struct queue *client_queue;
	int *client_sock;
	struct in_addr *client_ip;
	char *dir_name;
} arguments;

/* Initialise les sémaphores et mutex d'une structure arguments donnée */
void init_args(arguments *args);

/* Libère toutes les ressources associées à l'adresse args */
void destroy_args(arguments *args);

/* Ecrit dans le fichier de log la requête request demandée par le client d'adresse ip IP
   Si nc >= 0, cela correspond à une demande de comptine et affiche son numéro */
void write_log(struct in_addr ip, pthread_mutex_t *mutex_log, char *request, int nc, char *path_name);

/* Basé sur le problème de l'écrivain et des lecteurs */
void client_start_reading_catalogue(arguments *args);
void client_stop_reading_catalogue(arguments *args);

#endif /* ifndef THREAD_UTILS_H */