/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */
#ifndef QUEUE_H
#define QUEUE_H

#include <inttypes.h>

typedef struct queue {
	uint16_t max_size, size;
	int *client_sock;
	struct in_addr *client_ip;
} queue;

/** Alloue sur le tas une queue de taille maximale max_size 
 *  Renvoi NULL en cas d'erreur */
queue* init_queue(const uint16_t max_size);

/* Libère l'ensemble des ressources allouées sur le tas par init_queue */
void free_queue(queue* q);

/** Ajoute un couple int/in_addr à la fin de la file
 *  Renvoi 0 en cas d'erreur */
int enqueue(queue* q, int sock, struct in_addr ip);
/** Défile un couple int/in_addr du début de la file et
 *  les retournent aux adresses client_sock et client_ip
 *  Renvoi 0 en cas d'erreur */
int dequeue(queue* q, int *sock_addr, struct in_addr *ip_addr);

/* Affiche chaque élément de la file */
void display_queue(queue* q);

#endif /* ifndef QUEUE_H */