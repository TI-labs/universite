/* Corentin LEROY-BURY 12102536
Je déclare qu'il s'agit de mon propre travail. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "comptine_utils.h"

/* Ajout post tp10 */

ssize_t exact_read(int fd, void *buf, size_t nbytes) {
	ssize_t n, total;
	for(total = 0; total < nbytes; total += n)
		n = read(fd,(char *) buf + total, nbytes - total);
	return total;
}

int read_until_nl(int fd, char *buf) {
	int i;
	/* Tant que l'on peut lire des octets on continue */
	for(i = 0; read(fd,buf,sizeof(char)) == 1; i++) {
		/* Si cet octet est un saut de ligne on arrête */
		if(*buf == '\n')
			return i;
		buf++;
	} return i;
}

int est_nom_fichier_comptine(char *nom_fich) {
	int i = 0;
	while(nom_fich[i++] != '\0');
	if(i < 5) 
		return 0;
	/* On verifie que les 4 derniers caractères avant le '\0' correspondent bien à ".cpt" */
	return(nom_fich[i-5] == '.' && nom_fich[i-4] == 'c' && nom_fich[i-3] == 'p' && nom_fich[i-2] == 't');
}

struct comptine *init_cpt_depuis_fichier(const char *dir_name, const char *base_name) {
	/* On définie le chemin vers la comptine */
	char file_path[256] = "";
	strcat(file_path,dir_name);
	strcat(file_path,base_name);

	/* On verifie que la comptine visée existe et on l'ouvre */
	int fd;
	if((fd = open(file_path, O_RDONLY)) < 0) {
		perror("open");
		return NULL;
	}

	/* Allocation de l'espace mémoire nécessaire pour la comptine */
	struct comptine *result = malloc(sizeof(struct comptine));
	if(result == NULL) {
		perror("malloc");
		close(fd);
		return NULL;
	} 

	char buf[256];
	int titre_len = read_until_nl(fd,buf);
	result->titre = malloc((titre_len + 2) * sizeof(char));
	result->nom_fichier = malloc((strlen(base_name)+1) * sizeof(char));
	if(result->nom_fichier == NULL || result->titre == NULL) {
		perror("malloc");
		liberer_comptine(result);
		close(fd);
		return NULL;
	}
	/* Copie des chaînes de caractères dans la structure */
	buf[titre_len + 1] = '\0';
	strcpy(result->nom_fichier,base_name);
	strcpy(result->titre,buf);
	close(fd);
	return result;
}

void liberer_comptine(struct comptine *cpt) {
	if(cpt != NULL) {
		free(cpt->titre);
		free(cpt->nom_fichier);
	} free(cpt);
}

struct catalogue *creer_catalogue(const char *dir_name) {
	
	DIR* cp_dir;
	if((cp_dir = opendir(dir_name)) == NULL) {
		perror("opendir");
		return NULL;
	}

	/* Parcours du dossier pour compter le nombre de fichiers ayant l'extension .cpt */
	struct dirent *cp_file;
	int nb_cp = 0;
	for(cp_file = readdir(cp_dir); cp_file != NULL; cp_file = readdir(cp_dir))
		if(est_nom_fichier_comptine(cp_file->d_name))
			nb_cp++;

	rewinddir(cp_dir);
	/* Allocation de toutes les ressources nécessaires */
	struct catalogue *result = malloc(sizeof(struct catalogue));
	if(result == NULL) {
		perror("malloc");
		closedir(cp_dir);
		return NULL;
	} result->tab = malloc(nb_cp * sizeof(struct comptine *));
	if(result->tab == NULL) {
		perror("malloc");
		closedir(cp_dir);
		free(result);
		return NULL;
	} result->nb = nb_cp;

	/* Pour chaque fichier dans dir_name, s'il est une comptine, on créé une struct comptine associée
	   que l'on ajoute au catalogue */
	int i = 0;
	for(cp_file = readdir(cp_dir); cp_file != NULL; cp_file = readdir(cp_dir))
		if(est_nom_fichier_comptine(cp_file->d_name))
			if((result->tab[i++] = init_cpt_depuis_fichier(dir_name,cp_file->d_name)) == NULL)
				return NULL;
	closedir(cp_dir);
	return result;
}

void liberer_catalogue(struct catalogue *c) {
	if(c != NULL) {
		if(c->tab != NULL)
			for(int i = 0; i < c->nb; i++)
				liberer_comptine(c->tab[i]);
		free(c->tab);
	} free(c);
}
