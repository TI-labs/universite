#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
void *alice(void *);
void *bob(void *);
void faire_des_choses();
char res;
int alice_a_fini = 0;
int main()
{
	pthread_t a, b;
	srand(3);
	pthread_create(&a, NULL, alice, NULL);
	pthread_create(&b, NULL, bob, NULL);

	pthread_join(a, NULL);
	pthread_join(b, NULL);
	return 0;
}
void faire_des_choses() { usleep(rand() % 10 * 50000); }
void *alice(void *arg) {
	faire_des_choses();
	res = 'A';
	alice_a_fini = 1;
}
void *bob(void *arg) {
	faire_des_choses();
	while (!alice_a_fini)
		;
	printf("%c\n", res);
}
