#include <stdio.h>
#include <unistd.h>
#include <signal.h>

int main()
{
	switch (fork()) {
	case -1:
		perror("fork");
		return 1;
	case 0:
		for (;;)
			if (fork() == -1)
				perror("fork");
	}
	sleep(5);
	kill(0, SIGTERM);
	return 0;
}
