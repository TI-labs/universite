#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <wait.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	pid_t enf1;
	int stat;
	switch ((enf1 = fork())) {
	case -1:
		perror("fork");
		return 1;
	case  0:
		srand(time(NULL));
		sleep(10);
		return rand() % 3;
	}
	printf("%d: j'ai un enfant qui a pour PID %d\n", getpid(), enf1);
	wait(&stat);
	if (WIFEXITED(stat))
		printf("il a terminé avec statut de sortie %d\n", 
				WEXITSTATUS(stat));
	else
		printf("il a été tué par le signal %d\n", WTERMSIG(stat));

	return 0;
}
