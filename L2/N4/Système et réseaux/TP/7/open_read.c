#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSZ 256
int main()
{
	int fd = open("lol.txt", O_RDONLY);
	ssize_t n; /* signed size type pour pouvoir accueillir -1 */
	char buffer[BUFSZ];

	if (fd < 0) {
		perror("open");
		return 1;
	}

	//printf("Descripteur de fichier : %d\n", fd);
	
	n = read(fd, buffer, 1); /* lire au maximum 1 octet */
	//printf("n = %ld\n", n);
	//printf("octet 0 du fichier : %c\n", buffer[0]);
	write(0, buffer, n);

	n = read(fd, buffer, 1); /* lire au maximum 1 octet */
	//printf("n = %ld\n", n);
	//printf("octet 1 du fichier : %c\n", buffer[0]);
	write(0, buffer, n);

	n = read(fd, buffer, 4);
	//buffer[4] = '\0';
	//printf("n = %ld\n", n);
	//printf("4 octets du fichier : %s\n", buffer);
	write(0, buffer, n);

	n = read(fd, buffer, 20);
	//buffer[n] = '\0';
	//printf("n = %ld\n", n);
	//printf("20 octets du fichier : %s\n", buffer);
	write(0, buffer, n);

	n = read(fd, buffer, 1);
	//printf("n = %ld\n", n);
	write(0, buffer, n);

	close(fd);

	return 0;
}
