/* fichiers de la bibliothèque standard */
#include <asm-generic/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* threads */
#include <pthread.h>

#include "sys_utils.h"
#include "question.h"

#define PORT_QUIZZP 6666
#define FICH_QUESTIONS "questionnaire.txt"

/* Pour pouvoir partager les tâches entre différents threads (en fin d'énoncé) */
struct work {
	int socket;
	struct question *q;
};

/** Créer et configure une socket d'écoute TCP sur IPv4 sur le port associé au
 * Quizz Protocol. Retourne le descripteur de fichier de la socket ainsi créée
 * en cas de succès, met fin au programme sinon. */
int init_sock_ecoute();

/** Accepter les demandes de connection entrantes sur la socket d'écoute
 * sock.
 * Retourne le descripteur de fichier de la socket d'échange créée ou -1 en cas
 * d'erreur */
int connection_au_client(int sock);

/** Recevoir l'adresse d'une variable de type struct work convenablement
 * initialisée, puis échanger avec un client en utilisant le champ socket de
 * cette structure pour la question (champ q) de cette strucure.
 * Retourne : toujours NULL. */
void *worker(void *work);

/* Mener tous les échanges avec le client connecté à la socket fd, ainsi que 
 * spécifié dans le Quizz Protocol, avec la question d'adresse q */
void echanger_avec_client(int fd, const struct question *q);
void echanger_avec_client_test(int fd, const struct question *q);

int main()
{
	struct banque_questions *bq = init_banque_questions(FICH_QUESTIONS);
	int sock = init_sock_ecoute();
#define MONO 1
	for (;;) {
		/* Version 1 sans threads */
		int se = connection_au_client(sock);
		if (se < 0)
			continue;
		echanger_avec_client(se, question_aleatoire(bq));
		close(se);
		/* Version 2 avec threads */
		/* TODO */
	}
	/* On n'y arrive jamais normalement. */
	close(sock);
	detruire_banque_questions(bq);
	return 0;
}

int init_sock_ecoute()
{
	int sock;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}

	struct sockaddr_in sa = { .sin_family = AF_INET, .sin_addr.s_addr = htonl(INADDR_ANY), .sin_port = htons(PORT_QUIZZP) };
	
	int opt = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

	if (bind(sock, (struct sockaddr*) &sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(2);
	}

	if (listen(sock, 128) < 0) {
		perror("listen");
		exit(3);
	}

	return sock;
}

int connection_au_client(int sock)
{
	struct sockaddr_in sa_clt;
	socklen_t sl = sizeof(sa_clt);
	int se = accept(sock, (struct sockaddr*) &sa_clt, &sl);

	if (se < 0) {
		perror("accept");
		exit(4);
	}

	char addr[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &sa_clt, addr, sl);

	printf("connection de %s\n", addr);

	return se;
}

void *worker(void *work)
{
	return NULL;
}

void echanger_avec_client(int fd, const struct question *q)
{
	/*
	char buf[7];
	read(fd, buf, 7);
	printf("%s\n", buf);
	strcpy(buf, "hop!");
	write(fd, buf, 6);
	read(fd, buf, 6);
	printf("%s\n", buf);
	close(fd);
	*/
	uint16_t buf = htons(q->nb_props);
	write(fd, &buf, sizeof(uint16_t));
	write(fd, q->enonce, strlen(q->enonce) + 1);
	for (int i = 0; i < q->nb_props; i++)
		write(fd, q->propositions[i], strlen(q->propositions[i]) + 1);
	uint16_t rep;
	read(fd, &rep, sizeof(rep));
	rep = ntohs(rep);
	uint8_t res = rep == q->ind_rep;
	write(fd, &res, sizeof(res));
	close(fd);
}

