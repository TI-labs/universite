#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#define DELAI 100000

void tache_a();
void tache_b();
void tache_c();
void tache_d();
void tache_e();
void *b_d(void*);

int main()
{
	srand(time(NULL));
	pthread_t t;
	sem_t s;
	sem_init(&s, 0, 0);
	pthread_create(&t, NULL, b_d, &s);
	tache_a();
	sem_wait(&s);
	tache_c();
	pthread_join(t, NULL);
	tache_e();
	sem_destroy(&s);
	return 0;
}

void tache_a() { usleep((rand() % 10) * DELAI); printf("AAAA\n"); }
void tache_b() { usleep((rand() % 10) * DELAI); printf("BBBB\n"); }
void tache_c() { usleep((rand() % 10) * DELAI); printf("CCCC\n"); }
void tache_d() { usleep((rand() % 10) * DELAI); printf("DDDD\n"); }
void tache_e() { usleep((rand() % 10) * DELAI); printf("EEEE\n"); }

void *b_d(void* sem) {
	sem_t* s = (sem_t*) sem;
	tache_b();
	sem_post(s);
	tache_d();
	return NULL;
}
