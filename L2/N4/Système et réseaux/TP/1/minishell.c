#include <wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_MAX 256

int main() {
	char s[BUFFER_MAX];
	char *vs[BUFFER_MAX];

	while (1) {
		printf("$ ");
		if (!fgets(s, BUFFER_MAX, stdin)) return 0;
		for (int i = 0; i < BUFFER_MAX; i++) {
			if (s[i] == '\n') {
				s[i] = '\0';
				break;
			}
		}


		int i = 0;
		vs[0] = strtok(s, " ");
		while (vs[i] != NULL) {
			vs[++i] = strtok(NULL, " ");
		}

		switch (fork()) {
			case -1:
				perror("fork error");
				exit(1);
			case 0:
				execvp(vs[0], vs);
				perror("exec error");
				exit(2);
		}

		wait(NULL);
	}
}
