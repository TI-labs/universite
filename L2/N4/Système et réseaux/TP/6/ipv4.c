#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

void string_to_uint32_and_cidr(char *src, uint32_t *addr, unsigned char *cidr);
void uint32_to_string(uint32_t ipv4, char *dest);

int main(int argc, char** argv) {
	uint32_t addr, mask, res, num, brd;
	unsigned char cidr;
	char dest[16];

	for (int i = 1; i < argc; i++) {
		string_to_uint32_and_cidr(argv[i], &addr, &cidr);
		uint32_to_string(addr, dest);
		printf("adresse ipv4 : %s\n", dest);
		mask = UINT32_MAX << (32 - cidr);
		uint32_to_string(mask, dest);
		printf("masque de sous-réseau : %s\n", dest);
		res = addr & mask;
		uint32_to_string(res, dest);
		printf("adresse du réseau : %s\n", dest);
		num = addr - res;
		printf("numéro de la machine dans le réseau : %d\n", num);
		brd = res | (~mask);
		uint32_to_string(brd, dest);
		printf("adresse de broadcast dans le réseau : %s\n\n", dest);
	}
}

void string_to_uint32_and_cidr(char *src, uint32_t *addr, unsigned char *cidr) {
	uint8_t b0, b1, b2, b3;
	printf("%s\n", src);
	sscanf(src, "%" SCNu8 ".%" SCNu8 ".%" SCNu8 ".%" SCNu8 "/%hhu", &b0, &b1, &b2, &b3, cidr);
	*addr = (b0 << 24) + (b1 << 16) + (b2 << 8) + b3;
}

void uint32_to_string(uint32_t ipv4, char *dest) {
	sprintf(dest, "%d.%d.%d.%d", ipv4 >> 24, (ipv4 << 8) >> 24, (ipv4 << 16) >> 24, (ipv4 << 24) >> 24);
}
