#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#define N 5
#define COEFF_ATTENTE 50000
void work();
void *chiffres_et_alphabet(void *);
sem_t barriere;
pthread_mutex_t maj;
int nombres;

int main()
{
	int i, nums[N];
	pthread_t th[N];
	srand(time(NULL));

	sem_init(&barriere, 0, 0);
	pthread_mutex_init(&maj, NULL);

	for (i = 0; i < N; ++i) {
		nums[i] = i;
		if (pthread_create(&th[i], NULL, chiffres_et_alphabet, &nums[i]) != 0) {
			perror("create");
			return 1;
		}
	}

	for (i = 0; i < N; ++i)
		pthread_join(th[i], NULL);

	sem_destroy(&barriere);
	pthread_mutex_destroy(&maj);
	return 0;
}

void *chiffres_et_alphabet(void *arg)
{
	int i = *((int *) arg);
	printf("%d\n", i);

	pthread_mutex_lock(&maj);
	if (++nombres == N)
		sem_post(&barriere);
	pthread_mutex_unlock(&maj);

	sem_wait(&barriere);
	printf("%c\n", 'A' + i);
	sem_post(&barriere);
	return NULL;
}

void work() { usleep((rand() % 10) * COEFF_ATTENTE); }
