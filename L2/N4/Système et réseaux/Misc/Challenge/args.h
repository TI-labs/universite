#ifndef __PARSE_H
#define __PARSE_H

#include <sys/socket.h>

typedef struct {
	struct sockaddr* src;
	socklen_t src_s;
	struct sockaddr* dest;
	socklen_t dest_s;
} args_t;

args_t* parse_args(int argc, char** argv);
void destroy_args(args_t* args);

void wait_for_connection(args_t* args);

#endif
