#include "args.h"
#include "config.h"
#include "common.h"

#include <arpa/inet.h>
#include <asm-generic/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>

args_t* parse_args(int argc, char **argv) {
	args_t* args = (args_t*) malloc(sizeof(args_t));
	args->dest = NULL;
	struct sockaddr_in* def = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
	def->sin_family = AF_INET;
	def->sin_port = htons(DEFAULT_PORT);
	def->sin_addr.s_addr = htonl(INADDR_ANY);
	args->src = (struct sockaddr*) def;
	args->src_s = sizeof(struct sockaddr_in);

	int set_port = 0;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-p") == 0) {
			set_port = 1;
		} else if (set_port) {
			def->sin_port = htons(atoi(argv[i]));
			set_port = 0;
		} else {
			char ip[50]; uint16_t port; struct in6_addr addr6; int res;

			if (sscanf(argv[i], "[%49[^]]]:%" SCNu16, ip, &port) == 2) {
				struct sockaddr_in6* dest = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
				dest->sin6_family = AF_INET6;
				dest->sin6_port = htons(port);
				res = inet_pton(AF_INET6, ip, (void*) &dest->sin6_addr);
				args->dest = (struct sockaddr*) dest;
				args->dest_s = sizeof(struct sockaddr_in6);
			} else if ((res = inet_pton(AF_INET6, argv[i], (void*) &addr6)) > 0) {
				struct sockaddr_in6* dest = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
				dest->sin6_family = AF_INET6;
				dest->sin6_port = htons(DEFAULT_PORT);
				dest->sin6_addr = addr6;
				args->dest = (struct sockaddr*) dest;
				args->dest_s = sizeof(struct sockaddr_in6);
			} else if (sscanf(argv[i], "%49[^:]:%" SCNu16, ip, &port) == 2) {
				struct sockaddr_in* dest = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
				dest->sin_family = AF_INET;
				dest->sin_port = htons(port);
				res = inet_pton(AF_INET, ip, (void*) &dest->sin_addr);
				args->dest = (struct sockaddr*) dest;
				args->dest_s = sizeof(struct sockaddr_in);
			} else {
				struct sockaddr_in* dest = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
				dest->sin_family = AF_INET;
				dest->sin_port = htons(DEFAULT_PORT);
				res = inet_pton(AF_INET, argv[i], (void*) &dest->sin_addr);
				args->dest = (struct sockaddr*) dest;
				args->dest_s = sizeof(struct sockaddr_in);
			}

			if (res <= 0) {
				PRTERR("Could not parse the IP address\n");
				exit(1);
			}
		}
	}

	return args;
}

void destroy_args(args_t *args) {
	free(args->dest);
	free(args->src);
	free(args);
}

void wait_for_connection(args_t* args) {
	printf(LOG "Waiting for a connection...\n" CLR);
	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		ERRNO("socket");
		exit(2);
	}

	int opt = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

	if (bind(sock, args->src, args->src_s) < 0) {
		ERRNO("bind");
		exit(3);
	}

	struct sockaddr_in* dest = (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));
	socklen_t dest_s = sizeof(struct sockaddr_in);

	if (recvfrom(sock, NULL, 0, 0, (struct sockaddr*) dest, &dest_s) < 0) {
		ERRNO("recv ping");
		exit(4);
	}

	close(sock);

	printf(LOG "Established connection with %s:%" PRIu16 "\n" CLR, inet_ntoa(dest->sin_addr), ntohs(dest->sin_port));

	args->dest = (struct sockaddr*) dest;
	args->dest_s = dest_s;
}
