#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

void aff_proc(const char *ch);

int main()
{
	switch (fork()) {
	case -1:
		perror("fork 1");
		return 1;
	case 0:
		aff_proc("Enfant fork 1");
		break;
	default:
		aff_proc("Parent fork 1");
	}

	switch (fork()) {
	case -1:
		perror("fork 2");
		return 1;
	case 0:
		aff_proc("Enfant fork 2");
		break;
	default:
		aff_proc("Parent fork 2");
	}

	printf("Je suis le processus %d et j'ai terminé\n", getpid());
	return 0;
}

void aff_proc(const char *ch)
{
	printf("%s : PID : %d, PPID : %d\n", ch, getpid(), getppid());
}
