#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define N_DFT 2
#define P_DFT 3

#define FMT_PROC "pid : %d; ppid : %d; n : %d; p : %d\n"

int main(int argc, char *argv[])
{
	int n = argc < 2 ? N_DFT : atoi(argv[1]);
	int p = argc < 3 ? P_DFT : atoi(argv[2]);
	int i;

	if (p == 0) {
		printf("Sortie 1\n");
		return 0;
	}

	for (i = 0; i < n; ++i)
		switch (fork()) {
		case -1:
			perror("fork");
			return 1;
		case 0:
			printf(FMT_PROC, getpid(), getppid(), n, p);
			char str_n[11], str_p[11];
			sprintf(str_n, "%d", n);
			sprintf(str_p, "%d", p - 1);
			execl("encore", "encore", str_n, str_p, NULL);
			printf("Sortie 2\n");
			return 0;
		}

	while (wait(NULL) != -1)
		;
	printf("Sortie 3\n");
	return 0;
}
