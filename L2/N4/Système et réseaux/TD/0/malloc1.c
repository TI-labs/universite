#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

void f(void *p)
{
	uint8_t *c = (uint8_t *) p;
	printf("%x\n", *c);
}

int main()
{
	/* DANS LA VRAIE VIE, écrire sizeof(unsigned) au lieu de 4 */
	unsigned *n = (unsigned *) malloc(4);
	char *c = (char *) n;

	c[0] = 42;
	f((void *) c);
	printf("%c\n", *c);

	*n = 0x7e;
	f((void *) c);
	printf("%c\n", *c);
	printf("%c\n", *n);
	printf("%s\n", (char *) n);

	*n = 0x40000000;
	printf("%u\n", *n);
	c += 3;
	printf("%c\n", *c);

	*c = '\0';
	c -= 2;
	*c = 'A';
	f((void *) c);
	printf("%u\n", *n);

	strcpy(c, "lol\n");
	printf("%s\n", c);

	free((void *) c);
	return 0;
}
