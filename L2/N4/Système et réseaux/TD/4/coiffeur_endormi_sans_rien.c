#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_CLIENTS 10
#define NUM_CHAISES 6
#define COEFF_ATTENTE 500000
int num_chaises_vides = NUM_CHAISES;
/* Choses à ajouter ici */
void *client(void *arg);
void *coiffeur(void *arg);
void coiffer();
void bavarder_avec_le_coiffeur();

int main()
{
	int i
	pthread_t clt[NUM_CLIENTS], coiff;
	/* Préparatifs : initialisations... */
	/* Choses à faire ici */

	pthread_create(&coiff, NULL, coiffeur, NULL);
	for (i = 0; i < NUM_CLIENTS; ++i)
		pthread_create(&clt[i], NULL, client, NULL);

	/* De toute façon les boucles sont infinies, ... */
	for (i = 0; i < NUM_CLIENTS; ++i)
		pthread_join(clt[i], NULL);
	pthread_join(coiff, NULL);
	return 0;
}
void *coiffeur(void *arg)
{
	/* Fonction à modifier */
	for (;;) {
		coiffer();
	}
}
		
void *client(void *arg)
{
	/* Fonction à modifier */
	for (;;) {
		bavarder_avec_le_coiffeur();
	}
}
void coiffer() {
	usleep((double) rand() / RAND_MAX * COEFF_ATTENTE);
}
void bavarder_avec_le_coiffeur() {
	usleep((double) rand() / RAND_MAX * COEFF_ATTENTE);
}
