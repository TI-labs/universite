use sdl2;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::time::Duration;
use std::env;
use sierpinski::*;

/// Taille par défaut de l'écran.
const SIZE: u32 = 600;

/// Ratio par défaut de la bordure entre la figure et l'écran.
const BORDER_RATIO: u32 = 48;

/// Itération par défaut de l'algorithme.
const N: u32 = 5;

fn main() {
    // On récupère le premier argument passé à l'exécutable. Si ce dernier
    // n'existe pas ou qu'il n'est pas convertissable en u32, on utilise
    // la valeur par défaut de la constante N.
    // La beauté de Rust nous permet de faire ça hyper-facilement en 4 lignes
    // avec du pattern matching, des iterateurs et des génériques sous le
    // tapis.
    let n = match env::args().nth(1) {
        Some(n) => n.parse().unwrap_or(N),
        None => N
    };

    // On va initialiser SDL2, et récupérer le contexte, puis l'objet donnant
    // un interface au système d'affichage.
    let sdl_context = sdl2::init().expect("Unable to init SDL");
    let video_subsystem = sdl_context.video().unwrap();

    // On crée une fenêtre. Qu'on configure avec plusieurs options, qui ne sont
    // pas très intéressantes.
    let window = video_subsystem
        .window("sierpinski", SIZE, SIZE)
        .allow_highdpi()
        .position_centered()
        .resizable()
        // `vulkan()` permet des meilleures performances, mais, peut ne pas
        // marcher sur d'anciennes machines ou sur MacOS. Si vous rencontrez
        // des problèmes lors de l'exécution, remplacez `vulkan()` par
        // `opengl()`.
        .vulkan()
        .build()
        .unwrap();

    // Ensuite, on récupère une interface vers notre canevas, notre "toile" qui
    // nous permet de dessiner sur notre fenêtre.
    let mut canvas = window.into_canvas().build().unwrap();

    // On pense déjà à peindre notre canevas en blanc. On prend comme couleur
    // la couleur blanche représentée par `Color::RGB(255, 255, 255)`, puis on
    // nettoie l'écran avec `clear()``, ce qui peint l'entièreté de notre écran
    // avec la couleur définie précédemment.
    // Enfin, on affiche notre résultat avec `present()`.
    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.clear();
    canvas.present();

    // On récupère les évennements de notre fenêtre. Ça nous permettra de savoir
    // si par exemple l'utilisateur tente de fermer la fenêtre pour qu'on puisse
    // clore notre programme.
    let mut event_pump = sdl_context.event_pump().unwrap();

    // Ensuite, on rentre dans une boucle infinie, à laquelle on donne le nom
    // `'running`. Vous connaissez cette situation toujours incomfortable en C
    // où on a deux boucles imbriquées et qu'on essaye de break de la première.
    // Rust résout ça en donnant la possibilité de nommer ses boucles.
    //
    // `loop` est une boucle infinie. La raison pour laquelle dans les
    // programmes graphiques on entre toujours dans une boucle infinie, c'est
    // pour pouvoir garder le programme en vie (parce que si l'on continuait
    // l'exécution, on finirait le programme et ce dernier se fermerait quasi
    // immédiatement du point de vue de l'utilisateur), mais aussi de pouvoir
    // éventuellement réagir aux intéractions de l'utilisateur. Par exemple,
    // si l'utilisateur redimmensionne la fenêtre, on doit repeindre l'entièreté
    // du cannevas, vu que les positions et la taille de notre triangle aura
    // changé.
    'running: loop {
        // On regarde déjà si l'on a reçu des évennements.
        for event in event_pump.poll_iter() {
            // Et si l'un de ses évennement est `Event::Quit` (demande de
            // l'utilisatuer de quitter le programme, en gros, quand on essaye
            // de fermer la fenêtre) ou `Event::KeyDown` avec comme `keycode`
            // `Keycode::Escape` (en gros, quand l'utilisateur tape sur
            // `Keycode::Escape`), on quitte le programme en cassant note boucle
            // infinie. Ici, on peut d'ailleurs admirer la puissance du pattern
            // matching de Rust dans toute sa beauté.
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
        }

        // On récupère les dimensions de notre canevas. Pourquoi ne pas utiliser
        // `SIZE` ? Deux raisons :
        // 1- L'utilisateur a pu redimensionner la fenêtre, donc, ses dimensions
        //    ne correspondent plus à `SIZE`.
        // 2- Les écrans hautes définitions, ou HiDPI, font que la taille de
        //    la fenêtre (qui est """relative""") ne correspond plus à la
        //    largeur du canevas (qui est """absolue""") (C'est plus complexe
        //    que ça en vrai).
        let (x, y) = canvas.output_size().unwrap();

        // Ceci peut parraître barbare, mais, en gros, on prend le min de la
        // taille verticale et horizontale, et on calcul un "padding", une
        // différence entre les tailles, qui nous servira à centrer notre
        // triangle si la hauteur et la largeur ne sont pas de taille égale.
        let (mut size, padding) = if x <= y {
            (x, (0, (y - x)/2))
        } else {
            (y, ((x - y)/2, 0))
        };

        // On récupère la taille de notre bordure.
        let border = size / BORDER_RATIO;
        // Et on la soustrait à notre taille, qui va devenir la taille du
        // triangle.
        size -= 2*border;

        // On efface ce qu'il y avait précédemment sur le canevas (à la frame
        // d'avant).
        canvas.set_draw_color(sdl2::pixels::Color::RGB(255, 255, 255));
        canvas.clear();

        // On crée notre triangle de Sierpinski avec comme `p1` le coin
        // inférieur gauche, `p2` le coin supérieur et `p3` le coin inférieur
        // droit.
        sierpinski(&mut canvas, n,
                   (border + padding.0, size + border + padding.1),
                   ((size + 2*border)/2 + padding.0, border + padding.1),
                   (size + border + padding.0, size + border + padding.1));

        // On affiche le résultat.
        canvas.present();

        // On dort pendant 1/30s, ce qui fait que notre programme redessine la
        // fenêtre 30 fois par seconde (30fps).
        // En réalité, c'est un peu moins que ça en prenant en compte le temps
        // de réaliser les opéations précédentes.
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 30));
    }
}
