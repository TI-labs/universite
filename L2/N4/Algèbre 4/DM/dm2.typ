#align(center, text(20pt)[
	*Devoir Maison 2
	d'Algèbre*
])

#align(center, text(15pt)[
	Lyes Saadi \
	12107246
])

= Exercice 1
Soient $V$ un $K$-espace vectoriel de dimension $n$ et $V^*$ son espace dual.
$V$ est muni d'une base $Beta = (e_1, ..., e_n)$ et $Beta^* = (epsilon.alt_1, ..., epsilon.alt_n)$ est la base $V^*$, duale de la base $Beta$.
Pour tout $x in V$, on rappelle que l'application "évaluation en $x$" : $\ev_x : V^* arrow.r.long K; alpha arrow.r.long.bar \ev_x(alpha) = alpha(x)$ est une forme linéaire $V^*$, en d'autres termes un élément du bidual $V^(**)$ de $V$ et que l'application "évaluation", $\ev : V arrow.r.long V^(**), x arrow.r.long.bar \ev_x$ est linéaire et est, en dimension finie, un isomorphisme d'espaces vectoriels. Enfin, on rappelle les notations : pour tout $S subset V, S^0 := {alpha in V^*; forall x in S, alpha(x) = 0}$ et pour tout $T subset V^*, T_0 := {x in V; forall alpha in T, alpha(x) = 0}$.

== Question 1.1

#set enum(numbering: "(a)")

+ Rappeler comment sont définies les formes linéaires $epsilon.alt_i$ de la base $Beta^*$. Pour un vecteur $x$ de $V$ montrer alors que ${x}_Beta =\ ^t(epsilon.alt_1(x), epsilon.alt_2(x), ..., epsilon.alt_n(x))$.

#line(length: 100%)

#line(length: 100%)

#enum(start: 2, [
	Justifier le fait que $(\ev_e_1, \ev_e_2, ..., \ev_e_n)$ est la base de $V^(**)$, duale de la base $Beta^*$. Soit $alpha in V^*$, qu'elles sont les coordonnées ${alpha}_(Beta^*)$ de $alpha$ dans la base $Beta^*$ ?
])

#line(length: 100%)

#line(length: 100%)

#enum(start: 3, [
	Justifier que $(\(Vect)(e_1, ..., e_n))^0 = {0}$ et que $(\Vect(epsilon.alt_1, ..., epsilon.alt_n))_0 = {0}.$
])

#line(length: 100%)

#line(length: 100%)

#enum(start: 4, [
])

#line(length: 100%)

#line(length: 100%)

#enum(start: 5, [
])

/*
#line(length: 100%)

#line(length: 100%)

#enum(start: 0, [
])
*/
