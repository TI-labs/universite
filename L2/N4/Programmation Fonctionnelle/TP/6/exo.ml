let rec prefixe u v = match (u,v) with
    [],_ -> true
    | e::uu,e'::vv -> (e == e') && (prefixe uu vv)
    | _ -> false;;

let suffixe u v = prefixe (List.rev u) (List.rev v);;

let rec facteur u v = match v with
    [] -> (match u with [] -> true | _ -> false)
    | _::vv -> (prefixe u v) || (facteur u vv);;

let rec map f l = match l with
    [] -> [] | x::ll -> (f x)::(map f ll) ;;

let liste_des_decompositions u =
    match u with
        [] -> [[[];[]]]
        | e::u' ->
                let f l =
                    match l with
                        e'::ll -> (e::e')::ll
                        | _ -> l
                in
                [[]; u]::(map f (liste_des_decompositions u'));

