let rec pliage_gauche f a l = match l with
    [] -> a
    | x::ll -> pliage_gauche f (f a x) ll ;;

let somme l = pliage_gauche (+) 0 l;;

let filtre_bis f l = let ft f l' e =
        if (f e) then
            e::l'
        else
            l'
    in List.rev (pliage_gauche (ft f) [] l);;
