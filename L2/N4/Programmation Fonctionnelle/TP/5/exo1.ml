let rec map f l = match l with
    [] -> [] | x::ll -> (f x)::(map f ll) ;;

let somme l = let sq x = x * x in map sq l ;;

let liste_longueurs l =
    let rec long l' = match l' with
        [] -> 0
        | e::ll -> (long ll) + 1
    in map long l ;;
