let rec inserer x l = match l with
    [] -> x::[]
    | e::ll -> if (x < e) then
        x::l
    else
        e::(inserer x ll);;

let tri_insertion_acc l =
    let rec _tri_interne l1 l2 = match l1 with
        [] -> l2
        | e::ll1 -> _tri_interne ll1 (inserer e l2)
    in
        _tri_interne l [];;

let rec tri_insertion l =
    match l with
        [] -> []
        | e::ll -> inserer e (tri_insertion ll);;
