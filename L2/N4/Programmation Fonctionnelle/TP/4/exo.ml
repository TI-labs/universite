type fp = X of int | Vrai | Faux | Et of fp * fp | Ou of fp * fp | Imp of fp * fp | Equiv of fp * fp | Non of fp;;

(*
let rec elim f = match f with
    X(i) -> X(i)
    | Vrai -> Vrai
    | Faux -> Faux
    | Et(g, d) -> Et(elim g, elim d)
    | Ou(g, d) -> Ou(elim g, elim d)
    | Imp(g, d) -> Imp(elim g, elim d)
    | Equiv(g, d) -> Equiv(elim g, elim d)
    | Non(f') -> Non(elim f');;
*)

let rec elim_equiv f = match f with
    Et(g, d) -> Et(elim_equiv g, elim_equiv d)
    | Ou(g, d) -> Ou(elim_equiv g, elim_equiv d)
    | Imp(g, d) -> Imp(elim_equiv g, elim_equiv d)
    | Equiv(g, d) -> Ou(Et(elim_equiv g, elim_equiv d), Et(Non(elim_equiv g), Non(elim_equiv d)))
    | Non(f') -> Non(elim_equiv f')
    | _ -> f;;

let rec elim_imp f =
    match f with
        Et(g, d) -> Et(elim_imp g, elim_imp d)
        | Ou(g, d) -> Ou(elim_imp g, elim_imp d)
        | Imp(g, d) -> Ou(Non(elim_imp g), elim_imp d)
        | Non(f') -> Non(elim_imp f')
        | _ -> f;;

let elim_imp f = (elim_imp (elim_equiv f));;

let rec repousse_neg f =
    match f with
        Et(g, d) -> Et(repousse_neg g, repousse_neg d)
        | Ou(g, d) -> Ou(repousse_neg g, repousse_neg d)
        | Non(f') -> match f' with
            | Et(g, d) -> Ou(repousse_neg Non(g), repousse_neg Non(d))
            | Ou(g, d) -> Et(repousse_neg Non(g), repousse_neg Non(d))
            | Non(f'') -> f''
            | _ -> f
        | _ -> f;;

let repousse_neg f = (repousse_neg (elim_imp f));;

let rec inverse_ou_et f =
    match f with
        Et(g, d) -> Et(inverse_ou_et g, inverse_ou_et d)
        | Ou(g, d) -> match (inverse_ou_et g, inverse_ou_et d) with
            | Et(gg, dg),Et(gd, dd) -> Et(Et(inverse_ou_et Ou(gg, gd), inverse_ou_et Ou(gg, dd)), Et(inverse_ou_et Ou(dg, gd), inverse_ou_et Ou(dg, dd)))
            | Et(gg, dg),_ -> Et(inverse_ou_et Ou(gg, d), inverse_ou_et Ou(dg, d))
            | _,Et(gd, dd) -> Et(inverse_ou_et Ou(g, gd), inverse_ou_et Ou(g, dd))
            | _ -> f
        | _ -> f
    ;;

let inverse_ou_et f = (inverse_ou_et (repousse_neg f));;
