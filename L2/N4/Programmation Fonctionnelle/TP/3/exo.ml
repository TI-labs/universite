type arbre = Vide | Noeud of int * arbre * arbre;;

let rec appartient i a = match a with
    Vide -> false
    | Noeud(e, g, d) ->
        if (i == e) then
            true
        else if (i < e) then
            (appartient i g)
        else
            (appartient i d);;

(* Importé de ../2/exo2.ml *)
let rec concat l1 l2 = match l1 with
    e::ll1 -> e::(concat ll1 l2)
    | [] -> l2;;

let rec arbre_2_liste a = match a with
    Vide -> []
    | Noeud(e, g, d) ->
        (concat (arbre_2_liste g) (e::(arbre_2_liste d)));;

let rec inserer_bas i a = match a with
    Vide -> Noeud(i, Vide, Vide)
    | Noeud(i', g, d) ->
        if i <= i' then
            Noeud(i', inserer_bas i g, d)
        else
            Noeud(i', g, inserer_bas i d);;

let rec couper i a = match a with
    Vide -> Vide,Vide
    | Noeud(i', g, d) -> if i <= i' then
        let g', d' = couper i g in
            g',Noeud(i', d', d)
    else
        let g', d' = couper i d in
            Noeud(i', g, g'),d';;

let inserer_haut n a =
    let g, d = couper n a in
        Noeud(n, g, d);;

let tri_bas t = let rec tri_bas' l a = match l with
        [] -> arbre_2_liste a
        | e::l' -> tri_bas' l' (inserer_bas e a)
    in tri_bas' t Vide;;

let tri_haut t = let rec tri_haut' l a = match l with
        [] -> arbre_2_liste a
        | e::l' -> tri_haut' l' (inserer_haut e a)
    in tri_haut' t Vide;;
