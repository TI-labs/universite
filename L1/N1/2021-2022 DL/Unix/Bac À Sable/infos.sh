#!/bin/sh
# infos : donne des informations sur l'utilisateur

echo "Bonjour $USER !
Le répertoire courant est $PWD.
Le path de l'utilisateur : $PATH.
La langue est : $LANG.
Vous vous trouvez à $PWD.
Et vous étiez à $OLDPWD.
Vot' shell est $SHELL.
Et vot' éditeur $EDITOR.
La date est $(date).
Le calendrier est $(cal 7 20)
Amusez-vous bien !"
