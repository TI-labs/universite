#!/bin/sh

chaine=$1
entree=$2
sortie=$3
i=0

while IFS= read -r ligne; do
	if [ "$ligne" = "$1" ]; then
		i=$(( $i + 1 ))
	else
		echo $ligne >> temp_$2
	fi
done <$2

mv {temp_,}$2

i=$(( $i - 1 ))
while [ i -eq 0 ]; do
	$1 >> $3
	i=$(( $i - 1 ))
done

exit $(( $i + 1 ))
