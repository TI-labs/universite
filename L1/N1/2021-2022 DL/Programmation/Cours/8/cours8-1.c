#include <stdio.h>

void print(int str[64]);

int main() {
	return 0;
}

void print(int str[64]) {
	for (int i = 0; i < 64; i++) {
		if str[i] == 0 {
			return;
		}
		printf("%c", str[i]);
	}
	return;
}
