#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
	char chaine[] = "Hello, prison!";
	char con[] = " Sarkozy";
	char chaine_vide[1] = "";

	printf("%ld %ld %ld\n", sizeof(chaine), sizeof(con), sizeof(chaine_vide));

	strcat(chaine, con);

	printf("%ld %ld %ld\n", sizeof(chaine), sizeof(con), sizeof(chaine_vide));

	printf("%s\n", chaine);

	return EXIT_SUCCESS;
}
