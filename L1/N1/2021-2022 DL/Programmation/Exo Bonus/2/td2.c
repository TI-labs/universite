// `EXIT_SUCCESS`.
#include <stdlib.h>
// `printf()`, `scanf()`.
#include <stdio.h>

/*
 * Calcule le périmètre et la surface d'un rectangle depuis une longueur et une
 * largeur données par l'utilisateur.
 */
int main() {
	// Déclaration des entiers de la largeur et de la longueur du rectangle.
	int longueur, largeur;

	// Récupération de la longueur du rectangle.
	printf("Entrez la longueur du rectangle : ");
	scanf("%d", &longueur);

	// Récupération de la largeur du rectangle.
	printf("Entrez la largeur du rectangle : ");
	scanf("%d", &largeur);

	// Déclaration des entiers contenants la périmètre et la surface du
	// rectangle.
	int perimetre, surface;

	// Calcul et affichage du périmètre du rectangle.
	perimetre = (longueur + largeur) * 2;
	printf("Le périmètre d'un rectangle de longueur %d et de largeur %d est de %d.\n", longueur, largeur, perimetre);

	// Calcul et affichage de la surface du rectangle.
	surface = longueur * largeur;
	printf("La surface d'un rectangle de longueur %d et de largeur %d est de %d.\n", longueur, largeur, surface);

	// On retourne `EXIT_SUCCESS` (0) pour indiquer le succès du programme.
	return EXIT_SUCCESS;
}
