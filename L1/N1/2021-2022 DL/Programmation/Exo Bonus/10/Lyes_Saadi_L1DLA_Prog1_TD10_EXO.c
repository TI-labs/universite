#include <stdlib.h> // EXIT_FAILURE
#include <stdio.h> // printf & scanf

// On définit TRUE et FALSE pour améliorer la lisibilité du code
#define TRUE 1
#define FALSE 0

// On crée une structure date_t qui représente une date.
struct date_t {
	int jour;  // Le jour
	int mois;  // Le mois
	int annee; // L'année
};

// On définit des alias de type pour simplifier la compréhension.
typedef struct date_t date_t; // Un raccourcis pour la structure date_t.
typedef int bool; // Pour rendre le retour d'une fonction plus intuitif.

/**
 * Récupère une date à partir de l'entrée utilisateur.
 */
date_t entree_date();

/**
 * Vérifie qu'une date est valide.
 */
bool date_valide(date_t date);

/**
 * Incrémente une date et renvoie la date du lendemain.
 */
date_t incrementer_date(date_t date);

/**
 * Affiche une date et le jour d'après.
 */
void afficher_date(date_t date);

/**
 * Demande à l'utilisateur une date, l'affiche et affiche le jour suivant.
 */
int main() {
	date_t date;

	// On crée une boucle infinie.
	while (TRUE) {
		// On demande à l'utilisateur la date, et on répète si le format n'est
		// pas valide.
		do {
			printf("Saisir la date (format jj-mm-aaaa) : ");
			date = entree_date(); // On demande à l'utilisateur la date.
		} while (!date_valide(date)); // On vérifie la validité de la date.

		afficher_date(date); // On affiche la date et le jour suivant.
	}

	// Arriver à ce stade est normalement impossible, autrement, une erreur
	// s'est produite. Donc, on retourne EXIT_FAILURE.
	return EXIT_FAILURE;
}

/**
 * Récupère une date à partir de l'entrée utilisateur.
 */
date_t entree_date() {
	date_t date;

	// Récupère une date à partir d'un formattage jj-mm-aaaa.
	scanf("%d-%d-%d", &date.jour, &date.mois, &date.annee);

	return date;
}

/**
 * Vérifie qu'une date est valide.
 */
bool date_valide(date_t date) {
	// Si le jour ou le mois est négatif ou nul, la date est invalide.
	if (date.mois <= 0 || date.jour <= 0) return FALSE;

	// Si le mois est supérieur à 12, la date est invalide.
	if (date.mois > 12) return FALSE;

	// On gère le cas où Février a 28 ou 29 jours, si l'année est bissextile ou
	// pas.
	int fevrier;
	// Si l'année est divisible par 4 mais pas par 100, elle est bissextile.
	if (date.annee % 4 == 0 && date.annee % 100 != 0)
		fevrier = 29;
	// Si l'année est divisible par 400, elle est aussi bissextile.
	else if (date.annee % 400 == 0)
		fevrier = 29;
	// Sinon, elle n'est pas bissextile, et février a 28 jours.
	else
		fevrier = 28;

	// On crée un tableau de 12 élément contenant le nombre de jour de chaque mois.
	int jours[12] = {
		31, // Janvier
		fevrier, // Le cas de Février est variable
		31, // Mars
		30, // Avril
		31, // Mai
		30, // Juin
		30, // Juillet
		31, // Août
		30, // Septembre
		31, // Octobre
		30, // Novembre
		31  // Décembre
	};

	// Si le nombre de jours de la date dépasse le nombre de jour maximum d'un
	// mois, la date est invalide.
	if (date.jour > jours[date.mois - 1]) return FALSE;

	// Sinon, la date est valide.
	return TRUE;
}

/**
 * Incrémente une date et renvoie la date du lendemain.
 */
date_t incrementer_date(date_t date) {
	// On essaye d'incrémenter le jour.
	date.jour++;

	// Si la nouvelle date est valide, on la retourne.
	if (date_valide(date)) {
		return date;
	}

	// Sinon, on remet le jour à 1, et on incrémente le mois.
	date.jour = 1;
	date.mois++;

	// Si la nouvelle date est valide, on la retourne.
	if (date_valide(date)) {
		return date;
	}

	// Sinon, on remet le mois à 1, on incrémente l'année, et on renvoie la date
	// qui est nécessairement valide.
	date.mois = 1;
	date.annee++;

	return date;
}

/**
 * Affiche une date et le jour d'après.
 */
void afficher_date(date_t date) {
	// On affiche la date avec le format jj/mm/aaaa.
	printf("date saisie : %02d/%02d/%d\n", date.jour, date.mois, date.annee);
	// On récupère le jour suivant en incrémentant la date.
	date_t demain = incrementer_date(date);
	// On affiche le jour suivant avec le format jj/mm/aaaa.
	printf("date du jour suivant : %02d/%02d/%d\n\n", demain.jour, demain.mois, demain.annee);
}
