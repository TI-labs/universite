#include <stdlib.h> // EXIT_SUCCESS & EXIT_FAILURE
#include <stdio.h> // printf & scanf
#include <string.h> // strcmp & strcpy & strcat & strncat

// On définit TRUE pour rendre le code plus lisible.
#define TRUE 1

/*
 * Lit des mots de l'entrée standard et les traduit en louchbem.
 */
int main() {
	// Le mot original.
	char mot[256];
	// Le mot en louchbem.
	char resultat[259];

	// On répète à l'infini la boucle pour demander autant de fois que
	// nécessaire à l'utilisateur d'entrer sa chaîne de caractère.
	while (TRUE) {
		// On demande à l'utilisateur la chaîne de caractère à transformer
		// et on la stocke dans "mot".
		printf("Entrez un mot (q/Q pour sortir) : ");
		scanf("%s", mot);

		// Si l'utilisateur a entré "q" ou "Q" on sort de la boucle et on
		// arrête le programme avec EXIT_SUCCESS.
		if ((strcmp(mot, "q") == 0) || (strcmp(mot, "Q") == 0))
			return EXIT_SUCCESS;

		// On commence l'algorithme pour traduire le mot en louchbem.

		// On écrase la chaîne "resultat" par une chaîne avec "l".
		strcpy(resultat, "l");
		// On rajoute à la chaîne "resultat" la chaîne "mot" à partir du
		// deuxième caractère.
		strcat(resultat, &mot[1]);
		// On rajoute à la chaîne "resultat" le premier caractère de la
		// chaîne mot.
		strncat(resultat, mot, 1);
		// On rajoute à la chaîne "resultat" la chaîne "em".
		strcat(resultat, "em");

		// On affiche le résultat.
		printf("%s\n", resultat);
	}

	// Dans des conditions normales d'exécution, il est impossible d'arriver
	// à cette ligne vu qu'on utilise une boucle infinie. Si c'est le cas, c'est
	// qu'un problème est survenu. On retourne donc avec EXIT_FAILURE.
	return EXIT_FAILURE;
}
