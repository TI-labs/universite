/** Algorithme
 *
 * On définit une directive de préprocesseur LEN contenant la taille de notre
 * tableau.
 *
 * On déclare les valeurs du tableau t, de l'itérateur i et du résultat du
 * produit p. Et, on initialise t avec nos valeurs et une taille LEN, et p
 * avec 1 (1 étant
 * l'élément neutre de la multiplication).
 *
 * On parcoure le tableau grâce à une boucle for, et on multiple chaque valeur
 * par p, qui contiendra le résultat, et donc, le produit des éléments
 * précédants. Cela nous permet à la fin de la boucle d'avoir le produit de
 * l'entièreté des éléments du tableau.
 *
 * On affiche enfin le produit p des éléments du tableau et on retourne
 * EXIT_SUCCESS.
 */
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf()

// On définit dans une directive de préprocesseur la taille du tableau.
#define LEN 4

/**
 * Calcule et affiche le produit des valeurs d'un tableau d'entier quelconque.
 */
int main() {
	// On déclare et initialise notre tableau avec une taille LEN et des valeurs
	// quelconques.
	int t[LEN] = { 13, 42, 666, -1 };
	// On déclare les variables i et p, et on initialise la variable p contenant
	// le produit du tableau à 1 étant l'élément neutre de la multiplication.
	int i, p = 1;

	// On parcoure les éléments du tableau.
	for (i = 0; i < LEN; i++) {
		// On multiplie chaque élément du tableau par la variable p contenant le
		// produit des valeurs précédentes du tableau qu'on restocke après dans 
		// p.
		p *= t[i];
	}

	// On affiche le produit des éléments du tableau p.
	printf("Le produit des éléments du tableau t est de %d\n", p);

	// Tout s'est bien passé, on retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}
