// Ces libraries ne servent qu'à la fonction main qui proposent des tests des
// différentes fonctions.
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> //printf()

/**
 * Calcule la factorielle d'un entier naturel donné n.
 */
int factorielle(int n);

/**
 * Calcule le coefficient binomiale ou le nombre de combinaisons de p parmi n.
 */
int binomiale(int n, int p);

/**
 * Calcule la somme donné par :
 * ∑^(i=n-1)_(i=0) ( ∑^(j=i-1)_(j=0) i + j )
 */
int fonction(int n);

/**
 * Quelques tests pour vous simplifier la vie.
 *
 * Ces tests ne sont pas demandé par l'énoncé du TD, mais vous aideront à vous
 * assurer que les implémentations des fonctions est valide.
 */
int main() {
	// On performe plusieurs tests.

	// Pour factorielle()
	printf("5! = %d\n", factorielle(5));     // 120
	printf("0! = %d\n", factorielle(0));     // 1
	printf("1! = %d\n", factorielle(1));     // 1

	// Pour binomiale()
	printf("C^4_5 = %d\n", binomiale(5, 4)); // 5
	printf("C^5_5 = %d\n", binomiale(5, 5)); // 1
	printf("C^0_0 = %d\n", binomiale(0, 0)); // 1
	printf("C^2_5 = %d\n", binomiale(5, 2)); // 10

	// Pour fonction()
	printf("f(1) = %d\n", fonction(1));      // 0
	printf("f(2) = %d\n", fonction(2));      // 1
	printf("f(5) = %d\n", fonction(5));      // 40

	// On retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}

/*
On utilise une fonction récursive pour calculer la factorielle d'un nombre n.

Si n est strictement supérieur à 1, on calcule n * factorielle(n - 1).

Cela est permi grâce à une des propriétés de la factorielle qui est que :
n! = n * (n - 1)!

Si n est inférieur ou égal à 1, on retourne 1. C'est la condition de sortie,
sans laquelle la fonction récursive entrerait dans une boucle infinie jusqu'à
provoquer une crash dû à un manque de mémoire vive.
*/
int factorielle(int n) {
	if (n > 1)
		return n * factorielle(n - 1);
	return 1;
}

/*
On calcule le coefficient binomiale. Pour cela, on utilise factorielle().

On calcule le coefficient binomiale en utilisant cette formule :
C^p_n = n! / (p! * (n - p)!)
*/
int binomiale(int n, int p) {
	return factorielle(n) / (factorielle(p) * factorielle(n - p));
}

int fonction(int n) {
	// On déclare les variables i, j et résultat, et on initialise resultat à 0
	// (0 étant l'élément neutre de l'addition).
	int i, j, resultat = 0;

	// On fait la somme de 0 à n - 1 en utilisant une boucle for.
	for (i = 0; i < n; i++) {
		// On fait la somme de 0 à i - 1 en utilisant une deuxième boucle for.
		for (j = 0; j < i; j++) {
			// On finalise la somme en additionnant i + j à resultat contenant
			// le résultat final de la somme.
			resultat += i + j;
		}
	}

	// On retourne le résultat de la somme.
	return resultat;
}
