#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf & scanf

// On définit TRUE et FALSE pour améliorer la lisibilité du code
#define TRUE 1
#define FALSE 0

// On définit une taille maximale pour le tableau.
#define MAX 256

// On définit une énumération caractérisant l'ordre dans lequel est le tableau.
enum ordre {
	DESORDRE = 0,    // Le tableau est dans le désordre
	CROISSANT = 1,   // Le tableau est croissant
	DECROISSANT = 2, // Le tableau est décroissant
	CONSTANT = 3     // Le tableau est constant (pas demandé par l'exercice)
};

// On définit des alias de type pour simplifier la compréhension.
typedef enum ordre ordre; // Un raccourcis pour l'énumération ordre.
typedef int bool; // Pour rendre le retour d'une fonction plus clair.

/**
 * Retourne l'ordre dans lequel est un tableau d'une taille donnée.
 */
ordre tri(int tab[MAX], int taille);

/**
 * Vérifie si un tableau est ordonné selon un ordre croissant.
 */
bool croi(int tab[MAX], int i);

/**
 * Vérifie si un tableau est ordonné selon un ordre décroissant.
 */
bool decroi(int tab[MAX], int i);

/**
 * Vérifie si tous les éléments d'un tableau sont les mêmes.
 */
bool constant(int tab[MAX], int i);

/**
 * Vérifie si tous les éléments d'une suite donnée par l'utilisateur sont dans
 * un ordre donné.
 */
int main() {
	int i;
	int taille;

	// On demande à l'utilisateur la taille du tableau jusqu'à ce qu'elle soit
	// valide.
	do {
		printf("La taille de votre suite est de : ");
		scanf("%d", &taille);
	// La taille doit être inférieure à la taille maximale et strictement
	// supérieure à 1.
	} while (taille >= MAX || taille <= 1);

	int tab[taille];

	// On récupère tous les éléments de la suite depuis l'entrée standard.
	for (i = 0; i < taille; i++) {
		printf("Entrez l'élement n°%d de la suite : u_%d = ", i + 1, i);
		scanf("%d", &tab[i]);
	}

	// On récupère l'ordre dans lequel est trié le tableau, et on affiche un
	// message en fonction du résultat.
	switch (tri(tab, taille)) {
		case CROISSANT:
			printf("La suite est croissante.\n");
			break;
		case DECROISSANT:
			printf("La suite est décroissante.\n");
			break;
		case CONSTANT:
			printf("La suite est constante.\n");
			break;
		case DESORDRE:
			printf("La suite n'a pas d'ordre.\n");
			break;
	}

	// Tout s'est passé comme prévu, on retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}

/**
 * Retourne l'ordre dans lequel est un tableau d'une taille donnée.
 */
ordre tri(int tab[MAX], int taille) {
	// Chaque ordre du tableau est délégué à une fonction indépendante de façon
	// à découper le code. Chaque fonction est récursive pour améliorer les
	// performances (malgré une utilisation de la ram plus intense) et parce que
	// je trouve, personellement, que c'est une solution assez élégante.

	// Vérifie si le tableau est constant.
	if (constant(tab, taille))
		return CONSTANT;
	// Vérifie si le tableau est croissant.
	else if (croi(tab, taille))
		return CROISSANT;
	// Vérifie si le tableau est décroissant.
	else if (decroi(tab, taille))
		return DECROISSANT;
	// Sinon, le tableau est dans le désordre.
	else
		return DESORDRE;
}

/**
 * Vérifie si un tableau est ordonné selon un ordre croissant.
 */
bool croi(int tab[MAX], int i) {
	// Quand i == 1, on est arrivé à notre condition de sortie, et on a déjà
	// comparé tous les éléments précédents.
	if (i == 1)
		return TRUE;
	// On compare la valeur i-1 et i-2 du tableau. On commence donc par
	// comparer les éléments depuis la fin du tableau.
	if (tab[i - 2] <= tab[i - 1])
		// Si le résultat de la comparaison est vrai, il passe à la paire
		// suivante d'éléments.
		return croi(tab, i - 1);
	// Si le résultat est faux, le tableau n'est pas ordonné, on quitte revoyant
	// faux.
	else
		return FALSE;
}

/**
 * Vérifie si un tableau est ordonné selon un ordre décroissant.
 */
bool decroi(int tab[MAX], int i) {
	// Quand i == 1, on est arrivé à notre condition de sortie, et on a déjà
	// comparé tous les éléments précédents.
	if (i == 1)
		return TRUE;
	// On compare la valeur i-1 et i-2 du tableau. On commence donc par
	// comparer les éléments depuis la fin du tableau.
	if (tab[i - 2] >= tab[i - 1])
		// Si le résultat de la comparaison est vrai, il passe à la paire
		// suivante d'éléments.
		return decroi(tab, i - 1);
	// Si le résultat est faux, le tableau n'est pas ordonné, on quitte revoyant
	// faux.
	else
		return FALSE;
}

/**
 * Vérifie si tous les éléments d'un tableau sont les mêmes.
 */
bool constant(int tab[MAX], int i) {
	// Quand i == 1, on est arrivé à notre condition de sortie, et on a déjà
	// comparé tous les éléments précédents.
	if (i == 1)
		return TRUE;
	// On compare la valeur i-1 et i-2 du tableau. On commence donc par
	// comparer les éléments depuis la fin du tableau.
	if (tab[i - 2] == tab[i - 1])
		// Si le résultat de la comparaison est vrai, il passe à la paire
		// suivante d'éléments.
		return constant(tab, i - 1);
	// Si le résultat est faux, le tableau n'est pas ordonné, on quitte revoyant
	// faux.
	else
		return FALSE;
}
