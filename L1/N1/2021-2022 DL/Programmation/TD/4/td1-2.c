#include <stdlib.h>
#include <stdio.h>

int main() {
	unsigned int h, m, s;

	printf("L'heure actuelle est : ");
	scanf("%dh%dm%ds", &h, &m, &s);

	if (h >= 24) {
		printf("L'heure est invalide.\n");
		return EXIT_FAILURE;
	}

	if (m >= 60) {
		printf("Les minutes sont invalide.\n");
		return EXIT_FAILURE;
	}

	if (s >= 60) {
		printf("Les secondes sont invalide.\n");
		return EXIT_FAILURE;
	}

	s++;

	if (s == 60) {
		m++;
		s = 0;
	}

	if (m == 60) {
		h++;
		m = 0;
	}

	if (h == 24)
		h = 0;

	printf("Dans une seconde, il sera exactement : %02dh%02dm%02ds\n", h, m, s);


	return EXIT_SUCCESS;
}
