#include <stdlib.h>
#include <stdio.h>

int main() {
	int a, b, c;
	int min;

	printf("a = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%d", &b);
	printf("c = ");
	scanf("%d", &c);

	if (a < b) {
		min = a;
	} else {
		min = b;
	}

	if (c < min) {
		min = c;
	}

	printf("Le minimum est %d\n", min);

	return EXIT_SUCCESS;
}
