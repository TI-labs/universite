#include <stdlib.h>
#include <stdio.h>

int main() {
	int tab[256];
	int min = 0;
	int t, i;

	printf("Combien d'éléments (t < 256) a votre suite : ");
	scanf("%d", &t);

	for (i = 0; i < t; i++) {
		printf("Entrez l'élément n°%d de la suite = ", i + 1);
		scanf("%d", &tab[i]);
	}

	for (i = 1; i < t; i++) {
		if (tab[i] < tab[min]) {
			min = i;
		}
	}

	printf("L'élément le plus petit de la suite est l'élément n°%d = %d\n", min + 1, tab[min]);

	return EXIT_SUCCESS;
}
