#include <stdlib.h>
#include <stdio.h>

int main() {
	int tab[3] = {2, -4, 8};
	int temp;

	temp = tab[0];
	tab[0] = tab[1];
	tab[1] = tab[2];
	tab[2] = temp;

	printf("tab[0] = %d \n", tab[0]);
	printf("tab[1] = %d \n", tab[1]);
	printf("tab[2] = %d \n", tab[2]);

	return EXIT_SUCCESS;
}
