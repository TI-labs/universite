#include <stdlib.h>
#include <stdio.h>

#define MAX 50

int main() {
	int i = 0;
	int tab[MAX];

	do {
		printf("Entrez l'élement n°%d du tableau : tab[%d] = ", i + 1, i);
		scanf("%d", &tab[i]);
	} while (tab[i] != -1 && ++i < MAX);

	for (int j = 0; j < i; j++) {
		printf("tab[%d] = %d\n", j, tab[j]);
	}

	return EXIT_SUCCESS;
}
