#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define NB_MAX 100
#define TRUE 1
#define FALSE 0

enum menu demander_choix();
int saisir_entier(char enonce[]);

int est_premier(int p);
void nombre_secret();
int cube(int x);
int est_majeur(int x);
int somme(int n);
void afficher_rectangle(int largeur, int longueur);
int saisie_utilisateur();

enum menu {
	QUITTER = 0,
	PREMIER = 1,
	SECRET = 2,
	FACTORIELLE = 3,
	MAJEUR = 4,
	SOMME = 5,
	RECTANGLE = 6,
	SAISIE = 7,
};

int main() {
	int n;
	enum menu choix;

	do {
		choix = demander_choix();

		switch (choix) {
			case PREMIER:
				n = saisir_entier("p = ");

				if (est_premier(n))
					printf("p = %d est premier.\n", n);
				else
					printf("p = %d n'est pas premier.\n", n);
				break;
			case SECRET:
				nombre_secret();
				break;
			case FACTORIELLE:
				printf("n³ = %d\n", cube(saisir_entier("n = ")));
				break;
			case MAJEUR:
				if (est_majeur(saisir_entier("Quel est votre âge : ")))
					printf("Vous êtes majeur !\n");
				else
					printf("Vous êtes mineur !\n");
				break;
			case SOMME:
				printf("La somme est : %d\n", somme(saisir_entier("n = ")));
				break;
			case RECTANGLE:
				afficher_rectangle(saisir_entier("Largeur = "), saisir_entier("Longueur = "));
				break;
			case SAISIE:
				printf("Vous avez saisi : %d\n", saisie_utilisateur());
				break;
			case QUITTER:
				return EXIT_SUCCESS;
			default:
				printf("Votre choix est invalide.\n");
				break;
		}

	} while (choix);
}

enum menu demander_choix() {
	enum menu choix;

	printf("\n----------------------------------------------------\n");
	printf("Bienvenue dans le menu grr, faites votre choix :\n");
	printf("1 - Tester si un nombre est premier\n");
	printf("2 - Deviner un nombre\n");
	printf("3 - Calculer le cube d'un nombre\n");
	printf("4 - Êtes-vous majeur ?\n");
	printf("5 - Faites la sommes de tous les nombres jusqu'à n !\n");
	printf("6 - Affichez un rectangle\n");
	printf("7 - Récupérer la saisie d'un utilisateur\n");
	printf("0 - Quitter\n");
	printf("------------------------------------ Votre choix : ");
	scanf("%d", &choix);
	printf("\n\n");

	return choix;
}

int saisir_entier(char enonce[]) {
	int n;

	printf("%s", enonce);
	scanf("%d", &n);

	return n;
}

int est_premier(int p) {
	for (int i = 2; i < p; i++) {
		if (p % i == 0) {
			return FALSE;
		}
	}

	return TRUE;
}

void nombre_secret() {
	int nb;

	srand(time(NULL));
	
	int secret = rand() % (NB_MAX + 1);

	int trouve = FALSE;

	while (!trouve) {
		printf("Devinez un nombre entre 0 et %d : ", NB_MAX);
		scanf("%d", &nb);

		if (nb < 0 || nb > NB_MAX) {
			printf("Le nombre est entre 0 et %d.\n", NB_MAX);
			continue;
		}

		if (nb == secret) {
			trouve = TRUE;
		} else if (nb > secret) {
			printf("Le nombre est plus petit !\n");
		} else if (nb < secret) {
			printf("Le nombre est plus grand !\n");
		}
	}

	printf("Bravo ! Vous avez trouvé %d !\n", secret);
}

int cube(int x) {
	return x * x * x;
}

int est_majeur(int x) {
	if (x >= 18)
		return TRUE;
	return FALSE;
}

int somme(int n) {
	if (n > 0)
		return n + somme(n - 1);
	return 0;
}

void afficher_rectangle(int largeur, int longueur) {
	int i, j;

	for (i = 1; i <= largeur; i++) {
		printf("*");
		for (j = 2; j < longueur; j++) {
			if (i == 1 || i == largeur)
				printf("*");
			else
				printf(" ");
		}
		printf("*\n");
	}
}

int saisie_utilisateur() {
	int x;

	printf("Veuillez entrer un entier x = ");
	scanf("%d", &x);

	return x;
}
