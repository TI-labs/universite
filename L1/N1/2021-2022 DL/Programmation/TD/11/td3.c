#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
	int i;

	printf("Les %d paramètres de la commande %s sont : \n", argc - 1, argv[0]);

	for (i = 1; i < argc; i++) {
		printf("\t%s\n", argv[i]);
	}
}
