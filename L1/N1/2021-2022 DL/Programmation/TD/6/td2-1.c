#include <stdlib.h>
#include <stdio.h>

int main() {
	int x, n;
	int resultat = 1;

	printf("x = ");
	scanf("%d", &x);
	printf("n = ");
	scanf("%d", &n);

	if (n < 0) {
		printf("L'exposant doit être supérieur ou égal à 0.\n");
		return EXIT_FAILURE;
	}

	for (int i = 0; i < n; i++) {
		resultat *= x;
	}

	printf("x^n = %d\n", resultat);

	return EXIT_SUCCESS;
}
