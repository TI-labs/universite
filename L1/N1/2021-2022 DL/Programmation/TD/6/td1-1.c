#include <stdlib.h>
#include <stdio.h>

int main() {
	int nb, i, tmp;
	int somme = 0;

	printf("Entrez le nombre d'entiers à utiliser pour la moyenne : ");
	scanf("%d", &nb);

	for (i = 0; i < nb; i++) {
		printf("Entrez le nombre n°%d : ", i + 1);
		scanf("%d", &tmp);
		somme += tmp;
	}

	printf("La moyenne est : %d\n", somme / nb);

	return EXIT_SUCCESS;
}
