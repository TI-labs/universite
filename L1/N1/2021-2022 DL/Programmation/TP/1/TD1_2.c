#include <stdlib.h>

int main()
{
    int x = 1;
    int y = 2;
    int temp;

    temp = x;
    x = y;
    y = temp;

    return EXIT_SUCCESS;
}
