#include <stdio.h>
#include <stdlib.h>

int main() {
	int cote;

	printf("Le côté du triangle est ");
	scanf("%d", &cote);

	int i, j;

	/*
	for (i = 0; i < cote; i++) {
		for (j = 1; j < cote - i; j++) {
			printf(" ");
		}
		for (j = cote - i; j <= cote; j++) {
			printf("*");
		}
		printf("\n");
	}
	*/

	for (i = 0; i < cote; i++) {
		for (j = 0; j < cote; j++) {
			if (cote - i >= j) {
				printf("*");
			} else {
				printf("*");
			}
		}
		printf("\n");
	}

	return EXIT_SUCCESS;
}
