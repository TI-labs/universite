#include <stdio.h>
#include <stdlib.h>

int main() {
	int cote;

	printf("Le côté du triangle est ");
	scanf("%d", &cote);

	int i, j;

	int largeur = 1;

	for (i = 0; i < cote; i++) {
		for (j = 0; j < largeur; j++) {
			printf("*");
		}
		printf("\n");
		largeur++;
	}

	return EXIT_SUCCESS;
}
