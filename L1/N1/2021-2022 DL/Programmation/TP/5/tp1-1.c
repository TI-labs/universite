#include <stdlib.h>
#include <stdio.h>

int main() {
	int i;
	int len = 0;
	char phrase[500];
	int occurence[256];

	for (i = 0; i < 256; i++) {
		occurence[i] = 0;
	}

	printf("Entrez une phrase : ");
	scanf("%s", phrase);

	while (phrase[len] != '\0') {
		occurence[phrase[len]]++;
		len++;
	}
	
	occurence[0]++;

	for (i = 0; i < 256; i++) {
		printf("%d\t| %c | %d\t occurences\n", i, i < 32 || i > 126 ? ' ' : i, occurence[i]);
	}

	return EXIT_SUCCESS;
}
