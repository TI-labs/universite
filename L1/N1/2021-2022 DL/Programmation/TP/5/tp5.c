// On importe les librairies standards dont on a besoin.
#include <stdlib.h> // `EXIT_SUCCESS` et `EXIT_FAILURE`
#include <stdio.h> // `printf()`

enum temps {
	COUVERT = 0,
	ENSOLEILLE = 1,
	PLUVIEUX = 2,
	NEIGEUX = 3
};

enum pression {
	BAISSE,
	STABLE,
	AUGMENTE
};

enum vent {
	EST,
	OUEST,
	NORD,
	SUD
};

struct meteo {
	int temperature;
	enum temps temps;
	enum pression pression;
	enum vent vent;
};

struct meteo prevision(struct meteo actuel);
void affichage_temps(struct meteo meteo);

int main() {
	struct meteo meteo; // On déclare et initialise temps avec une valeur.

	printf("Quel température fait-il : ");
	scanf("%d", &meteo.temperature);

	printf("\n");
	printf("COUVERT    = %d\n", COUVERT);
	printf("ENSOLEILLE = %d\n", ENSOLEILLE);
	printf("PLUVIEUX   = %d\n", PLUVIEUX);
	printf("NEIGEUX    = %d\n", NEIGEUX);

	int temps;
	do {
		printf("Quel temps fait-il : ");
		scanf("%d", &temps);
	} while (!(temps == COUVERT || temps == ENSOLEILLE || temps == PLUVIEUX || temps == NEIGEUX));
	meteo.temps = temps;

	printf("\n");
	printf("BAISSE     = %d\n", BAISSE);
	printf("STABLE     = %d\n", STABLE);
	printf("AUGMENTE   = %d\n", AUGMENTE);

	int pression;
	do {
		printf("Comment la pression evolue-t-elle : ");
		scanf("%d", &pression);
	} while (!(pression == BAISSE || pression == STABLE || pression == AUGMENTE));
	meteo.pression = pression;

	printf("\n");
	printf("EST        = %d\n", EST);
	printf("OUEST      = %d\n", OUEST);
	printf("NORD       = %d\n", NORD);
	printf("SUD        = %d\n", SUD);

	int vent;
	do {
		printf("Quel est la direction du vent : ");
		scanf("%d", &vent);
	} while (!(vent == EST || vent == OUEST || vent == NORD || vent == SUD));
	meteo.vent = vent;

	printf("\nLa météo actuelle :\n");
	affichage_temps(meteo);
	printf("\nLa météo prévision :\n");
	affichage_temps(prevision(meteo));

	// Tout s'est bien passé, on retourne EXIT_SUCCESS!
	return EXIT_SUCCESS;
}

struct meteo prevision(struct meteo actuel) {
	struct meteo prev = actuel;

	if (prev.temps == ENSOLEILLE && prev.pression == STABLE && prev.vent == OUEST) {
		prev.temps = ENSOLEILLE;
		return prev;
	}

	if (prev.pression == AUGMENTE && prev.temperature > 0 && prev.vent == NORD) {
		prev.temps = COUVERT;
		return prev;
	}

	if (prev.pression == BAISSE && prev.temps == COUVERT) {
		prev.temps = PLUVIEUX;
		if (prev.temperature < 0) {
			prev.temps = NEIGEUX;
		}

		return prev;
	}

	return prev;
}

void affichage_temps(struct meteo meteo) {
	printf("Température : %d\n", meteo.temperature);

	printf("Temps : ");
	switch (meteo.temps) {
		case ENSOLEILLE:
			printf("ensoilleillé");
			break;
		case COUVERT:
			printf("couvert");
			break;
		case PLUVIEUX:
			printf("pluvieux");
			break;
		case NEIGEUX:
			printf("neigeux");
			break;
	}
	printf("\n");

	printf("Pression : ");
	switch (meteo.pression) {
		case BAISSE:
			printf("baisse");
			break;
		case STABLE:
			printf("stable");
			break;
		case AUGMENTE:
			printf("augmente");
			break;
	}
	printf("\n");

	printf("Vent : ");
	switch (meteo.vent) {
		case EST:
			printf("est");
			break;
		case OUEST:
			printf("ouest");
			break;
		case NORD:
			printf("nord");
			break;
		case SUD:
			printf("sud");
			break;
	}
	printf("\n");
}
