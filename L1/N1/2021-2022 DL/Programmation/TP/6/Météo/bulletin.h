#ifndef BULLETIN_H
#define BULLETIN_H

enum temps {
	COUVERT = 0,
	ENSOLEILLE = 1,
	PLUVIEUX = 2,
	NEIGEUX = 3
};

enum pression {
	BAISSE,
	STABLE,
	AUGMENTE
};

enum vent {
	EST,
	OUEST,
	NORD,
	SUD
};

struct meteo {
	int temperature;
	enum temps temps;
	enum pression pression;
	enum vent vent;
};

struct meteo saisie_meteo();
struct meteo saisie_temps(struct meteo meteo);
struct meteo saisie_pression(struct meteo meteo);
struct meteo saisie_vent(struct meteo meteo);
struct meteo saisie_temperature(struct meteo meteo);
struct meteo prevision(struct meteo actuel);
void affichage_temps(struct meteo meteo);

#endif
