 (** * Rudiments de logique dans le syst\u00e8me coq *)
Require Import Unicode.Utf8.
(** ** La fl\u00e8che ou implication *)
(** *** Type [Prop] et [\u2192] *)
(** L'objet fondamental du syst\u00e8me coq est le _type_ (on dit qu'il est bas\u00e9 sur
    la _th\u00e9orie des types_).
    Tous les objets que l'on manipule ont un certain type.
    Le type [Prop] est celui des propositions, c'est-\u00e0-dire des \u00e9nonc\u00e9s
    math\u00e9matiques que l'on peut chercher \u00e0 prouver ou \u00e0 r\u00e9futer.
*)

(** La commande [Check] permet d'imprimer le type d'un terme.  *)
Check 2 + 2 = 4.
Check 2 + 2 = 5.

(** Les \u00e9nonc\u00e9s [2 + 2 = 4] et [2 + 2 = 5] sont tous deux de type [Prop], 
    peu importe le fait qu'ils soient prouvables, r\u00e9futables (ou m\u00eame
    ind\u00e9cidables, c'est-\u00e0-dire ni prouvables ni r\u00e9futables). *)

(** \u00c9tant donn\u00e9es deux propositions [P] et [Q], on peut en former une nouvelle,
    [P \u2192 Q] (lire : "P fl\u00e8che Q" ou "P implique Q").

   Remarque : la fl\u00e8che peut \u00eatre entr\u00e9e des fa\u00e7ons suivantes :
    - avec << -> >> (le caract\u00e8re '-' puis le caract\u00e8re '>')
    - directement en unicode : sur beaucoup d'applications sur Linux taper
      Ctrl+Shift+u puis l'unicode, ici 2192 puis espace.
    - sur coqide : taper << \-> >> puis Shift+Espace.
*)

Module PropPlayground1.
(** On ne parlera pas des modules dans ce cours, on les utilise simplement pour
    isoler certaines parties d'un fichier coq
*)
Parameters P Q : Prop.
Check P.
Check Q.
Check P \u2192 Q.
Check (P \u2192 Q) \u2192 Q.
Check P \u2192 (Q \u2192 P).
Check P \u2192 ((Q \u2192 P) \u2192 Q).
End PropPlayground1.

(** Intuituivement, dans la logique (intuitionniste) de coq,
    la proposition [P \u2192 Q] signifie "si [P], alors [Q]" ou, plus pr\u00e9cis\u00e9ment,
    "je sais contruire une preuve de Q \u00e0 partir d'une preuve de P".

    Deux remarques _tr\u00e8s importantes_ :
    - La notation usuelle des math\u00e9matiques pour "P implique Q" est
    [P \u21d2 Q]. Continuez \u00e0 l'utilisez lors de vos cours plus
    classiques de math\u00e9matiques.
    - En logique _classique_ (voir plus loin) et en particulier dans vos cours
    de math\u00e9matiques, l'implication est d\u00e9finie par sa table de v\u00e9rit\u00e9 et a un
    sens un peu diff\u00e9rent. Ceci dit, toutes les formules et techniques de
    preuves que nous voyons dans ce cours sont encore valides dans un cours
    "classique" de math\u00e9matiques.
*)
(** *** Premi\u00e8re preuve *)

(** Nous allons maintenant \u00e9noncer et prouver un premier th\u00e9or\u00e8me. Ex\u00e9cuter pas
    \u00e0 pas cette preuve pour bien voir les modifications dans le contexte et le
    but. En coq, les commentaires sont \u00e9crits entre (* ... *) *)

Theorem imp_refl : \u2200 P : Prop, P \u2192 P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P.
  (* Pour montrer une implication on suppose que ce qui est \u00e0 gauche est prouv\u00e9.
     On doit prouver ce qui est \u00e0 droite avec cette hypoth\u00e8se suppl\u00e9mentaire. *)
  (* On suppose (hypoth\u00e8se (HP)) que P est prouv\u00e9e. *)
  intros HP.
  (* On doit prouver P. Mais cela fait partie des hypoth\u00e8ses ! *)
  assumption.
Qed. (* Quod erat demonstrandum. Ce qu'il fallait d\u00e9montrer. *)

(** Sans entrer dans les d\u00e9tails, quelques commentaires s'imposent :
    - [imp_refl] est le nom que nous avons choisi pour ce th\u00e9or\u00e8me.
    - La preuve se trouve entre les mots-cl\u00e9s [Proof.] et [Qed.]
    - Les \u00e9l\u00e9ments de la preuve [intros] et [assumption] sont appel\u00e9s des
    _tactiques_ de preuve.
    - Lorsque l'\u00e9valuation par coq atteint [Qed.], coq v\u00e9rifie que la preuve est
       correcte et dans le cas contraire affiche un message d'erreur.
*)

(** *** Plusieurs fl\u00e8ches \u00e0 la suite *)
(** L'implication n'est pas associative, ce qui veut dire que
    [(P \u2192 Q) \u2192 R] n'est pas \u00e9quivalente \u00e0 [P \u2192 (Q \u2192 R)], ces deux
    propositions ont des sens tr\u00e8s diff\u00e9rents.
    En coq, lorsqu'on ne met pas de parenth\u00e8ses dans une expression avec des
    [\u2192], c'est comme s'il y en avait \u00e0
    droite : [P \u2192 Q \u2192 R] est la m\u00eame proposition que [P \u2192 (Q \u2192 R)].
*)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem imp_ex1 : \u2200 P Q : Prop, P \u2192 (Q \u2192 P).
Proof.
  intros P.
  intros Q.
  intros HP.
  intros HQ.
  assumption.
Qed.
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem imp_ex2 : \u2200 P Q : Prop, Q \u2192 (P \u2192 P).
Proof.
  intros P.
  intros Q.
  intros HQ.
  intros HP.
  assumption.
Qed.
	

(** Remarque importante :
    Apr\u00e8s avoir introduit toutes les variables et toutes les hypoth\u00e8ses dans ces
    deux derniers exercices, les contextes sont exactement les m\u00eames. En fait,
    plut\u00f4t que de penser \u00e0 [P \u2192 Q \u2192 R] comme (P implique que (Q implique R)),
    il vaut mieux y voir "si P et Q, alors R", ou encore, "si j'ai une preuve de
    P et une preuve de Q, alors je sais contruire une preuve de R".
*)

(** coq ne va pas vous laisser prouver quelque chose de faux : essayez ! *)

(** **** Exercice : O\u00f9 \u00eates-vous bloqu\u00e9 dans cette preuve ? *)
Theorem faux_thm : \u2200 P Q : Prop, (P \u2192 Q) \u2192 P.
Proof.
  intros P.
  intros Q.
  intros HPQ.
Abort.

(** Avec de l'argent je peux acheter des g\u00e2teaux (ou de la drogue), \u00e7a ne veut 
    pas dire que j'ai de l'argent... *)

(** *** Appliquer une implication *)
(** Maintenant qu'on a vu comment _prouver_ (introduire) une implication, on va
  voir comment on _utilise_ (\u00e9limine, applique) une implication. *)

Theorem modus_ponens : \u2200 P Q : Prop, P \u2192 (P \u2192 Q) \u2192 Q. 
Proof. (* D\u00e9monstration. *)
  (* Soient P et Q deux propositions quelconques.*)
  intros P Q.
  (* Supposons que P est prouv\u00e9e ... *)
  intro HP.
  (* ... et que (P \u2192 Q) est prouv\u00e9e. *)
  intro HPimpQ.
  (* Comme (P \u2192 Q), pour montrer Q _il suffit_ de montrer P. *)
  apply HPimpQ.
  (* Or, P est vraie par hypoth\u00e8se. *)
  assumption.
Qed. (* CQFD. *)

(** Revenons sur la tactique [apply].
    Si la formule \u00e0 prouver est [Q] et qu'une hypoth\u00e8se [H] a la forme
    [P \u2192 Q], alors l'utilisation de [apply H.] entra\u00eene le changement du but en
    [P].
    Ceci est conforme \u00e0 la r\u00e8gle appel\u00e9e syllogisme ou _modus ponens_ qui dit
    que de [P \u2192 Q] et [P], on peut d\u00e9duire [Q].
    - Socrate est un homme. ([P])
    - Tous les hommes sont mortels. ([P \u2192 Q])
    - Donc Socrate est mortel. ([Q])
*)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem imp_trans : \u2200 P Q R : Prop,
  (P \u2192 Q) \u2192 (Q \u2192 R) \u2192 (P \u2192 R).
Proof.
  intros P Q R.
  intros HPimpQ.
  intros HQimpR.
  intros HPimpR.
  apply HQimpR.
  apply HPimpQ.
  assumption.
Qed.
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant (plus difficile) *)

Theorem weak_peirce : \u2200 P Q : Prop,
  ((((P \u2192 Q) \u2192 P) \u2192 P) \u2192 Q) \u2192 Q.
Proof.
  intros P Q.
  intros H1.
  apply H1.
  intros H2.
  apply H2.
  intros H3.
  apply H1.
  intros H4.
  assumption.
Qed.
	

(** *** Les sous-buts *)

(** Il arrive tr\u00e8s souvent qu'une tactique cr\u00e9e des _sous-buts_ (en anglais
    _subgoals_), comme dans cette preuve du th\u00e9or\u00e8me suivant. *)

Theorem imp3 : \u2200 P Q R : Prop,
  (P \u2192 Q \u2192 R) \u2192 P \u2192 Q \u2192 R.
Proof.
  (* Soient P, Q et R trois propositions quelconques. *)
  intros P Q R.
  (* Supposons :
    (HPQR) : P \u2192 Q \u2192 R.
    (HP) : P
    (HR) : Q *)
  intros HPQR HP HQ.
  (* D'apr\u00e8s (HPQR), pour prouver R il suffit d'avoir une preuve de P et une preuve de Q *)
  apply HPQR.
  (* Remarquer les deux sous-buts ici *)
  (* On peut (mais on n'est pas oblig\u00e9) utiliser les caract\u00e8res - + et * pour mettre en \u00e9vidence 
     le fait qu'on travaille sur un sous-but *)
  - (* Preuve de P *)
    assumption.
  - (* Preuve de Q *)
    assumption.
Qed.

 
(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem pr\u00e9misses_non_ordonn\u00e9es1 : \u2200 P Q R : Prop, (P \u2192 Q \u2192 R) \u2192 (Q \u2192 P \u2192 R).
Proof.
  intros P Q R.
  intros HPQR HQ HP.
  apply HPQR.
  -
    assumption.
  -
    assumption.
Qed.
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem diamond : \u2200 P Q R T : Prop,
  (P \u2192 Q) \u2192 (P \u2192 R) \u2192 (Q \u2192 R \u2192 T) \u2192 P \u2192 T.
Proof.
  intros P Q R T.
  intros HPQ HPR HQRT HP.
  apply HQRT.
  -
    apply HPQ.
    assumption.
  -
    apply HPR.
    assumption.
Qed.
	
 
(** *** Appliquer une implication dans une autre hypoth\u00e8se. *)

(** Jusqu'\u00e0 pr\u00e9sent, nous avons toujours prouv\u00e9 nos th\u00e9or\u00e8mes en travaillant sur
    le _but_. C'est ce qu'on appelle un raisonnement "vers l'arri\u00e8re"
    (_backward reasoning_). Ce style de raisonnement est favoris\u00e9 en coq : par
    d\u00e9faut, la plupart des tactiques transforment le but avec des raisonnements
    du type "d'apr\u00e8s telle hypoth\u00e8se, si je sais prouver truc (nouveau but),
    alors j'aurai bidule (ancien but)", autrement dit,
    "d'apr\u00e8s telle hypoth\u00e8se, pour
    prouver bidule (ancien but) _il suffit_ de prouver truc (nouveau but)". *)

Theorem imp_trans2 : \u2200 P Q R S : Prop,
  (P \u2192 Q) \u2192 (Q \u2192 R) \u2192 (R \u2192 S) \u2192 P \u2192 S.
Proof.
  (* Soient P, Q, R et S des propositions quelconques *)
  intros P Q R S.
  (* On suppose prouv\u00e9es
     (HPQ) : P \u2192 Q
     (HQR) : Q \u2192 R
     (HRS) : R \u2192 S
     (HP)  : P
     et on doit montrer S.*)
  intros HPQ HQR HRS HP.
  (* D'apr\u00e8s HPS, pour prouver S il suffit de prouver R. *)
  apply HRS.
  (* D'apr\u00e8s HQR, pour prouver R il suffit de prouver Q. *)
  apply HQR.
  (* D'apr\u00e8s HPQ, pour prouver Q il suffit de prouver P. *)
  apply HPQ.
  (* Or, P est prouv\u00e9e par hypoth\u00e8se. *)
  assumption.
Qed.

(** En math\u00e9matiques, on a plut\u00f4t l'habitude du raisonnement "vers l'avant" :
    construire pas \u00e0 pas le but en \u00e9non\u00e7ant de nouveaux faits. Ceci est
    aussi possible en coq. *)

Theorem imp_trans2' : \u2200 P Q R S : Prop,
  (P \u2192 Q) \u2192 (Q \u2192 R) \u2192 (R \u2192 S) \u2192 P \u2192 S.
Proof.
  (* La preuve commence de la m\u00eame mani\u00e8re. *)
  intros P Q R S HPQ HQR HRS HP. 
  (* Remarquez qu'on peut introduire en m\u00eame temps les variables et les
     hypoth\u00e8ses. *)
  (* D'apr\u00e8s (HP) et (HPQ), Q est prouv\u00e9e. *)
  apply HPQ in HP as HQ.
  (* Donc, par HQR, on a aussi une preuve de R. *)
  apply HQR in HQ as HR.
  (* Enfin, par HRS, on obtient une preuve de S. *)
  apply HRS in HR as HS.
  (* S est donc prouv\u00e9e par hypoth\u00e8se. *)
  assumption.
Qed.

(** *** Exercice :
    Prouver avec un raisonnement "vers l'avant" les th\u00e9or\u00e8mes [modus_ponens] et
    [imp_trans]. *)

Theorem modus_ponens' : \u2200 P Q : Prop, P \u2192 (P \u2192 Q) \u2192 Q.
Proof. (* D\u00e9monstration "vers l'avant". *)
  intros P Q HP HPQ.
  apply HPQ in HP as HQ.
  assumption.
Qed.
	

Theorem imp_trans' : \u2200 P Q R : Prop,
  (P \u2192 Q) \u2192 (Q \u2192 R) \u2192 (P \u2192 R).
Proof. (* D\u00e9monstration "vers l'avant". *)
  intros P Q R HPQ HQR HP.
  apply HPQ in HP as HQ.
  apply HQR in HQ as HR.
  assumption.
Qed.
	

(** ** N\u00e9gation [\u00ac] *)

(** La n\u00e9gation en coq (en fait en logique intuitionniste) est aussi un peu
    particuli\u00e8re par rapport \u00e0 la n\u00e9gation
    d'un cours de math\u00e9matique usuel. En effet, on ne d\u00e9finit pas la n\u00e9gation 
    d'une proposition par sa table de v\u00e9rit\u00e9 mais en ajoutant une nouvelle
    proposition appel\u00e9e [False].
*)

(** *** La proposition [False] *)
Check False.
(** La proposition [False] 
    d\u00e9signe le faux, l'absurde, souvent not\u00e9e en math\u00e9matiques \u22a5
    (symbole _bottom_). Elle est d\u00e9finie par la r\u00e8gle appel\u00e9e _ex falso quod
    libet_ (du faux, on peut d\u00e9duire n'importe quoi), aussi appel\u00e9e _principe
    d'explosion_.
*)

(** Si [False] est dans les hypoth\u00e8ses, alors la tactique [destruct] permet
    simplement de terminer la preuve.
*)
Theorem ex_falso_quod_libet : \u2200 P : Prop, False \u2192 P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P.
  (* Supposons (hypoth\u00e8se (contra)) que False est prouv\u00e9e. *)
  intros contra.
  (* Explosion : le faux fait partie des hypoth\u00e8ses. *)
  destruct contra.
Qed.

(** **** Exercice : prouver le th\u00e9or\u00e8me suivant : *)

Theorem exo_false : \u2200 P Q : Prop,
  (P \u2192 False) \u2192 P \u2192 Q.
Proof.
  intros P Q HPF HP.
  destruct HPF.
  assumption.
Qed.
	
  
(** La tactique [exfalso] remplace le but courant par False. En effet, si l'on prouve False on prouve
    tout de toute fa\u00e7on. *)

Theorem stupide : \u2200 P Q : Prop, (P \u2192 False) \u2192 P \u2192 (Q \u2192 P \u2192 Q).
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* On suppose
    (HnP) : P \u2192 False
    (HP) : P *)
  intros HnP HP.
  (* Comme le False implique n'importe quoi, il suffit de prouver False. *)
  exfalso.
  (* Par HnP il suffit de prouver P. *)
  apply HnP.
  (* Or P est vrai par hypoth\u00e8se. *)
  assumption.
Qed.
  
(** *** R\u00e9futer une proposition *)
(** En logique intuitionniste, plut\u00f4t que de dire qu'une proposition est
    _fausse_, on dit qu'elle est _contradictoire_, ce qui signifie qu'ajouter
    cette proposition dans les hypoth\u00e8se m\u00e8ne \u00e0 une explosion : une preuve de
    [False] (et donc, par _ex falso quod libet_ de _toutes_ les propositions).

    La _n\u00e9gation_ d'une proposition P, not\u00e9e en coq [~P], [\u00acP] ou encore [not
    P],  
    est en fait simplement une _notation_ pour [P \u2192 False].

    Pour entrer le symbole \u00ac :
    - Ctrl+Shift+u puis 00ac et Espace
    - ou sur coqide \not puis Shift+Espace.
*)
Check not.
Print not.
Locate "~".
Locate "\u00ac".

(** On peut _d\u00e9plier_ une notation, c'est-\u00e0-dire la remplacer par ce qu'elle
    repr\u00e9sente avec la tactique [unfold] (litt\u00e9ralement "d\u00e9plier").  *)

(* Le prochain th\u00e9or\u00e8me dit la chose suivante :
   - si C fait tout exploser
   - et que A \u2192 C
   - alors A aussi fait tout exploser. *)

Theorem contrapos\u00e9e : \u2200 A C : Prop, (A \u2192 C) \u2192 (\u00acC \u2192 \u00acA).
Proof.
  (* Soient A et C deux propositions quelconques. *)
  intros A C.
  (* On suppose :
    - (HAC) : [A \u2192 C]
    - (HnC) : [\u00acC] *)
  intros HAC HnC.
  (* Dans le but, on remplace \u00acA par sa d\u00e9finition, \u00e0 savoir [A \u2192 False]. *)
  unfold not.
  (* On suppose (HA) : A  et on cherche \u00e0 prouver False. *)
  intro HA.
  (* Par modus ponent appliqu\u00e9 \u00e0 (HAC) et (HA), on a (HC) : C *)
  apply HAC in HA as HC.
  (* Dans l'hypoth\u00e8se (HnC), on remplace \u00acC par sa d\u00e9finition. *)
  unfold not in HnC.
  (* Par modus ponent appliqu\u00e9 \u00e0 (HnC) et (HC) on prouve
     (explosion) : False *)
  apply HnC in HC as explosion.
  (* On a donc prouv\u00e9 le but. *)
  assumption.
  (* [destruct explosion.] aurait march\u00e9 aussi par ex falso quod libet. *)
Qed.

(** En math\u00e9matique, la _contrapos\u00e9e_ de l'implication [A \u2192 C] est
    [\u00acC \u2192 \u00acA]. On vient de voir que [A \u2192 C] implique [\u00acC \u2192 \u00acA]. En
    _logique classique (voir plus loin) la r\u00e9ciproque est vraie : une
    implication et sa contrapos\u00e9e sont \u00e9quivalentes. *)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem absurd : \u2200 A C : Prop, A \u2192 \u00acA \u2192 C.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	


(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem exo_neg : \u2200 P Q : Prop,
  (P \u2192 Q) \u2192 (P \u2192 \u00acQ) \u2192 \u00acP.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** La double n\u00e9gation *)
(** En logique classique, pour toute proposition A, \u00ac\u00acA est \u00e9quivalente \u00e0 A.
    En logique intuitionniste, ce n'est pas toujours le cas.
    \u00acA signifiant "A est contradictoire", c'est-\u00e0-dire "A fait tout exploser",
    \u00ac\u00acA signifie "Il est contradictoire d'affirmer que A est contradictoire",
    autrement dit "A est non contradictoire", ou encore "A ne fait pas tout
    exploser".
    \u00ac\u00acA est une notation pour (A \u2192 False) \u2192 False.
*)

(** Une proposition prouv\u00e9e est toujours non contradictoire. *)

(** **** Exercice : prouver le th\u00e9or\u00e8me suivant. *)
Theorem A_imp_NNA : \u2200 A : Prop, A \u2192 \u00ac\u00acA.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice bonus (assez difficile) *)
(* On peut prouver (pour deux propositions donn\u00e9es) que le raisonnement par
   contraposition est non contradictoire. *)

Theorem contraposee2 : \u2200 A C : Prop,
  \u00ac\u00ac((\u00acC \u2192 \u00acA) \u2192 (A \u2192 C)).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice bonus (assez difficile) *)

(** Dans la m\u00eame veine, on a le th\u00e9or\u00e8me suivant. *)

Theorem contraposee3 : \u2200 A C : Prop,
  (\u00acC \u2192 \u00acA) \u2192 \u00ac\u00ac(A \u2192 C).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Conjonction [\u2227] *)

(** Le "et logique" aussi appel\u00e9 op\u00e9rateur de conjonction est not\u00e9 en Coq [\u2227] ou
    [/\].
    Si [P] et [Q] sont deux propositions, alors [P \u2227 Q] est une proposition dont
    une preuve est \u00e0 la fois une preuve de [P] et une preuve de [Q].

    Pour entrer \u2227 :
    - Ctrl + Shift + u puis 2227 et Espace.
    - sur coqide : \and puis Shift + Espace.
*)

(** *** Utiliser une conjonction : [destruct] *)

(** Pour d\u00e9composer une preuve de [P \u2227 Q] en une preuve de [P] et une preuve de
    [Q], on utilise la tactiques
    [destruct]. *)
Theorem proj1 : \u2200 P Q : Prop, P \u2227 Q \u2192 P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (HPQ) : P \u2227 Q *)
  intros HPQ.
  (* De HPQ on d\u00e9duit :
      (HP) : P
      (HQ) : Q *)
  destruct HPQ as [HP HQ].
  (* Alors [P] est prouv\u00e9e par hypoth\u00e8se. *)
  assumption.
Qed.

(** Noter la syntaxe avec crochets pour [destruct] une proposition conjonctive. *)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem proj2 : \u2200 P Q : Prop, P \u2227 Q \u2192 Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** Prouver une conjonction : [split] *)

(** Prouver [P \u2227 Q] se fait en deux \u00e9tapes : il faut une preuve de P et une
    preuve de Q. Ceci se fait avec la tactique [split]. *)

Theorem conj : \u2200 P Q : Prop, P \u2192 Q \u2192 P \u2227 Q.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons :
      (HP) : P
      (HQ) : Q. *)
  intros HP HQ.
  (* Pour prouver P \u2227 Q on doit prouver P puis Q. *)
  split.
  - (* Preuve de P : *)
    (* P est prouv\u00e9e par hypoth\u00e8se. *)
    assumption.
  - (* Preuve de Q : *)
    (* Q est prouv\u00e9e par hypoth\u00e8se. *)
    assumption.
Qed.

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem and_comm1 : \u2200 P Q : Prop, P \u2227 Q \u2192 Q \u2227 P.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Remarque : comme pour  [\u2192] l'op\u00e9rateur [\u2227] est "associatif \u00e0 droite" en coq,
    ce qui signifie que P \u2227 Q \u2227 R d\u00e9note en fait P \u2227 (Q \u2227 R). Comme on va le
    voir ci-dessous, ceci n'a pas tellement d'importance ici. *)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem and_assoc1 : \u2200 P Q R : Prop, (P \u2227 Q) \u2227 R \u2192 P \u2227 Q \u2227 R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem and_assoc2 : \u2200 P Q R : Prop, P \u2227 Q \u2227 R \u2192 (P \u2227 Q) \u2227 R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** \u00c9quivalence [\u2194] *)

(** L'\u00e9quivalence logique est not\u00e9e en coq [\u2194] ou [<->].
    Une preuve de [P \u2194 Q] est \u00e0 la fois une preuve de [P \u2192 Q] et une preuve de
    [Q \u2192 R].  Les tactiques [destruct] (pour utiliser une \u00e9quivalence) et
    [split] (pour prouver une \u00e9quivalence) s'appliquent de la m\u00eame mani\u00e8re que
    pour la conjonction.

    Pour entrer \u2194 :
    - Ctrl + Shift + u puis 2194 et Espace.
    - sur coqide : \harr puis Shift + Espace.
*)

Theorem iff_sym : \u2200 P Q : Prop, P \u2194 Q \u2192 Q \u2194 P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* On suppose (H) : P \u2194 Q. *)
  intros H.
  (* De (H) on d\u00e9duit :
     (HPQ) : P \u2192 Q
     (HQP) : Q \u2192 P *)
  destruct H as [HPQ HQP].
  (* Pour prouver P \u2194 Q, il suffit de prouver P \u2192 Q et Q \u2192 P. *)
  split.
  - (* Preuve de P \u2192 Q : *)
    assumption.
  - (* Preuve de Q \u2192 P : *)
    assumption.
Qed.

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem iff_refl : \u2200 P : Prop, P \u2194 P.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem iff_trans : \u2200 P Q R : Prop, (P \u2194 Q) \u2192 (Q \u2194 R) \u2192 (P \u2194 R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Appliquer un th\u00e9or\u00e8me. *)

(** En coq, on peut fournir des _arguments_ \u00e0 un th\u00e9or\u00e8me pour produire des
    formules d\u00e9j\u00e0 prouv\u00e9es. *)
Module ThmPlayground.
(* Module pour ne pas polluer le reste du fichier avec les param\u00e8tres A, B et C. *)
Parameters A B C : Prop.
Check and_comm1.
(** On peut _appliquer_ le th\u00e9or\u00e8me and_comm1 \u00e0 deux propositions. *)
Check and_comm1 A B.
(** Ou bien \u00e0 une seule, l'autre restant universellement quantifi\u00e9e. *)
Check and_comm1 A.
(** On peut m\u00eame donner en argument des propositions plus compliqu\u00e9es. *)
Check and_comm1 (A \u2192 B) (B \u2192 C).
End ThmPlayground.

(** On peut alors utiliser dans des preuves des th\u00e9or\u00e8mes d\u00e9j\u00e0 prouv\u00e9s. *)
Theorem and_comm : \u2200 P Q : Prop, (P \u2227 Q) \u2194 (Q \u2227 P).
Proof.
  intros P Q.
  split.
  - apply (and_comm1 P Q).
  - apply (and_comm1 Q P).
Qed.

(** **** Exercice : *)

(** Prouver le th\u00e9or\u00e8me suivant en utilisant [pr\u00e9misses_non_ordonn\u00e9es1] *)

Theorem pr\u00e9misses_non_ordonn\u00e9es : \u2200 P Q R : Prop, (P \u2192 Q \u2192 R) \u2194 (Q \u2192 P \u2192 R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
  
(** **** Exercice : *)

(** Prouver le th\u00e9or\u00e8me suivant en utilisant [and_assoc1] et {and_assoc2] *)

Theorem and_assoc : \u2200 P Q R, (P \u2227 Q) \u2227 R \u2194 P \u2227 (Q \u2227 R).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Disjonction [\u2228] *)

(** En coq, le "ou logique", aussi appel\u00e9e op\u00e9rateur de disjonction est not\u00e9 [\u2228]
    ou [\/].
    Une preuve de [P \u2228 Q] est une preuve de P ou une preuve de Q.

    Pour entrer \u2228 :
    - Ctrl + Shift + u puis 2228 et Espace.
    - sur coqide : \or puis Shift + Espace.
*)

(** *** Prouver une disjonction : [left] et [right] *)

(** Pour pouvoir prouver une disjonction (par exemple [P \u2228 Q]) en logique
    intuitionniste, il faut dire explicitement
    si l'on donne une preuve de l'op\u00e9rande de gauche (ici [P]) ou de celui de
    droite (ici [Q]).  Ceci se fait avec les tactiques [left] et [right].  *)

Theorem or_introl : \u2200 P Q : Prop, P \u2192 P \u2228 Q.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (HP) : P *)
  intros HP.
  (* Pour prouver P \u2228 Q il suffit de prouver P. *)
  left.
  (* Or P est vrai par hypoth\u00e8se. *)
  assumption.
Qed.

(** **** Exercice : *)

(* Prouver le th\u00e9or\u00e8me suivant : *)

Theorem or_intror : \u2200 P Q : Prop, Q \u2192 P \u2228 Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : *)

(* Prouver le th\u00e9or\u00e8me suivant : *)

Theorem and_imp_or : \u2200 P Q : Prop, P \u2227 Q \u2192 P \u2228 Q.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** *** Utiliser une disjonction : [destruct] *)

(** Utiliser une disjonction (par exemple [P \u2228 Q] est un peu plus d\u00e9licat.
    On va alors faire une preuve _par cas_ :
    - Dans un premier temps, on prouve le but sous l'hypoth\u00e8se que l'op\u00e9rande de
      gauche (ici [P]) est prouv\u00e9e.
    - Dans un second temps, on prouve le but sous l'hypoth\u00e8se que  l'op\u00e9rande de
      droite (ici [Q]) est prouv\u00e9e. *)

Theorem or_comm1 : \u2200 P Q : Prop, P \u2228 Q \u2192 Q \u2228 P.
Proof.
  (* Soient P et Q deux propositions quelconques. *)
  intros P Q.
  (* Supposons
     (H) : P \u2228 Q. *)
  intros H.
  (* De (H) on d\u00e9duit 
     (HP) : P
     ou
     (HQ) : Q *)
  destruct H as [HP | HQ].
  (* Noter beaucoup de choses ici :
     - la syntaxe avec la barre verticale dans les crochets pour le "ou"
     - il y a maintenant deux sous-buts, un par cas. En fait ce ne sont pas les
       sous-buts qui diff\u00e8rent, mais les contextes (hypoth\u00e8ses). *)
  - (* cas 1 : hypoth\u00e8se (HP) *)
    right.
    assumption.
  - (* cas 2 : hypoth\u00e8se (HQ) *)
    left.
    assumption.
Qed.

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem or_ind : \u2200 P Q R: Prop, (P \u2192 R) \u2192 (Q \u2192 R) \u2192 P \u2228 Q \u2192 R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem or_assoc : \u2200 P Q R: Prop, (P \u2228 Q) \u2228 R \u2194 P \u2228 Q \u2228 R.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** ** Lois de de Morgan *)
(** Les "lois" de de Morgan sont un r\u00e9sultat tr\u00e8s connu (et tr\u00e8s utile !) de
    logique classique. Que se passe-t-il en logique intuitionniste ? *)

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem de_morgan_not_or : \u2200 P Q : Prop, \u00ac(P \u2228 Q) \u2194 (\u00ac P) \u2227 (\u00ac Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le th\u00e9or\u00e8me suivant. *)

Theorem de_morgan_not_or_not : \u2200 P Q : Prop, (\u00ac P) \u2228 (\u00ac Q) \u2192 \u00ac (P \u2227 Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Remarquez qu'on ne vous a demand\u00e9 qu'une seule implication dans l'exercice
    pr\u00e9c\u00e9dent...
    C'est parce que l'autre n'est tout simplement pas prouvable en logique
    intuitionniste. *)

(** **** Exercice : *)

(** En essayant de prouver le th\u00e9or\u00e8me suivant, se convaincre que ce n'est pas possible. *)
Theorem de_morgan_not_and : \u2200 P Q : Prop, \u00ac (P \u2227 Q) \u2192 (\u00ac P) \u2228 (\u00ac Q).
Proof.
  (* Essayez d'aller le plus loin possible dans la preuve. *)
Abort.

(** *** Logique intuitionniste et logique classique *)

(** La diff\u00e9rence avec votre cours de math\u00e9matiques classiques est que coq n'est
    pas \u00e9quip\u00e9 par d\u00e9faut du
    tiers-exclus. En pratique, cela veut dire pour les inconv\u00e9nients :
    - pas de preuve par l'absurde
    - pas de raisonnement par contraposition
    - il manque une loi de de Morgan sur les 4
    mais il y a un gros avantage \u00e0 cela : les preuves sont constructives. *)

(** Peut-on ajouter le tiers-exclus \u00e0 coq pour prouver les m\u00eames th\u00e9or\u00e8mes qu'en
    math\u00e9matiques ?
    Un th\u00e9or\u00e8me difficile montre que si coq est correct (c'est-\u00e0-dire ne permet
    pas de prouver quelque chose de faux), alors (coq + tiers-exclus) est aussi
    correct.  On peut donc, si on le souhaite ajouter l'axiome du tiers-exclus.
*)

Axiom excluded_middle : \u2200 P : Prop, P \u2228 \u00ac P.

(** Attention ! Un axiome n'est pas un th\u00e9or\u00e8me ! En ajoutant un axiome on prend
    le risque (\u00e9norme) que tout s'\u00e9croule ! *)

(** Avec cet axiome, on peut prouver, par exemple, que le raisonnement par
    l'absurde est correct. *)

Theorem NNPP : \u2200 P : Prop, \u00ac \u00ac P \u2192 P.
Proof.
  (* Soit P une proposition quelconque. *)
  intros P. unfold not.
  (* On suppose (hyp. (H)) que P est non contradictoire. *)
  intros H.
  (* Par le principe du tiers-exclus appliqu\u00e9 \u00e0 P, on a
     (HP) : P
     ou
     (HnP) : \u00ac P *)
  destruct (excluded_middle P) as [HP | HnP].
  - (* cas 1 : (HP) *)
  assumption.
  - (* cas 2 : (HnP) *)
  (* On va ici utiliser le principe d'explosion : ce cas est contradictoire. *)
  exfalso.
  unfold not in HnP.
  apply H.
  assumption.
Qed.

(** **** Exercice : *)

(** En utilisant le tiers-exclus ou l'une de ses cons\u00e9quences, prouver le
    th\u00e9or\u00e8me suivant : *)

Theorem de_morgan_not_and : \u2200 P Q : Prop, \u00ac (P \u2227 Q) \u2192 (\u00ac P) \u2228 (\u00ac Q).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	
  
(** **** Exercice : *)

(** En utilisant le tiers-exclus ou l'une de ses cons\u00e9quences, prouver le
    th\u00e9or\u00e8me suivant : *)

Theorem classical_imp : \u2200 P Q : Prop, (P \u2192 Q) \u2194 (Q \u2228 \u00ac P).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)
	

(** Pour terminer cette partie sur la logique classique.
    Il y a bien des th\u00e9or\u00e8me qu'on peut d\u00e9montrer en logique classique et pas en
   logique intuitionniste.  Ceci dit, une preuve dite "constructive",
   c'est-\u00e0-dire en logique intuitionniste, lorsqu'elle est possible, apporte
   toujours plus d'informations qu'une preuve non constructive. Les preuves par
   l'absurde peuvent tr\u00e8s souvent (m\u00eame dans les preuves de math\u00e9maticiens
   professionnels) \u00eatre remplac\u00e9es par des preuves plus simples et plus
   parlantes.  Dans vos \u00e9tudes de math\u00e9matiques, vous devriez \u00e9viter de vous
   jeter dessus. *)

(** ** Conclusion *)
(** Il est assez difficile de se lancer en preuves formelles. Il y a beaucoup,
   beaucoup de choses \u00e0 assimiler d'un seul coup.
    Le plus dur est sans doute fait. N'h\u00e9sitez pas \u00e0 refaire les exercices
   ci-dessus assez souvent. *)
