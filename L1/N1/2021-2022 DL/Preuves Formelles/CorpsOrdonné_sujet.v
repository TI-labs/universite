(** * Nombres réels *)

Require Import Unicode.Utf8.
Require Import Reals.
Open Scope R_scope.

(** ** Axiomatisation du corps des réels *)

(** Les nombres réels de Coq sont définis
    en utilisant le fait que l'ensemble R des nombres réels est
    - un corps (commutatif),
    - totalement ordonné,
    - archimédien et
    - complet,
    et que, à isomorphisme (c'est-à-dire en gros renommage des éléments) près,
    c'est le seul.
    Cette phrase vous fait certainement très peur... on va revenir un peu sur
    tous ces points dans ce cours et vous y reviendrez bien plus longuement dans
    les autres cours de mathématiques.

    En revanche, vous pouvez retenir dès maintenant que nous allons étudier les
    réels non pas par le biais de telle ou telle construction (coupures de
    Dedekind, complétion de Q, ...) mais par le biais d'une poignée (17) de
    propriétés admises (axiomes). On peut ensuite prouver que ces propriétés
    sont valables dans les diverses constructions des réels.

    Nous avions adopté une démarche complètement opposée pour les naturels : ils
    avaient été construits par induction.

    C'est courant en mathématiques et en informatique d'étudier (ou même
    d'implémenter partiellement) un objet sans se soucier de la façon dont il a
    été fabriqué.
*)

(** *** R est un corps commutatif
    
    En mathématiques, on appelle corps un ensemble muni de deux opérations,
    l'une qu'on appelle addition et note généralement + et l'autre appelée
    multiplication, notée généralement *, ces deux opérations satisfaisant
    quelques propriétés (voir plus loin). *)

(** Dans les réels de coq, l'addition s'appelle Rplus et est notée +, tandis que
    la multiplication s'appelle Rmult et est notée *. *)
Locate "+".
Check Rplus.
Locate "*".
Check Rmult.

(** L'addition doit être commutative, associative, et avoir un élément neutre
    (qu'on note généralement 0) *)

Check Rplus_comm. (* commutativité, AXIOME 1 *)
Check Rplus_assoc. (* associativité, AXIOME 2 *)
Check Rplus_0_l. (* 0 est élément neutre à gauche, AXIOME 3 *)
Check Rplus_0_r. (* 0 est élément neutre à droite, THÉORÈME (qui se déduit
  facilement de la commutativité et de Rplus_0_l *)

(** Évidemment, on peut ajouter un même élément à gauche et à droite d'une
    égalité. *)
Check Rplus_eq_compat_r. (* THÉORÈME, facile à prouver. *)
Check Rplus_eq_compat_l. (* THÉORÈME, facile à prouver. *)

(** C'est facile à prouver et ne necessite pas d'utiliser les axiomes, alors
    allons-y. *)
Theorem Rplus_eq_compat_r : ∀ r r1 r2 : R, r1 = r2 → r1 + r = r2 + r.
Proof.
  intros.
  rewrite H.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


Theorem Rplus_eq_compat_l : ∀ r r1 r2 : R, r1 = r2 → r + r1 = r + r2.
Proof.
  intros.
  rewrite H.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** De plus, tout élément x de R doit avoir un opposé, c'est-à-dire qu'il faut
    qu'il existe un élément y de R tel que x + y = 0. On peut commencer par
    prouver que cet opposé est toujours unique. *)

Theorem oppose_unique : ∀ x y z : R, x + y = 0 /\ x + z = 0 → y = z.
(* Avant de se lancer tête baissée, voici une preuve mathématique
   Soient x, y et z trois réels tels que
   (Hy) x + y = 0 et
   (Hz) x + z = 0.
   En ajoutant z de chaque côté dans (Hy), on obtient
   x + y + z = z donc y + x + z = z et comme x + z vaut 0 d'après (Hz),
   y = z, CQFD. *)
  intros.
  destruct H as [H1 H2].
  apply (Rplus_eq_compat_r z (x + y) 0) in H1.
  rewrite Rplus_0_l in H1.
  rewrite Rplus_assoc in H1.
  rewrite Rplus_comm in H1.
  rewrite Rplus_assoc in H1.
  rewrite (Rplus_comm z x) in H1.
  rewrite H2 in H1.
  rewrite Rplus_0_r in H1.
  assumption.
Qed.


(** Cet opposé, étant unique, peut avoir sa propre notation et vous la connaissez
    bien sûr, l'opposé de x est noté (-x) et notre 4ème axiome dit que tout réel
    en a un. *)
Locate "-".
Check Ropp.
Check Rplus_opp_l. (* AXIOME 4 *)
Check Rplus_opp_r. (* THÉORÈME, se déduit facilement de la commutativité et de
Rplus_opp_l *)
(** On l'utilise pour définir la soustraction : *)
Print Rminus.

(** On peut alors "simplifier" les équations. *)
Check Rplus_eq_reg_l.
(* On a tout ce qu'il faut pour le prouver. *)
Theorem Rplus_eq_reg_l: ∀ r r1 r2 : R, r + r1 = r + r2 → r1 = r2.
Proof.
  intros.
  apply (Rplus_eq_compat_l (- r) (r + r1) (r + r2)) in H.
  rewrite <- (Rplus_assoc (- r) r r1) in H.
  rewrite <- (Rplus_assoc (- r) r r2) in H.
  rewrite Rplus_opp_l in H.
  rewrite Rplus_0_l in H.
  rewrite (Rplus_0_l r2) in H.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** La multiplication doit également être associative, commutative (c'est pour
    ça qu'on parle de corps commutatif) et avoir un élément neutre, le plus
    souvent noté 1. *)
Check Rmult_assoc. (* associativité, AXIOME 5 *)
Check Rmult_comm. (* commutativité, AXIOME 6 *)
Check Rmult_1_r. (* 1 est neutre à droite, AXIOME 7 *)
Check Rmult_1_l. (* 1 est neutre à gauche, THÉORÈME, se déduit du précédent et de la
                    commutativité *)

(** Enfin, la multiplication doit être distributive sur l'addition. *)
Check Rmult_plus_distr_l. (* AXIOME 8 à gauche *)
Check Rmult_plus_distr_r. (* THÉORÈME à droite : se déduit des autres axiomes. *)

(** On peut maintenant montrer que 0 est absorbant pour la multiplication. *)
Lemma Rmult_O_l: ∀ r : R, r * 0 = 0.
(** Avant de se lancer sur la preuve formelle, on cherche une preuve en
    mathématiques plus informelles (mais arrière, pour garder le raisonnement "à
    la coq"). *)
(* Preuve mathématique (arrière):
Pour prouver r * 0 = 0, il suffit de montrer que r + (r * 0) = r + 0.
Comme r = r * 1 (élément neutre de la multiplication), il suffit de montrer
(r * 1) + (r * 0) = r + 0
On factorise par r dans le membre gauche. On doit maintenant prouver
r * (1 + 0) = r + 0
c'est-à-dire
r * 1 = r car 0 est élément neutre de l'addition
qui revient à
r = r car 1 est élément neutre de la multiplication.
La preuve est donc terminée.  *)
Proof. (* Maintenant la preuve formelle. *)
  intros.
  apply (Rplus_eq_reg_l r).
  rewrite <- (Rmult_1_r r).
  rewrite Rmult_assoc.
  rewrite <- (Rmult_plus_distr_l r 1 (1 * 0)).
  rewrite Rmult_1_l.
  rewrite Rmult_1_r.
  rewrite Rplus_0_r.
  rewrite Rplus_0_r.
  rewrite Rmult_1_r.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** Au passage, sachez que coq sait prouver tout seul ce genre de théorème :
    Ici, on travaille avec les axiomes des anneaux unitaires 
    (addition, opposés, multiplication avec élément neutre). Anneau, en anglais
    : _ring_. *)

Lemma Rmult_O_l_bis: ∀ r:R, r * 0 = 0.
Proof.
  intro r. ring.
Qed.
(** Mais à ce stade, c'est encore un peu tricher *)

(** Dans un corps, 0 et 1 doivent être distincts. *)
Check R1_neq_R0. (* AXIOME 9 *)
 
(** Et enfin (et c'est ce qui distingue vraiment les corps d'autres objets
    mathématiques appelés anneaux) tout élément x non nul a un inverse.
    Cet inverse est unique (même preuve que pour l'unicité de l'opposé et c'est
    normal...) et est noté en mathématiques 1 / x, en coq / x.*)
Check Rinv.
Check Rinv_l. (* AXIOME 10 *)
Check Rinv_r. (* Théorème *)

(** On définit la division à l'aide de la multiplication et de l'inverse. *)
Print Rdiv.

(** Comme exemples de théorèmes de théorie des corps commutatifs, on peut voir
    l'inversion d'une fraction. *)
Check Rinv_Rdiv.

(** Le chemin est encore un peu long : 
    on va d'abord montrer qu'on peut simplifier les équations. *)
(** On peut bien sûr multiplier les deux côtés d'une équation par un même
    nombre : *)
Check Rmult_eq_compat_l. (* Très facile à prouver, on laisse de côté. *)

Theorem Rmult_eq_reg_l: ∀ r r1 r2 : R, r * r1 = r * r2 → r ≠ 0 → r1 = r2.
Proof.
  intros.
  apply (Rmult_eq_compat_l (/ r) (r * r1) (r * r2)) in H.
  rewrite <- Rmult_assoc in H.
  rewrite <- Rmult_assoc in H.
  rewrite Rinv_l in H.
  -
    rewrite Rmult_1_l in H.
    rewrite Rmult_1_l in H.
    assumption.
  -
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** Puis on montre que pour qu'un produit soit nul, il faut qu'un de ses facteurs
    soit nuls. Mais pour ça on a besoin du théorème disant
    qu'on peut raisonner par cas sur l'égalité ou non de deux réels
    (conséquence du fait que l'ordre est total, voir plus loin. *)

Check Req_dec. (* On le prouve plus tard. *)

Check Rmult_integral.
Theorem Rmult_integral: ∀ r1 r2 : R, r1 * r2 = 0 → r1 = 0 ∨ r2 = 0.
Proof.
  (* Indice : considérer les cas r1 = 0 et r1 <> 0 grâce à Req_dec. *)
  intros.
  destruct (Req_dec r1 0).
  -
    left.
    assumption.
  -
    right.
    rewrite <- (Rmult_0_r r1) in H.
    apply (Rmult_eq_reg_l r1 r2 0) in H.
    +
      assumption.
    +
      assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** Maintenant, l'inversion d'un réel non nul est non nul : *)
Check Rinv_neq_0_compat.
Theorem Rinv_neq_0_compat: ∀ r : R, r ≠ 0 → / r ≠ 0.
Proof.
  intros.
  unfold not.
  intros.
  apply (Rmult_eq_compat_l r (/r) 0) in H0.
  rewrite Rmult_0_r in H0.
  rewrite Rinv_r in H0.
  -
    apply R1_neq_R0 in H0.
    assumption.
  -
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)

Theorem Rmult_neq_integral: ∀ r1 r2 : R, r1 ≠ 0 → r2 ≠ 0 → r1 * r2 ≠ 0.
Proof.
  intros.
  unfold not.
  intros H1.
  apply (Rmult_integral) in H1.
  destruct H1.
  -
    apply H in H1.
    assumption.
  -
    apply H0 in H1.
    assumption.
Qed.


(** Enfin, l'inverse de (x / y) vaut (y / x) à condition que ces réels soient
    non nuls. *)
Theorem Rinv_Rdiv: ∀ x y : R, x ≠ 0 → y ≠ 0 → / (x / y) = y / x.
Proof.
  intros.
  apply (Rmult_eq_reg_l (x / y) (/ (x / y)) (y / x)).
  -
    rewrite Rinv_r.
    +
      rewrite <- (Rinv_Rdiv y x).
      *
        rewrite Rmult_comm.
        apply Rinv_neq_0_compat in H.
        apply (Rmult_neq_integral y (/x)) in H.
        --
          fold (y / x) in H.
          symmetry.
          apply (Rinv_r (y / x)).
          assumption.
        --
          assumption.
      *
        assumption.
      *
        assumption.
    +
      apply Rinv_neq_0_compat in H0.
      apply (Rmult_neq_integral x (/y)) in H0.
      fold (x / y) in H0.
      assumption.
      assumption.
  -
    apply Rinv_neq_0_compat in H0.
    apply (Rmult_neq_integral x (/y)) in H0.
    fold (x / y) in H0.
    assumption.
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** Ouf ! C'était plus compliqué que prévu. *)
(** Au passage, coq possède des tactiques automatiques très avancées. Il y a
    même la possibilité d'écrire ses propres tactiques. Ici, voyons les
    tactiques [field] (_corps_ en anglais) pour vérifier les égalités dans les
    corps et [auto] pour conclure sur la logique de base. *)

Theorem Rinv_Rdiv': ∀ x y : R, x ≠ 0 → y ≠ 0 → / (x / y) = y / x.
Proof.  intros. field. auto.  Qed.

(** Nous n'avons utilisé pour l'instant
    que les axiomes de corps, nos raisonnement sont donc valables dans tous les
    corps (les réels, mais aussi les rationnels, les complexes, et bien
    d'autres...) *)

(** ** R est totalement ordonné
    Une des choses qui distinguent R de l'ensemble des nombres complexes par
    exemple est qu'il dispose d'une relation d'ordre < qui est totale (ça veut
    dire qu'on peut toujours comparer deux réels) et compatible avec l'addition
    et la multiplication par un nombre positif.
*)

(** Antisymétrie AXIOME 11 *)
Check Rlt_asym.
(** Transitivité AXIOME 12 *)
Check Rlt_trans.
(** Compatibilité de l'ordre par rapport à l'addition AXIOME 13 *)
Check Rplus_lt_compat_l.
(** Compatibilité de l'ordre par rapport à la multiplication AXIOME 14 *) 
Check Rmult_lt_compat_l.
(** L'ordre est total. AXIOME 15 *)
Check total_order_T.

(** On peut maintenant prouver que l'opposé est une fonction décroissante. *)
Check Ropp_lt_contravar.
Theorem Ropp_lt_contravar : ∀ r1 r2 : R, r2 < r1 → - r1 < - r2.
Proof.
  intros r1 r2 H.
  apply (Rplus_lt_compat_l (-r1)) in H.
  apply (Rplus_lt_compat_r (-r2)) in H.
  rewrite Rplus_assoc in H.
  rewrite Rplus_opp_l in H.
  rewrite Rplus_opp_r in H.
  rewrite Rplus_0_r in H.
  now rewrite Rplus_0_l in H.
Qed.

(** Nous avions laissé tout à l'heure la preuve de Req_dec, c'est-à-dire du fait
   que pour deux réels x et y, on a x = y ou x <> y. Ceci est une conséquence des
   axiomes Rlt_asym et total_order_T.*)

(** D'abord, on cherche un théorème qui nous dit qu'un réel n'est pas inférieur à
    lui-même. *)
Search Rlt.
Check Rlt_irrefl.
(** Autant le prouver. *)
Theorem Rlt_irrefl: ∀ r : R, ¬ r < r.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


Check Req_dec.
Print Req_dec.
Theorem Req_dec : ∀ r1 r2 : R, r1 = r2 ∨ r1 ≠ r2.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Les carrés sont positifs, mais nous avons besoin de la règle des signes et
    d'autres résultats pour le prouver *)

Locate "²".
Print Rsqr.
Locate "<=".
Check Rle.
Print Rle.

(** On peut chercher les théorèmes des bibliothèques importées
    à l'aide de Search. *)
Search Rsqr. (* Où l'on voit qu'on veut prouver Rle_Sqr. *)

Search Rmult Ropp. (* On commence par Ropp_involutive *)
Theorem Ropp_involutive : ∀ r : R, - - r = r.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


Theorem Ropp_mult_distr_r: ∀ r1 r2 : R, - (r1 * r2) = r1 * - r2.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


Theorem Rmult_opp_opp: ∀ r1 r2 : R, - r1 * - r2 = r1 * r2.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** On a maintenant tout ce qu'il faut pour prouver qu'un carré est positif. *)
Theorem Rle_0_sqr: ∀ r : R, 0 <= r².
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Comme jolie et importante application : 1 est plus grand que 0 :) *)
Theorem Rlt_0_1: 0 < 1.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Pour finir sur cette partie, la preuve, pas si simple à la main, que
    l'inverse d'un réel positif est positif. *)
Theorem Rinv_0_lt_compat : ∀ r : R, 0 < r → 0 < / r.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** *** Autres axiomes des réels

    Nous n'allons pas cette fois-ci nous étendre sur les deux derniers axiomes
    du corps des réels : la borne supérieure et le fait qu'il soit archimédien.
    Ces axiomes sont essentiels pour faire de l'analyse (prochaine séance). *)

(** R est archimédien AXIOME 16 *)
Check archimed.
(** R est complet AXIOME 17 *)
Check completeness.
(** Borne supérieure *) 
Print is_lub.
(** majorant *)
Print is_upper_bound.

(** Pour finir, voyons quelques opérations usuelles sur R : la puissance, la
    valeur absolu, le maximum et le minimum de deux nombres. *)

Print Rpower_plus.

Lemma Rpower_add: ∀ x y z : R,
    Rpower z (x + y) = Rpower z x * Rpower z y.
Proof.
(* Début solution *)
  intros x y z. 
  unfold Rpower.
  rewrite Rmult_plus_distr_r. 
  now rewrite exp_plus. 
Qed.
(* Fin solution *)

(** On n'est pas obligé de tout faire à la main: on peut utiliser des tactiques
    un peu plus automatiques. *)

Require Import Lra.

(** Maintenant, on peut utiliser la tactique lra pour "linear real or rational
    arithmetic", capable de résoudre des égalités et inégalités linéaires entre
    variables et/ou constantes. *)

Print Rmax.
Lemma max_xy: ∀ x y:R, 
    Rmax x y = (1/2) * (x + y + (Rabs (x-y))).
(** Commencer par faire la démonstration sur une feuille.
    Vous aurez besoin du fait que pour deux réels, on peut soit avoir une preuve
    que r1 <= r2 soit avoir une preuve du contraire (décidabilité). *)
Check Rle_dec.
(** Et peut-être d'une autre propriété similaire : *)
Check Rcase_abs.
(* On va séparer deux cas :
- Si x≥y, alors x−y≥0 et donc |x−y|=x−y. 
  On a aussi max(x,y)=x et 
  (1/2)*(x+y+|x−y|)=(1/2)*(x+y+x−y)=x.
- Si x<y, alors |x−y|=y−x et max(x,y)=y. 
  Dans ce cas (1/2)*(x+y+|x−y|)=(1/2)*(x+y+y−x)=y. *)
(* Pour séparer les cas, on peut utiliser la tactique 
   [destruct], après avoir déplié Rmax et Rabs. *) 
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(* Un résultat similaire sur le min. *)
Lemma min_xy: ∀ x y:R,
    Rmin x y = (1/2) * (x + y - (Rabs (x-y))). 
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)

