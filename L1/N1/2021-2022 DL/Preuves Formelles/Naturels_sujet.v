(** * Entiers naturels et induction *)
Require Import Unicode.Utf8.

(** ** Définition inductive des entiers naturels *)

Module EntiersNaturels.
(* Le type nat est un type inductif: il est fait de constantes et de
constructeurs, qui peuvent être recursifs, c'est à dire faire
référence au type lui-mêne. *)

Print nat.
Check O.
Check S.

(** Vous voyez que pour coq, un entier naturel est soit O (la lettre O
    qui représente l'entier 0), soit l'image par la fonction S (pour
    "successeur") d'un entier naturel.

    Ainsi : * O représente zéro * S O est le successeur de zéro donc
    un * S (S O) est le successeur de un donc deux * ...

    Heureusement coq permet la notation usuelle (décimale) de ces
    entiers, mais ce n'est qu'une notation pratique !  *)

Check O.
Check S (S O).
Check S (S (S (S (S O)))).

(** On dit que la constante O et la fonction S sont les
    _constructeurs_ du type nat. En coq :

    - les résultats de constructeurs différents sont toujours
      différents,
    - pour deux termes composés uniquement de constructeurs, s'ils
      s'écrivent différemment, alors ils sont différents.

    La tactique [discriminate] cherche dans les hypothèse une égalité
    contradictoire de ce type (des termes qui s'écrivent différemment
    avec les constructeurs mais sont supposés égaux).  *)

Theorem deux_non_egal_a_trois : ¬ (S (S O) = S (S (S 0))).
Proof.
  unfold not.
  intros nimp.
  discriminate.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Theorem zero_diff_succ_k : ∀ n : nat, ¬ (O = S n).
Proof.
  intros n.
  unfold not.
  intros H.
  discriminate.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** Définissons maintenant l'addition des entiers relatifs. Dans un
    premier temps, on note plus n m la somme de n et m. *)

Fixpoint plus (n m : nat) :=
  match n with (* "faire correspondre n avec" *)
    0 => m (* Si n = 0, alors n + m = m. *)
  | S p => S (plus p m) (* (S p) + m = S (p + m). *)
  end.

(** Explications : plus n m regarde si n vaut 0, si oui il renvoie m,
    sinon c'est que n = S p pour un certain p.  On calcule alors (plus
    p m) et on prend le successeur de ce nombre là.

    Cette définition de l'addition est _récursive_ (en coq, "Fixpoint"
    pour une définition récursive) : la fonction plus est elle-même
    utilisée dans sa définition. Pourtant il n'y a pas de raisonnement
    circulaire, cette définition permet bien de calculer (sinon, coq
    ne nous aurait pas laissé faire de toute façon). Vous ne comprenez
    peut être pas tout dans cette définition mais pas de panique, on
    ne va pas vous demander de construire des définitions comme ça
    mais simplement de les utiliser.  *)

Compute (plus 2 2).

(** **** Exercice qui a l'air bête mais est très important :

    Calculer à la main (papier et crayon), en utilisant la définition
    de [plus], S (S O) + S (S (S O)). *)

(** En fait Coq dispose des notations usuelles pour l'addition (et la
    plupart des opérations usuelles. *)
Compute 2 + 5.

(** Nous allons maintenant devoir prouver des égalités.

    - La tactique [simpl] demande à coq de simplifier une expression
      dans le but en utilisant la définition d'une (ou plusieurs)
      fonctions, autant que possible.

    - La tactique [reflexivity] demande à coq de conclure la preuve en
      constatant que les membres de gauche et de droite d'une égalité
      sont les mêmes.  *)

Theorem deux_et_deux_font_quatre : 2 + 2 = 4.
Proof.
  simpl. (* 2 + 2 = S (2 + 1) = S (S (2 + 0)) = S (S 2) = 4. *)
  reflexivity.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Theorem zero_neutre_plus_g : ∀ n : nat, 0 + n = n.
Proof.
  intros n.
  simpl.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem un_plus_egal_suc : ∀  n : nat, 1 + n = S n.
Proof.
  intros n.
  simpl.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

(* N'oubliez pas [discriminate] pour réfuter une égalité. *)
Theorem deux_et_deux_ne_font_pas_cinq : ¬ (2 + 2 = 5).
Proof.
  unfold not.
  simpl.
  discriminate.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** ** Preuve par induction (ou récurrence) *)

(** simpl et reflexivity ne suffisent pas pour tout prouver : *)

Theorem zero_neutre_plus_d : ∀ (n : nat), n + 0 = n.
Proof.
  intros n.
  simpl. (* Aïe, simpl ne marche pas *)
  (* On est bloqué. *)
Abort.

(** Nous avons besoin de prouver ce théorème par induction. Une preuve
    par induction sur entier naturel n a deux parties 
    - on prouve la proposition dans le cas où n vaut 0,
    - puis dans le cas où n s'écrit S n' pour un certain n', on prouve
      la propriété pour n _sous l'hypothèse qu'elle est vraie pour n'_
      (on appelle cette hypothèse "hypothèse d'induction"). *)

Theorem zero_neutre_plus_d : ∀ (n : nat), n + 0 = n.
Proof.
  induction n as [|n' IH].
  - (* On suppose que n = 0. *)
    (* Par définition de +, 0 + 0 vaut 0. *)
    simpl.
    (* Les deux membres de l'égalité sont les mêmes, ce cas est terminé. *)
    reflexivity.
  - (* On suppose que n = S n' et que n' vérifie
       (IH) : n' + 0 = n'. *)
    (* Par définition de +, S n' + 0 = S (n' + 0). *)
    simpl.
    (* On remplace, grâce à (IH), le terme (n' + 0) par n'. *)
    rewrite IH.
    (* Les deux membres de l'égalité sont les mêmes, ce cas est terminé. *)
    reflexivity.
Qed.

(** On vient de voir deux autres nouvelles tactiques :

- [induction n] permet de faire une récurrence sur une variable
  d'entier naturel n qui apparait dans une proposition. Elle
  transforme un but [P n] en deux sous-buts : [P 0] et [P n -> P (S
  n)], si (P : nat -> Prop) est une propriétée sur les entiers.

- [rewrite] est une tactique très importante. Elle permet de réécrire
  à l'aide d'une égalité en hypothèse. Si vous avez une hypothèse (H :
  a = b) alors [rewrite H] va transformer tous les [a] dans votre but
  avec un [b]. A l'inverse, [rewrite <- H] va transformer tous les [b]
  en [a] *)

(** Avec les tactiques intros, apply, destruct, simpl, discriminate,
    exfalso, reflexivity, induction et rewrite vous pouvez faire
    énormément de preuves *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem plus_un_egal_suc : ∀ n : nat, n + 1 = S n.
Proof.
  intros n.
  induction n as [|n' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

(** Remarque : le choix de la variable sur laquelle porte l'induction
    est important, il est guidé par la façon dont la fonction [plus] a
    été écrite.  *)

Theorem plus_S : ∀ n m, n + S m = S (n + m).
Proof.
  intros n m.
  induction n as [|n' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(* C'ÉTAIT UN BUG. JE VEUX MOURIR. *)

Theorem plus_assoc : ∀ n m p, n + (m + p) = (n + m) + p.
Proof.
  intros n m p.
  induction n as [|n' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    reflexivity.
Qed.


(** **** Exercice : Prouver le théorème suivant. *)

(* Indice : utiliser les théorèmes précédents avec la tactique
   rewrite. Par exemple, si le but contient une expression du type
   Truc + S Bidule, alors [rewrite plus_S] remplace cette expression
   par S (Truc + Bidule). *)

Theorem plus_commutatif : ∀ (n m : nat), n + m = m + n.
Proof.
  intros n m.
  induction n as [|n' HI].
  -
    simpl.
    rewrite zero_neutre_plus_d.
    reflexivity.
  -
    simpl.
    rewrite plus_S.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** ** Preuve par cas : destruct (encore) *)

(** Parfois, on a certes besoin de séparer les cas entre n = 0 et n = S n' pour
    un certain n', mais on ne se sert pas de l'hypothèse d'induction.
    Dans ce cas, il est très mal vu de dire que c'est une preuve par induction,
    cela embrouille le lecteur et c'est vraiment une grosse faute de rédaction.
    En coq, la tactique destruct sur une variable d'un type inductif permet les
    preuves par cas. *)

Theorem double_eq_0 : ∀ n : nat, n + n = 0 → n = 0.
Proof.
  (* Soit n un entier naturel. *)
  intros n.
  (* On suppose (hyp. (H)) que n + n = 0. *)
  intros H.
  (* Preuve par cas : n est soit nul soit successeur d'un entier n'. *)
  destruct n as [|n'].
  - (* Premier cas : n = 0. *)
    reflexivity.
  - (* Deuxième cas : n = S n' pour un certain n' : nat. *)
    (* Nous allons montrer que ce cas est impossible. *)
    exfalso.
    (* Par définition de +, l'hypothèse H devient S(n' + S n') = 0. *)
    simpl in H.
    (* Or, il est impossible que le successeur d'un naturel soit nul, par
       définition des entiers naturels. *)
    discriminate.
Qed.

(** Remarque : on vient de voir qu'on peut aussi utiliser simpl dans une
    hypothèse à l'aide de la syntaxte simpl in ... *)

(** **** Exercice : Prouver le théorème suivant. *)

(** Indice : pour transformer une expression _dans une hypothèse H_ avec, par
    exemple le théorème plus_S, utiliser
    [rewrite plus_S in H.] *)

Theorem npm_eq_m : ∀ (n m : nat), n + m = 0 → n = 0 ∧ m = 0.
Proof.
  intros n m.
  intros H.
  split.
  -
    destruct n as [|n'].
    *
      reflexivity.
    *
      discriminate.
  -
    destruct m as [|m'].
    *
      reflexivity.
    *
      rewrite plus_S in H.
      discriminate.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** ** La multiplication *)

(** Comme on pourrait s'y attendre, la multiplication est définie
    récursivement : *)
Fixpoint mult (n m : nat) :=
  match n with
  | 0 => 0
  | S p => m + mult p m
  end.

(** **** Exercice papier-crayon :

    Évaluer à la main mult (2 3). Les évaluations de l'addition
    peuvent être faites directement. *)

(** **** Exercice : Prouver le théorème suivant. *)

Theorem un_neutre_mul_g : ∀ n : nat, 1 * n = n.
Proof.
  intros n.
  simpl.
  rewrite zero_neutre_plus_d.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem un_neutre_mul_d : ∀ n : nat, n * 1 = n.
Proof.
  intros n.
  induction n as [|n' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem produit_nul : ∀ (n m : nat), n * m = 0 → n = 0 ∨ m = 0.
Proof.
  intros n m.
  intros H1.
  destruct n as [|n'].
  -
    left.
    reflexivity.
  -
    right.
    destruct m as [|m'].
    reflexivity.
    discriminate.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem nul_produit : ∀ n m : nat,  (n = 0 ∨ m = 0) → n * m = 0.
Proof.
  intros n m.
  intros Hnom.
  destruct Hnom as [Hn | Hm].
  -
    destruct n as [|n'].
    +
      reflexivity.
    +
      discriminate.
  -
    destruct m as [|m'].
    +
      induction n as [|n' HI].
      *
        reflexivity.
      *
        simpl.
        rewrite HI.
        reflexivity.
    +
      induction n as [|n' HI].
      *
        reflexivity.
      *
        simpl.
        rewrite HI.
        rewrite (zero_neutre_plus_d m').
        discriminate.
Qed.
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem mul_distributive : ∀ n m p : nat, (n + m) * p = n * p + m * p.
Proof.
  intros n m p.
  induction (n) as [|n' HI].
  induction (m) as [|m' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    rewrite plus_assoc.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	

(** **** Exercice : Prouver le théorème suivant. *)

Theorem mul_associative : ∀ n m p : nat, (n * m) * p = n * (m * p).
Proof.
  intros n m p.
  induction n as [|n' HI].
  -
    reflexivity.
  -
    simpl.
    rewrite mul_distributive.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)
	


(** Il y a quelque chose qui est parfois (souvent) un peu pénible... coq ne
    peut pas toujours deviner comment appliquer un théorème.
    On rappelle qu'on peut donner des arguments à un théorème pour en faire une
    version spécialisée : *)

Module AppliquerTheoremes.
Check (plus_commutatif 0).
Parameters n m p : nat.
Check (plus_commutatif m). (* Remarquer qu'il a renommé l'ancien m en m0 *)
Check (plus_commutatif (S (S m)) (n + m)).
End AppliquerTheoremes.

(** Ceci permet de préciser à coq sur quel sous-terme utiliser un théorème avec
    rewrite. Par exemple, si le but est
    [(a + b) + c = truc], appliquer simplement
    rewrite plus_commutatif donnera
    [c + (a + b) = truc], tandis que
    rewrite (plus_commutatif a) donnera
    [(b + a) + c = truc].
    *)

(** **** Exercice : Prouver le théorème suivant. *)
	

Theorem mul_S : ∀ n m : nat, n * S m = n + n * m.
Proof.
  intros n m.
  induction m as [|m' HI].
  -
    rewrite un_neutre_mul_d.
    rewrite nul_produit.
    rewrite zero_neutre_plus_d.
    reflexivity.
    right.
    reflexivity.
  -
    simpl.

Qed. (* Remplacer cette ligne par Qed. *)
	

Theorem mul_commutative : ∀ n m : nat, n * m = m * n.
Proof.
  intros n m.
  induction n as [|n' HI].
  -
    simpl.
    rewrite nul_produit.
    reflexivity.
    right.
    reflexivity.
  -
    simpl.
Admitted. (* Remplacer cette ligne par Qed. *)

End EntiersNaturels.
