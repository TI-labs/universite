(** * Initiation aux preuves formelles : TP noté 1
      
      L1 double-licence maths-info

      date : dim. 26 sept. 2021 08:49:57 CEST

      Marie Kerjean, Micaela Mayero, Pierre Rousselin
*)

(** ** Consignes :
    - Attention, avant toute chose, signer la déclaration suivante avec votre
      prénom, votre nom et votre numéro d'étudiant (les deux dans le cas d'un
      binôme) :

      Je déclare qu'il s'agit de mon propre travail.
      Lyes Saadi 12107246

    - C'est un travail personnel. Il ne doit pas y avoir de communication sauf
      au sein d'un binôme éventuel.
    - Vous avez droit à vos notes manuscrites ou numériques (par exemple, une
      fiche sur les tactiques) mais c'est tout (en particulier, vous ne devez
      pas consulter votre code des TP précédents).
    - Le travail est rendu avant la fin du temps imparti sur le dépôt créé à cet
      effet sur moodle.
*)

(** ** Barème : Il y a 10 exercices parmi lesquels vous êtes libres de choisir.
    - Le premier exercice fait correctement rapporte 4 points.
    - Les deux suivant rapportent chacun 3 points.
    - Les trois suivants rapportent chacun 2 points.
    - Les quatre derniers rapportent chacun 1 point.
    Si vous n'avez pas à faire un exercice, concluez par Admitted et passez à un
    autre.
*)

(** Les tactiques autorisées sont:
    - intros
    - apply
    - split
    - destruct
    - exfalso
    - reflexivity
    - rewrite
    - simple
    - induction
    - unfold *)

(**  *)

Require Import Unicode.Utf8.
(** * Logique propositionnelle *)

(** ** Exercices de base *)

(** **** Exercice : Prouver le théorème suivant. *)

Lemma imp_trans : forall P Q R : Prop, (P -> Q) -> (Q -> R) -> (P -> R).
Proof.
  intros P Q R.
  intros HPQ.
  intros HQR.
  intros HP.
  apply HQR.
  apply HPQ.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant. *)

Theorem and_idempotent : ∀ P : Prop, P ∧ P ↔ P.
Proof.
  intros P.
  split.
  -
    intros HPP.
    destruct HPP as [HP _].
    assumption.
  -
    intros HP.
    split.
    assumption.
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant. *)

Theorem modus_tollens : ∀ P Q : Prop,
  (P → Q) ∧ ¬ Q → ¬ P.
Proof.
  intros P Q.
  intros HPQnQ.
  intros HP.
  destruct HPQnQ as [HPQ HnQ].
  apply HnQ.
  apply HPQ.
  assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** ** Exercices plus difficiles *)

(** **** Exercice : Prouver le théorème suivant. *)

Lemma q_eq_q_and_p : forall P Q : Prop,  (Q <-> P /\ Q) <-> (Q -> P). 
Proof.
  intros P Q.
  split.
  -
    intros HQPQ.
    intros HQ.
    destruct HQPQ as [H1 _].
    apply H1 in HQ.
    destruct HQ as [HP _].
    assumption.
  -
    intros HQP.
    split.
    +
      intros HQ.
      apply HQP in HQ as HP.
      split.
      assumption.
      assumption.
    +
      intros HPQ.
      destruct HPQ as [HP HQ].
      assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Un théorème de logique classique : Peirce *)

(** Dans cet exercice, et uniquement celui-ci, on peut utiliser
   le tiers-exclus. *)
Axiom excluded_middle : ∀ P : Prop, P ∨ ¬ P.

(** Prouver le théorème suivant : *)
Theorem peirce : ∀ P Q : Prop,
  ((P → Q) → P) → P.
Proof.
  intros P Q.
  intros HPQP.
  destruct (excluded_middle P) as [HP | HnP].
  -
    assumption.
  -
    apply HPQP.
    intros HP.
    destruct HnP.
    assumption.
Qed. (* Remplacer cette ligne par Qed. *)


(** ** Entiers naturels *)

(** *** Déjà vus en cours. Savez-vous les refaire rapidement ? *)

(** **** Exercice : Prouver le théorème suivant. *)

Lemma n_plus_zero: forall n, n + 0 = n.
Proof.
  intros n.
  induction n as [|n' HI].
  reflexivity.
  simpl.
  rewrite HI.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant. *)

Lemma n_plus_succ : forall  n m : nat, n + S m = S (n + m).
Proof.
  intros n m.
  induction n as [|n' HI].
  -
    simpl.
    reflexivity.
  -
    simpl.
    rewrite HI.
    reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)


(** *** Simplifier à gauche ou à droite dans une addition *)

(** Le lemme suivant est considéré comme admis, vous pouvez l'utiliser
    dans les preuves ultérieures *)
Lemma S_inj : forall n m : nat, S n = S m -> n = m.
Proof.
  intros n m H.
  injection H.
  intros H'. assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant. *)

Lemma pn_pm : forall n m p : nat, p + n = p + m -> n = m.
Proof.
  intros n m p.
  intros H.
  induction p as [|p' HI].
  -
    simpl in H.
    apply H.
  -
    apply HI.
    simpl in H.
    rewrite (S_inj (p' + n) (p' + m)).
    +
      reflexivity.
    +
      apply H.
Qed. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant.

    Vous avez le choix : une preuve assez directe mais un peu longuette
    par induction ou bien énoncer et prouver un théorème (vu en cours) d'abord
    pour écrire une preuve très courte. *)

Lemma plus_commutatif : forall n m : nat, n + m = m + n.
Proof.
  intros n m.
  induction m as [|m' HI].
  simpl.
  rewrite n_plus_zero.
  reflexivity.
  simpl.
  rewrite <- HI.
  rewrite n_plus_succ.
  reflexivity.
Qed.

Lemma np_mp : forall n m p : nat, n + p = m + p -> n = m.
Proof.
  intros n m p.
  rewrite plus_commutatif.
  rewrite (plus_commutatif m p).
  apply (pn_pm n m p).
Qed. (* Remplacer cette ligne par Qed. *)


(** *** Une définition alternative de [+] *)

(** On aurait pu définir l'addition de la façon suivante. *)

Fixpoint plus' (n m : nat) :=
  match n with
    0 => m
  | S n' => plus' n' (S m)
  end.

Notation "x +' y" := (plus' x y)
                       (at level 50, left associativity)
                       : nat_scope.
Compute (3 +' 8).

(** Pour mémoire, voici l'addition telle que définie en coq : il y a une petite
    différence. *)
Print Nat.add.

(** **** Exercice : Montrer que les deux définitions aboutissent au même résultat en prouvant le théorème suivant. *)

Theorem plus_eq_plus' : ∀ (n m : nat), n +' m = n + m.
Proof.
  induction n as [|n' HI].
  simpl.
  reflexivity.
  simpl.
  intros m.
  rewrite HI.
  rewrite n_plus_succ.
  reflexivity.
Qed. (* Remplacer cette ligne par Qed. *)

