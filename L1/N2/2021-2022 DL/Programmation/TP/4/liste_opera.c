#include "liste_opera.h"
#include <stdlib.h>
#include <stdio.h>

/* Pour la fonction de chargement d'un fichier dans une liste d'opéras */
/* (voir à la fin du fichier) */
#define TAILLE_MAX 100

/** Fonction qui crée un maillon sans successeur */
/* et l'initialise avec les champs de son argument */
/* La fonction renvoie un pointeur vers ce maillon ainsi créé et initialisé */
/** N.B. Il faut éviter que deux objets du programme partagent le même espace mémoire */
maillon * creer_maillon () {
	maillon * res = malloc(sizeof(maillon));
	res->valeur = creer_opera ();
	res->suivant = NULL;
	return res;
}

void initialiser_valeur_maillon (maillon * m, const opera * op) {
	initialiser_opera (m->valeur, op->titre, op->date_creation, op->ville_creation, op->compositeur);
}

/** Fonction qui libère tout l'espace mémoire occupé par le maillon pointé par m */
void detruire_maillon (maillon * m) {
	detruire_opera (m->valeur);
	free(m);
}

/** Fonction qui crée une liste, l'initialise à une liste vide */
/* et renvoie un pointeur vers la liste ainsi créée et initialisée */
liste * initialiser_liste_vide () {
  liste * res = malloc(sizeof(liste));
  res->debut = NULL;
  res->taille = 0;
  return res;
}

/** Fonction qui renvoie 1 si la liste l est vide et 0 sinon */
int test_liste_vide (const liste *l) {
  if(l->taille == 0)
    return 1;
  return 0;
}

/** Fonction qui ajoute le maillon pointé par m au début de la liste pointée par l */
void ajouter_maillon_debut_liste (liste * l, maillon * m) {
  m->suivant = l->debut;
  l->debut = m;
  l->taille ++;
}

/** Fonction qui renvoie un pointeur vers le maillon qui occupe la position pos */
/* dans la liste pointée par l */
/** Le premier maillon de la liste l occupe la position 0 */
/** Elle renvoie NULL si pos est plus grand que la position du dernier maillon de la liste l */
maillon * acceder_liste (const liste * l, int pos) {
	int i;
	maillon* m = l->debut;

	for (i = 0; (i != pos) || (m->suivant != NULL); i++) {
		m = m->suivant;
	}

	if (i == pos)
		return m;
	return NULL;
}


/** Fonction qui extrait le premier maillon de la liste pointée par l */
/* et renvoie un pointeur vers le maillon extrait */
maillon * extraire_maillon_debut_liste (liste * l) {
  maillon * res = l->debut;
  if (res != NULL) {
    l->debut = res->suivant;
    l->taille--;
    res->suivant = NULL;
  }
  return res;
}

/** Fonction qui affiche la valeur de chaque maillon de la liste pointée par l */
/* en respectant l'ordre des maillons */
void afficher_liste (const liste * l) {
	maillon * m;
	for (m = l->debut; m != NULL; m = m->suivant) {
		afficher_opera(m->valeur);
	}
	printf("\n");
}

/** Fonction qui libère tout l'espace occupé par la liste pointée par l */
/* en extrayant puis détruisant itérativement le premier maillon jusqu'à vidage de la liste */
/* avant de détruire l'espace occupé par la liste elle-même */
void detruire_liste (liste * l) {
  while (!test_liste_vide (l))
    detruire_maillon(extraire_maillon_debut_liste(l));
  free(l);
}

/** Fonction qui inverse l'ordre des maillons de la liste pointée par l */
void renverser_liste (liste * l) {
	int i;
	liste* new_l = initialiser_liste_vide();

	maillon* m = new_l->debut;
	for (i = 0; (i < l->taille) || (m != NULL); i++) {
		ajouter_maillon_debut_liste(new_l, m);
	}

	free(l);
	l = new_l;
}

/** Fonction qui trie la liste pointée par l */
/* selon lexicographique des titres des opéras */
void trier_liste_titre (liste * l) {

	/** CORPS DE LA FONCTION À COMPLÉTER **/
}

/** Fonction qui supprime de la liste pointée par l */
/* les opéras qui n'ont pas été créés au i_ème siècle */
void filtrer_liste_siecle (liste * l, int i) {

	/** CORPS DE LA FONCTION À COMPLÉTER **/
}

/***********************************************************/
/**    FONCTIONS DE CHARGEMENT ET DE SAUVEGARDE           **/
/***********************************************************/

/** Fonction qui charge le contenu du fichier chemin dans la liste d'opéras l **/
liste * charger_liste_opera (const char * chemin) {
	FILE * f ; /* flux de données */
	int i, nbre_opera;
	liste * l; /* liste d'opéras destinataire du chargement */
	maillon * m; /* maillon qui sera ajouté en tête de liste lors du chargement */
	opera * op; /* opéra-tampon pour le chargement */

	/* Ouverture du fichier en lecture seule */
	if ((f = fopen(chemin, "r")) == NULL) {
		perror ("Fichier inexistant");
		exit(2);
	}

	/* Initialisation de la liste destinataire du chargement */
	l = initialiser_liste_vide ();
	/* Création de l'opéra-tampon pour le chargement : */
	/* on suppose que les titres, villes de création et noms de compositeur */
	/* des opéras du fichier ENTREE ont moins de TAILLE_MAX caractères */
	op = creer_opera ();
	op->titre = malloc(TAILLE_MAX*sizeof(char));
	if (op->titre == NULL) {
		perror("Échec allocation titre tmp");
		exit(2);
	}
	op->date_creation = creer_date(0,0,0);
	op->ville_creation = malloc(TAILLE_MAX*sizeof(char));
	if (op->ville_creation == NULL) {
		perror("Échec allocation ville création tmp");
		exit(2);
	}
	op->compositeur = malloc(sizeof(individu));
	if (op->compositeur == NULL) {
		perror("Échec allocation ville création tmp");
		exit(2);
	}
	op->compositeur->nom = malloc(TAILLE_MAX*sizeof(char));
	if (op->compositeur->nom == NULL) {
		perror("Échec allocation nom tmp");
		exit(2);
	}
	op->compositeur->prenom = malloc(TAILLE_MAX*sizeof(char));
	if (op->compositeur->prenom == NULL) {
		perror("Échec allocation prenom tmp");
		exit(2);
	}
	op->compositeur->naissance = creer_date(0,0,0);

	/* Lecture de l'entête du fichier et récupération du nombre d'opéras */
	fscanf(f,"%d \n",&nbre_opera);
	/* Récupération des opéras stockés dans le fichier */
	for (i = 0; i < nbre_opera; i++) {
		fscanf(f,"%s\n", op->titre);
		fscanf(f,"%u/%u/%u %s\n", &op->date_creation->jour, &op->date_creation->mois, &op->date_creation->annee, op->ville_creation);
		fscanf(f,"%s %s (%u/%u/%u)\n", op->compositeur->prenom, op->compositeur->nom, &op->compositeur->naissance->jour, &op->compositeur->naissance->mois,&op->compositeur->naissance->annee);
		m = creer_maillon();
		initialiser_valeur_maillon (m, op);
		ajouter_maillon_debut_liste (l,m);
	}
	/* Libération de l'espace occupé par l'"opéra-tampon" */
	detruire_opera (op);
	/* Fermeture du fichier */
	fclose(f);
	/* Renversement de la liste pour respecter l'ordre des opéras dans le fichier */
	renverser_liste (l);

	return l;
}

/** Fonction qui sauvegarde le contenu de la liste d'opéras l dans le fichier chemin **/
void sauvegarder_liste_opera (const char * chemin, liste * l) {
	FILE * f ; /* flux de données */
	maillon * m;

	/* Ouverture du fichier en écriture seule */
	if ((f = fopen(chemin, "w")) == NULL) {
		perror ("Écriture dans le fichier impossible");
		exit(2);
	}
	/* Écriture de l'entête du fichier */
	fprintf(f, "%d\n", l->taille);
	/* Sauvegarde des opéras en respectant le format du fichier d'entrée */
	/* et destruction des maillons sauvegardés */
	while (!test_liste_vide(l)) {
			m = extraire_maillon_debut_liste(l);
			fprintf(f, "%s\n", m->valeur->titre);
			fprintf(f, "%u/%u/%u %s\n", m->valeur->date_creation->jour, m->valeur->date_creation->mois, m->valeur->date_creation->annee, m->valeur->ville_creation);
			fprintf(f, "%s %s (%u/%u/%u)\n", m->valeur->compositeur->prenom, m->valeur->compositeur->nom, m->valeur->compositeur->naissance->jour, m->valeur->compositeur->naissance->mois, m->valeur->compositeur->naissance->annee);
			detruire_maillon(m);
	}
}
