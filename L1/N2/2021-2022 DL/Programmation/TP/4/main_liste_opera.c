#include <stdio.h>
#include <stdlib.h>
#include "liste_opera.h"

#define ENTREE argv[1] /* Nom de fichier passé en ligne de commande comme premier argument de l'exécutable */
#define SORTIE argv[2] /* Nom de fichier passé en ligne de commande comme deuxième argument de l'exécutable */

void usage_liste_opera (char *);

/** Le programme attend deux arguments (passés à l'exécutable en ligne de commande): */
/* - le premier argument est le nom du fichier ouvert en lecture pour initialiser la liste chaînée d'opéras*/
/* - le deuxième argument est le nom du fichier ouvert en écriture pour sauvegarder la liste d'opéras */
int main(int argc, char **argv) {
  liste * l;
  maillon * m;

  if (argc != 3) {
    fprintf(stderr, "Erreur sur le nombre d'arguments ! Recommencez.\n");
    usage_liste_opera (argv[0]);
    return EXIT_FAILURE;
  }

  /** Initialisation de la liste d'opéras à l'aide du fichier ENTREE **/
  l = charger_liste_opera (ENTREE);

  /* Affichage de la liste ainsi initialisée */
  printf("\nListe extraite du fichier %s (%d opéras):\n", ENTREE, l->taille);
  afficher_liste (l);
  /* Affichage de l'opéra occupant la position 7 dans la liste */
  /** À "DÉCOMMENTER" UNE FOIS LA FONCTION acceder_liste ÉCRITE
  m = acceder_liste (l, 7);
  printf("Opéra occupant la position 7 dans la liste :\n");
  afficher_opera (m->valeur);
  printf("\n"); **/
  /* Renversement et affichage de la liste */
  /** À "DÉCOMMENTER" UNE FOIS LA FONCTION renverser_liste ÉCRITE
  renverser_liste (l);
  printf("Liste renversée : \n");
  afficher_liste (l); **/
  /* Tri de la liste selon l'ordre lexicographique des titres et affichage */
  /** À "DÉCOMMENTER" UNE FOIS LA FONCTION trier_liste_titre ÉCRITE
  trier_liste_titre (l);
  printf("Liste triée selon l'ordre lexicographique des titres : \n");
  afficher_liste (l); **/
  /* Filtrage de la liste ne retenant que les opéras créés au 19e siècle et affichage */
  /** À "DÉCOMMENTER" UNE FOIS LA FONCTION filtrer_liste_siecle ÉCRITE
  filtrer_liste_siecle (l, 19);
  printf("Liste des opéras créés au 19e siècle (%d opéras): \n", l->taille);
  afficher_liste (l); **/

  /** sauvegarde du dernier état de la liste dans le fichier SORTIE **/
  sauvegarder_liste_opera (SORTIE, l);

  /* Libération de l'espace mémoire alloué sur le tas */
  printf("\nLibération de la mémoire allouée sur le tas...\nAu revoir !\n");
  detruire_liste (l);

  return EXIT_SUCCESS;
}

void usage_liste_opera (char * s) {
	  fprintf(stderr, "%s fichier_entree fichier_sortie \n", s);
}
