#include <asm-generic/errno-base.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct polynome {
	int *coefficients;
	int taille;
	int degre;
};

/* Retourner l'adresse d'un polynome de taille taille (ou 0 si taille <= 0) égal
 * au polynome nul.
 * Resp. de l'appelant : appeler detruire_polynome sur cette adresse. */
struct polynome *creer_polynome(int taille);

/* Détruire le polynome d'adresse donnée en argument. */
void detruire_polynome(struct polynome *p);

/* Mettre la taille du polynome à degre + 1 */
int reformater_polynome(struct polynome *p);

/* Retourner l'adresse d'un polynome égal à p fois le scalaire a.
 * Resp. de l'appelant : appeler detruire_polynome sur cette adresse. */
struct polynome *scalaire_polynome(const struct polynome *p, int a);

/* Retourner l'adresse d'un polynome égal à p fois q.
 * Resp. de l'appelant : appeler detruire_polynome sur cette adresse. */
struct polynome *multiplier_polynome(const struct polynome *p, const struct polynome *q);

/* Retourner l'adresse d'un polynome égal au polynôme dérivé de p.
 * Resp. de l'appelant : appeler detruire_polynome sur cette adresse. */
struct polynome *deriver_polynome(const struct polynome *p);

/* Retourner l'adresse d'un polynome égal à p + q.
 * Resp. de l'appelant : appeler detruire_polynome sur cette adresse. */
struct polynome *additionner_polynome(const struct polynome *p, const struct polynome *q);

/* Afficher sur le terminal le polynome p. */
void afficher_polynome(const struct polynome *p);

int main()
{
	struct polynome *p, *q,
			*mult_scalaire, *addition, *multiplication, *p_prime;

	p = creer_polynome(10);
	q = creer_polynome(10);
	p->coefficients[0] = 1;
	p->coefficients[1] = 3;
	p->coefficients[2] = 1;
	p->degre = 2;
	q->coefficients[0] = 2;
	q->coefficients[1] = 4;
	q->coefficients[2] = -1;
	q->degre = 2;
	printf("Polynôme p : ");
	afficher_polynome(p);
	printf("Polynôme q : ");
	afficher_polynome(q);

	mult_scalaire = scalaire_polynome(p, -3);
	printf("multiplication de p par - 3 : ");
	afficher_polynome(mult_scalaire);

	addition = additionner_polynome(p, q);
	printf("addition de p et q : ");
	afficher_polynome(addition);

	multiplication = multiplier_polynome(p, q);
	printf("multiplication de p par q : ");
	afficher_polynome(multiplication);

	p_prime = deriver_polynome(p);
	printf("polynôme dérivé de p : ");
	afficher_polynome(p_prime);

	detruire_polynome(p);
	detruire_polynome(q);
	detruire_polynome(mult_scalaire);
	detruire_polynome(addition);
	detruire_polynome(multiplication);
	detruire_polynome(p_prime);
	return 0;
}

struct polynome *creer_polynome(int taille)
{
	struct polynome* p = malloc(sizeof(struct polynome));
	p->taille = taille > 0 ? taille : 0;
	p->degre = -1;
	p->coefficients = malloc(p->taille * sizeof(int));

	if (taille != 0 && p->coefficients == NULL) {
		perror("VA TE FAIRE FOUTRE MERDE ABSOLUE. TU NE REGARDE MÊME PAS SI TA MÉMOIRE EST SATURÉE AVANT DE FAIRE N'IMPORTE ??? CASSE-TOI CONNARD.");
		exit(EXIT_FAILURE);
	}

	return p;
}

struct polynome *creer_polynome_vide(int taille)
{
	struct polynome* p = creer_polynome(taille);
	p->degre = taille - 1;

	for (int i = 0; i < p->taille; i++) {
		p->coefficients[i] = 0;
	}

	return p;
}

void detruire_polynome(struct polynome *p)
{
	free(p->coefficients);
	free(p);
}

/* Première version utilisant malloc et free */
int reformater_polynome(struct polynome *p)
{
	p->taille = p->degre + 1;
	p->coefficients = realloc(p->coefficients, p->taille * sizeof(int));
	return 0;
}

struct polynome *scalaire_polynome(const struct polynome *p, int a)
{
	struct polynome* q = creer_polynome(p->taille);
	q->degre = p->degre;

	for (int i = 0; i < q->taille; i++) {
		q->coefficients[i] = p->coefficients[i] * a;
	}

	return q;
}

int max(int x, int y) {
	return x > y ? x : y;
}

int min(int x, int y) {
	return x < y ? x : y;
}

struct polynome *additionner_polynome(const struct polynome *p, const struct polynome *q)
{
	int i = 0;

	struct polynome* r = creer_polynome(max(p->taille, q->taille));
	r->degre = max(p->degre, q->degre);

	const struct polynome* m = p->taille > q->taille ? p : q;

	for (; i < min(p->taille, q->taille); i++) {
		r->coefficients[i] = p->coefficients[i] + q->coefficients[i];
	}

	for (; i < max(p->taille, q->taille); i++) {
		r->coefficients[i] = m->coefficients[i];
	}

	for (i = r->degre; i >= 0; i--)
		if (r->coefficients[i] == 0)
			r->degre--;
		else
			break;

	reformater_polynome(r);

	return r;
}

struct polynome *multiplier_polynome(const struct polynome *p, const struct polynome *q)
{
	int i, j;

	struct polynome* r = creer_polynome(p->degre + q->degre + 1);
	r->degre = p->degre + q->degre;

	for (i = 0; i < r->taille; i++) {
		r->coefficients[i] = 0;
	}

	for (i = 0; i < p->taille; i++)
		for (j = 0; j < q->taille; j++)
			r->coefficients[i + j] += p->coefficients[i] * q->coefficients[j];

	return r;
}

struct polynome *deriver_polynome(const struct polynome *p)
{
	struct polynome* r = creer_polynome(p->taille - 1);
	r->degre = p->degre - 1;

	for (int i = 1; i < r->taille; i++) {
		r->coefficients[i - 1] = p->coefficients[i] * i;
	}

	return r;
}

void diviser_polynome(struct polynome *a, struct polynome *b, struct polynome *q, struct polynome *r) {
	if (!(a->degre - b->degre < 0)) {
		return;
	}

	int n = a->degre - b->degre;
	struct polynome* p = creer_polynome_vide(n + 1);

	p->coefficients[n] = a->coefficients[a->degre] / b->coefficients[b->degre];

	struct polynome* ap = additionner_polynome(a, scalaire_polynome(multiplier_polynome(b, p), - 1));
}

void afficher_polynome(const struct polynome *p)
{
	int i;
	if (p == NULL) {
		printf("NULL");
		return;
	}
	if (p->degre < 0) {
		printf("0");
		return;
	}
	for (i = p->degre; i >= 0; --i) {
		printf("%d", p->coefficients[i]);
		if (i > 0) {
			printf("X");
			if (i > 1)
				printf("^%d", i);
			printf(" + ");
		}
	}
	printf ("\n");
}
