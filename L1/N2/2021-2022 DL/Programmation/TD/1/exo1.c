#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

struct date {
	unsigned jour;
	unsigned mois;
	int annee;
};

int date_comparer(struct date d1, struct date d2);
int bissextile(int annee);
int nbre_jours_mois(int annee, unsigned mois);
struct date jour_suivant(struct date d);
int nbre_jours_int(struct date d1, struct date d2);
int nbre_jours_rec(struct date d1, struct date d2, unsigned step);
bool date_valide(struct date d);

int main() {
    unsigned jour, mois, annee;
    struct date d1, d2;
    
    do {
        printf("Insérer la date d1 (jj/mm/aaaa) : ");
        scanf("%d/%d/%d", &jour, &mois, &annee);
        d1.jour = jour; d1.mois = mois; d1.annee = annee;
    } while (!date_valide(d1));

    do {
        printf("Insérer la date d2 (jj/mm/aaaa) : ");
        scanf("%d/%d/%d", &jour, &mois, &annee);
        d2.jour = jour; d2.mois = mois; d2.annee = annee;
    } while (!date_valide(d2));
    
    printf("Entre le %d/%d/%d et %d/%d/%d, il y a %d jours.\n",
           d1.jour, d1.mois, d1.annee,
           d2.jour, d2.mois, d2.annee,
           nbre_jours_rec(d1, d2, 0));
    
	return EXIT_SUCCESS;
}

int date_comparer(struct date d1, struct date d2) {
	int d1sum, d2sum, sus;	

	d1sum = d1.jour + d1.mois * 100 + d1.annee * 10000;
	d2sum = d2.jour + d2.mois * 100 + d2.annee * 10000;

	sus = d1sum - d2sum;

	return (sus > 0) - (sus < 0);
}

int bissextile(int annee) {
	if ((annee % 400 == 0) || ((annee % 4 == 0) && (annee % 100 != 0))) return 1;
	return 0;
}

int nbre_jours_mois(int annee, unsigned mois) {
	int liste_mois[12] = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	if (mois != 2) return liste_mois[mois - 1];

	if (bissextile(annee)) return 29;

	return 28;
}

struct date jour_suivant(struct date s) {
	s.jour += 1;

	if (s.jour > nbre_jours_mois(s.annee, s.mois)) {
		s.jour = 1;
		s.mois += 1;
	}

	if (s.mois > 12) {
		s.mois = 1;
		s.annee += 1;
	}

	return s;
}

int nbre_jours_int(struct date d1, struct date d2) {
	if (date_comparer(d1, d2) != -1) {
		return 0;
	}

	int i = 0;

	do {
		d1 = jour_suivant(d1);
		i++;
	} while (date_comparer(d1, d2) != 0);

	return i;
}

int nbre_jours_rec(struct date d1, struct date d2, unsigned step) {
    if (date_comparer(d1, d2) >= 0)
        return step;
    return nbre_jours_rec(jour_suivant(d1), d2, 0);
}

bool date_valide(struct date d) {
    if (d.annee >= 3000)
        return true;
    
    if (d.annee < 1583)
        return false;
    
    if (d.mois > 12)
        return false;
    
    if (d.mois < 1)
        return false;
    
    if (d.jour < 0)
        return false;
    
    if (d.jour > nbre_jours_mois(d.annee, d.mois))
        return false;
    
    return true;
}
