#include <stdlib.h>
#include <stdio.h>

static char expr[] = "+7**46+89";
static int i = 0;

int mystere() {
	if (expr[i] == '+') {
		i++;
		return mystere() + mystere();
	} else if (expr[i] == '*') {
		i++;
		return mystere() * mystere();
	} else if ((expr[i] >= '0') && (expr[i] <= '9'))
		return expr[i++] - '0';
	exit(1);
}

int main() {
	printf("%d\n", mystere());
	return 0;
}
