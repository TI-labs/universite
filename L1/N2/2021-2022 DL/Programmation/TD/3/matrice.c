#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct matrice_t {
	unsigned nb_lignes, nb_colonnes;
	int coeff[10][10];
} matrice;

void initialiser_alea_mat(matrice*);
void afficher_mat(matrice);
void transposer_mat(matrice*, matrice);
void mineur_mat(matrice*);

int main() {
	matrice m, m_t;
	srand(time(NULL));
	initialiser_alea_mat(&m);
	transposer_mat(&m_t, m);
	mineur_mat(&m_t);
	afficher_mat(m_t);
	return 0;
}


void initialiser_alea_mat(matrice* m) {
	int i, j;
	m->nb_colonnes = 5 + rand() % 6;
	m->nb_lignes = 5 + rand() % 6;

	for (i = 0; i < m->nb_lignes; ++i) {
		for (j = 0; j < m->nb_colonnes; ++j) {
			m->coeff[i][j] = rand() % 10; // *( *(m->coeff + i) + j) = rand() % 10;
		}
	}
}

void afficher_mat(matrice m) {
	int i, j;

	for (i = 0; i < m.nb_lignes; ++i) {
		for (j = 0; j < m.nb_colonnes; ++j) {
			printf("mat[%d][%d] = %d\n", i, j, m.coeff[i][j]);
		}
	}
}

void transposer_mat(matrice* m_t, matrice m) {
	int i, j;

	m_t->nb_colonnes = m.nb_lignes;
	m_t->nb_lignes = m.nb_colonnes;

	for (i = 0; i < m.nb_colonnes; ++i) {
		for (j = 0; j < m.nb_colonnes; ++j) {
			m_t->coeff[j][i] = m.coeff[i][j];
		}
	}
}
