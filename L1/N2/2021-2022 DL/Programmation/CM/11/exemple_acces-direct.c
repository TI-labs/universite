#include <stdio.h>
#include <stdlib.h>
int main(void){
	FILE *f;
	int *tab;
	int i;

	tab = malloc(sizeof(int)*100);
	for (i = 0 ; i < 100; i++)
		tab[i] = i;
	/* Ecriture du tableau dans le fichier "fichier_test" */
	if ((f = fopen("fichier_test", "w")) == NULL) {
		printf("Impossible d'ecrire dans le fichier %s\n","fichier_test");
		return EXIT_FAILURE;
	}
	fwrite(tab, sizeof(int), 100, f);
	fclose(f);
	/* lecture dans le fichier "fichier_test" */
	if ((f = fopen("fichier_test", "r")) == NULL)	{
		printf("Impossible de lire dans le fichier %s\n","fichier_test");
		return EXIT_FAILURE;
	}
	/* on se positionne a la fin du fichier */
	fseek(f, 0, SEEK_END);
	printf("position %ld", ftell(f));
	/* deplacement de 20 entiers (80 octets) en arriere */
	fseek(f, sizeof(int)*(-20), SEEK_END);
	printf("\n position %ld", ftell(f));
	fread(&i, sizeof(i), 1, f);
	printf("\t i = %d", i);
	/* retour au debut du fichier */
	fseek(f, 0, SEEK_SET);
	printf("\n position %ld", ftell(f));
	fread(&i, sizeof(i), 1, f);
	printf("\t i = %d", i);
	/* deplacement de 10 entiers (40 octets) en avant */
	fseek(f, sizeof(int)*10, SEEK_CUR);
	printf("\n position %ld", ftell(f));
	fread(&i, sizeof(i), 1, f);
	printf("\t i = %d\n", i);
	fclose(f);

	return EXIT_SUCCESS;
}
