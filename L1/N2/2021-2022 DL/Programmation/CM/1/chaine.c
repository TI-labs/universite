#include <stdio.h>

int main() {
	char s1[4] = "oui";
	char s2[5] = {'n', 'o', 'n', '\0'};

	printf("%c %c %c %d %d %c %c %c %c\n", s2[0], s2[1], s2[2], s2[3], s2[4], s2[5], s2[6], s2[7], s2[8]);

	for (int i = 9; i != 0; i++) {
		printf("%c", s2[i]);
	}

	return 0;
}
