#include <stdlib.h>
#include <stdio.h>

void trier_par_selection(double *t, int taille);
int recherche_par_min(double *t, int n);
void echanger_double(double *x, double *y);

int main() {
	return EXIT_SUCCESS;
}

void trier_par_selection(double *t, int taille) {
	int i, pos_min;
	for (i = 0; i < taille - 1; i++) {
		pos_min = recherche_par_min(t + i, taille - i);
		echanger_double(&t[i], &t[i + pos_min]);
	}
}

/**
 * Renvoie la position du plus petit élément dans le tableau t contenant n éléments
 */
int recherche_par_min(double *t, int n) {
	int i, pos_min;

	for (i = 1; i < n; i++) {
		if (t[i] < t[pos_min]) {
			pos_min = i;
		}
	}

	return pos_min;
}

void echanger_double(double *x, double *y) {
	double tmp = *x;
	*x = *y;
	*y = tmp;
}
