#include <stdio.h> /* pour printf, perror */
#include <stdlib.h> /* pour EXIT_SUCCESS, EXIT_FAILURE, exit, atoi */
                    /* rand, srand, RAND_MAX */
                    /* malloc, free, NULL */
#include <time.h> /* pour time */

void echanger_double (double *, double *);
int recherche_pos_min (double *, int);
void trier_par_selection (double *, int);

int main (int argc, char * argv[]) {
  int i;
  /* le programme reçoit comme argument sur la ligne de commande */
  /* la taille du tableau à initialiser et à trier */
  int taille = atoi (argv[1]);
  /* allocation de mémoire sur le tas pour le tableau */
  double *tab = malloc (sizeof (double)*taille);
  if (tab == NULL) {
    perror ("Echec allocation");
    exit (EXIT_FAILURE);
  }
  /* germe pour le générateur pseudo-aléatoire */
  srand (time (NULL));
  /* initialisation des clés du tableau à des valeurs dans l'intervalle [0,1] */
  for (i = 0; i < taille; i++)
    tab[i] = rand () / (double) RAND_MAX;
  /* affichage du tableau avant le tri */
  for (i = 0; i < taille; i++)
      printf ("%lf ", tab[i]);
  printf("\n");
  /* appel du tri */
  trier_par_selection (tab, taille);
  /* affichage du tableau après le tri */
  printf ("-------------\n");
  for (i = 0; i < taille; i++)
    printf ("%lf ", tab[i]);
  printf("\n");
  /* libération de l'espace mémoire alloué sur le tas */
  free (tab);
  return EXIT_SUCCESS;
}

/** Fonction qui trie selon l'ordre croissant un tableau t de doubles, */
/* dont l'adresse est reçue comme comme premier argument */
/* et dont la taille est reçue comme deuxième argument */
/** Le nombre total de comparaisons de clés est : */
/* (taille-1) + (taille-2) + ... + 2 + 1 = taille(taille-1)/2, soit un O(taille^2) */
 void trier_par_selection (double *t, int taille) {
  int i, pos_min;
  /* après l'itération i de la boucle suivante, */
  /* le tableau est trié jusqu'à l'indice i inclus */
  for (i = 0; i < taille - 1; i++) {
    pos_min = recherche_pos_min (t + i, taille - i);
    echanger_double (&t[i], &t[i + pos_min]);
  }
}

/** Fonction qui renvoie l'indice du plus petit élément d'un tableau t de doubles */
/* dont l'adresse est reçue comme premier argument */
/* et la taille n est reçue comme deuxième argument */
/** La fonction balaie le tableau des indices 1 à n ==> n-1 comparaisons de clés */
int recherche_pos_min (double *t, int n) {
  int i, pos_min = 0;
  for (i = 1; i < n; i++)
    if (t[i] < t[pos_min])
      pos_min = i;
  return pos_min;
}

/* Fonction qui échange les valeurs de deux doubles */
/* dont les adresses sont reçues comme premier et deuxième arguments */
void echanger_double (double *x, double *y) {
  double tmp = *x;
  *x = *y;
  *y = tmp;
}
