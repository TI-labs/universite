/* fichier somme_tableau.c */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

float somme_tab (float * tab, unsigned n);
float somme_tab_bis (float * tab, unsigned n); /* variante de la précédente */
float somme_tab_term (float * tab, unsigned n, float y);
float somme_tab_term_bis (float * tab, unsigned n, float y); /* variante de lal précédente */
float somme_tab_iter (float * tab, unsigned n);
void usage_somme (char * s);

/** Fonction principale */
/* (non demandée) */
/* Cette fonction :
  - initialise et affiche le tableau de n flottants (n est l'argument de l'exécutable sur la ligne de commande) ;
  - affiche la somme des n premières valeurs flottantes stockées dans le tableau
    (pour chacune des fonctions implémentant ce calcul). */
int main (int argc, char ** argv) {

  unsigned n, i;
  float * tab; /* tableau de flottants qui sera initialisé ci-dessous */

  /* Affichage d'un message d'erreur si l'utilisateur se trompe dans le nombre d'arguments */
  /* sur la ligne de commande */
  if (argc != 2) {
    fprintf (stderr, "Nombre d'arguments erroné !\n");
    usage_somme (argv[0]);
    return EXIT_FAILURE;
  }

  /* Récupération dans la variable n de l'entier passé en argument de l'exécutable sur le ligne de commande */
  n = strtoul(argv[1], NULL, 10);

  /* Allocation de mémoire sur le tas pour le tableau tab */
  tab = malloc (sizeof(float)*n);
  if (tab == NULL) {
    perror("Échec de l'allocation");
    exit(2);
  }

  /* Initialisation pseudo-aléatoire et affichage du tableau */
  srand(time(NULL));
  for (i = 0; i < n; i ++)
    printf ("%f ", tab[i] = rand()/ (float) RAND_MAX);
  printf ("\n");

  /* Affichage de la somme des clés */
  printf ("Somme (rec) = %f\n", somme_tab (tab, n));
  printf ("Somme (rec variante) = %f\n", somme_tab_bis (tab, n));
  printf ("Somme (rec term) = %f\n", somme_tab_term (tab, n, 0));
  printf ("Somme (rec term variante) = %f\n", somme_tab_term_bis (tab, n, 0));
  printf ("Somme (iter) = %f\n", somme_tab_iter (tab, n));

  return EXIT_SUCCESS;
}

/******************************/
/****     Question 2.a     ****/
/******************************/
/** Fonction récursive non terminale qui renvoie la somme des n premières clés de tab */
float somme_tab (float * tab, unsigned n) {
  if (n == 0) return 0;
  return tab[n-1] + somme_tab (tab, n-1);
}

/** Question 2.a (variante) */
float somme_tab_bis (float * tab, unsigned n) {
  if (n == 0) return 0;
  return tab[0] + somme_tab_bis (tab + 1, n-1);
}


/******************************/
/****     Question 2.b     ****/
/******************************/
/** Fonction récursive terminale qui renvoie la somme des n premières clés de tab */
float somme_tab_term (float * tab, unsigned n, float y) {
  if (n == 0) return y;
  return somme_tab_term (tab, n-1, y + tab[n-1]);
}

/** Question 2.b (variante) */
float somme_tab_term_bis (float * tab, unsigned n, float y) {
  if (n == 0) return y;
  return somme_tab_term_bis (tab + 1, n-1, y + tab[0]);
}

/** Fonction itérative qui renvoie la somme des n premières clés de tab */
/* (non demandée par l'énoncé) */
float somme_tab_iter (float * tab, unsigned n) {
  float res = 0.0;
  for (; n > 0; n --)
    res += tab[n-1];
  return res;
}

/** Fonction qui affiche le "format" de la ligne de commande, */
/* appelée si l'utilisateur se trompe dans le nombre d'arguments en lançant l'exécutable */
/* (non demandée) */
void usage_somme (char * s) {
  fprintf (stderr, "%s unsigned \n", s);
}
