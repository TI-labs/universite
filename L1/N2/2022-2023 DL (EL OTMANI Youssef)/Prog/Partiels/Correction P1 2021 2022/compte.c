#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct date_s {
  unsigned jour;
  unsigned mois;
  unsigned annee;
} date;

/******************************/
/****     Question 3.a     ****/
/******************************/
struct mouvement_s {
  float montant;
  date date_mvt;
};
typedef struct mouvement_s mouvement;

typedef struct compte_s {
  unsigned taille;
  unsigned nbre_mvt;
  mouvement * tab_mvt;
} compte;

date ce_jour (void);
compte * ouverture_cpte (unsigned);
void detruire_compte (compte *);
void solder_cpte (compte *);
void afficher_mvt (mouvement);
void usage_compte (char *);

/** Fonction principale */
/* (non demandée) */
/* Cette fonction :
  - ouvre un compte de taille m (m est l'argument de l'exécutable sur la ligne de commande) ;
  - initialise et affiche le tableau de mouvements du compte ;
  - solde le compte et affiche le tableau de mouvements du compte soldé ;
  - libère l'espace mémoire occupé sur le tas par le compte. */
int main (int argc, char ** argv) {
  unsigned m; /* taille du compte qui sera créé */
  compte * cpt; /* adresse du compte qui sera créé */
  int i, r;

  /* Affichage d'un message d'erreur si l'utilisateur se trompe dans le nombre d'arguments */
  /* sur la ligne de commande */
  if (argc != 2) {
    fprintf (stderr, "Nombre d'arguments erroné !\n");
    usage_compte (argv[0]);
    return EXIT_FAILURE;
  }

  /* Récupération dans la variable m de l'entier passé en argument de l'exécutable sur le ligne de commande */
  m = strtoul (argv[1], NULL, 10);

  /* Ouverture d'un compte de taille m */
  cpt = ouverture_cpte (m);

  /* Initialisation pseudo-aléatoire et affichage du tableau de mouvements du compte adressé par cpt */
  srand(time(NULL));
  for (i = 0; i < m; i ++) {
    r = rand ();
    cpt->tab_mvt[i].montant = 10000 * (r / (float) RAND_MAX);
    if (r%2) cpt->tab_mvt[i].montant = - cpt->tab_mvt[i].montant;
    cpt->tab_mvt[i].date_mvt.jour = 1 + r%28;
    cpt->tab_mvt[i].date_mvt.mois = 1 + r%12;
    cpt->tab_mvt[i].date_mvt.annee = 2018 + r%4;
    cpt->nbre_mvt ++;
  }
  printf("Tableau des mouvements [pour chaque mouvement, montant (date)] :\n");
  for (i = 0; i < cpt->nbre_mvt; i ++)
    afficher_mvt (cpt->tab_mvt[i]);
  printf ("\n");

  /* Affichage du compte après qu'il a été soldé */
  solder_cpte (cpt);
  printf("Tableau des mouvements [pour chaque mouvement, montant (date)] :\n");
  for (i = 0; i < cpt->nbre_mvt ; i ++)
    afficher_mvt (cpt->tab_mvt[i]);
  printf ("\n");

  /* Libération de l'espace mémoire occupé sur le tas par le compte adressé par cpt */
  detruire_compte (cpt);

  return EXIT_SUCCESS;
}

/******************************/
/****     Question 3.b     ****/
/******************************/
compte * ouverture_cpte (unsigned m) {
  compte * res; /* adresse du compte qui sera renvoyée */
  /* Allocation de mémoire sur le tas pour le compte dont l'adresse est res */
  res = malloc (sizeof(compte));
  if (res == NULL) {
    perror ("Échec allocation compte");
    exit (2);
  }
  /* Allocation de mémoire sur le tas pour le tableau de mouvements res->tab_mvt */
  /* (un tableau pouvant enregistrer m mouvements) */
  res->tab_mvt = malloc (sizeof(mouvement) * m);
  if (m != 0 && res->tab_mvt == NULL) {
    perror ("Échec allocation tableau de mouvements");
    exit (2);
  }
  /* Mise à jour des compteurs de mouvements disponibles et enregistrés dans res->tab_mvt */
  res->taille = m;
  res->nbre_mvt = 0;
  /* Renvoi de l'adresse du compte alloué et initialisé ci-dessus */
  return res;
}

/******************************/
/****     Question 3.c     ****/
/******************************/
void detruire_compte (compte * cpt) {
  if (cpt->tab_mvt != NULL)
    free (cpt->tab_mvt);
  free (cpt);
}

/******************************/
/****     Question 3.d     ****/
/******************************/
void solder_cpte (compte * cpt) {
  unsigned i;
  float solde = 0;

  /* Calcul du solde en sommant les montants des mouvements enregistrés dans cpt->tab_mvt */
  for (i = 0; i < cpt->nbre_mvt; i ++) {
    solde += cpt->tab_mvt[i].montant;
  }

  /* Augmentation de la taille de cpt->tab si ce dernier est plein */
  if (cpt->nbre_mvt == cpt->taille) {
    unsigned i;
    /* Allocation d'un tableau de mouvements de taille cpt->taille + 1 */
    mouvement * nouveau_tab = malloc (sizeof(mouvement) * (cpt->taille + 1));
    if (nouveau_tab == NULL) {
      perror ("Échec réallocation tableau de mouvements");
      exit (2);
    }
    /* Recopie de cpt->tab_mvt dans le tableau de mouvements nouvellement alloué */
    for (i = 0; i < cpt->nbre_mvt; i ++)
      nouveau_tab[i] = cpt->tab_mvt[i];
    /* Libération de l'espace occupée par cpt->tab_mvt */
    free (cpt->tab_mvt);
    /* "Rebranchement" du tableau de mouvements nouvellement alloué sur cpt->tab_mvt */
    cpt->tab_mvt = nouveau_tab;
    /* Mise à jour du compteur de mouvements disponibles dans cpt->tab_mvt */
    cpt->taille ++;
  }
  /* Ajout du mouvement soldant le compte aujourd'hui */
  cpt->tab_mvt[cpt->nbre_mvt].montant = - solde;
  cpt->tab_mvt[cpt->nbre_mvt].date_mvt = ce_jour();
  /* Mise à jour du compteur de mouvements enregistrés dans cpt->tab_mvt */
  cpt->nbre_mvt ++;
}

/** Fonction qui renvoie la date courante */
/* (non demandée) */
date ce_jour (void) {
  date res;
  time_t maintenant;
  struct tm *local;

  time(&maintenant);
  local = localtime(&maintenant);
  res.jour = (unsigned) local->tm_mday;
  res.mois = (unsigned) local->tm_mon + 1;
  res.annee = (unsigned) local->tm_year + 1900;
  return res;
}

/** Fonction qui affiche le montant et la date d'un mouvement de compte */
/* (non demandée) */
void afficher_mvt (mouvement mvt) {
  printf("%6.2f (%u/%u/%u)  ", mvt.montant, mvt.date_mvt.jour, mvt.date_mvt.mois, mvt.date_mvt.annee);
}

/** Fonction qui affiche le "format" de la ligne de commande, */
/* appelée si l'utilisateur se trompe dans le nombre d'arguments en lançant l'exécutable */
/* (non demandée) */
void usage_compte (char * s) {
  fprintf (stderr, "%s unsigned \n", s);
}
