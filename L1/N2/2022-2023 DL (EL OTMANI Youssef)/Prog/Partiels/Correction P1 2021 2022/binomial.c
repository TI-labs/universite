#include <stdio.h>
#include <stdlib.h>

unsigned bin (unsigned, unsigned);
unsigned bin_mem (unsigned, unsigned, unsigned **);
void bin_iter (unsigned *, unsigned, unsigned);
void usage_bin (char *);

/* Variables globales pour le décompte des appels (fonctions bin et bin_mem) */
/* ou des multiplications et divisions (fonction bin_iter) */
unsigned c_bin = 0, c_mem = 0, c_iter = 0;

/** Fonction principale */
int main (int argc, char ** argv) {
  unsigned n, p;
  unsigned res, **tab_mem, i;

  /* Affichage d'un message d'erreur si l'utilisateur se trompe dans le nombre d'arguments */
  /* sur la ligne de commande */
  if (argc != 3) {
    fprintf (stderr, "Nombre d'arguments erroné !\n");
    usage_bin (argv[0]);
    return EXIT_FAILURE;
  }

  /* Récupération dans les variables n et p des entiers passés en arguments */
  /* de l'exécutable sur le ligne de commande */
  n = strtoul (argv[1], NULL, 10);
  p = strtoul (argv[2], NULL, 10);

  /* Calcul et affichage du coefficient binomial (n,p) par la méthode récursive naïve */
  printf ( "binomial (%u, %u) = %u [nbre d'appels = %u] (récursif naïf)\n", n, p, bin (n, p), c_bin );
  /* Création et initialisation de la table d'association qui sera passée en argument */
  /* de la fonction bin_mem (récursivité avec mémoïsation) */
  tab_mem = malloc ((n + 1) * sizeof(unsigned *));
  for (i = 0; i <= n; i ++)
    tab_mem[i] = calloc (p + 1, sizeof(unsigned));
  /* Calcul et affichage du coefficient binomial (n,p) par la méthode récursive avec mémoïsation */
  printf ( "binomial (%u, %u) = %u [nbre d'appels = %u] (récursif avec mémoïsation)\n", n, p, bin_mem (n, p, tab_mem), c_mem);
  /***********************************************/
  /****  Question 1.c (deux lignes suivantes) ****/
  /***********************************************/
  /* Calcul et affichage du coefficient binomial (n,p) par la méthode itérative */
  bin_iter (&res, n, p);
  printf ( "binomial (%u, %u) = %u [nbre de mult. et div. = %u] (itératif)\n", n, p, res, c_iter );

  return EXIT_SUCCESS;
}

/** Fonction qui renvoie le coefficient binomial(n,p) en implémentant "naïvement" */
/* le schéma de récursion */
/*     binomial(n,p) = { 0 si n>p */
/*                     { 1 si p=0 ou p=n */
/*                     { binomial(n-1,p) + binomial(n-1,p-1) sinon */
/******************************/
/****      Question 1.a    ****/
/******************************/
/** Nbre total d'appels (initial et récursifs) */
/*     Cpt_bin(n,p) = { 1 si n > p ou p = 0 ou p = n */
/*                    { 1 + Cpt_bin(n-1,p) + Cpt_bin(n-1,p-1) sinon */
/*  i.e. Cpt_bin(n,p) = 2*binomial(n,p) -1 si 0 < p < n et Cpt_(2n,n) ~ 2*4^n / sqrt(\pi * n) */
/*  Par exemple, C_bin(10, 4) = 419 */
unsigned bin (unsigned n, unsigned p) {
  c_bin ++; /* compteur d'appels */
  if (n < p) return 0;
  if (p == 0 || p == n) return 1;
  return bin (n-1, p-1) + bin (n-1, p);
}

/******************************/
/****     Question 1.b     ****/
/******************************/
/** Fonction qui renvoie le coefficient binomial(n,p) en implémentant */
/* le même schéma de récursion avec une table d'association (mémoïsation) */
/*     binomial(n,p) = { 0 si n > p */
/*                     { 1 si p = 0 ou p = n*/
/*                     { binomial(n-1,p) + binomial(n-1,p-1) sinon */
unsigned bin_mem (unsigned n, unsigned p, unsigned ** tab_mem) {
  c_mem ++; /* compteur d'appels */
  if (n < p) return 0;
  if (p == 0 || p == n) return (tab_mem[n][p] = 1);
  if (tab_mem[n][p] == 0)
    tab_mem[n][p] = bin_mem(n-1, p, tab_mem) + bin_mem(n-1, p-1, tab_mem);
  return tab_mem[n][p];
}

/***********************************************/
/****     Question 1.d     (bonus)          ****/
/***********************************************/
/** Nombre total d'appels (initial et récursifs) */
/*     C_mem(n,p) = 1 + 2p(n-p) si 0 ≤ p ≤ n */
/*     Par exemple, C_mem(10, 4) = 49 */


/** Fonction qui renvoie le coefficient binomial(n,p) en calculant n! / (p!(n-p)!) */
/** Nombre de multiplications et de divisions */
/*    C_iter(n,p) = { 2 * (min{p,n-p} - 1) + 1 si 0 < p < n */
/*                = { 0 sinon */
/*    Par exemple, C_iter(10,4) = 7 */
void bin_iter (unsigned * res, unsigned n, unsigned p) {
  unsigned den = 1, num = n;
  unsigned i, q;
  q = p > n-p ? n-p : p;
  if (p > n) { *res = 0; return; }
  if (p == 0 || p == n) { *res = 1; return; }
  for (i = 2; i <= q ; i ++) {
    den *= i;
    num *= n - i + 1;
    c_iter += 2; /* ajout de deux multiplications au compteur */
  }
  *res = num / den;
  c_iter ++; /* ajout d'une division au compteur */
}

/** Fonction qui affiche le "format" de la ligne de commande, */
/* appelée si l'utilisateur se trompe dans le nombre d'arguments en lançant l'exécutable */
/* (non demandée) */
void usage_bin (char * s) {
  fprintf (stderr, "%s unsigned unsigned\n", s);
}
