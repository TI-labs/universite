# Annales de l'Institut Galilée

Ce gitlab est une collection de documents compilé par les étudiants de
l'Institut Galilée afin de s'entraider dans les cours. Pour l'instant, les
archives contiennent surtout celle d'étudiants en Double-Licence
Mathématiques et Informatique.

## Contribuer

Nous accueillons toutes les contributions tant que ce sont celle d'étudiants
à l'Institut Galilée, surtout à fin de maintenir ce GitLab dans le futur.

Pour contribuer vous-même à ces archives, vous pouvez :
- Ouvrir un ticket sur GitLab
- Ouvrir une requête de fusion sur GitLab (si vous vous y connaissez en git)
- M'envoyer un e-mail à mon adresse personnelle : `mail+git-ig` \[at\] `lyes` \[dot\] `eu`
- M'envoyer un message sur Discord à \[at\] `ntlyes`

## Contributeurs

- Lyes Saadi
- Youssef El Otmani
- Kylian Maouchi
